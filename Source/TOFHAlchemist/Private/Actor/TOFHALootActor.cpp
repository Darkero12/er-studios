// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/TOFHALootActor.h"

// Sets default values
ATOFHALootActor::ATOFHALootActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LootActorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LootMesh"));

}

// Called when the game starts or when spawned
void ATOFHALootActor::BeginPlay()
{
	Super::BeginPlay();
	
}

