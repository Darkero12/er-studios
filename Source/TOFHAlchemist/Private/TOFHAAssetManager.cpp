// Fill out your copyright notice in the Description page of Project Settings.


#include "TOFHAAssetManager.h"
#include "AbilitySystemGlobals.h"
#include "DataAssets/TOFHAItem.h"

const FPrimaryAssetType UTOFHAAssetManager::EquipmentItemType = TEXT("Equipment");
const FPrimaryAssetType UTOFHAAssetManager::ConsumableItemType = TEXT("Consumable");
const FPrimaryAssetType UTOFHAAssetManager::KeyItemType = TEXT("Key");
const FPrimaryAssetType UTOFHAAssetManager::TextItemType = TEXT("Text");
const FPrimaryAssetType UTOFHAAssetManager::MaterialItemType = TEXT("Materials");
const FPrimaryAssetType UTOFHAAssetManager::SpecialItemType = TEXT("Special");
const FPrimaryAssetType UTOFHAAssetManager::CurrencyItemType = TEXT("Currency");

void UTOFHAAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();

	UAbilitySystemGlobals::Get().InitGlobalData();
}

UTOFHAAssetManager& UTOFHAAssetManager::Get()
{
	UTOFHAAssetManager* This = Cast<UTOFHAAssetManager>(GEngine->AssetManager);

	if (This)
	{
		return *This;
	}
	else
	{
		UE_LOG(LogTemp, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be RPGAssetManager!"));
		return *NewObject<UTOFHAAssetManager>(); // never calls this
	}
}

UTOFHAItem* UTOFHAAssetManager::ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	UTOFHAItem* LoadedItem = Cast<UTOFHAItem>(ItemPath.TryLoad());

	if (bLogWarning && LoadedItem == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to load item for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedItem;
}
