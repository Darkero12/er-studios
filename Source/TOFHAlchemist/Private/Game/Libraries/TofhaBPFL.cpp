// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Libraries/TofhaBPFL.h"
#include "GASSetup/Public/Game/Character/GASCharacter.h"
#include "Blueprint/UserWidget.h"
#include "Framework/Application/SlateApplication.h"

bool UTofhaBPFL::IsActorOfAlignment(const FEncounterAlignments& Input, AActor* InActor, EAlignmentType InAlignment)
{
	if (!InActor)
	{
		return false;
	}

		TArray<TSubclassOf<AGASCharacter>> ClassesToCheck;
		switch (InAlignment)
		{
		case EAlignmentType::EFriendly:
			ClassesToCheck = Input.Friendly;
			break;
		case EAlignmentType::EHostile:
			ClassesToCheck = Input.Hostile;
			break;
		case EAlignmentType::ENeutralFriendly:
			ClassesToCheck = Input.NeutralFriendly;
			break;
		case EAlignmentType::ENeutralHostile:
			ClassesToCheck = Input.NeutralHostile;
			break;
		}

		if (ClassesToCheck.Num() <= 0)
		{
			return false;
		}
		
		for (UClass* InClass : ClassesToCheck)
		{
			if (InActor->IsA(InClass))
			{
				return true;
			}
		}
		return false;
}

//UUserWidget* UTofhaBPFL::GetKeyboardFocusedWidget()
//{
//	TSharedPtr<SWidget> SWidget = FSlateApplication::Get().GetKeyboardFocusedWidget();
//	SWidget->
//	return nullptr;
//	
//}
