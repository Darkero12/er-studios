// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TOFHAStanceManager.h"
#include "Animations/GASAnimInstance.h"

// Sets default values for this component's properties
UTOFHAStanceManager::UTOFHAStanceManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTOFHAStanceManager::BeginPlay()
{
	Super::BeginPlay();

	// ...

	SwapWeaponStance(DefaultStance);
	SwapPlayerStance();
	SwapWeaponStance(PrimaryStance);
}


// Called every frame
void UTOFHAStanceManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FGameplayTag UTOFHAStanceManager::SwapWeaponStance(FGameplayTag NewStance)
{
	WeaponStance = NewStance;

	OnWeaponStanceChanged.Broadcast(WeaponStance);
	
	return NewStance;
}

FGameplayTag UTOFHAStanceManager::AutoChangeWeaponStance()
{
	if (WeaponStance != PrimaryStance)
	{
		WeaponStance = PrimaryStance;
	}
	else {
		WeaponStance = SecondaryStance;
	}

	OnWeaponStanceChanged.Broadcast(WeaponStance);

	return WeaponStance;
}

FGameplayTag UTOFHAStanceManager::SwapPlayerStance()
{
	UGASAnimInstance* AnimInstance = Cast<UGASAnimInstance>(Cast<ACharacter>(GetOwner())->GetMesh()->GetAnimInstance());
	if(AnimInstance)
	{
		AnimInstance->SetWeaponStance(WeaponStance);
	}

	PlayerStance = WeaponStance;

	OnPlayerStanceChanged.Broadcast(PlayerStance);
	
	return PlayerStance;
}

void UTOFHAStanceManager::SwapToPrimaryStance()
{
	SwapWeaponStance(PrimaryStance);
}

void UTOFHAStanceManager::SwapToSecondaryStance()
{
	SwapWeaponStance(SecondaryStance);
}

void UTOFHAStanceManager::SwapToDefaultStance()
{
	UGASAnimInstance* AnimInstance = Cast<UGASAnimInstance>(Cast<ACharacter>(GetOwner())->GetMesh()->GetAnimInstance());
	if(AnimInstance)
	{
		AnimInstance->SetWeaponStance(DefaultStance);
	}

	PlayerStance = DefaultStance;

	OnPlayerStanceChanged.Broadcast(PlayerStance);
}



