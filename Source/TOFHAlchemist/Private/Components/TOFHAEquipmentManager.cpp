// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TOFHAEquipmentManager.h"
#include "AbilitySystemComponent.h"
#include "TOFHAFunctionLibrary.h"
#include "Components/GASAbilityComponent.h"
#include "Components/TOFHAInventory.h"
#include "DataAssets/TOFHAEquipment.h"
#include "DataAssets/TOFHAUsableItem.h"

// Sets default values for this component's properties
UTOFHAEquipmentManager::UTOFHAEquipmentManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UTOFHAEquipmentManager::EquipItemFromInventory(FTOFHAItemData ItemToEquip, EEquipmentType EquipmentSlot, bool bStartingEquipment)
{
	if(InventoryComp)
	{
		const int Index = InventoryComp->FindItemInInventory(ItemToEquip);
		if(Index != -1 || bStartingEquipment)
		{
			if(EEquipmentType::Head == EquipmentSlot && !Head.bSlotTaken)
			{
				InternalEquipItem(&Head, ItemToEquip, bStartingEquipment);
				OnHeadChange.Broadcast(true);
				return;
			}
			
			if(EEquipmentType::Body == EquipmentSlot && !Body.bSlotTaken)
			{
				InternalEquipItem(&Body, ItemToEquip, bStartingEquipment);
				OnBodyChange.Broadcast(true);
				return;
			}

			if(EEquipmentType::Accessory == EquipmentSlot && !Accessory.bSlotTaken)
			{
				InternalEquipItem(&Accessory, ItemToEquip, bStartingEquipment);
				OnAccessoryChange.Broadcast(true);
				return;
			}

			if(EEquipmentType::Consumable == EquipmentSlot && !Consumable.bSlotTaken)
			{
				InternalEquipItem(&Consumable, ItemToEquip, bStartingEquipment);
				OnConsumableChange.Broadcast(true);
				return;
			}
			
		}
	}
	
}

void UTOFHAEquipmentManager::UnequipItem(EEquipmentType EquipmentSlot)
{
	if(EEquipmentType::Head == EquipmentSlot && Head.bSlotTaken)
	{
		InternalUnEquipItem(&Head);
		OnHeadChange.Broadcast(false);
		return;
	}
			
	if(EEquipmentType::Body == EquipmentSlot && Body.bSlotTaken)
	{
		InternalUnEquipItem(&Body);
		OnBodyChange.Broadcast(false);
		return;
	}

	if(EEquipmentType::Accessory == EquipmentSlot && Accessory.bSlotTaken)
	{
		InternalUnEquipItem(&Accessory);
		OnAccessoryChange.Broadcast(false);
		return;
	}

	if(EEquipmentType::Consumable == EquipmentSlot && Consumable.bSlotTaken)
	{
		InternalUnEquipItem(&Consumable);
		OnConsumableChange.Broadcast(false);
		return;
	}
}

void UTOFHAEquipmentManager::SetupEquipmentManager(UAbilitySystemComponent* InASC, UTOFHAInventory* InInventory)
{
	ASC = InASC;
	InventoryComp = InInventory;
}

// Called when the game starts
void UTOFHAEquipmentManager::BeginPlay()
{
	Super::BeginPlay();

	ASC = Cast<UGASAbilityComponent>(GetOwner()->GetComponentByClass(UGASAbilityComponent::StaticClass()));

	InventoryComp = UTOFHAFunctionLibrary::GetTOFHAInventoryManager(GetOwner());

	// for (const FTOFHAStartingEquipment StartingItem : StartingEquipment)
	// {
	// 	EquipItemFromInventory(StartingItem.ItemData, StartingItem.EquipmentSlot, true);
	// }
	
	// ...
}

void UTOFHAEquipmentManager::InternalEquipItem(FTOFHAEquipmentData* EquipmentData, FTOFHAItemData ItemData, bool bStartingEquipment)
{
	EquipmentData->bSlotTaken = true;
	EquipmentData->ItemData = ItemData;
	
	UTOFHAEquipment* EquipmentItem = Cast<UTOFHAEquipment>(EquipmentData->ItemData.Item);
	if(EquipmentItem)
	{
		if(ASC && EquipmentItem->EffectsToApply.Num() > 0)
		{
			FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
			EffectContext.AddSourceObject(this);
			for (TSoftClassPtr<UGameplayEffect> Effect : Cast<UTOFHAEquipment>(EquipmentData->ItemData.Item)->EffectsToApply)
			{
				UGameplayEffect* GE = Cast<UGameplayEffect>(Effect.LoadSynchronous());
				if(!GE)
				{
					return;
				}
				FGameplayEffectSpecHandle NewHandle = ASC->MakeOutgoingSpec(Effect.Get(), 1, EffectContext);
				if (NewHandle.IsValid())
				{
					EquipmentData->ActiveEffectHandle.Add(ASC->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), ASC));
				}
			}
		}
	}
	if(!bStartingEquipment)
	{	//Left for a later fix.
		//InventoryComp->RemoveItemFromInventory(Index, InventoryComp->GetItemFromIndex(Index).ItemCount, false);	
	}
}

void UTOFHAEquipmentManager::InternalUnEquipItem(FTOFHAEquipmentData* EquipmentData)
{
	EquipmentData->bSlotTaken = false;
	if(ASC)
	{
		for (FActiveGameplayEffectHandle EffectHandle : EquipmentData->ActiveEffectHandle)
		{
			ASC->RemoveActiveGameplayEffect(EffectHandle, -1);	
		}
	}
	InventoryComp->AddItemToInventory(EquipmentData->ItemData);
}

