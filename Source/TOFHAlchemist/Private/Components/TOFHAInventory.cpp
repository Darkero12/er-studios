// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/TOFHAInventory.h"

// Sets default values for this component's properties
UTOFHAInventory::UTOFHAInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
	
}


int UTOFHAInventory::AddItemToInventory(FTOFHAItemData ItemToAdd)
{
	if(ItemToAdd.Item && ItemToAdd.Item->bIsCurrency)
	{
		AddGold(ItemToAdd.Item->Currency);
		return -1;
	}
	
	int Index = FindItemInInventory(ItemToAdd, ItemToAdd.Item->bCanStack);

	if(Index == -1)
	{
		Index = Inventory.Add(ItemToAdd);
		Inventory[Index].bSlotTaken = true;
		Inventory[Index].InventoryIndex = Index;
		CurrentWeight = (CurrentWeight + (ItemToAdd.Item->ItemWeight * ItemToAdd.ItemCount));
		OnInventoryChange.Broadcast(true, Index);
		return Index;
	}
	else
	{
		const int LeftOver = Inventory[Index].ItemCount + ItemToAdd.ItemCount - ItemToAdd.Item->MaxCount;
		Inventory[Index].ItemCount = FMath::Clamp(Inventory[Index].ItemCount + ItemToAdd.ItemCount, 1, ItemToAdd.Item->MaxCount);
		Inventory[Index].bSlotTaken = true;
		Inventory[Index].InventoryIndex = Index;
		CurrentWeight = (CurrentWeight + (ItemToAdd.Item->ItemWeight * ItemToAdd.ItemCount));
		
		OnInventoryChange.Broadcast(true, Index);

		if(LeftOver > 0)
		{
			FTOFHAItemData NewItemData = ItemToAdd;
			NewItemData.ItemCount = LeftOver;
			Index = Inventory.Add(NewItemData);

			Inventory[Index].InventoryIndex = Index;
			CurrentWeight = (CurrentWeight + (ItemToAdd.Item->ItemWeight * LeftOver));
			OnInventoryChange.Broadcast(true, Index);
		}
		return Index;
	}
}

bool UTOFHAInventory::RemoveItemFromInventory(FTOFHAItemData ItemToRemove, int Count, bool bDropAll)
{
	const int ItemIndex = FindItemInInventory(ItemToRemove);
	if(ItemIndex != -1)
	{
		if(bDropAll)
		{
			Inventory[ItemIndex].bSlotTaken = false;
			CurrentWeight = (CurrentWeight - (Inventory[ItemIndex].Item->ItemWeight * Inventory[ItemIndex].ItemCount));
			OnInventoryChange.Broadcast(false, ItemIndex);
			return true;
		}
		Inventory[ItemIndex].ItemCount =  FMath::Clamp(Inventory[ItemIndex].ItemCount - Count, 0, Inventory[ItemIndex].Item->MaxCount);
		CurrentWeight = (CurrentWeight - (Inventory[ItemIndex].Item->ItemWeight * Inventory[ItemIndex].ItemCount));
		
		if(Inventory[ItemIndex].ItemCount == 0)
		{
			Inventory[ItemIndex].bSlotTaken = false;
		}
		OnInventoryChange.Broadcast(false, ItemIndex);
		return true;
		
	}
	return false;
}

bool UTOFHAInventory::DropItemFromInventory(FTOFHAItemData ItemToDrop, FTransform DropTransform, int Count, bool bDropAll)
{
	if(IsValid(ItemToDrop.Item))
	{
		ATOFHALootActor* SpawnedLoot = GetWorld()->SpawnActorDeferred<ATOFHALootActor>(LootActor, DropTransform, GetOwner(), nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		SpawnedLoot->Loot.Add(ItemToDrop);
		SpawnedLoot->FinishSpawning(DropTransform);

		RemoveItemFromInventory(ItemToDrop, Count, bDropAll);
	}
	return false;
}

int UTOFHAInventory::FindItemInInventory(FTOFHAItemData ItemToFind, bool bIsForStacking)
{
	int Index = 0;

	if(Inventory.Num() > 0)
	{
		for (FTOFHAItemData const Item : Inventory)
		{
			if(IsValid(Item.Item))
			{
				if(Item.Item->ItemName.ToString() == ItemToFind.Item->ItemName.ToString())
				{
					if(bIsForStacking)
					{
						if(Item.ItemCount != Item.Item->MaxCount)
						{
							return Index;
						}
					}
					else
					{
						return Index;
					}
				}
			}
			else
			{
				return -1;
			}
			
			Index++;
		}
	}
	return -1;
}

FTOFHAItemData UTOFHAInventory::GetItemFromIndex(int ItemIndex) const
{
	return Inventory[ItemIndex];
}

int32 UTOFHAInventory::AddGold(int32 GoldToAdd)
{
	return Gold = Gold + GoldToAdd;
}

int32 UTOFHAInventory::RemoveGold(int32 GoldToRemove)
{
	return FMath::Clamp(Gold = Gold - GoldToRemove, 0, 9999);
}

void UTOFHAInventory::TradeItem(FTOFHAItemData ItemToTrade, UTOFHAInventory* TraderInventory)
{
	if (TraderInventory && IsValid(ItemToTrade.Item))
	{
		if(TraderInventory->bHasInfiniteGold || TraderInventory->Gold >= ItemToTrade.Item->Price)
		{
			TraderInventory->AddItemToInventory(ItemToTrade);

			AddGold(ItemToTrade.Item->Price);

			if(!bHasInfiniteItems)
			{
				RemoveItemFromInventory(ItemToTrade, ItemToTrade.ItemCount);
			}
		}
	}
}

// Called when the game starts
void UTOFHAInventory::BeginPlay()
{
	Super::BeginPlay();

	for (FTOFHAItemData StartingItem : StartingInventory)
	{
		Inventory.Add(StartingItem);
	}

	StartingInventory.Empty();

	// ...
}
