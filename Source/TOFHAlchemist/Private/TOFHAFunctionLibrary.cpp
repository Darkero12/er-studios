// Fill out your copyright notice in the Description page of Project Settings.


#include "TOFHAFunctionLibrary.h"
#include "Components/TOFHAEquipmentManager.h"
#include "Components/TOFHAStanceManager.h"

UTOFHAInventory* UTOFHAFunctionLibrary::GetTOFHAInventoryManager(AActor* Actor)
{
	return IsValid(Actor)
		? Cast<UTOFHAInventory>(Actor->GetComponentByClass(UTOFHAInventory::StaticClass()))
		: nullptr;
}

UTOFHAEquipmentManager* UTOFHAFunctionLibrary::GetTOFHAEquipmentManager(AActor* Actor)
{
	return IsValid(Actor)
		? Cast<UTOFHAEquipmentManager>(Actor->GetComponentByClass(UTOFHAEquipmentManager::StaticClass()))
		: nullptr;
}

UTOFHAStanceManager* UTOFHAFunctionLibrary::GetTOFHAStanceManager(AActor* Actor)
{
	return IsValid(Actor)
		? Cast<UTOFHAStanceManager>(Actor->GetComponentByClass(UTOFHAStanceManager::StaticClass()))
		: nullptr;
}

bool UTOFHAFunctionLibrary::WriteStringToConfigFile(const FString Section, const FString Key, const FString Value,
	const FString FileName)
{
	if(!GConfig) return false;

	FString fileName = FPaths::GeneratedConfigDir().Append(*FileName).Append(".ini");

	

	UE_LOG(LogTemp, Warning, TEXT("%s"), *fileName);

	FConfigFile* File = GConfig->Find(fileName,true);
	GConfig->LoadFile(fileName, File);

	GConfig->SetString(
		*Section,
		*Key,
		*Value,
		fileName
	);

	GConfig->Flush(false, fileName);

	return true;

}

bool UTOFHAFunctionLibrary::ReadStringFromConfigFile(const FString Section, const FString Key, const FString FileName,
	FString& Value)
{
	if(!GConfig) return false;

	FString fileName = FPaths::GeneratedConfigDir().Append(*FileName).Append(".ini");

	FConfigFile* File = GConfig->Find(fileName,true);

	if(!File)
	{
		return false;
	}

	GConfig->LoadFile(fileName, File);

	return GConfig->GetString(
		*Section,
		*Key,
		Value,
		fileName
	);
}
