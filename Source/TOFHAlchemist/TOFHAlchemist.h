// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DataAssets/TOFHAItem.h"
#include "TOFHAlchemist.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnInventoryChange, bool, bItemAdded, int, SlotChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEquipmentChange, bool, bEquipped);

USTRUCT(BlueprintType)
struct FTOFHAItemData
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Inventory")
	UTOFHAItem* Item;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Inventory")
	bool bSlotTaken = false;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Inventory")
	int ItemCount = 0;
	
	UPROPERTY(BlueprintReadOnly, Category = "Inventory")
    int InventoryIndex = 0;
};

UENUM(BlueprintType)
enum class EEquipmentType : uint8
{
	Head = 0	UMETA(DisplayName = "Head"),
	Body		UMETA(DisplayName = "Body"),
	Accessory	UMETA(DisplayName = "Accessory"),
	Consumable	UMETA(DisplayName = "Consumable"),
	
};

USTRUCT(BlueprintType)
struct FTOFHAEquipmentData
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Equipment")
	EEquipmentType EquipmentSlot; 
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Equipment")
	FTOFHAItemData ItemData;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Equipment")
	bool bSlotTaken = false;

	UPROPERTY(BlueprintReadOnly, Category = "Equipment")
	TArray<FActiveGameplayEffectHandle> ActiveEffectHandle;
};


USTRUCT(BlueprintType)
struct FTOFHAWeaponData
{
	GENERATED_BODY()
	
public:
	
	//A Data Asset that holds information about the weapon like it's name and more.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Weapon")
	FTOFHAItemData ItemData;

	//The first stance that this weapon can take.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Weapon")
	FGameplayTag WeaponStanceOne;
	
	//The second stance that this weapon can take.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Weapon")
	FGameplayTag WeaponStanceTwo;
};

USTRUCT(BlueprintType)
struct FTOFHAStartingEquipment
{
	GENERATED_BODY()
	
public:
	
	//A Data Asset that holds information about the weapon like it's name and more.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Weapon")
	FTOFHAItemData ItemData;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Equipment")
	EEquipmentType EquipmentSlot;
};