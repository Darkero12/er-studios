// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TOFHAlchemist/TOFHAlchemist.h"
#include "TOFHALootActor.generated.h"

UCLASS()
class TOFHALCHEMIST_API ATOFHALootActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATOFHALootActor();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory", meta=(HideInDetailPanel))
	TArray<FTOFHAItemData> Loot;

	UPROPERTY(BlueprintReadOnly, Category = "TOFHA|Inventory", meta=(HideInDetailPanel))
	UStaticMeshComponent* LootActorMesh;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
