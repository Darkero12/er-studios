// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAInventory.h"
#include "Components/ActorComponent.h"
#include "TOFHAlchemist/TOFHAlchemist.h"
#include "TOFHAEquipmentManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOFHALCHEMIST_API UTOFHAEquipmentManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTOFHAEquipmentManager();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Equipment")
	FORCEINLINE FTOFHAEquipmentData GetHeadSlot() const {return Head;}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Equipment")
	FORCEINLINE FTOFHAEquipmentData GetBodySlot() const {return Body;}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Equipment")
	FORCEINLINE FTOFHAEquipmentData GetAccessorySlot() const {return Accessory;}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Equipment")
	FORCEINLINE FTOFHAEquipmentData GetConsumableSlot() const {return Consumable;}

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Equipment")
	void EquipItemFromInventory(FTOFHAItemData ItemToEquip, EEquipmentType EquipmentSlot, bool bStartingEquipment = false);

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Equipment")
	void UnequipItem(EEquipmentType EquipmentSlot);

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Equipment")
	void SetupEquipmentManager(UAbilitySystemComponent* InASC, UTOFHAInventory* InInventory);

	UPROPERTY(BlueprintAssignable)
	FOnEquipmentChange OnHeadChange;

	UPROPERTY(BlueprintAssignable)
	FOnEquipmentChange OnBodyChange;

	UPROPERTY(BlueprintAssignable)
	FOnEquipmentChange OnAccessoryChange;

	UPROPERTY(BlueprintAssignable)
	FOnEquipmentChange OnConsumableChange;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Equipment")
	TArray<FTOFHAStartingEquipment> StartingEquipment;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Equipment")
	FTOFHAWeaponData Weapon;

	UPROPERTY(BlueprintReadOnly, Category = "Equipment", meta=(HideInDetailPanel))
	FTOFHAEquipmentData Head;

	UPROPERTY(BlueprintReadOnly, Category = "Equipment", meta=(HideInDetailPanel))
	FTOFHAEquipmentData Body;

	UPROPERTY(BlueprintReadOnly, Category = "Equipment", meta=(HideInDetailPanel))
	FTOFHAEquipmentData Accessory;

	UPROPERTY(BlueprintReadOnly, Category = "Equipment", meta=(HideInDetailPanel))
	FTOFHAEquipmentData Consumable;

	UPROPERTY()
	UAbilitySystemComponent* ASC;

	UPROPERTY()
	UTOFHAInventory* InventoryComp;

	void InternalEquipItem(FTOFHAEquipmentData* EquipmentData, FTOFHAItemData ItemData, bool bStartingEquipment);

	void InternalUnEquipItem(FTOFHAEquipmentData* EquipmentData);
		
};
