// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"
#include "EnvironmentQuery/EnvQueryDebugHelpers.h"
#include "TOFHAlchemist/TOFHAlchemist.h"
#include "TOFHALootManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOFHALCHEMIST_API UTOFHALootManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTOFHALootManager();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory")
	TMap<float, FTOFHAItemData> DropItems;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
