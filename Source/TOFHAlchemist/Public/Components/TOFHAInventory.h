// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/TOFHALootActor.h"
#include "Components/ActorComponent.h"
#include "TOFHAlchemist/TOFHAlchemist.h"
#include "TOFHAInventory.generated.h"


UCLASS(ClassGroup=("TOFHA"), meta=(BlueprintSpawnableComponent), BlueprintType, Blueprintable)
class TOFHALCHEMIST_API UTOFHAInventory : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTOFHAInventory();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory")
	int MaxSlots = 10;

	UPROPERTY(BlueprintReadOnly, Category = "TOFHA|Currency")
	int32 Gold;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory")
	float MaxWeight = 20.0f;

	UPROPERTY(BlueprintReadOnly, Category = "TOFHA|Inventory", meta=(HideInDetailPanel))
	float CurrentWeight = 0.0f;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory")
	bool bHasInfiniteGold = false;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory")
	bool bHasInfiniteItems = false;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Inventory")
	TSubclassOf<ATOFHALootActor> LootActor;
	
	UPROPERTY(BlueprintAssignable)
	FOnInventoryChange OnInventoryChange;

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Inventory")
	int AddItemToInventory(FTOFHAItemData ItemToAdd);

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Inventory")
	bool RemoveItemFromInventory(FTOFHAItemData ItemToRemove, int Count = 1, bool bDropAll = false);

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Inventory")
	bool DropItemFromInventory(FTOFHAItemData ItemToRemove, FTransform DropTransform, int Count = 1, bool bDropAll = false);

	int FindItemInInventory(FTOFHAItemData ItemToFind, bool bIsForStacking = false);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Inventory")
	FORCEINLINE TArray<FTOFHAItemData> GetTOFHAInventory() const {return Inventory;}

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Inventory")
	FTOFHAItemData GetItemFromIndex(int ItemIndex) const;
	
	UFUNCTION(BlueprintCallable, Category = "TOFHA|Currency")
	FORCEINLINE int32 GetGold() const{return Gold;}

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Currency")
	int32 AddGold(int32 GoldToAdd);

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Currency")
	int32 RemoveGold(int32 GoldToAdd);
	
	UFUNCTION(BlueprintCallable, Category = "TOFHA|Trade")
	void TradeItem(FTOFHAItemData ItemToTrade, UTOFHAInventory* TraderInventory);

	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, SaveGame, Category = "TOFHA|Inventory", meta=(HideInDetailPanel))
	TArray<FTOFHAItemData> Inventory;

	UPROPERTY(BlueprintReadOnly, SaveGame, Category = "TOFHA|Inventory")
	TArray<FTOFHAItemData> StartingInventory;
};
