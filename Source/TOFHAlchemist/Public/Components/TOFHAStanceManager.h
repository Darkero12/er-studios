// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "TOFHAStanceManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponStanceChanged, FGameplayTag, CurrentStance);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerStanceChanged, FGameplayTag, CurrentStance);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOFHALCHEMIST_API UTOFHAStanceManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTOFHAStanceManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadOnly, Category = "TOFHA|Stances", meta=(HideInDetailPanel))
	FGameplayTag WeaponStance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Stances")
	FGameplayTag DefaultStance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Stances")
	FGameplayTag PrimaryStance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Stances")
	FGameplayTag SecondaryStance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Stances")
	FString PrimaryItemID;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Stances")
	FString SecondaryItemID;

	UPROPERTY(BlueprintReadOnly, Category = "TOFHA|Stances", meta=(HideInDetailPanel))
	FGameplayTag PlayerStance;

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Stances")
	FGameplayTag AutoChangeWeaponStance();

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Stances")
	FGameplayTag SwapWeaponStance(FGameplayTag NewStance);

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Stances")
	FGameplayTag SwapPlayerStance();

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Stances")
	void SwapToPrimaryStance();

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Stances")
	void SwapToSecondaryStance();

	UFUNCTION(BlueprintCallable, Category = "TOFHA|Stances")
	void SwapToDefaultStance();

	UPROPERTY(BlueprintAssignable)
	FOnWeaponStanceChanged OnWeaponStanceChanged;

	UPROPERTY(BlueprintAssignable)
	FOnPlayerStanceChanged OnPlayerStanceChanged;
	
};
