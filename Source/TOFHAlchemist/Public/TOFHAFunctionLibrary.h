// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TOFHAEquipmentManager.h"
#include "Components/TOFHAInventory.h"
#include "Components/TOFHAStanceManager.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TOFHAFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHAFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Inventory")
	static UTOFHAInventory* GetTOFHAInventoryManager(AActor* Actor);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Equipment")
	static UTOFHAEquipmentManager* GetTOFHAEquipmentManager(AActor* Actor);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA|Stances")
	static UTOFHAStanceManager* GetTOFHAStanceManager(AActor* Actor);


	// Config
	UPROPERTY(BlueprintReadOnly)
	FString SettingsConfigFileName = "Settings";

	UPROPERTY(BlueprintReadOnly)
	FString SettingsConfigFilePath = FPaths::GeneratedConfigDir().Append(SettingsConfigFileName).Append(".ini");

	UFUNCTION(BlueprintCallable)
	static bool WriteStringToConfigFile(const FString Section, const FString Key, const FString Value, const FString FileName);

	UFUNCTION(BlueprintCallable)
	static bool ReadStringFromConfigFile(const FString Section, const FString Key, const FString FileName, FString& Value);
	//End Config
};
