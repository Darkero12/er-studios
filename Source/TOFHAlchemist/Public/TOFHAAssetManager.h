// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "TOFHAAssetManager.generated.h"

class UTOFHAItem;
/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHAAssetManager : public UAssetManager
{
	GENERATED_BODY()
	
public:

	UTOFHAAssetManager(){};

	virtual void StartInitialLoading() override;

	/** Static types for items */
	static const FPrimaryAssetType ConsumableItemType;
	static const FPrimaryAssetType EquipmentItemType;
	static const FPrimaryAssetType TextItemType;
	static const FPrimaryAssetType SpecialItemType;
	static const FPrimaryAssetType KeyItemType;
	static const FPrimaryAssetType MaterialItemType;
	static const FPrimaryAssetType CurrencyItemType;
	
	
	static UTOFHAAssetManager& Get();

	UTOFHAItem* ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);
	
};
