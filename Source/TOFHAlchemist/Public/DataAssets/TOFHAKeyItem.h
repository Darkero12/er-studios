// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAItem.h"
#include "TOFHAKeyItem.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHAKeyItem : public UTOFHAItem
{
	GENERATED_BODY()
	
public:
	
	UTOFHAKeyItem()
	{
		ItemType = UTOFHAAssetManager::KeyItemType;
	}
	
};
