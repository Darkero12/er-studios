// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAItem.h"
#include "TOFHAMaterial.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHAMaterial : public UTOFHAItem
{
	GENERATED_BODY()

public:

	UTOFHAMaterial()
	{
		ItemType = UTOFHAAssetManager::MaterialItemType;
	}
	
};
