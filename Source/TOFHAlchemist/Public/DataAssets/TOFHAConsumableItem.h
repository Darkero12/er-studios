// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAUsableItem.h"
#include "TOFHAConsumableItem.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHAConsumableItem : public UTOFHAUsableItem
{
	GENERATED_BODY()

public:

	UTOFHAConsumableItem()
	{
		ItemType = UTOFHAAssetManager::ConsumableItemType;
	}
	
};
