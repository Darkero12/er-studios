// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAItem.h"
#include "TOFHACurrencyItem.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class TOFHALCHEMIST_API UTOFHACurrencyItem : public UTOFHAItem
{
	GENERATED_BODY()

public:

	UTOFHACurrencyItem()
	{
		ItemType = UTOFHAAssetManager::CurrencyItemType;
	}
	
};
