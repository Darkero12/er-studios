// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DataAssets/TOFHAItem.h"
#include "TOFHAUsableItem.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOFHALCHEMIST_API UTOFHAUsableItem : public UTOFHAItem
{
	GENERATED_BODY()
	
public:
	
	//Effects that are applied when this item is equipped or used.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item|Usable")
	TArray<TSoftClassPtr<UGameplayEffect>> EffectsToApply;
	
};
