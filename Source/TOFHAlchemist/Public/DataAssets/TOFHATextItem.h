// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAItem.h"
#include "TOFHATextItem.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHATextItem : public UTOFHAItem
{
	GENERATED_BODY()

public:

	UTOFHATextItem()
	{
		ItemType = UTOFHAAssetManager::TextItemType;
	}
	
};
