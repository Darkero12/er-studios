// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAUsableItem.h"
#include "TOFHAlchemist/TOFHAlchemist.h"
#include "TOFHAEquipment.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHAEquipment : public UTOFHAUsableItem
{
	GENERATED_BODY()

	UTOFHAEquipment()
	{
		ItemType = UTOFHAAssetManager::EquipmentItemType;
	}

public:

	//Skeletal mesh that should be set when this item is equipped.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item|Equipment")
	EEquipmentType EquipmentType;
	
	//Skeletal mesh that should be set when this item is equipped.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item|Equipment")
	TSoftObjectPtr<USkeletalMesh> EquipmentMesh;
};
