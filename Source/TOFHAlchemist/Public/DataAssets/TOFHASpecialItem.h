// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TOFHAAssetManager.h"
#include "DataAssets/TOFHAItem.h"
#include "TOFHASpecialItem.generated.h"

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTOFHASpecialItem : public UTOFHAItem
{
	GENERATED_BODY()

public:

	UTOFHASpecialItem()
	{
		ItemType = UTOFHAAssetManager::SpecialItemType;
	}
	
};
