// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Game/Abilities/GASAbility.h"
#include "TOFHAItem.generated.h"

/**
 * 
 */
/*Base Class for all items, do not BlueprintDirectly*/
UCLASS(Abstract, BlueprintType)
class TOFHALCHEMIST_API UTOFHAItem : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:

	//Constructor

	UTOFHAItem();

	//Type of this item set in the native parent class of the item type.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	FPrimaryAssetType ItemType;

	//User visible Short Name.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	FText ItemName;

	//User visible short description of the item.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	FText ItemDescription;

	//Icon to Display
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	TSoftObjectPtr<UTexture2D> ItemIcon;
	
	//Price of the item in game.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	int Price = 0;

	//Weight of this item.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	float ItemWeight = 0.0f;

	//Can this item stack?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	bool bCanStack = true;

	//Can this item sell?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	bool bCanSell = true;

	//Can this item be dropped?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	bool bCanDrop = true;

	//Can this item be lost?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	bool bCanBeLost = true;

	//Is this item just a Currency drop?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	bool bIsCurrency = false;

	//Is this item just a Currency drop?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item", meta=(EditCondition = "bIsCurrency", EditConditionHides))
	int Currency = 0;
	
	//Maximum number of this item that can be held.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	int MaxCount = 1;

	/** Ability to grant if this item is slotted */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|Item")
	TSubclassOf<UGASAbility> GrantedAbility;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "TOFHA|Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;

	/** Equality operators */
	bool operator==(const UTOFHAItem& Other) const
	{
		return ItemName.ToString() == Other.ItemName.ToString();
	}
	bool operator!=(const UTOFHAItem& Other) const
	{
		return !(*this == Other);
	}
	
};
