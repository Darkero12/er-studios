// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GASSetup/Public/Game/Data/TofhaData.h"
#include "TofhaBPFL.generated.h"

class AGASCharacter;
class UUserWidget;

/**
 * 
 */
UCLASS()
class TOFHALCHEMIST_API UTofhaBPFL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	
	/**
	 * @param Input The alignment struct we want to assess.
	 * @param InActor The Actor we're checking the alignment of.
	 * @param InAlignment The alignment we want to check for.
	 * @return True if InActor is of InAlignment to the caller based on the Input.
	 */ /// JLH ///
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA | AI")
	static bool IsActorOfAlignment(const FEncounterAlignments& Input, AActor* InActor, EAlignmentType InAlignment);

	//UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA | UI")
	//static UUserWidget* GetKeyboardFocusedWidget();
};
