# Updates the plugin from Git and writes a commit based version to the .uplugin file.

$file = 'SMSystem.uplugin'

Write-Host Updating plugin...

try
{
    git | Out-Null
}
catch [System.Management.Automation.CommandNotFoundException]
{
    Write-Error "Git is required. Please download from https://git-scm.com/downloads"
	Exit
}

$isInGit = git rev-parse --is-inside-work-tree
if ($isInGit -ne "true")
{
	Write-Error "Not inside a Git directory. Make sure you have cloned the repository using Git."
	Exit
}

$remoteUrl = git ls-remote --get-url
if (-Not($remoteUrl -like "*/LogicDriver.git"))
{
	Write-Error "Not inside LogicDriver's Git directory. Make sure you have cloned the official repository using Git."
	Exit
}

# Clear modifications from the last run.
git stash --quiet -- $file

if (-Not(Test-Path $file))
{
	Write-Error "$file not found"
	Exit
}

# Fetch and pull only the current branch.
$branch = git rev-parse --abbrev-ref HEAD
if ($branch -eq "HEAD")
{
	Write-Error "Not on a branch. Checkout to the correct branch and try again."
	Exit
}

git pull origin $($branch)
if ($LASTEXITCODE)
{
	Write-Error "Unable to continue; Git did not pull successfully. Resolve the error listed above and then try again."
	Exit
}

# The short commit hash we are on.
$commit = git rev-parse --short HEAD

$json = Get-Content $file -raw | ConvertFrom-Json
if(-Not $json.VersionName)
{
	Write-Error "VersionName not found in $file"
	Exit
}

$baseVersion = $json.VersionName
$newVersion = $baseVersion + "." + $commit

Write-Host -ForegroundColor Green $json.FriendlyName is now at version $newVersion for Unreal Engine $json.EngineVersion

# Replace within file rather than write the json object so as to preserve formatting. Otherwise
# git will think more than just the version field changed.
(Get-Content $file) | Foreach-Object {$_ -replace $baseVersion, $newVersion} | Set-Content $file