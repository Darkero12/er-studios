# Logic Driver

Welcome to the Logic Driver source code!

- [Discord Server](https://logicdriver.com/discord)
  - See `dev | github` for access to GitHub specific channels.
- [Project Backlog](https://logicdriver.com/roadmap)
- [Documentation - Release](https://logicdriver.com/docs "For released versions.")
- [Documentation - Development](https://dev.logicdriver.com/docs "May contain WIP docs for in-development versions.")
- [Example Project](https://github.com/Recursoft/LogicDriver-Example)

## Versioning

Everyone working on your project should be on the exact same version, down to the commit. Once you save assets on a version your team needs to stay on that version or higher.

Stable releases are tagged with the plugin version and the supported engine version, such as `2.5.2-4.26`. These releases align with the equivalent marketplace version.

When pulling and building from a branch you should treat the latest commit as the version since the friendly plugin version `2.x.x` is only accurate for marketplace and tagged releases.

Running `UpdatePlugin.bat` will pull the latest changes and update the `.uplugin` file with the current version number and commit hash so the accurate version can be read from your project. This requires you have cloned the plugin through Git and can run powershell scripts on your machine.

## Branches

Branches titled after UE version numbers, such as `4.26`, are the stable release branches of Logic Driver. They may be more up to date than the marketplace version.

**Warning:** Be careful of using any other branches in production. There are a lot of moving pieces in Logic Driver and if something goes wrong you could experience data corruption. Make sure to use source control and backup your data first.

Development branches include a `dev` tag.

Primary development branches will be named after the supported engine, such as `dev-4.26`. This should be mostly stable but may not be well tested and could still receive breaking changes as development continues. Please report any issues you discover!

Any other branches that show up are for developing specific features or hotfixes and may or may not be stable or even compile.

## Unreal Engine 5 and Engine Source Builds

- Use `5.X` branches or tags if you are on an Epic Games GitHub `5.X` branch or launcher build.
- Use `ue5-main` if you are on the Epic Games GitHub `ue5-main` branch. This branch should not be considered stable and is only occasionally tested against latest.
- Use `5.0-early-access` branches or `EA` tags for UE5 early-access.

Only dev versions of Logic Driver may be available for in-development engine builds. Due to engine changes Logic Driver may fall out of date periodically. Alternatively, Logic Driver may be more up-to-date than your local engine installation.

## Building

1. In an Unreal project of your choice create a `Plugins` folder if it does not already exist.
2. Clone Logic Driver into this folder so the layout is `YourProject/Plugins/LogicDriver`.
3. Make sure you checkout to the correct engine version branch or tag.
4. Right click on YourProject.uproject file and select `Generate Visual Studio project files`. Then open Visual Studio and compile.

## Automation Testing

The testing module is disabled by default. To enable it add the following to the `SMSystem.uplugin` file under the `Modules` section.

    {
      "Name": "SMSystemTests",
      "Type": "UncookedOnly",
      "LoadingPhase": "Default",
      "WhitelistPlatforms": [
        "Win64",
        "Mac",
        "Linux"
      ]
    }

Under your project's `uproject` file make sure the following plugins are enabled:

	{
		"Name": "FunctionalTestingEditor",
		"Enabled": true
	},
	{
		"Name": "RuntimeTests",
		"Enabled": true
	}

Compile and open the project. In the editor you can run the tests.
- For UE4 choose `Window` - `Test Automation`
- For UE5 choose `Tools` - `Test Automation`

`LogicDriver` should now be an option.
See [Unreal Automation](https://docs.unrealengine.com/automation-system-in-unreal-engine/) for more information.

## Contributing

If you wish to make a pull request please do so against a supported `dev-ENGINE` branch with changes squashed into a single commit. If approved the changes will be cherry-picked across supported engines. Please follow Epic Game's [Coding Standard](https://docs.unrealengine.com/epic-cplusplus-coding-standard-for-unreal-engine/).
