Use of Logic Driver is governed by:
- The terms of the Epic Content License Agreement, which can be found at https://www.unrealengine.com/eula/content; or
- From a license agreement privately entered into with Recursoft LLC.