// Some copyright should be here...

using UnrealBuildTool;

public class LogicSense : ModuleRules
{
	public LogicSense(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		CppStandard = CppStandardVersion.Latest;
		
		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"PhysicsCore",
			"GameplayTags",
			"SMSystem",
			"LogicCore"
		});


		PrivateDependencyModuleNames.AddRange(new string[]
		{ 
			"Slate", 
			"SlateCore",
			"DeveloperSettings",
		});
		
		if (Target.bBuildEditor)
		{
			PublicDependencyModuleNames.AddRange(new string[]
			{ 
				"UnrealEd",
			});
			PrivateDependencyModuleNames.AddRange(new string[]
			{
				"PropertyEditor",
			});
		}
	}
}
