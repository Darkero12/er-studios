﻿#pragma once
#include "LogicSensorStructs.h"
#include "UObject/Object.h"
#include "LogicSensorQuery.generated.h"

class ULogicSensorComponent;

UCLASS(Blueprintable, Abstract, ClassGroup="LogicSense", DisplayName="Sensor Query")
class LOGICSENSE_API ULogicSensorQuery : public UObject
{
	
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category = Query, meta=(ScriptName = "IsValid"))
	bool IsQueryValid(FSensorSearchQuery SearchQuery, TArray<FSensorHit>& OutHits) const;
	
	UFUNCTION(BlueprintNativeEvent, Category = Query, DisplayName = "On Executed", meta=(ScriptName = "OnQueryExecuted"))
	void OnExecuted();

	UFUNCTION(BlueprintImplementableEvent, Category = Query, DisplayName = "On Debug", meta=(ScriptName = "OnQueryDebug"))
	void OnDebug();

	UFUNCTION(BlueprintPure, Category = "Logic Sense|Sensor", DisplayName = "Get Sensor Component")
	ULogicSensorComponent* GetSensorComponent() const;

protected:
	/** True if this has been instanced, always true for blueprints */
	bool IsInstantiated() const;
	virtual UWorld* GetWorld() const override;
	
};
