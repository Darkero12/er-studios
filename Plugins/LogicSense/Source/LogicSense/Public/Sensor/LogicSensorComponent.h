﻿#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "LogicSensorStructs.h"
#include "LogicSensorComponent.generated.h"

class ULogicSubsystem;
class ULogicSenseSettings;
class ULogicSensorQuery;

UCLASS( ClassGroup="LogicSense", meta=(BlueprintSpawnableComponent, DisplayName="Sensor"), Blueprintable)
class LOGICSENSE_API ULogicSensorComponent : public UActorComponent
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSensorBroadcast, AActor*, Actor, FSensorHit, Hit);
	DECLARE_DYNAMIC_DELEGATE_OneParam(FSearchCallback, FSensorSearchResult, Output);

	
public:	
	// Sets default values for this component's properties
	ULogicSensorComponent();

	UPROPERTY(BlueprintAssignable)
	FSensorBroadcast OnActorDiscovered;
	
	UPROPERTY(BlueprintAssignable)
	FSensorBroadcast OnActorOutOfSight;

	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Sensor", meta = (DisplayName = "Add Sensor Socket"))
	FLogicSensorSocket AddSensorSocket(UPARAM(ref) USceneComponent* Component, FName SocketName, FTransform Transform);
	
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Sensor", meta = (DisplayName = "Get Sensor Sockets"))
    TArray<FLogicSensorSocket> GetSensorSockets();
    	
    UFUNCTION(BlueprintCallable, Category = "Logic Sense|Sensor", meta = (DisplayName = "Get Sensor Hits"))
    TArray<FSensorHit> GetSensorHits();


	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Sensor", meta=(DisplayName="Search Actors (Sync)", Keywords = "sync"))
	FSensorSearchResult SearchSync();
	
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Sensor", meta=(DisplayName="Search Actors (Async)", Keywords = "async"))
	void SearchAsync(FSearchCallback Callback);
	
    
protected:

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void SetActive(bool bNewActive, bool bReset) override;
	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;
	virtual bool ShouldTickIfViewportsOnly() const { return true; }

	virtual TArray<FSensorSearchQuery> PrepareSearchQueries();
	virtual TArray<FLogicSensorTarget> GetTargetsFromBoundingBox(TWeakObjectPtr<AActor> InActor);
	virtual TArray<FLogicSensorTarget> GetTargetsFromSkeleton(TWeakObjectPtr<USkeletalMeshComponent> InSkeleton);


	
	void ExecuteSearch(float TimeDelay);
	void Search();

	UFUNCTION()
	void Update(FSensorSearchResult SearchResult);

	void CalculatePrimeNumbers(int UpperLimit);

	
	
public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Settings", meta=(DisplayName = "Sensor Queries"))
	TArray<TSubclassOf<ULogicSensorQuery>> SensorQueryClasses;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Settings",  meta=(DisplayName = "Tick Rate"))
	float TickRate;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Settings", meta=(DisplayName = "Use Debug Mode"))
	bool bUseDebugMode;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Settings", meta=(DisplayName = "Use Stress-Test"))
	bool bUseStressTest;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Settings|Advanced")
	TEnumAsByte<ECollisionChannel> CollisionChannel;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Settings|Advanced", meta=(DisplayName = "Use Complex Trace"))
	bool bUseComplexTrace;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Settings|Advanced", meta=(DisplayName = "Ignore Actors by Sensor"))
	TArray<AActor*> IgnoreActors;


private:

	UPROPERTY()
	TArray<ULogicSensorQuery*> SensorQueries;
	
	TArray<FLogicSensorSocket> SensorSockets;
	FTimerHandle TimerHandle;
	
	FThreadSafeBool bIsPaused;
	FThreadSafeBool bIsKilled;

	TWeakObjectPtr<const ULogicSenseSettings> Settings;
	
	//DELETE
	ULogicSubsystem* LogicSubsystem;
	FSensorSearchResult SearchResult;
	
};