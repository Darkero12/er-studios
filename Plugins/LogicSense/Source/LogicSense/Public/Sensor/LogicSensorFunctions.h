﻿#pragma once

#include "Sensor/LogicSensorStructs.h"
#include "Library/CinematoFunctionLibrary.h"
#include "LogicSensorFunctions.generated.h"




UCLASS()
class LOGICSENSE_API ULogicSensorFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category="Logic Sense|Sensor|Socket", DisplayName="Get Socket Transform")
	static FTransform GetSocketTransform(FLogicSensorSocket Socket);

	UFUNCTION(BlueprintPure, Category="Logic Sense|Sensor|Socket", DisplayName="Get Socket Camera Settings")
	static FCameraSettings GetSocketCameraSettings(FLogicSensorSocket Socket);
};
