﻿#pragma once

#include "GameFramework/Character.h"

#include "Library/CinematoFunctionLibrary.h"
#include "LogicSensorStructs.generated.h"

USTRUCT(BlueprintType)
struct FLogicSensorSocket
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadWrite)
	USceneComponent* SceneComponent = nullptr;
	
	UPROPERTY(BlueprintReadWrite)
	FName SocketName;
	
	UPROPERTY(BlueprintReadWrite)
	FTransform Transform;

	UPROPERTY(BlueprintReadWrite)
	FCameraSettings CameraSettings;

	virtual FTransform GetTransform() const
	{
		USkeletalMeshComponent* Skeleton = Cast<USkeletalMeshComponent>(SceneComponent);
		if(Skeleton && !SocketName.IsNone())
		{
			FTransform SocketTransform = Skeleton->GetBoneTransform(Skeleton->GetBoneIndex(SocketName));
			return Transform * SocketTransform;
		}
		
		if(SceneComponent)
		{
			FTransform SceneTransform = SceneComponent->GetComponentTransform();
			return Transform * SceneTransform;
		}
		
		return Transform.Identity;
	}

	virtual FCameraSettings GetCameraSettings() const
	{
		return CameraSettings;
	}
};

USTRUCT(BlueprintType, Blueprintable)
struct FLogicSensorTarget
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)
	FVector Location = FVector(NAN,NAN,NAN);
};

USTRUCT(BlueprintType, Blueprintable)
struct FSensorSearchQuery
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadWrite)
	TWeakObjectPtr<AActor> Actor;
	
	UPROPERTY(BlueprintReadWrite)
	TArray<FLogicSensorTarget> SensorTargets;
	
	UPROPERTY(BlueprintReadWrite)
	TArray<FLogicSensorSocket> SensorSockets;


	bool IsValid() const
	{
		return !(!Actor.IsValid() || Actor->IsPendingKillPending() || Actor->IsActorBeingDestroyed());
	}
	
	TArray<FVector> GetTargetLocations()
	{
		TArray<FVector> TargetLocations;
		
		for (FLogicSensorTarget SensorTarget : SensorTargets)
		{
			TargetLocations.Add(SensorTarget.Location);
		}
		
		return TargetLocations;
	}

};

USTRUCT(BlueprintType, Blueprintable)
struct FSensorHit
{
	GENERATED_USTRUCT_BODY()
public:	
	UPROPERTY(BlueprintReadWrite)
	TWeakObjectPtr<AActor> Actor;

	UPROPERTY(BlueprintReadWrite)
	FVector SocketLocation = FVector(NAN,NAN,NAN);

	UPROPERTY(BlueprintReadWrite)
	FVector TargetLocation = FVector(NAN,NAN,NAN);

	UPROPERTY(BlueprintReadWrite)
	float Time = INFINITY;

	UPROPERTY(BlueprintReadWrite)
	FString Debug;
};

USTRUCT(BlueprintType, Blueprintable)
struct FSensorSearchResult
{
	GENERATED_USTRUCT_BODY()
public:	
	UPROPERTY(BlueprintReadWrite)
	TArray<FSensorHit> Hits;

	UPROPERTY(BlueprintReadWrite)
	float RequiredTime = INFINITY;
};

