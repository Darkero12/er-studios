﻿#pragma once

#include "CoreMinimal.h"
#include <GameFramework/Actor.h>

#include "GameFramework/Character.h"
#include "WeaponActor.generated.h"

class ULogicBehaviorComponent;

UENUM(BlueprintType)
enum EWeaponWield
{
	EPrimary	UMETA(DisplayName = "Primary"),
	EOffhand	UMETA(DisplayName = "Off-hand"),
	ETwoHanded	UMETA(DisplayName = "Two-Handed"),
};

UCLASS(Abstract, ClassGroup="LogicSense", hideCategories=(Object), Blueprintable)
class LOGICSENSE_API AWeaponActor : public AActor
{
	GENERATED_BODY()

	AWeaponActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

#if WITH_EDITOR

	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#endif // WITH_EDITOR

	//UVoxelComponent* GetVoxelComponent() const;
	UPROPERTY(Category=Weapon, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;

	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USkeletalMeshComponent* PrimaryHand;

	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USkeletalMeshComponent* SecondaryHand;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "LogicSense")
	ULogicBehaviorComponent* Behavior;

	UPROPERTY(Category=Settings, EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EWeaponWield> Wielding;

private:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* Root;
};