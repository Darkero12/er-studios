#pragma once

class FLogicSenseModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

private:
	virtual void LoadSettings() const;
	virtual void UnloadSettings() const;

};
