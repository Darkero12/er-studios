﻿#include "Sensor/LogicSensorComponent.h"

#include "LogicSenseSettings.h"
#include "Async/Async.h"
#include "Library/CoreFunctionLibrary.h"
#include "Logic/LogicSubsystem.h"
#include "Sensor/LogicSensorQuery.h"

// Sets default values for this component's properties
ULogicSensorComponent::ULogicSensorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;;
	PrimaryComponentTick.TickGroup = TG_PostUpdateWork;

	TickRate = 1;
	CollisionChannel = ECollisionChannel::ECC_Visibility;

	bUseDebugMode = false;
	bUseComplexTrace = true;
}

FLogicSensorSocket ULogicSensorComponent::AddSensorSocket(USceneComponent* Component, FName SocketName, FTransform Transform)
{
	FLogicSensorSocket Socket;
	Socket.SceneComponent = Component;
	Socket.SocketName = SocketName;
	Socket.Transform = Transform;

	SensorSockets.Add(Socket);

	return Socket;
}

TArray<FLogicSensorSocket> ULogicSensorComponent::GetSensorSockets()
{
	return SensorSockets;
}

TArray<FSensorHit> ULogicSensorComponent::GetSensorHits()
{
	return SearchResult.Hits;
}

FSensorSearchResult ULogicSensorComponent::SearchSync()
{
	FSensorSearchResult Result;
	float Time = GetWorld() ? GetWorld()->GetTimeSeconds() : 0;
	
	TArray<FSensorSearchQuery> SearchQueries = PrepareSearchQueries();
	
	for (auto SensorQuery : SensorQueries)
	{
		for (auto SearchQuery : SearchQueries)
		{
			if(bIsKilled) break;
			while(bIsPaused) FPlatformProcess::Sleep(TickRate);
			
			if(!SearchQuery.IsValid()) continue;
			
			TArray<FSensorHit> OutHits;
			if(SensorQuery->IsQueryValid(SearchQuery, OutHits) || true)
			{
				Result.Hits.Append(OutHits);
			}
		}
	}

	if (bUseStressTest)
	{
		CalculatePrimeNumbers(100000);
	}
	
	Result.RequiredTime = (GetWorld()->GetTimeSeconds() - Time);
	return Result;
}

void ULogicSensorComponent::SearchAsync(FSearchCallback Callback)
{
	AsyncTask(ENamedThreads::AnyHiPriThreadNormalTask, [this, Callback] ()
	{
		FSensorSearchResult SearchResult = SearchSync();
		
		AsyncTask(ENamedThreads::GameThread, [this, Callback, SearchResult] (){
			return Callback.ExecuteIfBound(SearchResult);
		});
	});
}

// Called when the game starts
void ULogicSensorComponent::BeginPlay()
{
	Super::BeginPlay();

	Settings = GetDefault<ULogicSenseSettings>();
	LogicSubsystem = ULogicSubsystem::Get(this);

	if(SensorSockets.Num() == 0)
	{
		//Default Character Template
		ACharacter* Character = Cast<ACharacter>(GetOwner());
		if(Character) 
		{
			USkeletalMeshComponent* SkeletalMesh = Character->GetMesh();

			FTransform Transform = FTransform::Identity;
			Transform.SetRotation(FQuat(FRotator(0,90,90)));

			FName Name = "head";
			
			AddSensorSocket(SkeletalMesh, Name, Transform);
		}
	}


	for (auto SensorQuery : SensorQueryClasses)
	{
		//If SensorQuery is null, ignore it and continue
		if(!SensorQuery) continue;
		
		ULogicSensorQuery* Query = NewObject<ULogicSensorQuery>(this, SensorQuery);
		SensorQueries.Add(Query);
	}
	
	float TimeDelay = FMath::RandRange(FMath::Max(TickRate * 0.05f, 0.05f),  FMath::Max(TickRate *  0.5f,  0.5f));
	ExecuteSearch(TimeDelay);
	
	//Garbage Collector Test to make sure, required UObjects aren't deleted
	FTimerHandle GarbageTimer;
	GetWorld()->GetTimerManager().SetTimer(GarbageTimer, [&]()
	{
		GLog->Logf(TEXT("GEngine Garbarge Collection"));
		GEngine->ForceGarbageCollection();
	}, 2.0f, false);
}

void ULogicSensorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	//Deactivate Update if Debug or Dedicated
	if (!bUseDebugMode || GetWorld()->GetNetMode() == NM_DedicatedServer) return;

	for (auto SensorQuery : SensorQueries)
	{
		SensorQuery->OnDebug();
	}
}

void ULogicSensorComponent::SetActive(bool bNewActive, bool bReset)
{
	bIsPaused.AtomicSet(bNewActive);
	Super::SetActive(bNewActive, bReset);
}

void ULogicSensorComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	if(UCoreFunctionLibrary::IsGameRunning()) bIsKilled.AtomicSet(true);
	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

TArray<FSensorSearchQuery> ULogicSensorComponent::PrepareSearchQueries()
{
	TArray<FSensorSearchQuery> SearchQueries;

	const TArray<FLogicSensorSocket> TempSensorSockets = GetSensorSockets();
	if(TempSensorSockets.Num() <= 0) return SearchQueries;

	for (AActor* Actor : LogicSubsystem->GetReferencedActors())
	{
		if(bIsKilled) break;
		while(bIsPaused) FPlatformProcess::Sleep(TickRate);
			
		if(Actor == GetOwner()) continue;
		
		FSensorSearchQuery SearchQuery;
		SearchQuery.Actor = Actor;
		SearchQuery.SensorSockets = TempSensorSockets;
		
		TArray<USkeletalMeshComponent*> Skeletons;
		if(const ACharacter* Character = Cast<ACharacter>(Actor))
		{
			Skeletons.Add(Character->GetMesh());
		}
		else
		{
			Actor->GetComponents<USkeletalMeshComponent>(Skeletons, true);
		}
			
		for (USkeletalMeshComponent* Skeleton : Skeletons)
		{
			TArray<FLogicSensorTarget> Targets = GetTargetsFromSkeleton(Skeleton);
			SearchQuery.SensorTargets.Append(Targets);
		}

		if(SearchQuery.SensorTargets.Num() == 0)
		{
			TArray<FLogicSensorTarget> Targets = GetTargetsFromBoundingBox(Actor);
			SearchQuery.SensorTargets.Append(Targets);
		}
		
		SearchQueries.Add(SearchQuery);
	}
	
	return SearchQueries;
}

TArray<FLogicSensorTarget> ULogicSensorComponent::GetTargetsFromBoundingBox(TWeakObjectPtr<AActor> InActor)
{
	TArray<FLogicSensorTarget> Targets;
	if(!InActor.IsValid()) return Targets;
	
	FVector Origin;
	FVector Box;
	InActor->GetActorBounds(true, Origin, Box);
	
	TArray<FVector> BoundingBox = { FVector::ZeroVector, FVector( Box.X,0,0), FVector(-Box.X,0,0),
		FVector(0,-Box.Y,0), FVector(0, Box.Y,0), FVector(0,0, Box.Z), FVector(0,0,-Box.Z),
	};

	for(int index = 0; index < BoundingBox.Num(); index++)
	{
		FLogicSensorTarget Target;
		Target.Location = Origin + BoundingBox[index];
		Targets.Add(Target);
	}

	return Targets;
}

TArray<FLogicSensorTarget> ULogicSensorComponent::GetTargetsFromSkeleton(TWeakObjectPtr<USkeletalMeshComponent> InSkeleton)
{
	TArray<FLogicSensorTarget> Targets;

	if(!InSkeleton.IsValid()) return Targets;
		
	const FSkeletonOptions* SkeletonOptions = Settings.IsValid() ?
			Settings->GetSensorSettings().SkeletonOptions.Find(InSkeleton->GetSkinnedAsset()->GetSkeleton())
	: nullptr;
	
	TArray<FName> SocketNames = SkeletonOptions && SkeletonOptions->SocketNames.Num() > 0 ? SkeletonOptions->SocketNames : InSkeleton->GetAllSocketNames();
		
	for (FName SocketName :  SocketNames)
	{
		
		//Only use Bones, ignore Sockets
		if(SocketName != InSkeleton->GetSocketBoneName(SocketName) || InSkeleton->GetParentBone(SocketName) == "None") continue;
				
		int32 BoneIndex = InSkeleton->GetBoneIndex(SocketName);
		if(BoneIndex == INDEX_NONE) continue;

		FLogicSensorTarget Target;
		Target.Location = InSkeleton->GetBoneTransform(BoneIndex).GetLocation();

		Targets.Add(Target);
	}

	return Targets;
}

void ULogicSensorComponent::Search()
{
	if(!Settings.IsValid() || Settings->GetSensorSettings().Runtime == ELogicSensorRuntime::EAsync)
	{
		FSearchCallback Callback;
		Callback.BindDynamic(this, &ULogicSensorComponent::Update);
	
		SearchAsync(Callback);
	}
	else
	{
		Update( SearchSync() );
	}
}




void ULogicSensorComponent::ExecuteSearch(float TimeDelay)
{
	if(bIsKilled || !IsInGameThread()) return;
	
	if(TimeDelay > 0)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&]() { Search(); }, TimeDelay, false);
	}
	else
	{
		GetWorld()->GetTimerManager().SetTimerForNextTick([&]() { Search(); });
	}
}

void ULogicSensorComponent::Update(FSensorSearchResult InSearchResult)
{
	SearchResult = InSearchResult;
	ExecuteSearch(TickRate - SearchResult.RequiredTime);
}



void ULogicSensorComponent::CalculatePrimeNumbers(int UpperLimit)
{
	//Calculating the prime numbers...
	for (int i = 1; i <= UpperLimit; i++)
	{
		
		bool isPrime = true;
		for (int j = 2; j <= i / 2; j++)
		{
			if(bIsKilled)
			{
				GLog->Log("GAME OVER ");
				return;
			}

			while(bIsPaused)
			{
				FPlatformProcess::Sleep(TickRate);
			}
			
			if (i%j == 0)
			{
				isPrime = false;
				break;
			}
		}
 
		if (isPrime) GLog->Log("Prime number #" + FString::FromInt(i) + ": " + FString::FromInt(i));
	}
}


