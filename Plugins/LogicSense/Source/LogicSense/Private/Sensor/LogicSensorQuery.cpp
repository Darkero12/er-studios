﻿#include "Sensor/LogicSensorQuery.h"
#include "Sensor/LogicSensorFunctions.h"

#include "Library/MathFunctionLibrary.h"
#include "Sensor/LogicSensorComponent.h"

bool ULogicSensorQuery::IsInstantiated() const
{
	return !HasAllFlags(RF_ClassDefaultObject);
}

UWorld* ULogicSensorQuery::GetWorld() const
{
	if (!IsInstantiated())
	{
		// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
		return nullptr;
	}
	return GetOuter()->GetWorld();
}


bool ULogicSensorQuery::IsQueryValid_Implementation(FSensorSearchQuery SearchQuery, TArray<FSensorHit>& OutHits) const
{
	ULogicSensorComponent* Sensor = GetSensorComponent();
	
	for (auto SensorSocket : SearchQuery.SensorSockets)
	{
		for (auto TargetLocation : SearchQuery.GetTargetLocations())
		{
			FVector SocketLocation = SensorSocket.GetTransform().GetLocation();

			TArray<FHitResult> Hits;
			bool const bHit = UMathFunctionLibrary::LineTraceAllByChannel(this, Hits, SocketLocation, TargetLocation, Sensor->CollisionChannel, Sensor->IgnoreActors, Sensor->bUseComplexTrace, true, false);

			if(!bHit)
			{
				FSensorHit Hit;
				Hit.Actor = SearchQuery.Actor;
				Hit.SocketLocation = SocketLocation;
				Hit.TargetLocation = TargetLocation;
				
				OutHits.Add(Hit);
				return true;
			}
		}
	}
	
	return false;
}

void ULogicSensorQuery::OnExecuted_Implementation()
{
	
}

ULogicSensorComponent* ULogicSensorQuery::GetSensorComponent() const
{
	return (IsInstantiated()) ? Cast<ULogicSensorComponent>(GetOuter()) : nullptr; 
}

