﻿#include "Sensor/LogicSensorFunctions.h"

FTransform ULogicSensorFunctions::GetSocketTransform(FLogicSensorSocket Socket)
{
	return Socket.GetTransform();
}

FCameraSettings ULogicSensorFunctions::GetSocketCameraSettings(FLogicSensorSocket Socket)
{
	return Socket.GetCameraSettings();
}
