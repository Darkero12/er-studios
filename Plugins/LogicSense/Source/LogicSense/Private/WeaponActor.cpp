﻿#include "WeaponActor.h"

#include "Animation/AnimSingleNodeInstance.h"
#include "Logic/LogicBehaviorComponent.h"

AWeaponActor::AWeaponActor(const FObjectInitializer& ObjectInitializer ): Super(ObjectInitializer)
{
	Wielding = EPrimary;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Behavior= CreateDefaultSubobject<ULogicBehaviorComponent>(TEXT("Logic Behavior"));

	


	//PRIMARY & SECONDARY

	/*ConstructorHelpers::FObjectFinder<USkeletalMesh> MannequinAsset(TEXT("/LogicSense/Assets/Mannequin/SKM_Hand.SKM_Hand"));
	USkeletalMesh* SkeletalMesh = nullptr;
	if (MannequinAsset.Succeeded()) SkeletalMesh = MannequinAsset.Object;
	

	UAnimSequence* Anim = nullptr;
	ConstructorHelpers::FObjectFinder<UAnimSequence>AnimAsset(TEXT("AnimSequence'/LogicSense/Assets/Mannequin/Animations/AS_Grab.AS_Grab'"));
	if (AnimAsset.Succeeded()) Anim = AnimAsset.Object;
	
	PrimaryHand = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Primary Socket"));
	PrimaryHand ->SetupAttachment(RootComponent);
	PrimaryHand ->SetRelativeLocation(FVector(9.35, 0, 45));
	PrimaryHand ->SetRelativeRotation(FRotator::MakeFromEuler(FVector(-90, -5, 180)));
	PrimaryHand ->SetHiddenInGame(true);
	PrimaryHand ->SetCollisionProfileName(TEXT("NoCollision"));
	PrimaryHand ->SetSkeletalMesh(SkeletalMesh);
	
	PrimaryHand->SetAnimationMode(EAnimationMode::AnimationSingleNode);

	PrimaryHand->AnimationData.AnimToPlay = Anim;
	PrimaryHand->AnimationData.bSavedLooping = false;
	PrimaryHand->AnimationData.bSavedPlaying = false;

	

	SecondaryHand = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Secondary Socket"));
	SecondaryHand ->SetupAttachment(RootComponent);
	SecondaryHand ->SetRelativeLocation(FVector(9.8, 0, 60));
	SecondaryHand ->SetRelativeRotation(FRotator::MakeFromEuler(FVector(-90, -5, 180)));
	SecondaryHand ->SetRelativeScale3D(FVector(1,1,-1));
	SecondaryHand ->SetHiddenInGame(true);
	SecondaryHand ->SetCollisionProfileName(TEXT("NoCollision"));
	SecondaryHand ->SetSkeletalMesh(SkeletalMesh);

	SecondaryHand->SetAnimationMode(EAnimationMode::AnimationSingleNode);

	SecondaryHand->AnimationData.AnimToPlay = Anim;
	SecondaryHand->AnimationData.bSavedLooping = false;
	SecondaryHand->AnimationData.bSavedPlaying = false;

	//MESH
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	Mesh ->SetupAttachment(RootComponent);
	Mesh ->SetRelativeLocation(FVector(0, 0, 50));
	Mesh ->SetRelativeRotation(FRotator(0,0,0));
	Mesh ->SetCollisionProfileName(TEXT("NoCollision"));

	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/LogicSense/Assets/Weapons/SM_Sword.SM_Sword"));
	if (MeshAsset.Succeeded()) Mesh->SetStaticMesh(MeshAsset.Object);*/
	
}


#if WITH_EDITOR
void AWeaponActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{

	static const FName NAME_Wielding = FName(TEXT("Wielding"));

	if (PropertyChangedEvent.Property && PropertyChangedEvent.Property->GetFName() == NAME_Wielding)
	{
		GLog->Logf(TEXT("WIELDING CHANGED %s"), *GetName());
	}

	GLog->Logf(TEXT("SOMETHING CHANGED"));
	
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif // WITH_EDITOR
