#include "LogicSenseModule.h"

#define LOCTEXT_NAMESPACE "FLogicSenseModule"

void FLogicSenseModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	LoadSettings();
	
	/*FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomPropertyTypeLayout( FMyObjectWrapperStruct::StaticStruct()->GetFName(), FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FMyWrapperStructCustomization::MakeInstance));
	PropertyModule.NotifyCustomizationModuleChanged();*/
}

void FLogicSenseModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	UnloadSettings();

	/*if (FModuleManager::Get().IsModuleLoaded("PropertyEditor")) {
		FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
		PropertyModule.UnregisterCustomPropertyTypeLayout(FMyObjectWrapperStruct::StaticStruct()->GetFName());

		PropertyModule.NotifyCustomizationModuleChanged();
	}*/
}


void  FLogicSenseModule::LoadSettings() const
{
	/*if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		SettingsModule->RegisterSettings("Project", "Plugins", "LogicSense",
			LOCTEXT("Project Settings Name", "Logic Sense"),
			LOCTEXT("Project Settings Description", "Configure Logic Sense"),
			GetMutableDefault<ULogicProjectSettings>());
	}*/
}

void  FLogicSenseModule::UnloadSettings() const
{
	/*if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		SettingsModule->UnregisterSettings("Project", "Plugins", "LogicSense");
	}*/
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FLogicSenseModule, LogicSense)