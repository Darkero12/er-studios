#include "LogicCoreModule.h"
#include "GameplayTagsManager.h"

#define LOCTEXT_NAMESPACE "FLogicCoreModule"

void FLogicCoreModule::StartupModule()
{
	FString TagConfigPath = FPaths::ProjectPluginsDir() / TEXT("LogicSense") / TEXT("Config");
	UGameplayTagsManager::Get().AddTagIniSearchPath(TagConfigPath);

	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	/*UGameplayTagsManager::OnLastChanceToAddNativeTags().AddLambda([this]() {
		GLog->Logf(ELogVerbosity::Warning, TEXT("Delegate CSTART MODULE"));
	});*/
}

void FLogicCoreModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FLogicCoreModule, LogicCore)