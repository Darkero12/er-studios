﻿#include "Logic/Notification/LogicNotificationLibrary.h"

#include "Library/CoreFunctionLibrary.h"
#include "Logic/LogicSubsystem.h"
#include "Logic/Notification/LogicNotification.h"
#include "UObject/UnrealTypePrivate.h"


class ULogicNotification;


ULogicNotification* ULogicNotificationLibrary::BroadcastProperty(UObject* WorldContextObject, UProperty* Payload)
{
	
	ULogicNotification* Notification = NewObject<ULogicNotification>();
	//Notification->Payload = Payload;

	

	GLog->Logf(ELogVerbosity::SetColor,TEXT("## BroadcastProperty sending"), OutputDeviceColor::COLOR_PURPLE);
	return Notification;
/*
	if(!WorldContextObject) return Notification;
	ULogicSubsystem::GetLogicSubsystem(WorldContextObject)->OnNotifyEvent.Broadcast(Notification);
	return Notification;*/
}

ULogicNotification* ULogicNotificationLibrary::BroadcastDialogue(AActor* Sender, FGameplayTag Tag)
{
	ULogicNotification* Notification = NewObject<ULogicNotification>();
	return Notification;
}

ULogicNotification* ULogicNotificationLibrary::Notify(AActor* Sender, TArray<AActor*> Receivers,
                                                      FGameplayTagContainer Tags,
                                                      const FOnNotifyCallbackEvent& Callback)
{
	GLog->Logf(ELogVerbosity::Error,TEXT("%s send Nofification"), *Sender->GetName());

	ULogicNotification* Notification = NewObject<ULogicNotification>();
	Notification->Sender = Sender;
	//Notification->Receivers = Receivers;
	Notification->Tags = Tags;
	Notification->Callback = Callback;

	ULogicSubsystem::Get(Sender)->OnNotifyEvent.Broadcast(Notification);

	return Notification;
}

/*void ULogicNotificationLibrary::PredicateFilterWildcard(const TArray<UStructProperty*>& Array,
	const FWildcardFilterDelegate& PredicateFunction, TArray<UStructProperty*>& Results, bool InvertResult)
{
	
}

/*ULogicNotification* ULogicNotificationLibrary::NotifyActor(AActor* Sender, AActor* Receiver, FGameplayTagContainer Tags)
{

	ULogicNotification* Notification = NewObject<ULogicNotification>();
	Notification->Sender = Sender;
	Notification->Receivers.Add(Receiver);
	Notification->Tags = Tags;

	//ULogicSubsystem::GetLogicInstance(Sender)->OnNotify.Broadcast(Notification);
	
	return Notification;
}*/
/*ULogicNotification* ULogicNotificationLibrary::NotifyTag(AActor* Sender, FGameplayTagContainer Tags)
{
	ULogicNotification* Notification = NewObject<ULogicNotification>();
	Notification->Sender = Sender;
	Notification->Tags = Tags;

	ULogicSubsystem::GetLogicSubsystem(Sender)->OnNotifyDelegate.Broadcast(Notification);
	
	return Notification;
}*/

DEFINE_FUNCTION(ULogicNotificationLibrary::execBroadcastProperty)
{
	// Steps into the stack, walking to the next property in it
	Stack.Step(Stack.Object, NULL);

	// Grab the last property found when we walked the stack
	// This does not contains the property value, only its type information
	FProperty* StructProperty = CastField<FProperty>(Stack.MostRecentProperty);

	// Grab the base address where the struct actually stores its data
	// This is where the property value is truly stored
	void* StructPtr = Stack.MostRecentPropertyAddress;

	// We need this to wrap up the stack
	P_FINISH;

	// Iterate through the struct
	UCoreFunctionLibrary::IterateThroughStructProperty(StructProperty, StructPtr);
}


