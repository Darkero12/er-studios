﻿#include "Logic/Transitions/LogicTransition.h"

#include "EnhancedInputComponent.h"
#include "Logic/Nodes/Dialogues/LogicDialogueChoiceNode.h"
#include "Logic/Nodes/Dialogues/LogicDialogueState.h"
#include "Logic/Nodes/Dialogues/LogicDialogueNode.h"
#include "UObject/ConstructorHelpers.h"


ULogicTransition::ULogicTransition()
{
#if WITH_EDITORONLY_DATA
	bInit = false;
	
	SetUseCustomColor(true);
	SetNodeColor(FLinearColor(0.12f,0.44f,1.0f, 1.0f));

	NodeIconSize = FVector2D(64.0f,32.0f);

	AddRule(ULogicNodeInstance::StaticClass(), true, false);
	AddRule(ULogicNodeInstance::StaticClass(), false, true);

	AddRule(ULogicDialogueState::StaticClass(), true, false);
	AddRule(ULogicDialogueState::StaticClass(), false, true);
	
	AddRule(ULogicDialogueNode::StaticClass(), true, false);
	AddRule(ULogicDialogueNode::StaticClass(), false, true);
	AddRule(ULogicDialogueChoiceNode::StaticClass(), true, false);
	AddRule(ULogicDialogueChoiceNode::StaticClass(), false, true);
	
	auto TextureAsset = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("/LogicSense/Assets/Icons/T_Dialogue_To_Dialogue_256x128"));
	if (TextureAsset.Object != nullptr) TextureDTD = TextureAsset.Object;

	TextureAsset = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("/LogicSense/Assets/Icons/T_Dialogue_To_Choice_256x128"));
	if (TextureAsset.Object != nullptr) TextureDTC = TextureAsset.Object;
		
#endif
}

void ULogicTransition::OnTransitionInitialized_Implementation()
{
	Super::OnTransitionInitialized_Implementation();

	

	UInputComponent* PlayerInputComponent = nullptr;
	if(APawn* Pawn = Cast<APawn>(GetStateMachineInstance(true)->GetContext()))
	{
		PlayerInputComponent = Pawn->InputComponent;
	}

	if(!InputAction) return;

	if(UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		Event = &EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Triggered, this, &ULogicTransition::Evaluate);
	}
}

void ULogicTransition::OnTransitionShutdown_Implementation()
{
	//GLog->Logf(ELogVerbosity::Error,TEXT("#SHUTDOWN %s"), *GetNodeName());
	Super::OnTransitionShutdown_Implementation();

	UInputComponent* PlayerInputComponent = nullptr;
	if(APawn* Pawn = Cast<APawn>(GetStateMachineInstance(true)->GetContext()))
	{
		PlayerInputComponent = Pawn->InputComponent;
	}

	if(!InputAction) return;
	
	if(UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->RemoveBinding(*Event);
	}
}

void ULogicTransition::ConstructionScript_Implementation()
{
	Super::ConstructionScript_Implementation();

#if WITH_EDITORONLY_DATA
	//if(bInit) return;

	USMStateInstance_Base* PreviousInstance = GetPreviousStateInstance();
	USMStateInstance_Base* NextInstance = GetNextStateInstance();

	if((Cast<ULogicDialogueNode>(PreviousInstance) || Cast<ULogicDialogueChoiceNode>(PreviousInstance)) && Cast<ULogicDialogueNode>(NextInstance))
	{
		SetNodeColor(FLinearColor(1.0f,0.09f,0.04f, 1.0f));
		
		SetUseCustomIcon(true);
		NodeIcon = TextureDTD;
		NodeIconTintColor = FLinearColor(1.0f,0.09f,0.04f, 0.7f);
	}
	
	if(Cast<ULogicDialogueNode>(PreviousInstance) && Cast<ULogicDialogueChoiceNode>(NextInstance))
    {
		SetNodeColor(FLinearColor(1.0f,0.001f,0.0f, 1.0f));
		
		SetUseCustomIcon(true);
		NodeIcon = TextureDTC;
		NodeIconTintColor = FLinearColor(1.0f,0.001f,0.0f, 0.7f);
		
		
    }
	bInit = true;
	
#endif
}

bool ULogicTransition::CanEnterTransition_Implementation() const
{
	USMStateInstance_Base* Instance = GetPreviousStateInstance();

	//Check if previous Node uses LogicNode Interface and use custom behavior
	if(ILogicTransitionInterface* Interface = Cast<ILogicTransitionInterface>(Instance))
	{
		//GLog->Logf(ELogVerbosity::Error,TEXT("Execute_IsAccessible  --> %s"), Interface->Execute_IsAccessible(Instance) ? *FString("True") : *FString("False"));
		//GLog->Logf(ELogVerbosity::Error,TEXT("CanEnterTransition %s -->"), *Instance->GetNodeName());
		return Interface->Execute_CanExecuteTransition(Instance);
	}

	return true;
	
	//Otherwise use default Node behavior
	//return Super::CanEnterTransition_Implementation();
}



void ULogicTransition::Evaluate(const FInputActionValue& Value)
{
	//GLog->Logf(ELogVerbosity::Error,TEXT("# Evaluate %s <= %s"),*this->GetName(), *GetStateMachineInstance(true)->GetName() );
	EvaluateFromManuallyBoundEvent();
}

#if WITH_EDITORONLY_DATA
void ULogicTransition::AddRule(UClass* Class, bool bToState, bool bFromState)
{
	FSMNodeConnectionRule ConnectionRule;
	FSMStateClassRule Rule;

	Rule.StateClass = TSoftClassPtr<USMStateInstance_Base>(Class);
	Rule.bIncludeChildren = true;

	if(bToState) ConnectionRule.ToState = Rule;
	if(bFromState) ConnectionRule.FromState = Rule;
	
	ConnectionRules.AllowedConnections.Add(ConnectionRule);
}
#endif