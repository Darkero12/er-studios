﻿#include "Logic/Nodes/Dialogues/LogicDialogueChoiceNode.h"

#include "UObject/ConstructorHelpers.h"

ULogicDialogueChoiceNode::ULogicDialogueChoiceNode() : Super()
{
#if WITH_EDITORONLY_DATA
	
	NodeDescription.Category = FText::FromString("Dialogue");
	NodeDescription.Name = FName( "Choice");

	bShowDisplayNameOnly = true;
	bDisplayNameWidget = false;

	SetUseCustomColor(true);
	SetNodeColor(FLinearColor(1.0f,0.1f,0.0f, 0.7f));
	
	SetUseCustomIcon(true);
	NodeIconSize = FVector2D(64.0f,64.0f);

	auto TextureAsset = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("/LogicSense/Assets/Icons/T_Dialogue_256x256"));
	//auto TextureAsset = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("/LogicSense/Assets/Icons/T_Choice_256x256"));
	if (TextureAsset.Object != nullptr) NodeIcon = TextureAsset.Object;
#endif
}

void ULogicDialogueChoiceNode::OnStateBegin_Implementation()
{
	Super::OnStateBegin_Implementation();
	EvaluateTransitions();
}

bool ULogicDialogueChoiceNode::IsAccessible_Implementation()
{
	return Super::IsAccessible_Implementation();
}



void ULogicDialogueChoiceNode::Trigger()
{
	TArray<USMTransitionInstance*> Transitions;
	GetIncomingTransitions(Transitions,false);

	TArray<USMStateInstance_Base*> PreviousInstances;
	for(USMTransitionInstance* Transition : Transitions)
	{
		USMStateInstance_Base* Instance = Transition->GetPreviousStateInstance();
		if(Instance->IsActive()) PreviousInstances.Add(Instance);
	}

	for(USMStateInstance_Base* PreviousInstance : PreviousInstances)
	{
		Transitions.Empty();
		PreviousInstance->GetOutgoingTransitions(Transitions,false);

		for(USMTransitionInstance* Transition : Transitions)
		{
			if(Transition->GetNextStateInstance() != this) continue;
			if(GetStateMachineInstance()->EvaluateAndTakeTransitionChain(Transition)) break;
		}
		
	}
}


TArray<ULogicDialogueChoiceNode*> ULogicDialogueChoiceNode::GetChoiceNodes(USMStateInstance_Base* Instance)
{
	TArray<ULogicDialogueChoiceNode*> ChoiceNodes;

	TArray<USMTransitionInstance*> Transitions;
	Instance->GetOutgoingTransitions(Transitions,false);
	
	for(auto Transition : Transitions)
	{
		USMStateInstance_Base* NextInstance = Transition->GetNextStateInstance();

		if(ULogicDialogueChoiceNode* ChoiceInstance = Cast<ULogicDialogueChoiceNode>(NextInstance))
		{
			ChoiceNodes.Add(ChoiceInstance);
		} 
	}
	
	return ChoiceNodes;
}
