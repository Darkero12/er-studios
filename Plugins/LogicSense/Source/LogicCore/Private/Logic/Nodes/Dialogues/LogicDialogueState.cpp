﻿#include "Logic/Nodes/Dialogues/LogicDialogueState.h"

#include "Logic/Nodes/Dialogues/ILogicDialogueInterface.h"
#include "Logic/Nodes/Dialogues/LogicDialogueNode.h"
#include "Logic/Nodes/LogicNodeSubsystem.h"

#include "EnhancedInputComponent.h"
#include "SMInstance.h"
#include "Kismet/GameplayStatics.h"

#include "UObject/ConstructorHelpers.h"

ULogicDialogueState::ULogicDialogueState() : Super()
{

#if WITH_EDITORONLY_DATA

	SetUseCustomColor(true);
	SetNodeColor(FLinearColor(1.0f,0.1f,0.0f, 0.7f));

	SetUseCustomIcon(true);
	NodeIconSize = FVector2D(64.0f,64.0f);

	auto TextureAsset = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("/LogicSense/Assets/Icons/T_Choice_256x256"));
	if (TextureAsset.Object != nullptr) NodeIcon = TextureAsset.Object;
#endif
	
}

void ULogicDialogueState::OnRootStateMachineStart_Implementation()
{
	Super::OnRootStateMachineStart_Implementation();
	
	//Register to Director Subsystem
	ULogicNodeSubsystem::Get(GetWorld())->Register(this);
	
	/*FTimerHandle GarbageTimer;
	GetWorld()->GetTimerManager().SetTimer(GarbageTimer, [this]()
	{
		TESTIT();
	}, 2.0f, false);*/
}


void ULogicDialogueState::OnStateBegin_Implementation()
{
	Super::OnStateBegin_Implementation();

	//OnStateUpdateEvent.AddDynamic(this, &ULogicDialogueState::Update );
	//class USMStateInstance_Base*, StateInstance, float, DeltaSeconds

	APlayerController* PlayerController = GetPlayerControllers()[0];
	if(!PlayerController) GLog->Logf(ELogVerbosity::Error, TEXT("## PLAYER CONTROLLER IS NULL"));

	//KeyControlls
	UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerController->InputComponent);
	if(EnhancedInputComponent && InputAction)
	{
		InputBinding = &EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Triggered, this, &ThisClass::EvaluateInput);
	}
	
	if(WidgetClass)
	{
		//UUserWidget* Widget
		Widget = CreateWidget<UUserWidget>(PlayerController, WidgetClass.Get());
		Widget->AddToViewport();
			
		if (Widget->GetClass()->ImplementsInterface(ULogicDialogueInterface::StaticClass()))
		{
			
			ILogicDialogueInterface::Execute_SetupDialogue(Widget, this);

			//Delegate MyDelegate;
			//MyDelegate.BindUFunction(this, FName("Refresh2_Implementation"));

			//ULogicDialogueNode Node;
			//OnUpdateEvent.AddDynamic(Node, &ULogicDialogueNode::SetupDialogue_Implementation);

			//OnUpdateEvent.AddDynamic( Widget, &ILogicDialogueInterface::Execute_SetupDialogue );
		}
	}

	GetStateMachineReference()->OnStateMachineStateChangedEvent.AddDynamic(this, &ThisClass::OnUpdate );
	OnUpdateEvent.Broadcast(this);
}

void ULogicDialogueState::OnUpdate(USMInstance* Instance, FSMStateInfo ToState, FSMStateInfo FromState)
{
	//GLog->Logf(ELogVerbosity::Error, TEXT("## OnUpdate"));
	OnUpdateEvent.Broadcast(this);
}

void ULogicDialogueState::OnStateEnd_Implementation()
{
	Widget->RemoveFromParent();

	auto Players = GetPlayerControllers();

	if(Players.Num() > 0)
	{
		APlayerController* PlayerController = GetPlayerControllers()[0];
		if(!PlayerController) GLog->Logf(ELogVerbosity::Error, TEXT("## PLAYER CONTROLLER IS NULL"));

		UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerController->InputComponent);
	
		if(EnhancedInputComponent && InputBinding)
		{
			EnhancedInputComponent->RemoveBinding(*InputBinding);
		}
	}

	GetStateMachineReference()->OnStateMachineStateChangedEvent.RemoveDynamic(this, &ThisClass::OnUpdate );
	
	Super::OnStateShutdown_Implementation();
}

bool ULogicDialogueState::CanExecuteTransition_Implementation()
{
	return IsInEndState();
}

void ULogicDialogueState::Trigger()
{
	if(IsActive())
	{
		Evaluate();
	}
	else {
		SetActive(true, false, true);
	}
	
}

USMNodeInstance* ULogicDialogueState::GetNodeInstance()
{
	if(!(GetStateMachineReference() && GetStateMachineReference()->GetSingleActiveState())) return nullptr;
	return GetStateMachineReference()->GetSingleActiveState()->GetNodeInstance();
}


FText ULogicDialogueState::GetText()
{
	if(auto DialogueNode = GetActiveDialogueNode())
	{
		return DialogueNode->GetText();
	}
	
	return FText::FromString("#MISSING DIALOGUE TEXT");
}

void ULogicDialogueState::Evaluate()
{
	bool bExit = false;
	if(IsInEndState())
	{
		OnUpdateEvent.Clear();
		bExit = true;
	}

	TArray<USMStateInstance_Base*> Instances;
	GetStateMachineReference()->GetAllActiveStateInstances(Instances);

	for(auto Instance : Instances)
	{
		if(auto CastInstance = Cast<ULogicDialogueNode>(Instance))
		{
			CastInstance->Evaluate();
		}
	}
	
	//OnUpdateEvent.Broadcast(this);

	if(bExit)
	{
		SetActive(false);
	}
}

void ULogicDialogueState::EvaluateInput(const FInputActionValue& Value)
{
	Evaluate();
}



ULogicDialogueNode* ULogicDialogueState::GetActiveDialogueNode()
{
	return Cast<ULogicDialogueNode>(GetNodeInstance());
}

TArray<APlayerController*> ULogicDialogueState::GetPlayerControllers()
{
	TSet<APlayerController*> PlayerControllers;

	PlayerControllers.Add(GetStateMachineReference()->GetInputController());
	PlayerControllers.Add(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	

	//Remove any Null Pointer
	PlayerControllers.Remove(nullptr);

	
	//for( FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator )
	
	
	return PlayerControllers.Array();
}


/*template <typename UserClass, typename... VarTypes>
void ULogicDialogueState::BindUObject(UserClass* InUserObject, typename TMemFunPtrType<false, UserClass, VarTypes... > Func, VarTypes... Vars)
{
	//static_assert(!TIsConst<UserClass>::Value, "Attempting to bind a delegate with a const object pointer and non-const member function.");
	//*this = CreateUObject(InUserObject, InFunc, Vars...);
}*/

/*APlayerController* ULogicDialogueState::GetPlayerController()
{
	if(APawn* Pawn = Cast<APawn>(GetStateMachineInstance()->GetContext()))
	{
		if(APlayerController* PlayerController = Cast<APlayerController>(Pawn->GetController())) return PlayerController;
	}
	
	return nullptr;
}*/
