﻿#include "Logic/Nodes/Dialogues/LogicDialogueNode.h"

#include "Logic/Nodes/Dialogues/LogicDialogueChoiceNode.h"
#include "UObject/ConstructorHelpers.h"

ULogicDialogueNode::ULogicDialogueNode() : Super()
{

#if WITH_EDITORONLY_DATA
	
	NodeDescription.Category = FText::FromString("Dialogue");
	NodeDescription.Name = FName( "Dialogue");

	bShowDisplayNameOnly = true;
	bDisplayNameWidget = false;

	SetUseCustomColor(true);
	SetNodeColor(FLinearColor(1.0f,0.3f,0.21f, 0.7f));
	
	SetUseCustomIcon(true);
	NodeIconSize = FVector2D(64.0f,64.0f);

	auto TextureAsset = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("/LogicSense/Assets/Icons/T_Dialogue_256x256"));
	if (TextureAsset.Object != nullptr) NodeIcon = TextureAsset.Object;
#endif
}

void ULogicDialogueNode::OnStateEnd_Implementation()
{
	OnUpdateEvent.Broadcast(this);
	Super::OnStateEnd_Implementation();
}

bool ULogicDialogueNode::CanExecuteTransition_Implementation()
{
	return true;
}

void ULogicDialogueNode::Evaluate()
{
	TArray<USMTransitionInstance*> Transitions;
	GetOutgoingTransitions(Transitions,false);

	for(USMTransitionInstance* Transition : Transitions)
	{
		if(Cast<ULogicDialogueChoiceNode>(Transition->GetNextStateInstance())) continue;
		if(GetStateMachineInstance()->EvaluateAndTakeTransitionChain(Transition)) break;
	}
}

FText ULogicDialogueNode::GetText()
{
	return Text.Result;
}


