﻿#include "Logic/Nodes/Dialogues/LogicDialogueMachine.h"

#include "Blueprint/UserWidget.h"
#include "Logic/Nodes/Dialogues/LogicDialogueState.h"

ULogicDialogueMachine::ULogicDialogueMachine() : Super()
{
	StateMachineClass = ULogicDialogueState::StaticClass();
	AutoReceiveInput = ESMStateMachineInput::UseContextController;
}

void ULogicDialogueMachine::OnStateMachineInitialized_Implementation()
{
	Super::OnStateMachineInitialized_Implementation();
}

void ULogicDialogueMachine::OnStateMachineShutdown_Implementation()
{
	Super::OnStateMachineShutdown_Implementation();
}
