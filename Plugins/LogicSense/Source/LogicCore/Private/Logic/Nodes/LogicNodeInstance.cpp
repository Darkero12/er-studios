﻿#include "Logic/Nodes/LogicNodeInstance.h"

#include "GameplayTagsManager.h"
#include "Library/CoreFunctionLibrary.h"
#include "Logic/LogicBehaviorInstance.h"
#include "Logic/LogicSubsystem.h"
#include "Logic/Nodes/LogicNodeSubsystem.h"
#include "Logic/Notification/LogicNotification.h"
#include "UObject/UnrealTypePrivate.h"

#if WITH_EDITORONLY_DATA
#include "ISMEditorGraphNodeInterface.h"
#include "ISMEditorGraphPropertyNodeInterface.h"
#endif

ULogicNodeInstance::ULogicNodeInstance() : Super()
{
	SetDefaultToParallel(true);
	SetStayActiveOnStateChange(true);

	TagGroup = DEFAULT_NODE_TAG;
	DisplayPrefix = NAME_None;

#if WITH_EDITORONLY_DATA
	
	FString Name = GetClass()->GetName();
	UCoreFunctionLibrary::Substring(Name, "Logic");
	UCoreFunctionLibrary::Substring(Name, "NodeInstance");
	
	NodeDescription.Name = FName(Name);
	NodeDescription.Category = FText::FromString("Undefined");
	
	bShowDisplayNameOnly = true;
	bDisplayNameWidget = true;

	SetUseCustomIcon(true);
	NodeIconSize = FVector2D(64.0f,64.0f);
	
	SetUseCustomColor(true);

	

	SetVariableHidden(GET_MEMBER_NAME_CHECKED(ULogicNodeInstance, TagGroup), true);
	SetVariableHidden(GET_MEMBER_NAME_CHECKED(ULogicNodeInstance, DisplayPrefix), true);
#endif
}

void ULogicNodeInstance::OnNotify_Implementation(ULogicNotification* Notification)
{
	Notification->Callback.Execute(this, Notification);
	//Evaluate();
	//if(ULogicBehaviorInstance* StateMachine = GetBehaviorInstance(); !StateMachine || !StateMachine->bUseDebugMode) return;
	/*FString ActorName = "";
	if (AActor* Actor = Cast<AActor>(GetContext())) ActorName = Actor->GetName() + " ";
	
	FString StateMachineName="";
	if(USMInstance* SMInstance = GetStateMachineInstance(true)) StateMachineName = SMInstance->GetName();

	FString SenderName="Unknown";
	if(Notification->Sender) SenderName = Notification->Sender->GetName();
	
	GLog->Logf(ELogVerbosity::Error,TEXT("%s(%s -> %s) received Nofification from %s"), *ActorName, *StateMachineName,  *Execute_GetTitle(this).ToString(), *SenderName);*/
}

void ULogicNodeInstance::OnStateInitialized_Implementation()
{
	Super::OnStateInitialized_Implementation();
	
	ULogicNodeSubsystem::Get(GetWorld())->Register(this);
	
	if(ULogicSubsystem* Subsystem = ULogicSubsystem::Get(GetWorld()))
	{
		Subsystem->OnNotifyEvent.AddDynamic(this, &ULogicNodeInstance::OnNotify_Delegate );
	}
	
	/*if(ULogicBehaviorInstance* BehaviorInstance = GetBehaviorInstance())
	{
		BehaviorInstance->OnNotifyDelegate.AddDynamic(this, &ULogicNodeInstance::OnNotify);
	}
	else if(ULogicSubsystem* Subsystem = ULogicSubsystem::GetLogicSubsystem(GetWorld()))
	{
		Subsystem->OnNotifyDelegate.AddDynamic(this, &ULogicNodeInstance::OnNotify );
	}*/
}

void ULogicNodeInstance::OnStateShutdown_Implementation()
{
	if(ULogicNodeSubsystem* Subsystem = ULogicNodeSubsystem::Get(GetWorld())) Subsystem->Remove(this);
	
	if(ULogicSubsystem* Subsystem = ULogicSubsystem::Get(GetWorld()))
	{
		Subsystem->OnNotifyEvent.RemoveDynamic(this, &ULogicNodeInstance::OnNotify_Delegate );
	}

	/*if(ULogicBehaviorInstance* StateMachine = GetBehaviorInstance())
	{
		StateMachine->OnNotifyDelegate.RemoveDynamic(this, &ULogicNodeInstance::OnNotify);
	}
	else if(ULogicSubsystem* Subsystem = ULogicSubsystem::GetLogicSubsystem(GetWorld()))
	{
		Subsystem->OnNotifyDelegate.RemoveDynamic(this, &ULogicNodeInstance::OnNotify );
	}*/
	
	Super::OnStateShutdown_Implementation();
}


void ULogicNodeInstance::OnRootStateMachineStart_Implementation()
{
	Super::OnRootStateMachineStart_Implementation();
	
	TArray<USMTransitionInstance*> Transitions;
	GetIncomingTransitions(Transitions,false);

	if(Transitions.Num() == 0)
	{
		SetActive(true, false, true);
	}
}

void ULogicNodeInstance::RunConstructionScript()
{
	//Super::RunConstructionScript();
	
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("SMNodeInstance::RunConstructionScript"), STAT_SMNodeInstance_RunConstructionScript, STATGROUP_LogicDriver);

#if WITH_EDITORONLY_DATA
	const USMNodeInstance* Archetype = CastChecked<USMNodeInstance>(GetArchetype());
	ExposedPropertyOverrides = Archetype->ExposedPropertyOverrides;
	NodeDescription = Archetype->GetNodeDescription();
#endif

	if(!UCoreFunctionLibrary::IsGameRunning())
	{
		FName Name = GetInternName();
		if(!Name.IsNone())
		{
			if(!DisplayPrefix.IsNone()) Name = FName(DisplayPrefix.ToString() + " " + Name.ToString());
			SetDisplayName(Name);
		}
	}
	
	ConstructionScript();
}

bool ULogicNodeInstance::CanExecuteTransition_Implementation()
{
	return true;
}

void ULogicNodeInstance::Evaluate()
{
	//GLog->Logf(ELogVerbosity::Error,TEXT("EVALUATE %s"), *Execute_GetTitle(this).ToString());
	
	if(!Execute_CanExecuteTransition(this)) return;

	TArray<USMTransitionInstance*> Transitions;
	GetOutgoingTransitions(Transitions,false);

	for(USMTransitionInstance* Transition : Transitions)
	{
		if(Transition->DoesTransitionPass())
		{
			SwitchToLinkedState(Transition->GetNextStateInstance(), false, true);
		}
	}
}



ULogicBehaviorInstance* ULogicNodeInstance::GetBehaviorInstance()
{
	ULogicBehaviorInstance* StateMachine = Cast<ULogicBehaviorInstance>(GetStateMachineInstance(false));

	//In case the Node is located inside a Non-Behavior State Machine Instance
	if(!StateMachine)
	{
		StateMachine = Cast<ULogicBehaviorInstance>(GetStateMachineInstance(true));
	}

	return StateMachine;
}

FName ULogicNodeInstance::GetInternName()
{
	return FName(NAME_None);
}

FName ULogicNodeInstance::GetTagName()
{
	FString Out_Tag_Name = TagGroup.GetTagName().ToString();
	FString InternName = GetInternName().ToString();
	
	Out_Tag_Name += (!Out_Tag_Name.IsEmpty() ? "." : "") + (!InternName.IsEmpty() ? InternName : FString("Undefined"));
	
	return FName(Out_Tag_Name);
}

void ULogicNodeInstance::OnNotify_Delegate(ULogicNotification* Notification)
{
	if(Tag.MatchesAnyExact(Notification->Tags)) OnNotify(Notification);
}



#if WITH_EDITORONLY_DATA
void ULogicNodeInstance::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
}


void ULogicNodeInstance::OnPreCompileValidate_Implementation(USMCompilerLog* CompilerLog) const
{
	const_cast<ULogicNodeInstance*>(this)->ModifyTag();
	Super::OnPreCompileValidate_Implementation(CompilerLog);
}

void ULogicNodeInstance::ModifyTag() {
	if(!GConfig) return;

	const FString ConfigPath = FConfigCacheIni::NormalizeConfigIniPath( FPaths::ProjectPluginsDir() / TEXT("LogicSense") / TEXT("Config") / TEXT("LogicGameplayTags.ini"));

	GConfig->LoadFile(ConfigPath);
	
	FGameplayTagTableRow GameplayTagTableRow;
	GameplayTagTableRow.Tag = GetTagName();
	GameplayTagTableRow.DevComment = GetGuid().ToString();
	
	TArray<FString> TagArray;
	GConfig->GetArray(TEXT("/Script/GameplayTags.GameplayTagsList"), TEXT("GameplayTagList"), TagArray, ConfigPath);

	int index = -1;
	for(int i = 0; i < TagArray.Num(); ++i)
	{
		
		//DESERIALIZE (Import)
		FGameplayTagTableRow DeserializedRow;
		FGameplayTagTableRow::StaticStruct()->ImportText(*TagArray[i], &DeserializedRow, nullptr, 0, nullptr, "", true);
		
		//if entry found
		if(DeserializedRow.DevComment == GameplayTagTableRow.DevComment)
		{
			index = i;
			break;
		}
	}

	FString SerializedStruct;
	FGameplayTagTableRow::StaticStruct()->ExportText(SerializedStruct, &GameplayTagTableRow, nullptr, nullptr, 0, nullptr);
	
	if(index >= 0)
	{
		TagArray[index] = SerializedStruct;
	}
	else
	{
		TagArray.Add(SerializedStruct);
	}

	GConfig->SetArray(TEXT("/Script/GameplayTags.GameplayTagsList"), TEXT("GameplayTagList"), TagArray, ConfigPath);
	GConfig->Flush(false, ConfigPath);

	UGameplayTagsManager::Get().EditorRefreshGameplayTagTree();
	
	Tag = FGameplayTag::RequestGameplayTag(GameplayTagTableRow.Tag, true);
	GetOwningEditorGraphNode().GetInterface()->GetEditorGraphProperty(GET_MEMBER_NAME_CHECKED(ULogicNodeInstance, Tag),this).GetInterface()->RefreshPropertyPinFromValue();
}

#endif
