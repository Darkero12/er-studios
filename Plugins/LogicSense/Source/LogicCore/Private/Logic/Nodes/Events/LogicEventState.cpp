﻿#include "Logic/Nodes/Events/LogicEventState.h"

ULogicEventState::ULogicEventState()
{
#if WITH_EDITORONLY_DATA
	
	SetUseCustomColor(true);
	SetNodeColor(FLinearColor(0.0f,1.0f,0.0f, 1.0f));

#endif
}

void ULogicEventState::OnStateInitialized_Implementation()
{
	Super::OnStateInitialized_Implementation();

	if (this->GetClass()->ImplementsInterface(ULogicDialogueInterface::StaticClass()))
	{
		//GLog->Logf(ELogVerbosity::Error, TEXT("## Interface implemented"));
	}
}

void ULogicEventState::Refresh2_Implementation(ULogicDialogueState* Dialogue)
{
	
}

bool ULogicEventState::CanExecuteTransition_Implementation()
{
	return false;
}
