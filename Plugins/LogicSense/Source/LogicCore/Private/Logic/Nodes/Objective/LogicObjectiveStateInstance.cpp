﻿
#include "Logic/Nodes/Objective/LogicObjectiveStateInstance.h"

#include "UObject/ConstructorHelpers.h"

ULogicObjectiveStateInstance::ULogicObjectiveStateInstance()
{
	SetDefaultToParallel(true);
	SetStayActiveOnStateChange(true);

#if WITH_EDITORONLY_DATA
	NodeDescription.Name = FName("DEPRECIATE Objective Node (State Machine)");
	NodeDescription.Category = FText::FromString("Quests & Achievements");
	
	bShowDisplayNameOnly = true;
	bDisplayNameWidget = true;
	bDisplayCustomIcon = true;
	
	SetUseCustomColor(true);
	SetNodeColor(FLinearColor(0.7f,0.643f,0.247f, 0.701f));
	
	ConstructorHelpers::FObjectFinder<UTexture2D> IconAsset(TEXT("/LogicSense/Assets/Icons/ui_icn_Shadow-Stomp.ui_icn_Shadow-Stomp"));
	NodeIcon = IconAsset.Object;
	NodeIconSize = FVector2D(64.0f,64.0f);
#endif
}

void ULogicObjectiveStateInstance::ConstructionScript_Implementation()
{
	Super::ConstructionScript_Implementation();

	ESMExecutionEnvironment Environment;
	WithExecutionEnvironment(Environment);
	if(!(Environment == ESMExecutionEnvironment::EditorExecution)) return;
	
	if(!GetTitle().IsEmpty())
	{
		FName TitleText = FName(GetTitle().ToString());
		SetDisplayName(TitleText);
	}
}

FText ULogicObjectiveStateInstance::GetTitle()
{
	return Title.Result;
}

FText ULogicObjectiveStateInstance::GetDescription()
{
	return Description.Result;
}

bool ULogicObjectiveStateInstance::CanExecuteTransition_Implementation()
{
	return false;
}
