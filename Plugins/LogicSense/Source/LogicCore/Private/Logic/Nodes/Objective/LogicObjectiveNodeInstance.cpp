﻿#include "Logic/Nodes/Objective/LogicObjectiveNodeInstance.h"

#include "Logic/LogicBehaviorInstance.h"
#include "UObject/ConstructorHelpers.h"

UE_DEFINE_GAMEPLAY_TAG(DEFAULT_OBJECTIVE_NODE_TAG, "Logic.Objective");

ULogicObjectiveNodeInstance::ULogicObjectiveNodeInstance() : Super()
{
	TagGroup = DEFAULT_OBJECTIVE_NODE_TAG;
	
#if WITH_EDITORONLY_DATA

	NodeDescription.Category = FText::FromString("Achievements & Quests");
	//if(this->GetClass() == StaticClass()) NodeDescription.Name = FName( "Achievement Node");

	SetNodeColor(FLinearColor(0.7f,0.64f,0.25f, 0.7f));
	
	ConstructorHelpers::FObjectFinder<UTexture2D> IconAsset(TEXT("/LogicSense/Assets/Icons/ui_icn_merit.ui_icn_merit"));
	NodeIcon = IconAsset.Object;
#endif
	
}

void ULogicObjectiveNodeInstance::ConstructionScript_Implementation()
{	
	Super::ConstructionScript_Implementation();
}

bool ULogicObjectiveNodeInstance::CanExecuteTransition_Implementation()
{
	return bHasCompleted;
}

FName ULogicObjectiveNodeInstance::GetInternName()
{
	return FName(GetTitle().ToString());
}

FText ULogicObjectiveNodeInstance::GetTitle()
{
	return Title.Result;
}

FText ULogicObjectiveNodeInstance::GetDescription()
{
	return Description.Result;
}

void ULogicObjectiveNodeInstance::SetComplete(bool bValue)
{
	bHasCompleted = bValue;
	Evaluate();
}
