#include "Logic/Nodes/LogicNodeSubsystem.h"
#include "Logic/Nodes/Dialogues/LogicDialogueState.h"
#include "Kismet/GameplayStatics.h"

UE_DEFINE_GAMEPLAY_TAG(DEFAULT_NODE_TAG, "Logic.Node");

void ULogicNodeSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
}

void ULogicNodeSubsystem::Deinitialize()
{
	Super::Deinitialize();
}

USMNodeInstance* ULogicNodeSubsystem::GetInstanceByTag(UObject* WorldContextObject, FGameplayTag Tag)
{
	if(!WorldContextObject) return nullptr;
	ULogicNodeSubsystem* Subsystem = ULogicNodeSubsystem::Get(WorldContextObject);
	
	if(!Subsystem) return nullptr;
	TWeakObjectPtr<USMNodeInstance>* InstanceRef = Subsystem->ReferencedNodes.Find(Tag);
	if(!InstanceRef || !InstanceRef->Get()) return nullptr;

	return InstanceRef->Get();
}

void ULogicNodeSubsystem::Trigger(UObject* WorldContextObject, FGameplayTag Tag)
{
	if(!WorldContextObject) return;
	ULogicNodeSubsystem* Subsystem = ULogicNodeSubsystem::Get(WorldContextObject);
	
	if(!Subsystem) return;
	TWeakObjectPtr<USMNodeInstance>* InstanceRef = Subsystem->ReferencedNodes.Find(Tag);
	if(!InstanceRef || !InstanceRef->Get()) return;

	if(auto DialogueState = Cast<ULogicDialogueState>(InstanceRef->Get()))
	{
		DialogueState->Trigger();
	}
}

void ULogicNodeSubsystem::Register(USMNodeInstance* Instance)
{
	if(auto DialogueState = Cast<ULogicDialogueState>(Instance))
	{
		FGameplayTag Tag = DialogueState->GetGameplayTag();
		if(Tag != FGameplayTag::EmptyTag)
		{
			ReferencedNodes.Add(DialogueState->GetGameplayTag(), DialogueState);
		}
	}
	
	const FGuid GUID = Instance->GetGuid();
	
}

void ULogicNodeSubsystem::Register(USMStateInstance* StateInstance)
{
	const FGuid GUID = StateInstance->GetGuid();
	ReferencedNodesOld.Add(GUID, StateInstance);
}

void ULogicNodeSubsystem::Remove(USMStateInstance* StateInstance)
{
	const FGuid GUID = StateInstance->GetGuid();
	ReferencedNodesOld.Remove(GUID);
}

ULogicNodeSubsystem* ULogicNodeSubsystem::Get(const UObject* WorldContextObject)
{
	if(!WorldContextObject) return nullptr;
	return UGameplayStatics::GetGameInstance( WorldContextObject->GetWorld())->GetSubsystem<ULogicNodeSubsystem>();
}

void ULogicNodeSubsystem::GetNodes(UObject* WorldContextObject, TSubclassOf<USMStateInstance> NodeClass,   TArray<USMStateInstance*>& OutNodes)
{
	ULogicNodeSubsystem* Subsystem = ULogicNodeSubsystem::Get(WorldContextObject);
	if(!NodeClass || !Subsystem) return;
	
	for (auto Instance :Subsystem->ReferencedNodesOld)
	{
		USMStateInstance* InstanceValue = Instance.Value.Get();
		if(InstanceValue->IsA(NodeClass)) OutNodes.Add(InstanceValue);
	}
}
	
/*
	UClass* ReturnType = USMStateInstance::StaticClass();
	
	Subsystem->FindNodes< USMStateInstance>();
	
	//OutNodes = 
	
	//Subsystem->FindNodes<ReturnType>();*/


/*TArray<USMStateInstance*> ULogicNodeSubsystem::GetReferencedNodes()
{
	return FindNodes<USMStateInstance>();
}*/

/*template <typename T>
TArray<T*> ULogicNodeSubsystem::FindNodes()
{
	static_assert(TPointerIsConvertibleFromTo<T, const USMStateInstance>::Value, "'T' template parameter to FindNodes must be derived from USMStateInstance");
	
	TArray<T*> Instances;

	for (auto Instance :ReferencedNodes)
	{
		if(!Instance.Value.IsValid()) continue;
		
		if(T* CastedInstance = Cast<T>(Instance.Value)) Instances.Add(CastedInstance);
	}
	return Instances;
}*/


/*template  < class  TClassA,  class  =  typename  TEnableIf < TPointerIsConvertibleFromTo < TClassA, TClass > :: Value > :: Type >
FORCEINLINE TSubclassOf &  operator = ( const  TSubclassOf < TClassA > &  From )
{
	Class  =  * From ;
	return  * this ;
}*/


