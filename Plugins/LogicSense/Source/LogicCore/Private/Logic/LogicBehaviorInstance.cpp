﻿#include "Logic/LogicBehaviorInstance.h"

#include "GameFramework/PlayerController.h"
#include "Logic/Nodes/Objective/LogicObjectiveNodeInstance.h"
#include "Logic/Notification/LogicNotification.h"


ULogicBehaviorInstance::ULogicBehaviorInstance() : Super()
{
	bCanEverTick = false;
	bTickRegistered = false;
}

void ULogicBehaviorInstance::OnNotify_Implementation(ULogicNotification* Notification)
{
	if(bUseDebugMode) GLog->Logf(ELogVerbosity::Error,TEXT("%s received Nofification from %s"), *GetName(), *Notification->Sender->GetName());
}

APlayerController* ULogicBehaviorInstance::GetPlayerController()
{
	if(APawn* Pawn = Cast<APawn>(GetContext()))
	{
		if(APlayerController* PlayerController = Cast<APlayerController>(Pawn->GetController())) return PlayerController;
	}
	
	return nullptr;
}

void ULogicBehaviorInstance::OnNotify_Delegate(ULogicNotification* Notification)
{
	OnNotifyEvent.Broadcast(Notification);
}

void ULogicBehaviorInstance::Start()
{
	
    for(auto StateMachine : GetStateMachinesWithReferences())
    {
	    for(auto Node: StateMachine->GetInstanceReference()->GetRootStateMachine().GetAllNodes())
    	{
    		auto Instance = Cast<USMStateInstance_Base>(Node->GetOrCreateNodeInstance()); //Node->GetNodeInstance()
    		if(Instance) Instance->OnRootStateMachineStart();
    	}
    }
	
	/*if(ULogicSubsystem* Subsystem = ULogicSubsystem::GetLogicSubsystem(GetWorld()))
	{
		Subsystem->OnNotifyEvent.AddDynamic(this, &ULogicBehaviorInstance::OnNotify_Internal );
	}

	OnNotifyDelegate.AddDynamic(this, &ULogicBehaviorInstance::OnNotify);*/
	
	Super::Start();
}

void ULogicBehaviorInstance::Shutdown()
{
	/*if(ULogicSubsystem* Subsystem = ULogicSubsystem::GetLogicSubsystem(GetWorld()))
	{
		Subsystem->OnNotifyEvent.RemoveDynamic(this, &ULogicBehaviorInstance::OnNotify );
	}

	OnNotifyDelegate.RemoveDynamic(this, &ULogicBehaviorInstance::OnNotify );*/
	
	Super::Shutdown();
}

/*TArray<ULogicObjectiveNodeInstance*> ULogicBehaviorInstance::GetObjectiveNodes()
{
	TArray<ULogicObjectiveNodeInstance*> OutNodes;
	
	TArray<USMStateInstance_Base*> StateInstances;
	GetAllStateInstances(StateInstances);
	
	for (USMStateInstance_Base* StateInstance : StateInstances)
	{
		ULogicObjectiveNodeInstance* Node = Cast<ULogicObjectiveNodeInstance>(StateInstance);
		if(Node) OutNodes.Add(Node);
	}

	return OutNodes;
}*/