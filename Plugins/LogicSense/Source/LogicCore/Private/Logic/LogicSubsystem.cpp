// Fill out your copyright notice in the Description page of Project Settings.

#include "Logic/LogicSubsystem.h"
#include "Logic/LogicBehaviorInstance.h"
#include "LogicSenseSettings.h"

#include "Kismet/GameplayStatics.h"
#include "Blueprints/SMBlueprint.h"


void ULogicSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	ReferencedActors.Empty();
	
	const ULogicSenseSettings* Settings = GetDefault<ULogicSenseSettings>();
	

	auto FIX = Settings->FIX.LoadSynchronous();
	
	if(Settings->FIX.IsValid())
	{
		if (ULogicBehaviorInstance* Instance = NewObject<ULogicBehaviorInstance>(this, FIX, NAME_None, RF_NoFlags, nullptr))
		{
			
			GlobalAchievementInstance = Instance;
			GlobalAchievementInstance->AddToRoot();
			
			GlobalAchievementInstance->Initialize(this);
			GlobalAchievementInstance->Start();
		}
		
	}
	
	/*if(const USMBlueprint* BlueprintInstance = Settings->Test2.LoadSynchronous())
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("#2 HIHIHIH")));
		
		if (ULogicBehaviorInstance* Instance = NewObject<ULogicBehaviorInstance>(this, BlueprintInstance->GeneratedClass, NAME_None, RF_NoFlags, nullptr))
		{

			if(GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("#3")));
			
			GlobalAchievementInstance = Instance;
			GlobalAchievementInstance->AddToRoot();
			
			GlobalAchievementInstance->Initialize(this);
			GlobalAchievementInstance->Start();
		}
	}*/
}

void ULogicSubsystem::Deinitialize()
{
	if (GlobalAchievementInstance)
	{
		GlobalAchievementInstance->Shutdown();
		GlobalAchievementInstance->RemoveFromRoot();
	}

	Super::Deinitialize();
}

TArray<AActor*> ULogicSubsystem::GetReferencedActors()
{
	TArray<AActor*> Actors;

	for (auto Actor :ReferencedActors)
	{
		if(!Actor.IsValid() || Actor->IsPendingKillPending() || Actor->IsActorBeingDestroyed()) continue;
		Actors.Add(Actor.Get());
	}

	for (auto Component : ReferencedActorComponents)
	{
		if(!Component.IsValid()) continue;

		AActor* Actor = Cast<AActor>(Component->GetOwner());
		if(!Actor || Actor->IsPendingKillPending() || Actor->IsActorBeingDestroyed()) continue;
		
		Actors.Add(Actor);
	}

	return Actors;
}

ULogicSubsystem* ULogicSubsystem::Get(UObject* WorldContextObject)
{
	if(!WorldContextObject) return nullptr;
	return UGameplayStatics::GetGameInstance( WorldContextObject->GetWorld())->GetSubsystem<ULogicSubsystem>();
}




