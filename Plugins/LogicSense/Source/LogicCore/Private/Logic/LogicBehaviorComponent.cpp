﻿#include "Logic/LogicBehaviorComponent.h"

#include "Logic/LogicBehaviorInstance.h"
#include "Logic/LogicSubsystem.h"

ULogicBehaviorComponent::ULogicBehaviorComponent()
{
    bStartOnBeginPlay = true;
}

void ULogicBehaviorComponent::BeginPlay()
{
	LogicSubsystem = ULogicSubsystem::Get(this);
	if(LogicSubsystem) LogicSubsystem->Register(this);

	StateMachineClass = BehaviorClass;
	
	Super::BeginPlay();
}

void ULogicBehaviorComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(LogicSubsystem) LogicSubsystem->Remove(this);
	
	Super::EndPlay(EndPlayReason);
}

void ULogicBehaviorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
