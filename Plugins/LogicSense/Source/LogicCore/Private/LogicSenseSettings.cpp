﻿#include "LogicSenseSettings.h"

#include "Blueprints/SMBlueprint.h"
#include "Interfaces/IPluginManager.h"
#include "Misc/App.h"
#include "UObject/ConstructorHelpers.h"

ULogicSenseSettings::ULogicSenseSettings()
{
	InstalledVersion = ULogicSenseSettings::GetPluginVersion();

	if(!bInitializedSettings)
	{
		BlueprintAchievement = FSoftObjectPath(TEXT("/LogicSense/Logic/Story/SMB_NoticeBoard.SMB_NoticeBoard"));

		bInitializedSettings = true;
	}

	
	//auto BlueprintAsset = ConstructorHelpers::FObjectFinder<FSoftObjectPath> (TEXT("/LogicSense/Logic/Story/SMB_NoticeBoard"));
	//if(BlueprintAsset.Succeeded()) BlueprintAchievement = BlueprintAsset.Object;
	
}

FLogicSensorSettings ULogicSenseSettings::GetSensorSettings() const
{
	return SensorSettings;
}

FName ULogicSenseSettings::GetCategoryName() const
{
	return FApp::GetProjectName();
}

FString ULogicSenseSettings::GetPluginVersion() const
{
	const TSharedPtr<IPlugin> Plugin = IPluginManager::Get().FindPlugin(TEXT("LogicSense"));
	return Plugin.IsValid() ? Plugin->GetDescriptor().VersionName : "0.0.0";
}

#if WITH_EDITOR
ULogicSenseSettings::FOnUpdateSettings ULogicSenseSettings::OnUpdateSettings;

void ULogicSenseSettings::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	OnUpdateSettings.Broadcast(this);
}

#endif // WITH_EDITOR

/*#include "Misc/App.h"
#include "Widgets/Notifications/SNotificationList.h"
#include "Framework/Notifications/NotificationManager.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"
#include "EngineUtils.h"

#define LOCTEXT_NAMESPACE "LyraCheats"

#define LOCTEXT_NAMESPACE "LyraCheats"

ULogicSenseSettings::ULogicSenseSettings()
{
}



#if WITH_EDITOR

void ULogicSenseSettings::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	ApplySettings();
}

void ULogicSenseSettings::PostReloadConfig(FProperty* PropertyThatWasLoaded)
{
	Super::PostReloadConfig(PropertyThatWasLoaded);

	ApplySettings();
}

void ULogicSenseSettings::PostInitProperties()
{
	Super::PostInitProperties();

	ApplySettings();
}

void ULogicSenseSettings::ApplySettings()
{
	if (GIsEditor && (GEngine != nullptr))
	{
		ReapplyLoadoutIfInPIE();
	}
}

void ULogicSenseSettings::ReapplyLoadoutIfInPIE()
{

}

void ULogicSenseSettings::OnPlayInEditorStarted() const
{
	// Show a notification toast to remind the user that there's an experience override set
	if (CheatCosmeticCharacterParts.Num() > 0)
	{
		FNotificationInfo Info(LOCTEXT("CosmeticOverrideActive", "Applying Cosmetic Override"));
		Info.ExpireDuration = 2.0f;
		FSlateNotificationManager::Get().AddNotification(Info);
	}
}

#endif // WITH_EDITOR

#undef LOCTEXT_NAMESPACE*/

