﻿#include "Library/CoreFunctionLibrary.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputAction.h"
#include "LogicGameplayTags.h"
#include "Engine/LocalPlayer.h"
#include "GameFramework/PlayerController.h"


FLogicGameplayTags FLogicGameplayTags::GameplayTags;

bool UCoreFunctionLibrary::IsGameRunning() {
#if WITH_EDITOR
	return (GEditor && GEditor->IsPlaySessionInProgress());
#endif
	return true;
}

void UCoreFunctionLibrary::Substring(FString &Text, FString Search)
{
	int index = Text.Find(Search);
	if(index != INDEX_NONE) Text.RemoveAt(index, Search.Len());
	
}

void UCoreFunctionLibrary::TestInput(APlayerController* PlayerController)
{
	//UEnhancedInputComponent* Test;
	//Test->BindAction()
	
	//Cast<APlayerController>(GetController())->GetLocalPlayer())
	
	FInputActionInstance* InputActionInstance = nullptr;
	UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	//Subsystem->Bind
	
	if (Subsystem)
	{
		const UEnhancedPlayerInput* EnhancedPlayerInput = Subsystem->GetPlayerInput();
		//EnhancedPlayerInput->
		//InputActionInstance = EnhancedPlayerInput->FindActionInstanceData(InputAction);
	}
	/*
	if (InputActionInstance)
	{
		auto GetTriggerEventName = [&](ETriggerEvent Event)->FString
		{
			FString TriggerEventName;
			switch (Event)
			{
			case ETriggerEvent::Triggered:
				TriggerEventName = TEXT("Triggered");
				break;
			case ETriggerEvent::Started:
				TriggerEventName = TEXT("Started");
				break;
			case ETriggerEvent::Ongoing:
				TriggerEventName = TEXT("Ongoing");
				break;
			case ETriggerEvent::Canceled:
				TriggerEventName = TEXT("Canceled");
				break;
			case ETriggerEvent::Completed:
				TriggerEventName = TEXT("Completed");
				break;
			default:
				TriggerEventName = TEXT("None");
				break;
			}
			return TriggerEventName;
		};

		FString ListeningTriggerEventName = GetTriggerEventName(ListeningEvent);
		FString CurTriggerEventName = GetTriggerEventName(InputActionInstance->GetTriggerEvent());
		
		UE_LOG(LogTemp, Warning, TEXT("InputHandle Process! ListeningTriggerEvent: %s, CurTriggerEvent: %s, IA Name: %s, InputValue: %s"),
			*ListeningTriggerEventName,
			*CurTriggerEventName,
			*InputAction->GetName(),
			*InputActionInstance->GetValue().ToString()
			)
	}*/
}



// Function for iterating through all properties of a struct
void UCoreFunctionLibrary::IterateThroughStructProperty(FProperty* Property, void* StructPtr)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Property->GetAuthoredName());
	FStructProperty* StructProperty = CastField<FStructProperty>(Property);
	//check for null
	if (StructProperty)
	{
		// Walk the structs' properties
		for (TFieldIterator<FProperty> PropertyIt(StructProperty->Struct); PropertyIt; ++PropertyIt)
		{
			// This is the variable name if you need it
			UE_LOG(LogTemp, Warning, TEXT("%s"), *PropertyIt->GetAuthoredName());
			// Never assume ArrayDim is always 1
			for (int32 ArrayIndex = 0; ArrayIndex < PropertyIt->ArrayDim; ArrayIndex++)
			{
				// This grabs the pointer to where the property value is stored
				void* ValuePtr = PropertyIt->ContainerPtrToValuePtr<void>(StructPtr, ArrayIndex);

				// Parse this property
				ParseProperty(*PropertyIt, ValuePtr);
			}
		}
	}
}

// Example function for parsing a single property
void UCoreFunctionLibrary::ParseProperty(FProperty* Property, void* ValuePtr)
{
	float FloatValue;
	int32 IntValue;
	bool BoolValue;
	FString StringValue;
	FName NameValue;
	FText TextValue;

	// Here's how to read integer and float properties
	if (FNumericProperty* NumericProperty = CastField<FNumericProperty>(Property))
	{
		if (NumericProperty->IsFloatingPoint())
		{
			FloatValue = NumericProperty->GetFloatingPointPropertyValue(ValuePtr);
			UE_LOG(LogTemp, Warning, TEXT("integer%d]"), FloatValue);
		}
		else if (NumericProperty->IsInteger())
		{
			IntValue = NumericProperty->GetSignedIntPropertyValue(ValuePtr);
			UE_LOG(LogTemp, Warning, TEXT("integer:'%i'"), IntValue);
		}
	}
	// How to read booleans
	else if (FBoolProperty* BoolProperty = CastField<FBoolProperty>(Property))
	{
		BoolValue = BoolProperty->GetPropertyValue(ValuePtr);
		if (BoolValue)
		{
			UE_LOG(LogTemp, Warning, TEXT("Bool: True"));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Bool: False"));
		}
	}

	// Reading names
	else if (FNameProperty* NameProperty = CastField<FNameProperty>(Property))
	{
		NameValue = NameProperty->GetPropertyValue(ValuePtr);
		UE_LOG(LogTemp, Warning, TEXT("Name:'%s'"), *NameValue.ToString());
	}

	// Reading strings
	else if (FStrProperty* StringProperty = CastField<FStrProperty>(Property))
	{
		StringValue = StringProperty->GetPropertyValue(ValuePtr);
		UE_LOG(LogTemp, Warning, TEXT("String:'%s'"), *StringValue);
	}

	// Reading texts
	else if (FTextProperty* TextProperty = CastField<FTextProperty>(Property))
	{
		TextValue = TextProperty->GetPropertyValue(ValuePtr);
		UE_LOG(LogTemp, Warning, TEXT("Text:'%s'"), *TextValue.ToString());
	}

	// Reading an array
	else if (FArrayProperty* ArrayProperty = CastField<FArrayProperty>(Property))
	{
		// We need the helper to get to the items of the array       
		FScriptArrayHelper Helper(ArrayProperty, ValuePtr);
		for (int32 i = 0, n = Helper.Num(); i < n; ++i)
		{
			UE_LOG(LogTemp, Warning, TEXT("Array:%i"), i);
			ParseProperty(ArrayProperty->Inner, Helper.GetRawPtr(i));
		}
	}

	// Reading a nested struct
	else if (Property)
	{
		IterateThroughStructProperty(Property, ValuePtr);
	}
}