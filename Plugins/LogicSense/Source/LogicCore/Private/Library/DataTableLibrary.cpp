﻿#include "Library/DataTableLibrary.h"

#if WITH_EDITOR
#include "DataTableEditorUtils.h"
#endif

#define LOCTEXT_NAMESPACE "DataTableLibrary"

bool UDataTableLibrary::AddDataTableRow2(UDataTable* Table, FName RowName, const FTableRowBase& RowData)
{
	// We should never hit this! Stubbed to avoid NoExport on the class.
	check(0);
	return false;
}

bool UDataTableLibrary::Generic_AddDataTableRow2(UDataTable* Table, FName RowName, const uint8* RowData)
{
	//GLog->Logf(ELogVerbosity::Error, TEXT("## Generic Add Data Table :%s"), *RowName.ToString() );
	return AddNewRow(Table, RowName, RowData);

}

bool UDataTableLibrary::AddNewRow(UDataTable* Table, FName RowName, const uint8* NewRowData)
{
#if WITH_EDITOR
	if (NewRowData && Table)
	{
		if ((RowName == NAME_None) || Table->GetRowMap().Contains(RowName) || !Table->GetRowStruct())
		{
			return false;
		}

		const FScopedTransaction Transaction(LOCTEXT("AddNewDataTableRow", "Add New Data Table Row"));
		Table->Modify();
		uint8* NewRow = FDataTableEditorUtils::AddRow(Table, RowName);
		check(NewRow);
		FDataTableEditorUtils::BroadcastPreChange(Table, FDataTableEditorUtils::EDataTableChangeInfo::RowData);
		Table->RowStruct->CopyScriptStruct(NewRow, NewRowData);
		FDataTableEditorUtils::BroadcastPostChange(Table, FDataTableEditorUtils::EDataTableChangeInfo::RowData);
		return true;
	}
#endif
	
	return false;
}


FName UDataTableLibrary::CreateUniqueRowNameFromDataTable(FName CandidateName, UDataTable* Table)
{
	if (!Table)
	{
		return CandidateName;
	}

	TArray<FName> RowNames;
	Table->GetRowMap().GenerateKeyArray(RowNames);
	return GetUniqueRowNameFromList(CandidateName, RowNames);
}

FName UDataTableLibrary::GetUniqueRowNameFromList(FName CandidateName, const TArray<FName>& ExistingNames)
{
	if (!ExistingNames.Contains(CandidateName))
	{
		return CandidateName;
	}

	FString CandidateNameString = CandidateName.ToString();
	FString BaseNameString = CandidateNameString;
	if (CandidateNameString.Len() >= 3 && CandidateNameString.Right(3).IsNumeric())
	{
		BaseNameString = CandidateNameString.Left(CandidateNameString.Len() - 3);
	}

	FName UniqueName = FName(*BaseNameString);
	int32 NameIndex = 1;
	while (ExistingNames.Contains(UniqueName))
	{
		UniqueName = FName(*FString::Printf(TEXT("%s%i"), *BaseNameString, NameIndex));
		NameIndex++;
	}

	return UniqueName;
}



DEFINE_FUNCTION(UDataTableLibrary::execAddDataTableRow2)
{
	P_GET_OBJECT(UDataTable, Table);
	P_GET_PROPERTY(FNameProperty, RowName);

	Stack.StepCompiledIn<FStructProperty>(NULL);
	void* RowDataPtr = Stack.MostRecentPropertyAddress;

	P_FINISH;
	bool bSuccess = false;

	FStructProperty* StructProp = CastField<FStructProperty>(Stack.MostRecentProperty);
	if (!Table)
	{
		FBlueprintExceptionInfo ExceptionInfo(
			EBlueprintExceptionType::AccessViolation,
			LOCTEXT("AddDataTableRow_MissingTableInput", "Failed to resolve the table input. Be sure the DataTable is valid.")
		);
		FBlueprintCoreDelegates::ThrowScriptException(P_THIS, Stack, ExceptionInfo);
	}
	else if (StructProp && RowDataPtr)
	{
		UScriptStruct* OutputType = StructProp->Struct;
		const UScriptStruct* TableType = Table->GetRowStruct();

		const bool bCompatible = (OutputType == TableType) ||
			(OutputType->IsChildOf(TableType) && FStructUtils::TheSameLayout(OutputType, TableType));
		if (bCompatible)
		{
			P_NATIVE_BEGIN;
			uint8* RowData = reinterpret_cast<uint8*>(RowDataPtr);
			bSuccess = Generic_AddDataTableRow2(Table, RowName, RowData);
			P_NATIVE_END;
		}
		else
		{
			FBlueprintExceptionInfo ExceptionInfo(
				EBlueprintExceptionType::AccessViolation,
				LOCTEXT("AddDataTableRow_IncompatibleProperty", "Incompatible output parameter; the data table's type is not the same as the return type.")
			);
			FBlueprintCoreDelegates::ThrowScriptException(P_THIS, Stack, ExceptionInfo);
		}
	}
	else
	{
		FBlueprintExceptionInfo ExceptionInfo(
			EBlueprintExceptionType::AccessViolation,
			LOCTEXT("AddDataTableRow_MissingOutputProperty", "Failed to resolve the output parameter for AddDataTableRow.")
		);
		FBlueprintCoreDelegates::ThrowScriptException(P_THIS, Stack, ExceptionInfo);
	}
	*(bool*)RESULT_PARAM = bSuccess;
}

#undef  LOCTEXT_NAMESPACE