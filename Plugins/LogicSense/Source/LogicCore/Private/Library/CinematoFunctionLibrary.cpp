﻿#include "Library/CinematoFunctionLibrary.h"

#include "CinematicCamera/Public/CineCameraComponent.h"
#include "Components/SceneCaptureComponent2D.h"

#include "Components/LineBatchComponent.h"

void UCinematoFunctionLibrary::DrawCameraFrustum_CameraComponent_Blueprint(UCameraComponent* SrcCamera)
{
	DrawCameraFrustum(SrcCamera);
}

void UCinematoFunctionLibrary::DrawCameraFrustum_CameraSettings_Blueprint(const UObject* WorldContextObject, FTransform Transform, FCameraSettings Setting)
{
	DrawCameraFrustum(WorldContextObject, Transform, Setting);
}

void UCinematoFunctionLibrary::DrawCameraFrustum(UCameraComponent* SrcCamera)
{
	if(!SrcCamera) return;
	
	DrawCameraFrustum(SrcCamera, SrcCamera->GetComponentTransform(), GetCameraSetting(SrcCamera));
}


void UCinematoFunctionLibrary::DrawCameraFrustum(const UObject* WorldContextObject, FTransform Transform, FCameraSettings Settings)
{
	if(!WorldContextObject)
	{
		GLog->Logf(TEXT("NO WORLD CONTEXT"));
		return;
	}
	
	UWorld* World = WorldContextObject->GetWorld();
	TArray<FVector> Points = CalculateFrustumCorners(Transform, Settings);

	if(!World)
	{
		//GLog->Logf(TEXT("NO WORLD CONTEXT %s", &));

		//GLog->Logf(TEXT("Test: %s"), *WorldContextObject->GetName());
		
		return;
	}
	
	if (World->GetNetMode() == NM_DedicatedServer) return; // no debug line drawing on dedicated server
	
	FColor FrustumColor = FColor::Magenta;
	float Thickness = 1.0f;
	int DepthPriority = SDPG_World;
	
	World->LineBatcher->DrawLine(Points[0], Points[1], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[1], Points[2], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[2], Points[3], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[3], Points[0], FrustumColor, DepthPriority, Thickness);

	World->LineBatcher->DrawLine(Points[4], Points[5], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[5], Points[6], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[6], Points[7], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[7], Points[4], FrustumColor, DepthPriority, Thickness);

	World->LineBatcher->DrawLine(Points[0], Points[4], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[1], Points[5], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[2], Points[6], FrustumColor, DepthPriority, Thickness);
	World->LineBatcher->DrawLine(Points[3], Points[7], FrustumColor, DepthPriority, Thickness);
}


FCameraSettings UCinematoFunctionLibrary::GetCameraSetting(UCameraComponent* SrcCamera)
{
	FCameraSettings Settings;
	
	UCineCameraComponent* SrcCineCamera = Cast<UCineCameraComponent>(SrcCamera);
	if(SrcCineCamera)
	{
		Settings.FieldOfView = SrcCineCamera->GetHorizontalFieldOfView();//->CurrentHorizontalFOV;
		Settings.AspectRatio = SrcCineCamera->AspectRatio;
	}
	else
	{
		if(SrcCamera->ProjectionMode == ECameraProjectionMode::Perspective)
		{
			Settings.FieldOfView = SrcCamera->FieldOfView;
		}
		else
		{
			Settings.FieldOfView = 0;
			Settings.OrthoWidth = SrcCamera->OrthoWidth;
		}
		Settings.AspectRatio =SrcCamera->AspectRatio;
	}

	return Settings;
}

TArray<FVector> UCinematoFunctionLibrary::CalculateFrustumCorners(FTransform Transform, FCameraSettings Settings)
{
	TArray<FVector> Vertices;
	//Points.Init(FVector::ZeroVector, 8);
	Vertices.SetNum(8, false);

	// FOVAngle controls the horizontal angle.
	const float HozHalfAngleInRadians = FMath::DegreesToRadians(Settings.FieldOfView * 0.5f);

	float HozLength = (Settings.FieldOfView > 0.0f) ? Settings.NearClipPlaneDistance * FMath::Tan(HozHalfAngleInRadians) : Settings.OrthoWidth * 0.5f;
	float VertLength = HozLength / Settings.AspectRatio;
	
	// near plane verts
	Vertices[0] = FVector::ForwardVector * Settings.NearClipPlaneDistance + FVector::UpVector * VertLength + FVector::RightVector * HozLength;
	Vertices[1] = FVector::ForwardVector * Settings.NearClipPlaneDistance + FVector::UpVector * VertLength - FVector::RightVector * HozLength;
	Vertices[2] = FVector::ForwardVector * Settings.NearClipPlaneDistance - FVector::UpVector * VertLength - FVector::RightVector * HozLength;
	Vertices[3] = FVector::ForwardVector * Settings.NearClipPlaneDistance - FVector::UpVector * VertLength + FVector::RightVector * HozLength;
	
	HozLength = (Settings.FieldOfView > 0.0f) ? Settings.FarClipPlaneDistance * FMath::Tan(HozHalfAngleInRadians) : HozLength;
	VertLength = (Settings.FieldOfView > 0.0f) ? HozLength / Settings.AspectRatio : VertLength;

	// far plane verts
	Vertices[4] = FVector::ForwardVector * Settings.FarClipPlaneDistance + FVector::UpVector * VertLength + FVector::RightVector * HozLength;
	Vertices[5] = FVector::ForwardVector * Settings.FarClipPlaneDistance + FVector::UpVector * VertLength - FVector::RightVector * HozLength;
	Vertices[6] = FVector::ForwardVector * Settings.FarClipPlaneDistance - FVector::UpVector * VertLength - FVector::RightVector * HozLength;
	Vertices[7] = FVector::ForwardVector * Settings.FarClipPlaneDistance - FVector::UpVector * VertLength + FVector::RightVector * HozLength;
	
	for (int i = 0; i < 8; ++i)
	{
		Vertices[i] = Transform.ToMatrixWithScale().TransformPosition(Vertices[i]);
	}

	return Vertices;
}



void UCinematoFunctionLibrary::CopyCameraSettingsToSceneCapture(UCameraComponent* Src, USceneCaptureComponent2D* Dst)
{
	if (!Src || !Dst) return;

	Dst->SetWorldLocationAndRotation(Src->GetComponentLocation(), Src->GetComponentRotation());
	Dst->FOVAngle = Src->FieldOfView;

	FMinimalViewInfo CameraViewInfo;
	Src->GetCameraView(/*DeltaTime =*/0.0f, CameraViewInfo);

	const FPostProcessSettings& SrcPPSettings = CameraViewInfo.PostProcessSettings;
	FPostProcessSettings& DstPPSettings = Dst->PostProcessSettings;

	FWeightedBlendables DstWeightedBlendables = DstPPSettings.WeightedBlendables;

	// Copy all of the post processing settings
	DstPPSettings = SrcPPSettings;

	// But restore the original blendables
	DstPPSettings.WeightedBlendables = DstWeightedBlendables;
}