﻿#include "Library/MathFunctionLibrary.h"

#include "Kismet/KismetSystemLibrary.h"

bool UMathFunctionLibrary::LineTraceAllByChannel(const UObject* WorldContextObject, TArray<struct FHitResult>& OutHits,  const FVector Start, const FVector End, ECollisionChannel TraceChannel, const TArray<AActor*>& ActorsToIgnore, bool bUseComplexTrace, bool bUseIgnoreSelf, bool bUseDebugMode)
{

	FCollisionQueryParams QueryParams;
	QueryParams.OwnerTag = "MultiLineTraceAllByChannel";
	QueryParams.AddIgnoredActors(ActorsToIgnore);
	QueryParams.bTraceComplex = bUseComplexTrace;
	QueryParams.bReturnPhysicalMaterial = false;

	if (bUseIgnoreSelf)
	{
		const AActor* IgnoreActor = Cast<AActor>(WorldContextObject);
		if (IgnoreActor)
		{
			QueryParams.AddIgnoredActor(IgnoreActor);
		}
		else
		{
			// find owner
			const UObject* CurrentObject = WorldContextObject;
			while (CurrentObject)
			{
				CurrentObject = CurrentObject->GetOuter();
				IgnoreActor = Cast<AActor>(CurrentObject);
				if (IgnoreActor)
				{
					QueryParams.AddIgnoredActor(IgnoreActor);
					break;
				}
			}
		}
	}

	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	bool const bHit = World ? World->LineTraceMultiByChannel(OutHits, Start, End, TraceChannel, QueryParams) : false;
	
	return bHit;
}

void UMathFunctionLibrary::CalculatePrimeNumbers(int UpperLimit)
{
	//Calculating the prime numbers...
	for (int i = 1; i <= UpperLimit; i++)
	{
		bool isPrime = true;
		for (int j = 2; j <= i / 2; j++)
		{
			if (i%j == 0)
			{
				isPrime = false;
				break;
			}
		}
 
		if (isPrime) 
			GLog->Log("Prime number #" + FString::FromInt(i) + ": " + FString::FromInt(i));
	}
}

