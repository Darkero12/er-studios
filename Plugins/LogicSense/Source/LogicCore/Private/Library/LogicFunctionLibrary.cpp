﻿#include "Library/LogicFunctionLibrary.h"

#include "Logic/LogicBehaviorInstance.h"
#include "Logic/LogicSubsystem.h"
#include "Logic/Nodes/Objective/LogicObjectiveNodeInstance.h"
#include "Logic/Notification/LogicNotification.h"


class ULogicNotification;




TArray<ULogicObjectiveNodeInstance*> ULogicFunctionLibrary::GetQuests(UObject* WorldContextObject, bool bOnlyActive)
{
	//ULogicBehaviorInstance* BehaviorInstance = ULogicSubsystem::GetLogicSubsystem(WorldContextObject)->GlobalAchievementInstance;
	//return GetObjectiveNodes(BehaviorInstance, bOnlyActive);

	TArray<ULogicObjectiveNodeInstance*> Test;
	return Test;
}

TArray<ULogicObjectiveNodeInstance*> ULogicFunctionLibrary::GetObjectiveNodes(ULogicBehaviorInstance* BehaviorInstance, bool bOnlyActive)
{
	TArray<ULogicObjectiveNodeInstance*> OutNodes;

	if(!BehaviorInstance) return OutNodes;

	TArray<USMStateInstance_Base*> StateInstances;
	BehaviorInstance->GetAllStateInstances(StateInstances);
	
	for (USMStateInstance_Base* StateInstance : StateInstances)
	{
		ULogicObjectiveNodeInstance* ObjectiveNode = Cast<ULogicObjectiveNodeInstance>(StateInstance);
		if(ObjectiveNode  && (bOnlyActive && StateInstance->IsActive() || !bOnlyActive))
		{
			OutNodes.Add(ObjectiveNode);
		}
	}

	return OutNodes;
}


FString ULogicFunctionLibrary::GetNodeDisplayName(USMNodeInstance* NodeInstance)
{
#if WITH_EDITORONLY_DATA
	return NodeInstance->GetNodeDisplayName();
#else
	return "";
#endif
}

FGameplayTagTableRow ULogicFunctionLibrary::CreateGameplayTagTableRow(FName InTag, const FString& InDevComment)
{
	FGameplayTagTableRow TableRow;
	TableRow.Tag = InTag;
	TableRow.DevComment = InDevComment;

	return TableRow;
}

TArray<ULogicObjectiveNodeInstance*> ULogicFunctionLibrary::GetAchievements(UObject* WorldContextObject, bool bOnlyActive)
{
	//ULogicBehaviorInstance* BehaviorInstance = ULogicSubsystem::GetLogicSubsystem(WorldContextObject)->GlobalAchievementInstance;
	//return GetObjectiveNodes(BehaviorInstance, bOnlyActive);

	TArray<ULogicObjectiveNodeInstance*> Test;
	return Test;
}



/*void ULogicFunctionLibrary::GetAchievements(UObject* WorldContextObject, bool bOnlyActiveNodes, TArray<TScriptInterface<ILogicObjectiveNodeInterface>>& OutNodes)
{
	ULogicBehaviorInstance* BehaviorInstance = ULogicSubsystem::GetLogicSubsystem(WorldContextObject)->AchievementInstance;
	if(!BehaviorInstance) return;

	TArray<USMStateInstance_Base*> StateInstances;
	BehaviorInstance->GetAllStateInstances(StateInstances);
	
	for (USMStateInstance_Base* StateInstance : StateInstances)
	{
		//GLog->Logf(TEXT("## GET NODE NAME %s"), *StateInstance->GetNodeName());

		if(Cast<ILogicObjectiveNodeInterface>(StateInstance) && ((bOnlyActiveNodes && StateInstance->IsActive()) || !bOnlyActiveNodes))
		{
			GLog->Logf(TEXT("## GET INTERFACE NAME %s"), *StateInstance->GetNodeName());
			OutNodes.Add(StateInstance);
		}
	}
}*/
/*
FName variableName = TEXT("intVariable");
UProperty* property = GetClass()->FindPropertyByName(variableName);
void* ptr = property->ContainerPtrToValuePtr<void>(this);
const UNumericProperty* intProperty = Cast<const UNumericProperty>(property);
int readValue = intProperty->GetSignedIntPropertyValue(ptr);*/
