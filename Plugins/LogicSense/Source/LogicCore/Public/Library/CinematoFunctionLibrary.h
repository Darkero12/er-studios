﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "CinematoFunctionLibrary.generated.h"

class USceneCaptureComponent2D;
class UCameraComponent;

USTRUCT(BlueprintType)
struct FCameraSettings
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters",  meta=(DisplayName = "Field of View"))
	float FieldOfView = 90.0f;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters",  meta=(DisplayName = "Orthographic Width"))
	float OrthoWidth = 1000.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters",  meta=(DisplayName = "Aspect Ratio"))
	float AspectRatio = 1.333333f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters",  meta=(DisplayName = "Near Clip"))
	float NearClipPlaneDistance = 10.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters",  meta=(DisplayName = "Far Clip"))
	float FarClipPlaneDistance = 1000.0f;
};


UCLASS()
class LOGICCORE_API UCinematoFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable, Category=Cinemato,  meta = (DisplayName = "Draw Camera Frustum (CameraComponent)"))
	static void DrawCameraFrustum_CameraComponent_Blueprint(UPARAM(DisplayName="Camera")UCameraComponent* SrcCamera);
	
	UFUNCTION(BlueprintCallable, Category=Cinemato,  meta = (DisplayName = "Draw Camera Frustum (CameraSettings)", WorldContext="WorldContextObject")) //CallableWithoutWorldContext //UnsafeDuringActorConstruction="true"
	static void DrawCameraFrustum_CameraSettings_Blueprint(UPARAM(DisplayName="Context") const UObject* WorldContextObject, FTransform Transform, FCameraSettings Setting);

	static void DrawCameraFrustum(UCameraComponent* SrcCamera);
	static void DrawCameraFrustum(const UObject* WorldContextObject, FTransform Transform, FCameraSettings Settings);


	
	UFUNCTION(BlueprintPure, Category=Cinemato,  meta = (DisplayName = "Get Camera Setting"))
	static FCameraSettings GetCameraSetting(UCameraComponent* SrcCamera);
	
	static TArray<FVector> CalculateFrustumCorners(FTransform Transform, FCameraSettings Settings);


	
	UFUNCTION(BlueprintCallable, Category = Cinemato)
	static void CopyCameraSettingsToSceneCapture(UCameraComponent* Src, USceneCaptureComponent2D* Dst);
};
