﻿#pragma once

#include "GameplayTagContainer.h"
#include "GameplayTagsManager.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "LogicFunctionLibrary.generated.h"

class USMInstance;
class USMStateInstance_Base;
class USMNodeInstance;
class ULogicBehaviorInstance;
class ULogicObjectiveNodeInstance;
class ULogicNotification;
class ILogicObjectiveNodeInterface;

UCLASS()
class LOGICCORE_API ULogicFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	//State Machine
	
	//UFUNCTION(BlueprintPure, Category = "Logic Sense|Instance", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Quests", DevelopmentOnly))
	static FString GetNodeDisplayName(USMNodeInstance* NodeInstance);

	//UFUNCTION(BlueprintPure, Category = "Logic Sense|Struct", meta = (DisplayName = "Create GameplayTagTableRow"))
	static FGameplayTagTableRow CreateGameplayTagTableRow(FName InTag, const FString& InDevComment);

	/*UFUNCTION(BlueprintCallable, Category = "Logic Driver|State Machine Instances", meta = (DefaultToSelf = "Target", UseLogicDriverStatePicker="InFullPath"))
	static USMStateInstance_Base* GetStateInstanceByName(USMInstance* Target, const FString& InFullPath);*/
	
		
	//INSTANCES - Achievements
	//UFUNCTION(BlueprintPure, Category = "Logic Sense|Instance|Achievements", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Achievements"))
	static UPARAM(DisplayName = "Achievments") TArray<ULogicObjectiveNodeInstance*> GetAchievements(UObject* WorldContextObject, bool bOnlyActive);
	
	//UFUNCTION(BlueprintCallable, Category = "Logic Sense|Instance|Achievements", meta = (DisplayName = "Make Achievement Completed"))
	//static void SetAchievement(UPARAM(DisplayName="Node") const FString& InFullPath, UPARAM(DisplayName="Value") bool bValue);

	//INSTANCES - Quests
	//UFUNCTION(BlueprintPure, Category = "Logic Sense|Subsystem", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Quests"))
	static UPARAM(DisplayName = "Quests") TArray<ULogicObjectiveNodeInstance*> GetQuests(UObject* WorldContextObject, bool bOnlyActive);

public:
	static TArray<ULogicObjectiveNodeInstance*> GetObjectiveNodes(ULogicBehaviorInstance* BehaviorInstance, bool bOnlyActive);
};




/*UFUNCTION(BlueprintPure, Category = "Logic Sense|Subsystem", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Achievements"))
	static void GetAchievements(UObject* WorldContextObject, bool bOnlyActiveNodes, UPARAM(DisplayName="Nodes")TArray<TScriptInterface<ILogicObjectiveNodeInterface>>& OutNodes);*/

