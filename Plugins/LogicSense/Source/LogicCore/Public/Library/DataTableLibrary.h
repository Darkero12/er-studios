﻿#pragma once

#include "GameplayTagsManager.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DataTableLibrary.generated.h"

UENUM()
enum EDataTableMoveDirection
{
	EAbove	UMETA(DisplayName = "Above"),
	EBelow	UMETA(DisplayName = "Belob"),
};

UCLASS()
class LOGICCORE_API UDataTableLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/**
	* Adds a new row to a given Data Table.
	* @param Table				Data Table asset.
	* @param RowName			New Row name.
	* @param RowData			New Row data.
	* @return Whether the row is successfully added or not.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Logic Sense|Data Table", CustomThunk, meta = (CustomStructureParam = "RowData"))
	static bool AddDataTableRow2(UDataTable* Table, FName RowName, const FTableRowBase& RowData);
	static bool Generic_AddDataTableRow2(UDataTable* Table, FName RowName, const uint8* RowData);
	
	DECLARE_FUNCTION(execAddDataTableRow2);


	static bool AddNewRow(UDataTable* Table, FName RowName, const uint8* NewRowData);
	static uint8* AddRow4(UDataTable* DataTable, FName RowName);

	/**
	 * Generates a unique row name from a candidate name given a Data Table asset.
	 * The name is made unique by appending a number to the end.
	 */
	//UFUNCTION(BlueprintCallable, Category = "Logic Sense|Data Table")
	static FName CreateUniqueRowNameFromDataTable(FName CandidateName, UDataTable* Table);

	static FName GetUniqueRowNameFromList(FName CandidateName, const TArray<FName>& ExistingNames);
};