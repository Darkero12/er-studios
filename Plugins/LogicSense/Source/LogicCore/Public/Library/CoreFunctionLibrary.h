﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "CoreFunctionLibrary.generated.h"

UCLASS()
class LOGICCORE_API UCoreFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category="Logic Sense|Core")
	static bool IsGameRunning();
	void TestInput(APlayerController* PlayerController);

	static void Substring(FString& Text, FString Search);

	
	static void IterateThroughStructProperty(FProperty* Property, void* StructPtr);
	static void ParseProperty(FProperty* Property, void* ValuePtr);
};
