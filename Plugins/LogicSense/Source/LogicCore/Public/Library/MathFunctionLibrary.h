﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MathFunctionLibrary.generated.h"

UCLASS()
class LOGICCORE_API UMathFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="Collision", meta=(bIgnoreSelf="true", WorldContext="WorldContextObject", AutoCreateRefTerm="ActorsToIgnore", DisplayName = "Line Trace All By Channel", Keywords="raycast"))
	static bool LineTraceAllByChannel(const UObject* WorldContextObject, TArray<struct FHitResult>& OutHits,  const FVector Start, const FVector End, ECollisionChannel TraceChannel, const TArray<AActor*>& ActorsToIgnore, bool bUseComplexTrace, bool bUseIgnoreSelf, bool bUseDebugMode);

	UFUNCTION()
	static void CalculatePrimeNumbers(int UpperLimit);
};
