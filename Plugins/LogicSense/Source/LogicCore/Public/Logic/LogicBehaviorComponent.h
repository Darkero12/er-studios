﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SMStateMachineComponent.h"
#include "LogicBehaviorComponent.generated.h"

class ULogicBehaviorInstance;
class ULogicSubsystem;

UCLASS( ClassGroup="LogicSense", meta=(BlueprintSpawnableComponent, DisplayName="Behavior Component"), Blueprintable)
class LOGICCORE_API ULogicBehaviorComponent : public USMStateMachineComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	ULogicBehaviorComponent();

	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Instance")
	void SetInstance(USMInstance* InInstance){ R_Instance = InInstance; }

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (ExposeOnSpawn = true), meta=(DisplayName = "Behavior Machine Class"))
	TSubclassOf<ULogicBehaviorInstance> BehaviorClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Settings", meta=(DisplayName = "Use Debug Mode"))
	bool bUseDebugMode = false;

private:
	ULogicSubsystem* LogicSubsystem;
		
};