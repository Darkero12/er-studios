﻿#pragma once

#include "CoreMinimal.h"
#include "SMInstance.h"
#include "LogicSubsystem.h"

#include "LogicBehaviorInstance.generated.h"

class ULogicNotification;
class ULogicObjectiveNodeInstance;

UCLASS(ClassGroup="LogicSense", Abstract, Blueprintable, meta=(DisplayName="Behavior Machine"))
class LOGICCORE_API ULogicBehaviorInstance : public USMInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnNotifyDelegate OnNotifyEvent;
	
	UFUNCTION(BlueprintNativeEvent)
	void OnNotify(ULogicNotification* Notification);
	

	ULogicBehaviorInstance();
	
	// Called when the game starts
	virtual void Start() override;
	virtual void Shutdown() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,  Category = "Behavior", meta=(DisplayName = "Use Debug Mode"))
	bool bUseDebugMode = false;

protected:
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Behavior", meta = (DisplayName = "Get Quests", DevelopmentOnly))
	APlayerController* GetPlayerController();
	
	
private:
	UFUNCTION()
	void OnNotify_Delegate(ULogicNotification* Notification);

};