﻿#pragma once

#include "SMStateMachineInstance.h"
#include "SMTextGraphProperty.h"
#include "Logic/Transitions/ILogicTransitionInterface.h"
#include "LogicObjectiveStateInstance.generated.h"

UCLASS(Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateMachineInstance), meta = (DisplayName = "Objective SM"))
class LOGICCORE_API ULogicObjectiveStateInstance : public USMStateMachineInstance, public ILogicTransitionInterface
{
	GENERATED_BODY()

public:
	ULogicObjectiveStateInstance();
	
	virtual void ConstructionScript_Implementation() override;
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Node", meta=(DisplayName = "Get Title"))
	virtual FText GetTitle();

	UFUNCTION(BlueprintPure, Category = "Logic Sense|Node", meta=(DisplayName = "Get Description"))
	virtual FText GetDescription();
	
	virtual bool CanExecuteTransition_Implementation() override;
	

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta=(DisplayName = "Title"))
	FSMTextGraphProperty Title;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta=(DisplayName = "Description"))
	FSMTextGraphProperty Description;
	
};
