﻿#pragma once
#include "GameplayTagContainer.h"
#include "SMTextGraphProperty.h"
#include "Logic/Nodes/LogicNodeInstance.h"

#include "LogicObjectiveNodeInstance.generated.h"

UE_DECLARE_GAMEPLAY_TAG_EXTERN(DEFAULT_OBJECTIVE_NODE_TAG);

UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateInstance), meta = (DisplayName = "Objective Node"))
class LOGICCORE_API ULogicObjectiveNodeInstance : public ULogicNodeInstance
{
	GENERATED_BODY()

public:
	ULogicObjectiveNodeInstance();

	virtual void ConstructionScript_Implementation() override;
	virtual bool CanExecuteTransition_Implementation() override;

	UFUNCTION(BlueprintPure, Category = "Logic Sense", meta=(DisplayName = "Get Title"))
	virtual FText GetTitle();

	UFUNCTION(BlueprintPure, Category = "Logic Sense", meta=(DisplayName = "Get Description"))
	virtual FText GetDescription();
	
	UFUNCTION(BlueprintCallable, BlueprintSetter, Category = "Logic Sense", meta=(DisplayName = "Set Complete"))
	void SetComplete(bool bValue);

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta=(DisplayName = "Title"))
	FSMTextGraphProperty Title;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta=(DisplayName = "Description"))
	FSMTextGraphProperty Description;

	//UPROPERTY(EditDefaultsOnly, Category = "GameplayTags")
	//FGameplayTagBlueprintPropertyMap GameplayTagPropertyMap;
	
	virtual FName GetInternName() override;
	
private:
	UPROPERTY(EditAnywhere, BlueprintSetter=SetComplete, Category = "Default|Parameters", meta=(DisplayName = "Has Completed"))
	bool bHasCompleted;
	

};
