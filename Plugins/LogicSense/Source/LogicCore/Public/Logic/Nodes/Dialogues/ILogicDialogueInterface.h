﻿#pragma once

#include "UObject/Interface.h"
#include "ILogicDialogueInterface.generated.h"

class ULogicDialogueState;

UINTERFACE(BlueprintType)
class LOGICCORE_API ULogicDialogueInterface : public UInterface
{
	GENERATED_BODY()

	ULogicDialogueInterface(const class FObjectInitializer& ObjectInitializer): Super(ObjectInitializer) { };
};

class LOGICCORE_API ILogicDialogueInterface
{
	GENERATED_IINTERFACE_BODY()

	
protected:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, meta=(DisplayName = "Setup Dialogue"))
	void SetupDialogue(ULogicDialogueState* Dialogue);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, meta=(DisplayName = "Refresh2"))
	void Refresh2(ULogicDialogueState* Dialogue);
};