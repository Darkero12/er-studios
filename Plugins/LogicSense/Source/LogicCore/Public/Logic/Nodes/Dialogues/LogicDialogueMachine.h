﻿#pragma once

#include "CoreMinimal.h"
#include "Logic/LogicBehaviorInstance.h"

#include "LogicDialogueMachine.generated.h"

class ULogicNotification;
class ULogicObjectiveNodeInstance;

UCLASS(ClassGroup="LogicSense", Abstract, Blueprintable, meta=(DisplayName="Dialogue Machine"))
class LOGICCORE_API ULogicDialogueMachine : public ULogicBehaviorInstance
{
	GENERATED_BODY()

	ULogicDialogueMachine();

	virtual void OnStateMachineInitialized_Implementation() override;
	virtual void OnStateMachineShutdown_Implementation() override;
	
};