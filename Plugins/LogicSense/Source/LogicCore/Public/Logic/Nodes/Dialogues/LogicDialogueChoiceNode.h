﻿#pragma once

#include "SMTextGraphProperty.h"
#include "Logic/Nodes/LogicNodeInstance.h"
#include "LogicDialogueNode.h"
#include "LogicDialogueChoiceNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FChoiceCallback);

UCLASS(Blueprintable, BlueprintType, ClassGroup = "Logic Sense", meta = (DisplayName = "Choice Node"))
class LOGICCORE_API ULogicDialogueChoiceNode : public ULogicDialogueNode
{
	GENERATED_BODY()

public:
	ULogicDialogueChoiceNode();

	virtual void OnStateBegin_Implementation() override;
	virtual bool IsAccessible_Implementation() override;
	
	
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Choice", meta=(DisplayName = "Trigger"))
	virtual void Trigger();
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Choice", meta=(DisplayName = "Get Choice Nodes"))
	static TArray<ULogicDialogueChoiceNode*> GetChoiceNodes(USMStateInstance_Base* Instance);
	
	/*UFUNCTION(BlueprintPure, Category = "Logic Sense|Choice", meta=(DisplayName = "Get Choice Nodes"))
	static TFunction<void> GetDelegate(USMStateInstance_Base* Instance);*/
	
};