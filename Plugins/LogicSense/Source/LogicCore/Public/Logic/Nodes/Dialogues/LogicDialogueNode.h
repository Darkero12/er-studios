﻿#pragma once

#include "SMTextGraphProperty.h"
#include "Logic/Nodes/LogicNodeInstance.h"
#include "LogicDialogueNode.generated.h"

UCLASS(Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateInstance), meta = (DisplayName = "Dialogue Node"))
class LOGICCORE_API ULogicDialogueNode : public USMStateInstance, public ILogicTransitionInterface
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateDelegate, ULogicDialogueNode*, DialogueNode);

	UPROPERTY(BlueprintAssignable)
	FOnUpdateDelegate OnUpdateEvent;

public:
	ULogicDialogueNode();

	virtual void OnStateEnd_Implementation() override;
	virtual bool CanExecuteTransition_Implementation() override;

	virtual void Evaluate();
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Dialogue", meta=(DisplayName = "Get Text"))
	virtual FText GetText();

	

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta=(DisplayName = "Text"))
	FSMTextGraphProperty Text;
	
};