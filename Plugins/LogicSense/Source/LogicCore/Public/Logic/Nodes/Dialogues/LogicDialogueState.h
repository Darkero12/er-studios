﻿#pragma once
#include "EnhancedInputComponent.h"
#include "GameplayTagContainer.h"
#include "InputActionValue.h"
#include "LogicDialogueNode.h"
#include "SMStateMachineInstance.h"
#include "Blueprint/UserWidget.h"
#include "Logic/Transitions/ILogicTransitionInterface.h"

#include "LogicDialogueState.generated.h"

struct FEnhancedInputActionEventBinding;
class UInputAction;

UCLASS(Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateMachineInstance), meta = (DisplayName = "Dialogue State"))
class LOGICCORE_API ULogicDialogueState : public USMStateMachineInstance, public ILogicTransitionInterface
{
	
	GENERATED_BODY()
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateDelegate, ULogicDialogueState*, Dialogue);

	//DECLARE_DELEGATE_OneParam(Delegate, ULogicDialogueState*, Dialogue);

public:
	ULogicDialogueState();

	UPROPERTY(BlueprintAssignable)
	FOnUpdateDelegate OnUpdateEvent;

	virtual void OnRootStateMachineStart_Implementation() override;
	
	virtual void OnStateBegin_Implementation() override;
	virtual void OnStateEnd_Implementation() override;

	virtual bool CanExecuteTransition_Implementation() override;

	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Instance", meta=(DisplayName = "Trigger"))
	virtual void Trigger();
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Instance", meta=(DisplayName = "Get Node"))
	virtual USMNodeInstance* GetNodeInstance();
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Instance", meta=(DisplayName = "Get Text"))
	virtual FText GetText();

	UFUNCTION(BlueprintPure, Category = "Logic Sense|Instance", meta=(DisplayName = "Get Gameplay Tag"))
	FGameplayTag GetGameplayTag() const {return Tag;}
	
	
	UFUNCTION()
	void OnUpdate(USMInstance* Instance, FSMStateInfo ToState, FSMStateInfo FromState);
	
	virtual void Evaluate();
	virtual void EvaluateInput(const FInputActionValue& Value);
	
	ULogicDialogueNode* GetActiveDialogueNode();

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters", meta=(DisplayName = "Widget"))
	TSubclassOf<UUserWidget> WidgetClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parameters", meta=(DisplayName = "Input"))
	UInputAction* InputAction;

	//BlueprintReadOnly
	UPROPERTY(BlueprintReadWrite, Category = "Parameters", meta=(DisplayName = "Tag"))
	mutable FGameplayTag Tag;

	

private:

	virtual TArray<APlayerController*> GetPlayerControllers();
	
	FEnhancedInputActionEventBinding* InputBinding;
	UUserWidget* Widget;
};


//template <typename UserClass, typename... VarTypes>
//void BindUObject(UserClass* InUserObject, typename TMemFunPtrType<false, UserClass, VarTypes... > Func, VarTypes... Vars);








/*USTRUCT(BlueprintType)
struct FLogicInputBinding
{
	GENERATED_USTRUCT_BODY()

	FLogicInputBinding(UInputAction* InputAction, APlayerController* PlayerController, void(*Lambda)(const FInputActionValue)) :
		PlayerController(PlayerController), InputAction(InputAction)
	{

		
		UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerController->InputComponent);
		if (EnhancedInputComponent && InputAction)
		{
			InputBinding = &EnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Triggered, this, Lambda);
		}
	}

	FLogicInputBinding(void(const FInputActionValue&));

	APlayerController* PlayerController;
	FEnhancedInputActionEventBinding* InputBinding;

	UInputAction* InputAction;

	/*void{
		UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(GetStateMachineReference()->GetInputComponent());
	
		if(EnhancedInputComponent && InputBindings.Num() > 0)
		{
			for(FEnhancedInputActionEventBinding* InputBinding : InputBindings)
			{
				EnhancedInputComponent->RemoveBinding(*InputBinding);

				InputBinding->
			}
		}
	}
};*/