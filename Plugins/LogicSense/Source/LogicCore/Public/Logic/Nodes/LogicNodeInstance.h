﻿#pragma once
#include "GameplayTagContainer.h"
#include "NativeGameplayTags.h"
#include "SMStateInstance.h"
#include "Logic/LogicBehaviorInstance.h"
#include "Logic/Transitions/ILogicTransitionInterface.h"
#include "LogicNodeInstance.generated.h"

class ULogicNotification;

UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateInstance), meta = (DisplayName = "Logic Node"))
class LOGICCORE_API ULogicNodeInstance : public USMStateInstance, public ILogicTransitionInterface
{
	GENERATED_BODY()

public:
	ULogicNodeInstance();
	
	UFUNCTION(BlueprintNativeEvent)
	void OnNotify(ULogicNotification* Notification);

	virtual void OnStateInitialized_Implementation() override;
	virtual void OnStateShutdown_Implementation() override;
	
	virtual void OnRootStateMachineStart_Implementation() override;
	virtual void RunConstructionScript() override;
	
	virtual bool CanExecuteTransition_Implementation() override;

protected:
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Node Instance", meta=(DisplayName = "Evaluate"))
	void Evaluate();
	
	UPROPERTY(BlueprintReadOnly, Category = "Default", meta=(DisplayName = "Tag"))
	mutable FGameplayTag Tag;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default|Settings", meta=(DisplayName = "Prefix"))
	FName DisplayPrefix;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default|Settings", meta=(DisplayName = "Tag Group"))
	FGameplayTag TagGroup;
	
	virtual ULogicBehaviorInstance* GetBehaviorInstance();

	
	virtual FName GetInternName();
	virtual FName GetTagName();
	
	
private:
	UFUNCTION()
	virtual void OnNotify_Delegate(ULogicNotification* Notification);



#if WITH_EDITORONLY_DATA
	
protected:
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	virtual void OnPreCompileValidate_Implementation(USMCompilerLog* CompilerLog) const override;
	void ModifyTag();

#endif
	
};
