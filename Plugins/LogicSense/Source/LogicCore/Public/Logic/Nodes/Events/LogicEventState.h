﻿#pragma once
#include "SMStateMachineInstance.h"
#include "Logic/Nodes/Dialogues/ILogicDialogueInterface.h"
#include "Logic/Transitions/ILogicTransitionInterface.h"

#include "LogicEventState.generated.h"

UCLASS(Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateMachineInstance), meta = (DisplayName = "Event State"))
class LOGICCORE_API ULogicEventState : public USMStateMachineInstance, public ILogicTransitionInterface, public ILogicDialogueInterface
{
	GENERATED_BODY()

public:

	ULogicEventState();

	virtual void OnStateInitialized_Implementation() override;
	
	virtual void Refresh2_Implementation(ULogicDialogueState* Dialogue) override;
	virtual bool CanExecuteTransition_Implementation() override;
};
