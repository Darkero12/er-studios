#pragma once

#include "CoreMinimal.h"
#include "NativeGameplayTags.h"
#include "SMStateInstance.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "LogicNodeSubsystem.generated.h"

class ULogicDialogueState;
UE_DECLARE_GAMEPLAY_TAG_EXTERN(DEFAULT_NODE_TAG);

UCLASS()
class LOGICCORE_API ULogicNodeSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category="Logic Sense|Director",  meta=(DisplayName = "Trigger", WorldContext="WorldContextObject"))
	static void Trigger(UObject* WorldContextObject, FGameplayTag Tag);

	UFUNCTION(BlueprintPure, Category="Logic Sense|Director",  meta=(DisplayName = "Get Instance by Tag", WorldContext="WorldContextObject"))
	static USMNodeInstance* GetInstanceByTag(UObject* WorldContextObject, FGameplayTag Tag);
	
	void Register(USMNodeInstance* Instance);
	
	UPROPERTY(EditAnywhere)
	TMap< FGameplayTag, TWeakObjectPtr<USMNodeInstance>> ReferencedNodes;




	
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	


	void Register(USMStateInstance* StateInstance);
	void Remove(USMStateInstance* StateInstance);
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Director", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Node Subsystem"))
	static ULogicNodeSubsystem* Get(const UObject* WorldContextObject);

	/*UFUNCTION(BlueprintPure, Category = "Logic Sense|Subsystem", meta = (DisplayName = "Get Nodes"))
	TArray<USMStateInstance*> GetReferencedNodes();*/

	UFUNCTION(BlueprintPure, Category="Logic Sense|Director",  meta=(DisplayName = "Get Nodes", WorldContext="WorldContextObject", DeterminesOutputType="NodeClass", DynamicOutputParam="OutNodes"))
	static void GetNodes(UObject* WorldContextObject, TSubclassOf<USMStateInstance> NodeClass, TArray<USMStateInstance*>& OutNodes);
	
	

	//UFUNCTION(BlueprintPure, Category = "Logic Sense|Node", meta = (DisplayName = "Get Registered Actors"))
	//static TArray<AActor*> GetReferencedActors();
	/*template <typename T>
	T* FindNode( UObject* WorldContextObject, FGameplayTag Tag)
	{
	
	}*/

private:
	UPROPERTY(EditAnywhere)
	TMap< FGuid, TWeakObjectPtr<USMStateInstance>> ReferencedNodesOld;

	template<typename T>
	TArray<T*> FindNodes();
	
	/*UPROPERTY(EditAnywhere)
	TSet<TWeakObjectPtr<USMStateInstance>> ReferencedStateInstances;*/
	
};
