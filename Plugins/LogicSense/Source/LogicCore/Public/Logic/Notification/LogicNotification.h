﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "LogicNotificationLibrary.h"
#include "LogicNotification.generated.h"


UCLASS( ClassGroup="LogicSense", Blueprintable, meta=(DisplayName="Notification"))
class LOGICCORE_API ULogicNotification : public UObject
{
	GENERATED_BODY()

public:
	ULogicNotification(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//CONTEXT
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Notification", meta=(DisplayName = "Sender"))
	UObject* Sender;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Notification", meta=(DisplayName = "Receivers"))
	TArray<UObject*> Receivers;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Notification", meta=(DisplayName = "Tags"))
	FGameplayTagContainer Tags;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Notification", meta=(DisplayName = "Callback"))
	FOnNotifyCallbackEvent Callback;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Notification", meta=(DisplayName = "Context"))
	UObject* Payload;
};


/*template <typename T>
static bool TryIncrement(std::atomic<T>& Value, const T Max)
{
for (T Existing = Value.load(std::memory_order_relaxed);;)
{
if (Existing >= Max)
{
return false;
}
if (Value.compare_exchange_weak(Existing, Existing + 1, std::memory_order_relaxed))
{
return true;
}
}
}
*/