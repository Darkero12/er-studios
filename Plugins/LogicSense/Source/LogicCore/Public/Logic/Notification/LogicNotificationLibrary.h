﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayTagContainer.h"
#include "LogicNotificationLibrary.generated.h"

class UProperty;
class ULogicNotification;

DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnNotifyCallbackEvent, UObject*, Target, ULogicNotification*, Notification);
DECLARE_DYNAMIC_DELEGATE_TwoParams(FWildcardFilterDelegate, const UProperty*, Value, bool&, Result);

UCLASS()
class LOGICCORE_API ULogicNotificationLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:

	
	DECLARE_FUNCTION(execBroadcastProperty);
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Notification", CustomThunk, meta = (DisplayName = "Broadcast \nw/(Payload)", CustomStructureParam = "Payload", WorldContext = "WorldContextObject"))
	static UPARAM(DisplayName = "Notification") ULogicNotification* BroadcastProperty(UObject* WorldContextObject, UProperty* Payload);

	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Director", meta = (DisplayName = "Join\nw/(Dialoge)"))
	static UPARAM(DisplayName = "Notification") ULogicNotification* BroadcastDialogue(UPARAM(DisplayName="Sender")AActor* Sender, FGameplayTag Tag);
	




	
	
	/*UFUNCTION(BlueprintCallable, Category = "Logic Sense|Notification", meta = (DefaultToSelf = "Sender", DisplayName = "Notify (Broadcast)", AutoCreateRefTerm="Sender"))
	static UPARAM(DisplayName = "Notification") ULogicNotification* NotifyTag(UPARAM(DisplayName="Sender")AActor* Sender, FGameplayTagContainer Tags);*/

	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Notification", meta = (DefaultToSelf = "Sender", DisplayName = "Notify (Broadcast)", AutoCreateRefTerm="Sender,Receivers", AdvancedDisplay="Receivers,Tags"))
	static UPARAM(DisplayName = "Notification") ULogicNotification* Notify(UPARAM(DisplayName="Sender")AActor* Sender, TArray<AActor*> Receivers, FGameplayTagContainer Tags, const FOnNotifyCallbackEvent& Callback);
	
	
	/*UFUNCTION(BlueprintCallable, Category = "Logic Sense|Notification", meta = (DefaultToSelf = "Sender", DisplayName = "Notify Actor"))
	static UPARAM(DisplayName = "Notification") ULogicNotification* NotifyActor(UPARAM(DisplayName="Sender")AActor* Sender, AActor* Receiver, FGameplayTagContainer Tags);*/

	/*UFUNCTION(BlueprintCallable, meta = (DisplayName = "w/PredicateFilterWildcard", CompactNodeTitle = "PredicateFilter", ArrayParm = "Array", ArrayTypeDependentParams = "Results", BlueprintThreadSafe, CustomThunk, Keywords = "Filter plugin Array Wildcard Predicate", ToolTip = "Filter an array using a predicate", CustomStructureParam = "Array"), Category = "Filter")
	static void PredicateFilterWildcard(const TArray<UStructProperty*>& Array, const FWildcardFilterDelegate& PredicateFunction, TArray<UStructProperty*>& Results, bool InvertResult = false);
	*/
};