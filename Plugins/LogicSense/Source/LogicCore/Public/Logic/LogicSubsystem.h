#pragma once

#include "CoreMinimal.h"
#include "Notification/LogicNotificationLibrary.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "LogicSubsystem.generated.h"

class ULogicBehaviorInstance;
class ULogicNotification;
class ULogicBehaviorComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNotifyDelegate, ULogicNotification*, Notification);

UCLASS()
class LOGICCORE_API ULogicSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnNotifyDelegate OnNotifyEvent;
	
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Subsystem", meta = (DisplayName = "Register (Actor)"))
	void RegisterActor_Function(AActor* Actor){ Register(Actor); }
	void Register(AActor* Actor){ ReferencedActors.Add(Actor); }
	
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Subsystem", meta = (DisplayName = "Remove (Actor)"))
	void RemoveActor_Function(AActor* Actor){ Remove(Actor); }
	void Remove(AActor* Actor){ ReferencedActors.Remove(Actor); }

	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Subsystem", meta = (DisplayName = "Register (ActorComponent)"))
	void RegisterActorComponent_Function(UActorComponent* ActorComponent){ Register(ActorComponent); }
	void Register(UActorComponent* ActorComponent){ ReferencedActorComponents.Add(ActorComponent); }
	
	UFUNCTION(BlueprintCallable, Category = "Logic Sense|Subsystem", meta = (DisplayName = "Remove (ActorComponent)"))
	void RemoveActorComponent_Function(UActorComponent* ActorComponent){Remove(ActorComponent);}
	void Remove(UActorComponent* ActorComponent){ReferencedActorComponents.Remove(ActorComponent);}
	
	UFUNCTION(BlueprintPure, Category = "Logic Sense|Subsystem", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Logic Subsystem"))
	static ULogicSubsystem* Get(UObject* WorldContextObject);

	UFUNCTION(BlueprintPure, Category = "Logic Sense|Subsystem", meta = (DisplayName = "Get Registered Actors"))
	TArray<AActor*> GetReferencedActors();

private:
	
	UPROPERTY(EditAnywhere)
	TSet<TWeakObjectPtr<AActor>> ReferencedActors;

	UPROPERTY(EditAnywhere)
	TSet<TWeakObjectPtr<UActorComponent>> ReferencedActorComponents;

	UPROPERTY(EditAnywhere)
	ULogicBehaviorInstance* GlobalAchievementInstance;
	
};
