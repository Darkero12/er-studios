﻿#pragma once

#include "InputAction.h"
#include "SMTransitionInstance.h"


#include "EnhancedInputComponent.h"

#include "LogicTransition.generated.h"



UCLASS(Blueprintable, BlueprintType, ClassGroup = "Logic Sense", hideCategories = (SMStateInstance), meta = (DisplayName = "Objective Node"))
class LOGICCORE_API ULogicTransition : public USMTransitionInstance
{
	GENERATED_BODY()

public:
	ULogicTransition();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Event Trigger", meta=(DisplayName = "Inputs"))
	UInputAction* InputAction;

	virtual void OnTransitionInitialized_Implementation() override;
	virtual void OnTransitionShutdown_Implementation() override;
	
	virtual void ConstructionScript_Implementation() override;
	virtual bool CanEnterTransition_Implementation() const override;
	
	void Evaluate(const FInputActionValue& Value);

private:
	FEnhancedInputActionEventBinding* Event;

#if WITH_EDITORONLY_DATA
	
private:
	void AddRule(UClass* Class, bool bToState, bool bFromState);

	bool bInit;
	
	UTexture2D* TextureDTD;
	UTexture2D* TextureDTC;

#endif	
	
};
