﻿#pragma once
#include "UObject/Interface.h"
#include "ILogicTransitionInterface.generated.h"

UINTERFACE(BlueprintType)
class LOGICCORE_API ULogicTransitionInterface : public UInterface
{
	GENERATED_BODY()

	ULogicTransitionInterface(const class FObjectInitializer& ObjectInitializer): Super(ObjectInitializer) { };
};

class LOGICCORE_API ILogicTransitionInterface
{
	GENERATED_IINTERFACE_BODY()

	
protected:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, meta=(DisplayName = "Can Execute Transition"))
	bool CanExecuteTransition();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, meta=(DisplayName = "Is Accessible"))
	bool IsAccessible();
};