﻿#pragma once

#include "NativeGameplayTags.h"
#include "GameFramework/Actor.h"
#include "UObject/NameTypes.h"
#include "Engine/DeveloperSettingsBackedByCVars.h"

#include "UObject/SoftObjectPtr.h"

#include "LogicSenseSettings.generated.h"

class USMBlueprint;
class ULogicBehaviorInstance;
class USkeletalMesh;

UENUM()
enum ELogicSensorRuntime
{
	EAsync	UMETA(DisplayName = "Async (Faster)"),
	ESync	UMETA(DisplayName = "Sync"),
};

USTRUCT(BlueprintType)
struct FSkeletonOptions
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, meta=(DisplayName = "Sockets"))
	TArray<FName> SocketNames;
};

USTRUCT(BlueprintType)
struct FLogicSensorSettings
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, Category="Sensor - Settings", meta=(DisplayName = "Runtime"))
	TEnumAsByte<ELogicSensorRuntime> Runtime  = EAsync;

	UPROPERTY(EditAnywhere, Category="Sensor - Settings", meta=(DisplayName = "Use Shadows to Hide"))
	bool bUseShadowsToHide = true;
	
	UPROPERTY(EditAnywhere, Category="Sensor - Settings", meta=(DisplayName = "Use Cast-Shadows to Detect"))
	bool bUseShadowsToDetect = true;
	
	UPROPERTY(EditAnywhere, Category="Sensor - Settings", meta=(DisplayName = "Refine Skeleton Search "), AdvancedDisplay)
	TMap<USkeleton*, FSkeletonOptions> SkeletonOptions;
	
	UPROPERTY(EditAnywhere, Category="Sensor - Settings", meta=(DisplayName = "Ignore Obstruction by Actors"), AdvancedDisplay)
	TArray<TSoftClassPtr<AActor>> IgnoreActors;
	
	UPROPERTY(EditAnywhere, Category="Sensor - Settings", meta=(DisplayName = "Ignore Obstruction by Physical Materials"), AdvancedDisplay)
	TArray<TSoftClassPtr<UPhysicalMaterial>> IgnoreMaterials;

	
};

UCLASS(config=Game, defaultconfig, meta=(DisplayName="Logic Sense"))
class LOGICCORE_API ULogicSenseSettings : public UDeveloperSettingsBackedByCVars
{
	GENERATED_BODY()

public:
	ULogicSenseSettings();
	
	virtual FLogicSensorSettings GetSensorSettings() const;

private:
	virtual FName GetCategoryName() const override;
	virtual FString GetPluginVersion() const;

public:
	UPROPERTY(VisibleAnywhere, Category="Details", meta=(DisplayName = "Installed Version"))
	FString InstalledVersion;

	UPROPERTY(config, EditAnywhere, Category="Game|Achievements & Quests", meta = (DisplayName = "FIX"))
	TSoftClassPtr<ULogicBehaviorInstance> FIX;
	
	UPROPERTY(config, EditAnywhere, Category="Game|Achievements & Quests", meta = (DisplayName = "TEst"))
	TSoftObjectPtr<USMBlueprint> Test2;
	
	/*template<class T=UObject>
	UPROPERTY(config, EditAnywhere, Category="Game|Achievements & Quests", meta = (DisplayName = "Test 3", AllowedClasses = "/Script/SMSystem.SMBlueprint"))
	struct TSoftObjectPtr;*/
	
	UPROPERTY(config, EditAnywhere, Category="Game|Achievements & Quests", meta = (DisplayName = "Achievements & Quests", AllowedClasses = "/Script/SMSystem.SMBlueprint"))
	FSoftObjectPath BlueprintAchievement;

	UPROPERTY(config, EditAnywhere, Category="Game|Achievements & Quests", meta = (DisplayName = "Alignments", AllowedClasses = "/Script/SMSystem.SMBlueprint"))
	FSoftObjectPath BlueprintAlignments;

private:
	UPROPERTY(config, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLogicSensorSettings SensorSettings;

	UPROPERTY(meta=(DisplayName = "Initialized Settings"))
	bool bInitializedSettings;


#if WITH_EDITOR

public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnUpdateSettings, ULogicSenseSettings const*);
	static FOnUpdateSettings OnUpdateSettings;

	void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#endif
};
	
	