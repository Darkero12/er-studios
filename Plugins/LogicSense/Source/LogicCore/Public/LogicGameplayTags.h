﻿#pragma once
 
#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GameplayTagsManager.h"
 
struct LOGICCORE_API FLogicGameplayTags : public FGameplayTagNativeAdder
{
	inline static FGameplayTag Logic_Condition_IsPlayer;
	inline static FGameplayTag Logic_Condition_IsEnemy;
	inline static FGameplayTag Logic_Condition_IsUnit;

	inline static FGameplayTag Logic_Attribute_Unit;
	inline static FGameplayTag Logic_Attribute_Item;
	inline static FGameplayTag Logic_Attribute_Hidden;
	
	inline static FGameplayTag Logic_Trigger_OnActorsDiscovered;
	inline static FGameplayTag Logic_Trigger_OnActorsOutOfSight;
	inline static FGameplayTag Logic_Trigger_OnEnteringArea;
	inline static FGameplayTag Logic_Trigger_OnExitingArea;

	//FORCEINLINE static const FGlobalGameTags& Get() { return BehaviorTags; }
 
protected:
	//Called to register and assign the native tags
	virtual void AddTags() override
	{
		UGameplayTagsManager& Manager = UGameplayTagsManager::Get();
 
		/*Logic_Condition_IsPlayer = Manager.AddNativeGameplayTag(TEXT("Logic.Condition.IsPlayer"));
		Logic_Condition_IsEnemy = Manager.AddNativeGameplayTag(TEXT("Logic.Condition.IsEnemy"));
		Logic_Condition_IsUnit = Manager.AddNativeGameplayTag(TEXT("Logic.Condition.IsAlly"));

		Logic_Attribute_Unit = Manager.AddNativeGameplayTag(TEXT("Logic.Attribute.Unit"));
		Logic_Attribute_Item = Manager.AddNativeGameplayTag(TEXT("Logic.Attribute.Item"));
		Logic_Attribute_Hidden = Manager.AddNativeGameplayTag(TEXT("Logic.Attribute.Hidden"));
		
		Logic_Trigger_OnActorsDiscovered = Manager.AddNativeGameplayTag(TEXT("Logic.Trigger.OnActorsDiscovered"));
		Logic_Trigger_OnActorsOutOfSight = Manager.AddNativeGameplayTag(TEXT("Logic.Trigger.OnActorsOutOfSight"));
		Logic_Trigger_OnEnteringArea = Manager.AddNativeGameplayTag(TEXT("Logic.Trigger.OnEnteringArea"));
		Logic_Trigger_OnExitingArea = Manager.AddNativeGameplayTag(TEXT("Logic.Trigger.OnExitingArea"));*/
	}

private:
	//Private static object for the global tags. Use the Get() function to access externally.
	static FLogicGameplayTags GameplayTags;
};