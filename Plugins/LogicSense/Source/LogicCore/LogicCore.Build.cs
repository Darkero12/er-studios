// Some copyright should be here...

using UnrealBuildTool;

public class LogicCore : ModuleRules
{
	public LogicCore(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		CppStandard = CppStandardVersion.Latest;
		
		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"PhysicsCore",
			"GameplayTags", 
			"CinematicCamera",
			"EnhancedInput",
			"UMG",
			"SMSystem", 
			"SMExtendedRuntime"
		});


		PrivateDependencyModuleNames.AddRange(new string[]
		{ 
			"Slate",
			"SlateCore",
			"Projects",
			"DeveloperSettings", 
		});

		if (Target.bBuildEditor)
		{
			PublicDependencyModuleNames.AddRange(new string[]
			{
				"UnrealEd",
				"GraphEditor",
			});
		}
	}
}
