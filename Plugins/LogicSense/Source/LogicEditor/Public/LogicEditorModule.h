#pragma once
#include "ILogicExtensionInterface.h"
#include "Modules/ModuleInterface.h"

#define LOGIC_EDITOR_MODULE "LogicEditorModule"

class FLogicEditorModule : public IModuleInterface
{
	
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	virtual void InitModules();

	static FORCEINLINE FLogicEditorModule& Get()
	{
		return FModuleManager::LoadModuleChecked<FLogicEditorModule>(LOGIC_EDITOR_MODULE);
	}

	static FORCEINLINE bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded(LOGIC_EDITOR_MODULE);
	}
	

	void Test();

	void DeleteSelectedNodes();
	bool CanExecuteDelete();

protected:
	TArray<TSharedRef<ILogicExtensionInterface>> Extensions;
};
