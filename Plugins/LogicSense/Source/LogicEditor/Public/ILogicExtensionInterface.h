﻿#pragma once

class LOGICEDITOR_API ILogicExtensionInterface
{
public:
	virtual void StartupExtension() = 0;
	virtual void ShutdownExtension() = 0;
};