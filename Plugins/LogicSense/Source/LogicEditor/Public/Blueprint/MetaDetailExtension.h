#pragma once

#include "ILogicExtensionInterface.h"

class FMetaDetailExtension : public ILogicExtensionInterface
{
public:
	virtual void StartupExtension() override;
	virtual void ShutdownExtension() override;
};