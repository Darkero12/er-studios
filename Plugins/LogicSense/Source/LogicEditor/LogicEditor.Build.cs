// Some copyright should be here...

using UnrealBuildTool;

public class LogicEditor : ModuleRules
{
	public LogicEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		CppStandard = CppStandardVersion.Latest;

		PublicDependencyModuleNames.AddRange(new string[] 
		{ 
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"UnrealEd", 
			"PhysicsCore",
			
			"LogicCore",
			"LogicSense",
			"SMSystem",
		});
		
		PrivateDependencyModuleNames.AddRange(new string[]
		{
			"Slate",
			"SlateCore",
		});

		if (Target.bBuildEditor)
		{
			PublicDependencyModuleNames.AddRange(new string[]
			{ 
				"UnrealEd",
			});
			PrivateDependencyModuleNames.AddRange(new string[]
			{ 
				"EditorStyle", 
				"PropertyEditor",
				"AssetTools",
				"DetailCustomizations",
			});
		}
	}
}
