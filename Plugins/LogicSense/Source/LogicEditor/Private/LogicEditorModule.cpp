#include "LogicEditorModule.h"
#include "Blueprint/MetaDetailExtension.h"

#define LOCTEXT_NAMESPACE "FLogicEditorModule"

void FLogicEditorModule::StartupModule()
{
	InitModules();
	
	if (IsRunningCommandlet()) return;

	for (auto Module : Extensions) Module->StartupExtension();
}

void FLogicEditorModule::ShutdownModule()
{
	for (auto Module : Extensions) Module->ShutdownExtension();
}

void FLogicEditorModule::InitModules()
{
	//Extensions.Add(MakeShareable(new FMetaDetailExtension()));
}




void FLogicEditorModule::Test()
{
	/*const FScopedTransaction Transaction( FGenericCommands::Get().Delete->GetDescription() );

	const FGraphPanelSelectionSet SelectedNodes = GetSelectedNodes();
	for (FGraphPanelSelectionSet::TConstIterator NodeIt( SelectedNodes ); NodeIt; ++NodeIt)
	{
		if (UEdGraphNode* Node = Cast<UEdGraphNode>(*NodeIt))
		{
		}
	}*/
	
	//FBlueprintEditor::DeleteSelectedNodes();
	//FCoreUObjectDelegates::OnObjectTransacted;

	
	/*TSharedPtr<FUICommandList> GraphEditorCommands = MakeShareable(new FUICommandList);
	
	GraphEditorCommands->MapAction(FGenericCommands::Get().Delete,
		FExecuteAction::CreateRaw(this, &FLogicEditorModule::DeleteSelectedNodes),
		FCanExecuteAction::CreateRaw(this, &FLogicEditorModule::CanExecuteDelete)
	);

	auto ExecuteDeleteAction = [this]()
	{
		GLog->Logf(ELogVerbosity::Error, TEXT("## DELETE ACTION"));
		
		if (CanExecuteDelete())
		{
			//BeginEditTransation();
			//DeleteSelectedText();
			//EndEditTransaction();
		}
	};

	TSharedPtr<FUICommandList> UICommandList = MakeShareable(new FUICommandList());
	
	UICommandList->MapAction(FGenericCommands::Get().Delete,
		FExecuteAction::CreateLambda(ExecuteDeleteAction),
		FCanExecuteAction::CreateRaw(this, &FLogicEditorModule::CanExecuteDelete)
	);*/
}

/*FGraphPanelSelectionSet FLogicEditorModule::GetSelectedNodes() const
{
	FGraphPanelSelectionSet CurrentSelection;
	TSharedPtr<SGraphEditor> FocusedGraphEd = FocusedGraphEdPtr.Pin();
	if (FocusedGraphEd.IsValid())
	{
		CurrentSelection = FocusedGraphEd->GetSelectedNodes();
	}
	return CurrentSelection;
}*/



void FLogicEditorModule::DeleteSelectedNodes()
{
	GLog->Logf(ELogVerbosity::Warning, TEXT("DeleteSelectedNodes"));
	/*TSharedPtr<SGraphEditor> CurrentGraphEditor = GetCurrGraphEditor();
	if (!CurrentGraphEditor.IsValid())
	{
		return;
	}

	const FScopedTransaction Transaction(FGenericCommands::Get().Delete->GetDescription());

	CurrentGraphEditor->GetCurrentGraph()->Modify();

	const FGraphPanelSelectionSet SelectedNodes = CurrentGraphEditor->GetSelectedNodes();
	CurrentGraphEditor->ClearSelectionSet();

	for (FGraphPanelSelectionSet::TConstIterator NodeIt(SelectedNodes); NodeIt; ++NodeIt)
	{
		UEdGraphNode* EdNode = Cast<UEdGraphNode>(*NodeIt);
		if (EdNode == nullptr || !EdNode->CanUserDeleteNode())
			continue;;

		if (UEdNode_GenericGraphNode* EdNode_Node = Cast<UEdNode_GenericGraphNode>(EdNode))
		{
			EdNode_Node->Modify();

			const UEdGraphSchema* Schema = EdNode_Node->GetSchema();
			if (Schema != nullptr)
			{
				Schema->BreakNodeLinks(*EdNode_Node);
			}

			EdNode_Node->DestroyNode();
		}
		else
		{
			EdNode->Modify();
			EdNode->DestroyNode();
		}
	}*/
}

bool FLogicEditorModule::CanExecuteDelete()
{
	GLog->Logf(ELogVerbosity::Error, TEXT("## CanExecuteDelete"));
	// If any of the nodes can be deleted then we should allow deleting
	/*const FGraphPanelSelectionSet SelectedNodes = GetSelectedNodes();
	for (FGraphPanelSelectionSet::TConstIterator SelectedIter(SelectedNodes); SelectedIter; ++SelectedIter)
	{
		UEdGraphNode* Node = Cast<UEdGraphNode>(*SelectedIter);
		if (Node != nullptr && Node->CanUserDeleteNode())
		{
			return true;
		}
	}

	return false;*/
	return true;
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FLogicEditorModule, LogicEditor)