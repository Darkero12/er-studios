#include "Blueprint/MetaDetailExtension.h"
#include "Blueprint/MetaDetailCustomization.h"

#include "BlueprintEditorModule.h"

void FMetaDetailExtension::StartupExtension()
{
	GLog->Logf(ELogVerbosity::Error, TEXT("## FIRE FMetaDetailExtension"));
	
	// Register Blueprint editor variable customization
	FBlueprintEditorModule& BlueprintEditorModule = FModuleManager::LoadModuleChecked<FBlueprintEditorModule>("Kismet");
	BlueprintEditorModule.RegisterVariableCustomization( FProperty::StaticClass(), FOnGetVariableCustomizationInstance::CreateStatic(&FMetaDetailCustomization::MakeInstance));
}

void FMetaDetailExtension::ShutdownExtension()
{
	FBlueprintEditorModule* BlueprintEditorModule = FModuleManager::GetModulePtr<FBlueprintEditorModule>("Kismet");
	if (BlueprintEditorModule)
	{
		const FDelegateHandle Delegate;
		BlueprintEditorModule->UnregisterVariableCustomization(FProperty::StaticClass(), Delegate);
	}
}