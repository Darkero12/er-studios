// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Abilities/GASAbility.h"

#include "Components/GASAbilityQueueComponent.h"
#include "Game/Character/GASCharacter.h"
#include "GASSetup/Public/Components/GASAbilityComponent.h"

UGASAbility::UGASAbility()
{
	// Default to Instance Per Actor
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	// Default tags that block this ability from activating
	ActivationBlockedTags.AddTag(FGameplayTag::RequestGameplayTag(FName("State.Dead")));
	ActivationBlockedTags.AddTag(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun")));
}

void UGASAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
                                  const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	//TODO: Should we register on ActivatedAbility or granted?
	//for listen to ASC tag and cancel itself if match AbilityCancelTag
	TArray<FGameplayTag> CancelTagArray;
	AbilityCancelTag.GetGameplayTagArray(CancelTagArray);

	UAbilitySystemComponent* const ASC = GetAbilitySystemComponentFromActorInfo();
	for (FGameplayTag CancelTag : CancelTagArray)
	{
		ASC->RegisterGameplayTagEvent(CancelTag, EGameplayTagEventType::NewOrRemoved).AddUObject(this, &UGASAbility::OnCancelTagEventCallback);
	}
}

void UGASAbility::OnCancelTagEventCallback(const FGameplayTag CallbackTag, int32 NewCount)
{
	if(NewCount>0) 
	{
		UAbilitySystemComponent* const ASC = GetAbilitySystemComponentFromActorInfo();
		//for listen to ASC tag and cancel itself if match AbilityCancelTag
		TArray<FGameplayTag> CancelTagArray;
		AbilityCancelTag.GetGameplayTagArray(CancelTagArray);

		for (const FGameplayTag CancelTag : CancelTagArray)
		{
			ASC->RegisterGameplayTagEvent(CancelTag, EGameplayTagEventType::NewOrRemoved).RemoveAll(this);
		}
        
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}
}

void UGASAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	if (ActivateAbilityOnGranted)
	{
		bool ActivatedAbility = ActorInfo->AbilitySystemComponent->TryActivateAbility(Spec.Handle, false);
	}
}

void UGASAbility::PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
                              const FGameplayAbilityActivationInfo ActivationInfo,
                              FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate, const FGameplayEventData* TriggerEventData)
{
	Super::PreActivate(Handle, ActorInfo, ActivationInfo, OnGameplayAbilityEndedDelegate, TriggerEventData);

	OnGameplayAbilityEnded.AddUObject(this, &UGASAbility::AbilityEnded);
	
	// Open ability queue only if told to do so
	if (!bEnableAbilityQueue)
	{
		return;
	}

	AActor* Avatar = GetAvatarActorFromActorInfo();
	if (!Avatar)
	{
		return;
	}

	UGASAbilityQueueComponent* AbilityQueueComponent = Cast<AGASCharacter>(Avatar)->AbilityQueueComponent;
	if (!AbilityQueueComponent)
	{
		
		return;
	}

	AbilityQueueComponent->OpenAbilityQueue();
	AbilityQueueComponent->SetAllowAllAbilitiesForAbilityQueue(true);
}


void UGASAbility::AbilityEnded(UGameplayAbility* Ability)
{
	UE_LOG(LogTemp, Warning, TEXT("UGSCGameplayAbility::AbilityEnded"));
	AActor* Avatar = GetAvatarActorFromActorInfo();
	if (!Avatar)
	{
		return;
	}

	AGASCharacter* Character = Cast<AGASCharacter>(Avatar);
	if (!Character)
	{
		return;
	}
	
	// Trigger OnEndDelegate now in case Blueprints need it
	OnAbilityEnded.Broadcast();
	OnAbilityEnded.Clear();
}

void UGASAbility::RemoveAbilityCoolDown()
{
	if (CurrentActorInfo)
	{
		UAbilitySystemComponent* const ASC = CurrentActorInfo->AbilitySystemComponent.Get();
		if (ASC)
		{
			UGameplayEffect* CDGE = GetCooldownGameplayEffect();
			if (CDGE)
			{
				FGameplayTagContainer CooldownTags = CDGE->InheritableOwnedTagsContainer.CombinedTags;

				if (CooldownTags.Num() > 0)
				{
					FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(CooldownTags);
					ASC->RemoveActiveEffects(Query, -1);
				}
			}
		}
	}
}
