// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/DamageExecutions/AttributeSwapCalcClass.h"
#include "AbilitySystemComponent.h"
#include "Game/Character/GASCharacter.h"


void UAttributeSwapCalcClass::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
                                                     FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	Super::Execute_Implementation(ExecutionParams, OutExecutionOutput);

	UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();

	AActor* SourceActor = SourceASC ? SourceASC->GetAvatarActor() : nullptr;

	AGASCharacter* SourceCharacter = Cast<AGASCharacter>(SourceActor);
	
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	
	float MaxHealth = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.MaxHealth")), false, -1.0f), 0.0f);
	float Health = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Health")), false, -1.0f), 0.0f);
	float MaxStamina = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.MaxStamina")), false, -1.0f), 0.0f);
	float Stamina = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Stamina")), false, -1.0f), 0.0f);
	float MaxMana = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.MaxMana")), false, -1.0f), 0.0f);
	float Mana = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Mana")), false, -1.0f), 0.0f);
	/*float Constitution = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Constitution")), false, -1.0f), 0.0f);
	float Endurance = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Endurance")), false, -1.0f), 0.0f);
	float Intelligence = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Intelligence")), false, -1.0f), 0.0f);
	float Agility = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Agility")), false, -1.0f), 0.0f);
	float Tenacity = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Tenacity")), false, -1.0f), 0.0f);*/

	if(MaxHealth > 0)
	{
		SourceCharacter->SetMaxHealth(MaxHealth);
	}
	
	if(Health > 0)
	{
		SourceCharacter->SetHealth(Health);
	}
	
	if(MaxStamina > 0)
	{
		SourceCharacter->SetMaxStamina(MaxStamina);
	}

	if(Stamina > 0)
	{
		SourceCharacter->SetStamina(Stamina);
	}

	if(MaxMana > 0)
	{
		SourceCharacter->SetMaxMana(MaxMana);
	}

	if(Mana > 0)
	{
		SourceCharacter->SetMana(Mana);
	}
	
}
