// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/DamageExecutions/DamageAppRequirement.h"
#include "AbilitySystemComponent.h"
#include "GASFunctionLibrary.h"

bool UDamageAppRequirement::CanApplyGameplayEffect_Implementation(const UGameplayEffect* GameplayEffect,
                                                                  const FGameplayEffectSpec& Spec, UAbilitySystemComponent* ASC) const
{
	if(ASC->HasMatchingGameplayTag(BlockTag))
	{
		const AActor* SourceActor = Spec.GetEffectContext().GetOriginalInstigatorAbilitySystemComponent()->GetAvatarActor();
		if(UGASFunctionLibrary::GetHitDirection(SourceActor, ASC->GetAvatarActor()) == EHitReactDirection::Front)
		{
			return false;
		}
		
	}
	
	return Super::CanApplyGameplayEffect_Implementation(GameplayEffect, Spec, ASC);
	
}
