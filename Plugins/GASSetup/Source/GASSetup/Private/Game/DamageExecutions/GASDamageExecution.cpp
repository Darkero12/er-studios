// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/DamageExecutions/GASDamageExecution.h"

#include "GASFunctionLibrary.h"
#include "Game/AttributeSet/GASAttributeSet.h"
#include "Game/Character/GASCharacter.h"
#include "GASSetup/Public/Components/GASAbilityComponent.h"

struct FDamageStatistics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);
	DECLARE_ATTRIBUTE_CAPTUREDEF(StaminaDamage);
    DECLARE_ATTRIBUTE_CAPTUREDEF(SharpenModifier);
    DECLARE_ATTRIBUTE_CAPTUREDEF(Shield);
    DECLARE_ATTRIBUTE_CAPTUREDEF(OverDamage);
    
	FDamageStatistics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UGASAttributeSet, Damage, Source, true);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UGASAttributeSet, Shield, Source, true);
	}
};

static const FDamageStatistics DamageStatics()
{
	static FDamageStatistics DStatistics;
	return DStatistics;
}

UGASDamageExecution::UGASDamageExecution()
{
	RelevantAttributesToCapture.Add(DamageStatics().DamageDef);
	RelevantAttributesToCapture.Add(DamageStatics().StaminaDamageDef);
    RelevantAttributesToCapture.Add(DamageStatics().SharpenModifierDef);
    RelevantAttributesToCapture.Add(DamageStatics().ShieldDef);
}

void UGASDamageExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
	FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	UAbilitySystemComponent* TargetAbilitySystemComponent = ExecutionParams.GetTargetAbilitySystemComponent();
    UAbilitySystemComponent* SourceAbilitySystemComponent = ExecutionParams.GetSourceAbilitySystemComponent();

    AActor* TargetActor = TargetAbilitySystemComponent ? TargetAbilitySystemComponent->GetAvatarActor() : nullptr;
    AActor* SourceActor = SourceAbilitySystemComponent ? SourceAbilitySystemComponent->GetAvatarActor() : nullptr;

    const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
    const FGameplayEffectContextHandle Context = Spec.GetEffectContext();
	FGameplayTagContainer AssetTags;
	Spec.GetAllAssetTags(AssetTags);
	
    // Gather the tags from the source and target as that can affect which buffs should be used
    const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
    const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

    FAggregatorEvaluateParameters EvaluationParameters;
    EvaluationParameters.SourceTags = SourceTags;
    EvaluationParameters.TargetTags = TargetTags;

    float Damage = 0.0f;
    float StaminaDamage = 0.0f;
    float CutDamage = 0.0f;
    float PierceDamage = 0.0f;
    float SharpenBonus = 1.0f;
    float SharpenedDamage = 0.0f;
    float Shield = 0.0f;
    float OverDamage = 0.0f;
   
    // Capture optional damage value set on the damage GE as a CalculationModifier under the ExecutionCalculation
    ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().DamageDef, EvaluationParameters, Damage);
    ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().StaminaDamageDef, EvaluationParameters, StaminaDamage);
    ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().SharpenModifierDef, EvaluationParameters, SharpenBonus);
    ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().ShieldDef, EvaluationParameters, Shield);
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().ShieldDef, EvaluationParameters, OverDamage);
    // Add SetByCaller damage if it exists
    Damage += FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Damage")), false, -1.0f), 0.0f);
    CutDamage += FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("DamageType.CutDamage")), false, -1.0f), 0.0f);
    PierceDamage += FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("DamageType.PierceDamage")), false, -1.0f), 0.0f);
    StaminaDamage += FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Damage.StaminaDamage")), false, -1.0f), 0.0f);
    Shield += FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Shield")), false, -1.0f), 0.0f);

	UE_LOG(LogTemp, Warning, TEXT("Sharepened value is:  %f,"),SharpenBonus);
    SharpenedDamage = (Damage + CutDamage + PierceDamage) * SharpenBonus/100.0f;
    float UnmitigatedDamage = Damage + CutDamage + PierceDamage + SharpenedDamage ; // Can multiply any damage boosters here
    float UnmitigatedStaminaDamage = StaminaDamage;
	
    if(EvaluationParameters.TargetTags->HasTag(FGameplayTag::RequestGameplayTag("Effect.Dodge")))
    {
        UE_LOG(LogTemp, Warning, TEXT("Character dodged."));
        UnmitigatedDamage = 0.f;
    }
    UE_LOG(LogTemp, Warning, TEXT("The hit happened from behind."));
    float MitigatedDamage = 0.0f;
	if(OverDamage > 0)
	{
		UnmitigatedDamage = UnmitigatedDamage + OverDamage;
	}
    if(Shield > 0)
    {
        MitigatedDamage = FMath::Abs(UnmitigatedDamage - Shield);
        Shield = FMath::Clamp(Shield - UnmitigatedDamage, 0.0f, Shield);
    }
    else
    {
        MitigatedDamage = UnmitigatedDamage;
    }
    float MitigatedStaminaDamage = UnmitigatedStaminaDamage;

	//If the target has blocking tag, reduce the damage by 100%.
	if(SourceTags->HasTag(FGameplayTag::RequestGameplayTag("State.Block.Shield")) && !AssetTags.HasTag(FGameplayTag::RequestGameplayTag("HitType.AoE")))
	{
		float Angle = UGASFunctionLibrary::FindDegreeFromSourceToTarget(SourceActor, TargetActor);
		if(Angle <= 45.0f || Angle >= -45.0f)
		{
			MitigatedDamage = 0.0f;
		}
		//MitigatedDamage = 0.0f;
	}

    if (MitigatedDamage > 0.f)
    {
        // Set the Target's damage meta attribute
        OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().DamageProperty, EGameplayModOp::Additive, MitigatedDamage));
    }
    if(MitigatedStaminaDamage > 0.f)
    {
        OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().StaminaDamageProperty, EGameplayModOp::Additive, MitigatedStaminaDamage));
    }
    if(Shield > 0.f)
    {
        OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().ShieldProperty, EGameplayModOp::Additive, Shield));
    }

    // Broadcast damages to Target ASC
    UGASAbilityComponent* TargetASC = Cast<UGASAbilityComponent>(TargetAbilitySystemComponent);
    if (TargetASC)
    {
        UGASAbilityComponent* SourceASC = Cast<UGASAbilityComponent>(SourceAbilitySystemComponent);
        TargetASC->ReceiveDamage(SourceASC, UnmitigatedDamage, MitigatedDamage);
        TargetASC->ReceiveDamage(SourceASC, UnmitigatedStaminaDamage, MitigatedStaminaDamage);
    }
}
