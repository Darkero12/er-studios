// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Character/GASCharacter.h"
#include "AI/AIController/GASAIController.h"
#include "GASSetup/Public/Components/GASAbilityComponent.h"
#include "GASSetup/Public/Components/GASAbilityQueueComponent.h"
#include "Game/AttributeSet/GASAttributeSet.h"
#include "Components/GASCharMoveComp.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Game/Abilities/GASAbility.h"
#include "Game/GameModes/GASGameMode.h"
#include "Player/PlayerController/GASPlayerController.h"

AGASCharacter::AGASCharacter(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer.SetDefaultSubobjectClass<UGASCharMoveComp>(ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AbilitySystemComponent = CreateDefaultSubobject<UGASAbilityComponent>(TEXT("AbilitySystemComponent"));
	
	AbilitySystemComponent->ReplicationMode = EGameplayEffectReplicationMode::Full;
	
	//AttributeSetBase = CreateDefaultSubobject<UGASAttributeSet>(TEXT("AttributeSetBase"));
	//AbilitySystemComponent->AddAttributeSetSubobject(AttributeSetBase);
	
	AbilityQueueComponent = CreateDefaultSubobject<UGASAbilityQueueComponent>("AbilityQueueComponent");
	//AbilityComboComponent = CreateDefaultSubobject<UAbilityComboManager>("AbilityComboManager");


	CollisionHandlerComponent = CreateDefaultSubobject<UCCCollisionHandlerComponent>("CollisionHandlerComponent");
	HitDirectionBackTag = FGameplayTag::RequestGameplayTag(FName("Effect.HitReact.Back"));
	HitDirectionFrontTag = FGameplayTag::RequestGameplayTag(FName("Effect.HitReact.Front"));
	HitDirectionLeftTag = FGameplayTag::RequestGameplayTag(FName("Effect.HitReact.Left"));
	HitDirectionRightTag = FGameplayTag::RequestGameplayTag(FName("Effect.HitReact.Right"));
	ReviveTag = FGameplayTag::RequestGameplayTag(FName("State.Revive"));
	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
}

// Called when the game starts or when spawned
void AGASCharacter::BeginPlay()
{
	Super::BeginPlay();

	//AIControllerClass = AGASAIController::StaticClass();

	if(AbilitySystemComponent)
	{
		HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).AddUObject(this, &AGASCharacter::HealthChanged);
		/*MaxHealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxHealthAttribute()).AddUObject(this, &AGASCharacter::MaxHealthChanged);
		ManaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaAttribute()).AddUObject(this, &AGASCharacter::ManaChanged);
		MaxManaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxManaAttribute()).AddUObject(this, &AGASCharacter::MaxManaChanged);
		ManaRegenRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaRegenRateAttribute()).AddUObject(this, &AGASCharacter::ManaRegenRateChanged);
		StaminaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaAttribute()).AddUObject(this, &AGASCharacter::StaminaChanged);
		MaxStaminaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxManaAttribute()).AddUObject(this, &AGASCharacter::MaxStaminaChanged);
		StaminaRegenRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaRegenRateAttribute()).AddUObject(this, &AGASCharacter::StaminaRegenRateChanged);*/
	}
	
}

// Called every frame
void AGASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void AGASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

UAbilitySystemComponent* AGASCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

bool AGASCharacter::IsAlive() const
{
	return GetHealth() > 0.0f;
}

int32 AGASCharacter::GetAbilityLevel(EAbilityInputID AbilityID) const
{
	return 1;
}

void AGASCharacter::RemoveCharacterAbilities()
{
	// Remove any abilities added from a previous call. This checks to make sure the ability is in the startup 'CharacterAbilities' array.
	TArray<FGameplayAbilitySpecHandle> AbilitiesToRemove;
	for (const FGameplayAbilitySpec& Spec : AbilitySystemComponent->GetActivatableAbilities())
	{
		if ((Spec.SourceObject == this) && CharacterAbilities.Contains(Spec.Ability->GetClass()))
		{
			AbilitiesToRemove.Add(Spec.Handle);
		}
	}

	// Do in two passes so the removal happens after we have the full list
	for (int32 i = 0; i < AbilitiesToRemove.Num(); i++)
	{
		AbilitySystemComponent->ClearAbility(AbilitiesToRemove[i]);
	}

	AbilitySystemComponent->CharacterAbilitiesGiven = false;
}

EHitReactDirection AGASCharacter::GetHitReactDirection(const FVector& ImpactPoint)
{
	const FVector& ActorLocation = GetActorLocation();
	// PointPlaneDist is super cheap - 1 vector subtraction, 1 dot product.
	float DistanceToFrontBackPlane = FVector::PointPlaneDist(ImpactPoint, ActorLocation, GetActorRightVector());
	float DistanceToRightLeftPlane = FVector::PointPlaneDist(ImpactPoint, ActorLocation, GetActorForwardVector());


	if (FMath::Abs(DistanceToFrontBackPlane) <= FMath::Abs(DistanceToRightLeftPlane))
	{
		// Determine if Front or Back

		// Can see if it's left or right of Left/Right plane which would determine Front or Back
		if (DistanceToRightLeftPlane >= 0)
		{
			return EHitReactDirection::Front;
		}
		else
		{
			return EHitReactDirection::Back;
		}
	}
	else
	{
		// Determine if Right or Left

		if (DistanceToFrontBackPlane >= 0)
		{
			return EHitReactDirection::Right;
		}
		else
		{
			return EHitReactDirection::Left;
		}
	}

	return EHitReactDirection::Front;
}

void AGASCharacter::PlayHitReact(FGameplayTag HitDirection, AActor* DamageCauser)
{
	if (IsAlive())
	{
		if (HitDirection == HitDirectionLeftTag)
		{
			ShowHitReact.Broadcast(EHitReactDirection::Left);
		}
		else if (HitDirection == HitDirectionFrontTag)
		{
			ShowHitReact.Broadcast(EHitReactDirection::Front);
		}
		else if (HitDirection == HitDirectionRightTag)
		{
			ShowHitReact.Broadcast(EHitReactDirection::Right);
		}
		else if (HitDirection == HitDirectionBackTag)
		{
			ShowHitReact.Broadcast(EHitReactDirection::Back);
		}
	}
}

int32 AGASCharacter::GetCharacterLevel() const
{
	return 1;
}

float AGASCharacter::GetHealth() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetHealth();
	}
	return 0.0f;
}

float AGASCharacter::GetMaxHealth() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetMaxHealth();
	}
	return 0.0f;
}

float AGASCharacter::GetMana() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetMana();
	}
	return 0.0f;
}

float AGASCharacter::GetMaxMana() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetMaxMana();
	}
	return 0.0f;
}

float AGASCharacter::GetStamina() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetStamina();
	}
	return 0.0f;
}

float AGASCharacter::GetMaxStamina() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetMaxStamina();
	}
	return 0.0f;
}

float AGASCharacter::GetMoveSpeed() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetMoveSpeed();
	}
	return 0.0f;
}

float AGASCharacter::GetMoveSpeedBaseValue() const
{
	if(IsValid(AttributeSetBase))
	{
		return AttributeSetBase->GetMoveSpeedAttribute().GetGameplayAttributeData(AttributeSetBase)->GetBaseValue();
	}
	return 0.0f;
}

void AGASCharacter::Revive()
{
	if(!AbilitySystemComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("%s() ASC invalid for %s()."), *FString(__FUNCTION__), *GetName());
		return;
	}
	
	SetHealth(GetMaxHealth());
	SetStamina(GetMaxStamina());
	SetMana(GetMaxMana());
	
	if(GetWorld() && IsPlayerControlled())
	{
		AGASGameMode* GM =  Cast<AGASGameMode>(GetWorld()->GetAuthGameMode());

		if(GM)
		{
			GM->IncreaseAliveCount();
		}
	}
	
	AbilitySystemComponent->SetTagMapCount(DeadTag, 0);
	AbilitySystemComponent->SetTagMapCount(ReviveTag, 0);

	CharacterReviveEvent();
}

void AGASCharacter::Die()
{
	if(GetWorld())
	{
		AGASGameMode* GM =  Cast<AGASGameMode>(GetWorld()->GetAuthGameMode());

		if(GM)
		{
			if(AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("State.KnockUp")) || AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("State.KnockDown")))
			{
				if (!bKnockUp) {
					//UE_LOG(LogTemp, Warning, TEXT("TAGS WORKING") );
					FTimerHandle TimerHandle_DeathDelay;
					FTimerDelegate Delegate;
					Delegate.BindUFunction(this, "DeathDelay_Elapsed");
					GetWorld()->GetTimerManager().SetTimer(TimerHandle_DeathDelay, Delegate, 1.5f, false);
					bKnockUp = true;
				}
				return;
			} 
			OnCharacterDied.Broadcast(this);
			CharacterDiedEvent();
			if (IsValid(AbilitySystemComponent))
			{
				AbilitySystemComponent->CancelAllAbilities();

				FGameplayTagContainer EffectTagsToRemove;
				EffectTagsToRemove.AddTag(EffectRemoveOnDeathTag);
				int32 NumEffectsRemoved = AbilitySystemComponent->RemoveActiveEffectsWithTags(EffectTagsToRemove);

				AbilitySystemComponent->AddLooseGameplayTag(ReviveTag);
				AbilitySystemComponent->AddLooseGameplayTag(DeadTag);
				UE_LOG(LogTemp, Warning, TEXT("TAGS dead running") );
			}
		}
	}
}

void AGASCharacter::RemoveCoolDownWithAbility(TSubclassOf<UGameplayAbility> InAbilityClass)
{
	auto ActivatableAbilities = AbilitySystemComponent->GetActivatableAbilities();
	for (const FGameplayAbilitySpec& AbilitySpec : ActivatableAbilities)
	{
		if (AbilitySpec.Ability->GetClass() == InAbilityClass)
		{
			auto Ability = Cast<UGASAbility>(AbilitySpec.Ability);
			if (Ability)
			{
				UGameplayEffect* CDGE = Ability->GetCooldownGameplayEffect();
				if (CDGE)
				{
					FGameplayTagContainer CooldownTags = CDGE->InheritableOwnedTagsContainer.CombinedTags;

					if (CooldownTags.Num() > 0)
					{
						FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(CooldownTags);
						AbilitySystemComponent->RemoveActiveEffects(Query, -1);
					}
				}
			}
		}
	}
}

float AGASCharacter::GetActiveEffectsTimeRemaining(FGameplayTagContainer Tag) const
{
	FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(Tag);
	auto Result = AbilitySystemComponent->GetActiveEffectsTimeRemaining(Query);
	if (Result.Num() > 0)
	{
		return Result[0];
	}

	return 0.f;
}

void AGASCharacter::FinishDying()
{
	UE_LOG(LogTemp, Error, TEXT("FinihDying called on CharacterBase."));
	GetWorldTimerManager().ClearTimer(DeathTimerHandle);
	Destroy();
}

void AGASCharacter::IncreaseComboCount()
{
	if(CurrentComboCount == MaxComboCount)
	{
		CurrentComboCount = 1;
		MaxComboReached();
		ComboReset();
	}
	else
	{
		CurrentComboCount++;
		ComboCountChange(CurrentComboCount);
	}
	GetWorldTimerManager().SetTimer(ComboTimerHandle,this, &AGASCharacter::ResetCombo, ComboWindow, false);
}

void AGASCharacter::ResetCombo()
{
	CurrentComboCount = 1;
	GetWorldTimerManager().ClearTimer(ComboTimerHandle);
	ComboReset();
}

void AGASCharacter::AddCharacterAbilities()
{
	if (!IsValid(AbilitySystemComponent) || AbilitySystemComponent->CharacterAbilitiesGiven)
	{
		return;
	}

	for (TSubclassOf<UGASAbility>& StartupAbility : CharacterAbilities)
	{
		AbilitySystemComponent->GiveAbility(
            FGameplayAbilitySpec(StartupAbility, GetAbilityLevel(StartupAbility.GetDefaultObject()->AbilityID), static_cast<int32>(StartupAbility.GetDefaultObject()->AbilityInputID), this));
	}

	AbilitySystemComponent->CharacterAbilitiesGiven = true;
}

void AGASCharacter::InitializeAttributes()
{
	if (!IsValid(AbilitySystemComponent))
	{
		return;
	}

	AttributeSetBase = AbilitySystemComponent->AttributeSetBase;
	
	if (!DefaultAttributes)
	{
		UE_LOG(LogTemp, Error, TEXT("%s() Missing DefaultAttributes for %s. Please fill in the character's Blueprint."), *FString(__FUNCTION__), *GetName());
		return;
	}

	// Can run on Server and Client
	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(DefaultAttributes, 1, EffectContext);
	if (NewHandle.IsValid())
	{
		FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
	}
}

void AGASCharacter::ResetCharacterAttributes()
{
	SetHealth(GetMaxHealth());
	SetStamina(GetMaxStamina());
	SetMana(GetMaxMana());
}

void AGASCharacter::AddStartupEffects()
{
	if (!AbilitySystemComponent || AbilitySystemComponent->StartupEffectsApplied)
	{
		return;
	}

	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	for (TSubclassOf<UGameplayEffect> GameplayEffect : StartupEffects)
	{
		FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffect, 1, EffectContext);
		if (NewHandle.IsValid())
		{
			FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
		}
	}

	AbilitySystemComponent->StartupEffectsApplied = true;
}

void AGASCharacter::HealthChanged(const FOnAttributeChangeData& Data)
{
	float Health = Data.NewValue;
	if(!IsAlive() && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
	{
		Die();
	}
}

void AGASCharacter::SetConstitution(float Constitution)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetConstitution(Constitution);
	}
}

void AGASCharacter::SetEndurance(float Endurance)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetEndurance(Endurance);
	}
}

void AGASCharacter::SetHealth(float Health)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetHealth(Health);
	}
}

void AGASCharacter::SetMaxHealth(float Health)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetMaxHealth(Health);
	}
}

void AGASCharacter::SetMana(float Mana)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetMana(Mana);
	}
}

void AGASCharacter::SetMaxMana(float MaxMana)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetMaxMana(MaxMana);
	}
}

void AGASCharacter::SetStamina(float Stamina)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetStamina(Stamina);
	}
}

void AGASCharacter::SetMaxStamina(float Stamina)
{
	if(IsValid(AttributeSetBase))
	{
		AttributeSetBase->SetMaxStamina(Stamina);
	}
}

void AGASCharacter::DeathDelay_Elapsed()
{
	OnCharacterDied.Broadcast(this);
	CharacterDiedEvent();
			
	if (IsValid(AbilitySystemComponent))
	{
		AbilitySystemComponent->CancelAllAbilities();

		FGameplayTagContainer EffectTagsToRemove;
		EffectTagsToRemove.AddTag(EffectRemoveOnDeathTag);
		int32 NumEffectsRemoved = AbilitySystemComponent->RemoveActiveEffectsWithTags(EffectTagsToRemove);

		AbilitySystemComponent->AddLooseGameplayTag(ReviveTag);
		AbilitySystemComponent->AddLooseGameplayTag(DeadTag);
		UE_LOG(LogTemp, Warning, TEXT("TAGS dead running") );
	}
	bKnockUp = false;
}

FName AGASCharacter::GetInputDirection_Implementation()
{
	return FName("Fwd");
}


