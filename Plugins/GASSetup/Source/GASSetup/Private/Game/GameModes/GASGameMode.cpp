// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GameModes/GASGameMode.h"
#include "GameFramework/SpectatorPawn.h"
#include "Player/PlayerCharacter/GASPlayerCharacter.h"

AGASGameMode::AGASGameMode()
{
}

void AGASGameMode::DecreaseAliveCount()
{
	AliveMembersCount = FMath::Clamp(AliveMembersCount - 1, 0, TotalMembersCount);

	MembersDown = FMath::Clamp(MembersDown + 1,0,TotalMembersCount);
		
	if(AliveMembersCount == 0)
	{
		PartyWipe();
	}

}

void AGASGameMode::IncreaseAliveCount()
{
	AliveMembersCount = FMath::Clamp(AliveMembersCount + 1, 0, TotalMembersCount);

	MembersDown = FMath::Clamp(MembersDown - 1,0,TotalMembersCount);
}

void AGASGameMode::BeginPlay()
{
	Super::BeginPlay();
}
