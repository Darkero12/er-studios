// Fill out your copyright notice in the Description page of Project Settings.


#include "GASSetup/Public/Components/GASAbilityComponent.h"
#include "Components/GASAbilityQueueComponent.h"
#include "Game/Character/GASCharacter.h"
#include "Logging/LogMacros.h"



void UGASAbilityComponent::BeginPlay()
{
	Super::BeginPlay();
	AbilityEndedCallbacks.AddUObject(this, &UGASAbilityComponent::OnAbilityEndedCallBack);
	AbilityFailedCallbacks.AddUObject(this, &UGASAbilityComponent::OnAbilityFailedCallBack);
}

void UGASAbilityComponent::InitAbilityActorInfo(AActor* InOwnerActor, AActor* InAvatarActor)
{
	Super::InitAbilityActorInfo(InOwnerActor, InAvatarActor);
	
	GrantDefaultAttributeSet(InOwnerActor, InAvatarActor);
}

void UGASAbilityComponent::GrantDefaultAttributeSet(AActor* InOwnerActor, AActor* InAvatarActor)
{
	const bool bHasAttributeSet = GetAttributeSubobject(AttributeSetClass) != nullptr;

	if (!bHasAttributeSet)
	{
		UAttributeSet* LAttributeSet = NewObject<UAttributeSet>(InOwnerActor, AttributeSetClass);
		AttributeSetBase = Cast<UGASAttributeSet>(LAttributeSet);
		
		// if (AttributeSetDefinition.InitializationData)
		// {
		// 	AttributeSet->InitFromMetaDataTable(AttributeSetDefinition.InitializationData);
		// }
		//AddedAttributes.Add(AttributeSet);
		AddAttributeSetSubobject(LAttributeSet);
	}
}

void UGASAbilityComponent::ReceiveDamage(UGASAbilityComponent* SourceASC, float UnmitigatedDamage,
                                         float MitigatedDamage)
{
	ReceivedDamage.Broadcast(SourceASC, UnmitigatedDamage, MitigatedDamage);
}

void UGASAbilityComponent::OnAbilityEndedCallBack(UGameplayAbility* Ability)
{
	OnAbilityEndedBP.Broadcast(Ability->AbilityTags);
	if(!Cast<AGASCharacter>(GetAvatarActor()))
	{
		UE_LOG(LogTemp, Warning, TEXT("Ability ended doesn't have an owner."));
		return;
	}
	UGASAbilityQueueComponent* AbilityQueueComponent = Cast<AGASCharacter>(GetAvatarActor())->AbilityQueueComponent;
	if (AbilityQueueComponent)
	{
		UGASAbility* GASAbility = Cast<UGASAbility>(Ability);
		AbilityQueueComponent->OnAbilityEnded(GASAbility);
	}
}

void UGASAbilityComponent::OnAbilityFailedCallBack(const UGameplayAbility* Ability, const FGameplayTagContainer& Tags)
{
	if(!Cast<AGASCharacter>(GetAvatarActor()))
	{
		UE_LOG(LogTemp, Warning, TEXT("Ability ended doesn't have an owner."));
		return;
	}
	UGASAbilityQueueComponent* AbilityQueueComponent = Cast<AGASCharacter>(GetAvatarActor())->AbilityQueueComponent;
	if (AbilityQueueComponent)
	{
		const UGASAbility* GASAbility = Cast<UGASAbility>(Ability);
		AbilityQueueComponent->OnAbilityFailed(GASAbility, Tags);
	}
}

void UGASAbilityComponent::GrantAbility(TSubclassOf<UGASAbility> Ability)
{
	GiveAbility(FGameplayAbilitySpec(Ability, 1, static_cast<int32>(Ability.GetDefaultObject()->AbilityInputID), this));
}


void UGASAbilityComponent::RemoveAbility(TSubclassOf<UGASAbility> Ability)
{
	for (FGameplayAbilitySpec GameplayAbilitySpec : GetActivatableAbilities())
	{
		if(GameplayAbilitySpec.Ability->GetClass() == Ability)
		{
			ClearAbility(GameplayAbilitySpec.Handle);
			return;
		}
		else
		{
			GLog->Logf(ELogVerbosity::Warning, TEXT("Ability to remove not found."));
		}
	}
}
