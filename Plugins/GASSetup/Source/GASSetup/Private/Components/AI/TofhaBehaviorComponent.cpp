// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/AI/TofhaBehaviorComponent.h"
#include "Game/Abilities/GASAbility.h"
#include "Game/Character/GASCharacter.h"
#include "Game/AttributeSet/GASAttributeSet.h"

// Sets default values for this component's properties
UTofhaBehaviorComponent::UTofhaBehaviorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

bool UTofhaBehaviorComponent::ExecuteOnAbilityQueue()
{

	if (!GetGASOwner())
	{
		return false;
	}

	if (!FirstAbility)
	{
		return false;
	}
	
	bool ret = GetGASOwner()->GetAbilitySystemComponent()->TryActivateAbilityByClass(FirstAbility);

	if (ret)
	{
		// This slides the queue forward if we successfully activate. If we don't, the queue doesn't change.
		FirstAbility = SecondAbility ? SecondAbility : nullptr;
		SecondAbility = nullptr;
	}
	return ret;
}


bool UTofhaBehaviorComponent::GetAreAbilitiesContained(TArray<TSubclassOf<UGASAbility>> InAbilities)
{
	for (TSubclassOf<UGASAbility> ability : InAbilities)
	{
		if (ability == FirstAbility || ability == SecondAbility)
		{
			return true;
		}
	}
	return false;
}

// Called when the game starts
void UTofhaBehaviorComponent::BeginPlay()
{
	Super::BeginPlay();
	// ...
	GASOwner = Cast<AGASCharacter>(GetOwner());
	
	if (!GASOwner)
	{
		return;
	}
	
	if (GetIsBoss() && !OrganizeBossStages())
	{
		BossSettings.bIsBoss = false;
	};

	if (GetIsBoss())
	{
		GetGASOwner()->OnCharacterTakeDamage.AddDynamic(this, &UTofhaBehaviorComponent::CheckForNewStage);
	}
	
}

// Called every frame
void UTofhaBehaviorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

AGASCharacter* UTofhaBehaviorComponent::GetGASOwner() const
{
	if (GASOwner)
	{
		return GASOwner;
	}
	return nullptr;
}

bool UTofhaBehaviorComponent::OrganizeBossStages()
{
	if (!BossSettings.bIsBoss) {return true;} // Should be 'successful' as we don't care what data is here if this isn't a boss class.

	for (auto& Stage: BossSettings.BossStages)
	{
		Stage.BossStage = 0;
	}
	
	// Check whether two boss stages have the same health trigger value, and set their stage in the editor to -1
	for (int i = 0; i < BossSettings.BossStages.Num() - 1; i++)
	{
		for (int n = i + 1; n < BossSettings.BossStages.Num(); n++)
		{
			if (BossSettings.BossStages[i] == BossSettings.BossStages[n])
			{
				BossSettings.BossStages[i].BossStage = -1;
				BossSettings.BossStages[n].BossStage = -1;
			}
		}
	}

	// If there are any conflicts set to -1, we need to abort the process so the designer can fix the issue.
	for (FBossStage& Stage : BossSettings.BossStages)
	{
		if (Stage.BossStage == -1)
		{
			UE_LOG(LogTemp, Error, TEXT(
				"There is a conflict between boss stages. Please check the stages on %s. Aborting organizing."
				), *GetOwner()->GetClass()->GetName())
			return false; // Fails, implied we're a boss closs, so should be handled during gameplay checking as well.
		}
	}
	
	BossSettings.BossStages.Sort([](const FBossStage& A, const FBossStage& B) {return B < A;});
	
	for (int i = 0; i < BossSettings.BossStages.Num(); i++)
	{
		BossSettings.BossStages[i].BossStage = i+1;
	}

	return true;
	
}

void UTofhaBehaviorComponent::CheckForNewStage(AGASCharacter* DamageCauser, AGASCharacter* DamageTaker)
{
	if (!DamageTaker)
	{
		return;
	}

	if (BossSettings.CurrentStage == BossSettings.BossStages.Num()) // Remove the dispatcher once we're on the final stage
	{
		GetGASOwner()->OnCharacterTakeDamage.RemoveDynamic(this, &UTofhaBehaviorComponent::CheckForNewStage);
	}

	OnNewStageCheck(BossSettings);
	
}


void UTofhaBehaviorComponent::SetNewStage(int InStage)
{

	BossSettings.CurrentStage = InStage;

	UsableAbilities = BossSettings.GetActiveStageAbilities();

	if (BossSettings.BossStages[InStage - 1].IntroductoryAbilities.Num() > 0)
	{
		int ArrayMax = BossSettings.BossStages[InStage - 1].IntroductoryAbilities.Num() - 1;
		AddAbilityByClass(BossSettings.BossStages[InStage - 1]
			.IntroductoryAbilities[FMath::RandRange(0, ArrayMax)], true);
	}
	
}

bool UTofhaBehaviorComponent::GetTargetChance(float& OutRoll) const
{
	float f = TargetingSettings.PercentageChanceOfAttack;
	OutRoll = FMath::FRand();
	return OutRoll <= f;
}

bool UTofhaBehaviorComponent::AddAbilityByClass(TSubclassOf<UGASAbility> InAbility, bool bOverrideSecondAbility)
{
	if (!InAbility)
	{
		return false;
	}

	if (FirstAbility)
	{
		// Refuses to replace any of the abilities, and we shouldn't ever have to if we design this right. We also shouldn't stack the same ability twice.
		if (FirstAbility->IsChildOf(UGASAbility::StaticClass()))
		{
			if (SecondAbility)
			{
				if (SecondAbility->IsChildOf(UGASAbility::StaticClass()) &&
				!bOverrideSecondAbility ||
				InAbility == SecondAbility ||
				InAbility == FirstAbility) // Avoids adding any ability class already in the queue 
					{
					return false;
					}
				SecondAbility = InAbility;
				return true;
			}
			FirstAbility = InAbility;
			return true;
		}
	}
	return false;
}

