// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/AI/TofhaCaptainComponent.h"

#include "GASFunctionLibrary.h"

#include "Components/AI/TofhaBehaviorComponent.h"

#include "Game/Character/GASCharacter.h"

// Sets default values for this component's properties
UTofhaCaptainComponent::UTofhaCaptainComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTofhaCaptainComponent::BeginPlay()
{
	Super::BeginPlay();
	
	GASOwner = Cast<AGASCharacter>(GetOwner());
	if (!GASOwner) {return;}
	BehaviorComponent = Cast<UTofhaBehaviorComponent>(GASOwner->FindComponentByClass<UTofhaBehaviorComponent>());
	if (!BehaviorComponent)
	{
		UE_LOG(LogTemp,Error,TEXT("CAPTAIN COMPONENT: BEHAVIOR NOT FOUND ON %s"), *GetOwner()->GetName());
		return;
	}

	if (Lifetime > 0.f && GASOwner)
	{
		GASOwner->GetWorld()->GetTimerManager().SetTimer(KillTimer, this, &UTofhaCaptainComponent::KillCaptainComponent, Lifetime);
	}

	// ...

	switch (CaptainEventTriggers.CaptainResponse)
	{
	case ECaptainResponseSelection::EPassive:
		GASOwner->GetWorld()->GetTimerManager().SetTimer(
			CaptainEventTriggers.OnPassiveTimerHandle, this,
			&UTofhaCaptainComponent::SetPassiveIsReady,
			CaptainEventTriggers.PassiveEventTimer,true
			);
		UGASFunctionLibrary::GrantAbilitiesToCharacter(CaptainEventTriggers.PassiveEvent, GASOwner);
		break;
	case ECaptainResponseSelection::EOnDied:
		GASOwner->OnCharacterDied.AddDynamic(this, &UTofhaCaptainComponent::CaptainDiedEvent);
		UGASFunctionLibrary::GrantAbilitiesToCharacter(CaptainEventTriggers.OnDiedEvent, GASOwner);
		break;
	case ECaptainResponseSelection::EOnHit:
		GASOwner->OnCharacterTakeDamage.AddDynamic(this, &UTofhaCaptainComponent::CaptainOnHitEvent);
		UGASFunctionLibrary::GrantAbilitiesToCharacter(CaptainEventTriggers.OnHitEvent, GASOwner);
		break;
	default:
		break;
	}
	
}


// Called every frame
void UTofhaCaptainComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (CaptainEventTriggers.bIsPassiveReady)
	{
		CaptainEventTriggers.bIsPassiveReady = !CaptainEventTriggers.bIsPassiveReady;
		if (BehaviorComponent)
		{
			BehaviorComponent->AddAbilityByClass(CaptainEventTriggers.PassiveEvent, false);
		}
	}
}

void UTofhaCaptainComponent::CaptainDiedEvent(AGASCharacter* Character)
{
	if (!GASOwner || !BehaviorComponent) {return;}
	GASOwner->OnCharacterDied.RemoveDynamic(this,&UTofhaCaptainComponent::CaptainDiedEvent);
	BehaviorComponent->AddAbilityByClass(CaptainEventTriggers.OnDiedEvent,true);
}

void UTofhaCaptainComponent::CaptainOnHitEvent(AGASCharacter* DamageCauser, AGASCharacter* DamageTaker)
{
	if (!GASOwner || !BehaviorComponent || !CaptainEventTriggers.bIsOnHitReady) {return;}
	CaptainHitByActor = DamageCauser;
	CaptainEventTriggers.bIsOnHitReady = !CaptainEventTriggers.bIsOnHitReady;
	BehaviorComponent->AddAbilityByClass(CaptainEventTriggers.OnHitEvent,false);
}

void UTofhaCaptainComponent::KillCaptainComponent()
{
	OnRemoveCaptainComponent.Broadcast();
	this->DestroyComponent();
}

