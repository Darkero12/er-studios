// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GASCharMoveComp.h"

#include "Game/Character/GASCharacter.h"

float UGASCharMoveComp::GetMaxSpeed() const
{
	AGASCharacter* Owner = Cast<AGASCharacter>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("%s() No Owner"), *FString(__FUNCTION__));
		return Super::GetMaxSpeed();
	}

	if (!Owner->IsAlive())
	{
		return 0.0f;
	}

	if (Owner->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun"))))
	{
		return 0.0f;
	}

	return Owner->GetMoveSpeed();
}
