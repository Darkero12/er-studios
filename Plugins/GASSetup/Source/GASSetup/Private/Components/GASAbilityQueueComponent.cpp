// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/GASAbilityQueueComponent.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

// Sets default values for this component's properties
UGASAbilityQueueComponent::UGASAbilityQueueComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	SetIsReplicatedByDefault(false);
}

// Called when the game starts
void UGASAbilityQueueComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	SetupOwner();
}

void UGASAbilityQueueComponent::SetupOwner()
{
	if(!GetOwner())
	{
		return;
	}

	OwnerPawn = Cast<APawn>(GetOwner());
	if(!OwnerPawn)
	{
		return;
	}

	IAbilitySystemInterface* AbilitySystemInterface = Cast<IAbilitySystemInterface>(OwnerPawn);
	if (!AbilitySystemInterface)
	{
		return;
	}

	OwnerAbilitySystemComponent = AbilitySystemInterface->GetAbilitySystemComponent();
}

void UGASAbilityQueueComponent::OpenAbilityQueue()
{
	if (!bAbilityQueueEnabled)
	{
		return;
	}

	bAbilityQueueOpened = true;
}

void UGASAbilityQueueComponent::CloseAbilityQueue()
{
	if (!bAbilityQueueEnabled)
	{
		return;
	}

	if (OwnerPawn)
	{
		UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueComponent:CloseAbilityQueue() Closing Ability Queue for %s"), *OwnerPawn->GetName());
	}

	bAbilityQueueOpened = false;

}

void UGASAbilityQueueComponent::UpdateAllowedAbilitiesForAbilityQueue(TArray<TSubclassOf<UGASAbility>> AllowedAbilities)
{
	if (!bAbilityQueueEnabled)
	{
		return;
	}

	QueuedAllowedAbilities = AllowedAbilities;

	// Notify Debug Widget if any is on screen
	//UpdateDebugWidgetAllowedAbilities();
}

void UGASAbilityQueueComponent::SetAllowAllAbilitiesForAbilityQueue(bool bAllowAllAbilities)
{
	if (!bAbilityQueueEnabled)
	{
		return;
	}

	bAllowAllAbilitiesForAbilityQueue = bAllowAllAbilities;

	//UpdateDebugWidgetAllowedAbilities();
}

bool UGASAbilityQueueComponent::IsAbilityQueueOpened() const
{
    return bAbilityQueueOpened;
}

bool UGASAbilityQueueComponent::IsAllAbilitiesAllowedForAbilityQueue() const
{
    return bAllowAllAbilitiesForAbilityQueue;
}

UGASAbility* UGASAbilityQueueComponent::GetCurrentQueuedAbility()
{
    return const_cast<UGASAbility*>(QueuedAbility);
}

TArray<TSubclassOf<UGASAbility>> UGASAbilityQueueComponent::GetQueuedAllowedAbilities() const
{
    return QueuedAllowedAbilities;
}

void UGASAbilityQueueComponent::OnAbilityEnded(const UGASAbility* Ability)
{
	UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueComponent::OnAbilityEnded() %s, ability queue enabled: %d, character queue enabled %d"), *Ability->GetName(), Ability->bEnableAbilityQueue ? 1 : -1, bAbilityQueueEnabled ? 1 : -1)

	if (Ability->bEnableAbilityQueue)
	{
		// Make sure to close the ability queue for abilities that have it enabled
		CloseAbilityQueue();
	}

	if (bAbilityQueueEnabled)
	{
		UE_LOG(LogTemp, Warning, TEXT("bAbilityQueueEnable is true."));
		if (QueuedAbilityClass)
		{
			UE_LOG(LogTemp, Error, TEXT("UGASAbilityQueueComponent::OnAbilityEnded() has a queued input: %s [AbilityQueueSystem]"), *QueuedAbilityClass->GetName())
			
			if (bAllowAllAbilitiesForAbilityQueue || QueuedAllowedAbilities.Contains(QueuedAbilityClass))
			{
				ResetAbilityQueueState();

				UE_LOG(LogTemp, Error, TEXT("UGASAbilityQueueComponent::OnAbilityEnded() %s is within Allowed Abilties, try activate [AbilityQueueSystem]"), *QueuedAbilityClass->GetName());
				if (OwnerAbilitySystemComponent)
				{
					OwnerAbilitySystemComponent->TryActivateAbilityByClass(QueuedAbilityClass);
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("Ability System is invalid."));
				}
			}
			else
			{
				ResetAbilityQueueState();
				UE_LOG(LogTemp, Verbose, TEXT("UGASAbilityQueueComponent::OnAbilityEnded() not allowed ability, do nothing: %s [AbilityQueueSystem]"), *QueuedAbilityClass->GetName());
			}
		}
		else
		{
			ResetAbilityQueueState();
		}
	}
}

void UGASAbilityQueueComponent::OnAbilityFailed(const UGASAbility* Ability, const FGameplayTagContainer& ReasonTags)
{
	UE_LOG(LogTemp, Verbose, TEXT("UGASAbilityQueueComponent::OnAbilityFailed() %s, Reason: %s"), *Ability->GetName(), *ReasonTags.ToStringSimple());
	OnAbilityFailedDelegate.Broadcast(Ability, ReasonTags);
	if (bAbilityQueueEnabled && bAbilityQueueOpened)
	{
		UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueComponent::OnAbilityFailed() Set QueuedAbility to %s"), *Ability->GetName())
		QueuedAbility = Ability;
	}
}
/*
UGASUWDebugAbilityQueue* UGASAbilityQueueComponent::GetDebugWidgetFromHUD()
{
	if (!OwnerPawn)
	{
		return nullptr;
	}

	// TODO: Cache HUDWidget ?
	APlayerController* PC = Cast<APlayerController>(OwnerPawn->GetController());
	if (!PC)
	{
		return nullptr;
	}

	AGASHUD* HUD = PC->GetHUD<AGASHUD>();
	if (!HUD)
	{
		return nullptr;
	}

	return Cast<UGASUWDebugAbilityQueue>(HUD->GetAbilityQueueWidget());
}
*/
void UGASAbilityQueueComponent::ResetAbilityQueueState()
{
	//UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueComponent::ResetAbilityQueueState()"))
	QueuedAbility = nullptr;
	bAllowAllAbilitiesForAbilityQueue = false;
	QueuedAllowedAbilities.Empty();

	// Notify Debug Widget if any is on screen
	//UpdateDebugWidgetAllowedAbilities();
}
/*
void UGASAbilityQueueComponent::UpdateDebugWidgetAllowedAbilities()
{
	UGASUWDebugAbilityQueue* DebugWidget = GetDebugWidgetFromHUD();
	if (!DebugWidget)
	{
		return;
	}

	DebugWidget->UpdateAllowedAbilities(QueuedAllowedAbilities);
}
*/
