// Fill out your copyright notice in the Description page of Project Settings.


#include "Debug/DebugCharacter.h"

// Sets default values
ADebugCharacter::ADebugCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AbilityComponent = CreateDefaultSubobject<UGASAbilityComponent>(TEXT("AbilityComponent"));
	AbilityComponent->ReplicationMode = EGameplayEffectReplicationMode::Full;
	
	AttributeSet = CreateDefaultSubobject<UGASAttributeSet>(TEXT("AttributeSet"));
	AbilityComponent->AddAttributeSetSubobject(AttributeSet);
}

// Called when the game starts or when spawned
void ADebugCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ADebugCharacter::SetHeath(float InHealth)
{
	if(IsValid(AttributeSet))
	{
		AttributeSet->SetHealth(InHealth);
	}
}

void ADebugCharacter::InitAttributes()
{
	if (!IsValid(AbilityComponent))
	{
		return;
	}

	if (!StarterAttributes)
	{
		UE_LOG(LogTemp, Error, TEXT("%s() Missing DefaultAttributes for %s. Please fill in the character's Blueprint."), *FString(__FUNCTION__), *GetName());
		return;
	}

	// Can run on Server and Client
	FGameplayEffectContextHandle EffectContext = AbilityComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	FGameplayEffectSpecHandle NewHandle = AbilityComponent->MakeOutgoingSpec(StarterAttributes, 1, EffectContext);
	if (NewHandle.IsValid())
	{
		FActiveGameplayEffectHandle ActiveGEHandle = AbilityComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilityComponent);
	}
}

// Called every frame
void ADebugCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADebugCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ADebugCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if(AbilityComponent)
	{
		AbilityComponent->InitAbilityActorInfo(this, this);
		InitAttributes();
	}
	
}

