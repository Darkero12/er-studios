// Fill out your copyright notice in the Description page of Project Settings.


#include "Debug/DebugPlayerCharacter.h"

#include "Components/GASAbilityComponent.h"

ADebugPlayerCharacter::ADebugPlayerCharacter(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
	
}

void ADebugPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if(AbilitySystemComponent)
	{
		AbilitySystemComponent->InitAbilityActorInfo(this, this);

		InitializeAttributes();
		AddStartupEffects();
		AddCharacterAbilities();
	}
}
