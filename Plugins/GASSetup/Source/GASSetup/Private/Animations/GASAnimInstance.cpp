// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/GASAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

void UGASAnimInstance::SetCacheValues(float DeltaSeconds)
{
	if(CharacterOwner && MovementComponent)
	{
		Acceleration = (CharacterOwner->GetVelocity()-Velocity)/DeltaSeconds;
		Velocity = CharacterOwner->GetVelocity();
		CurrentLocation = CharacterOwner->GetActorLocation();
		CurrentRotation = CharacterOwner->GetActorRotation();
		Speed = CharacterOwner->GetVelocity().Size();	

		UpSpeed = CurrentRotation.UnrotateVector(TryGetPawnOwner()->GetVelocity()).Z;
		ForwardSpeed = CurrentRotation.UnrotateVector(TryGetPawnOwner()->GetVelocity()).X;
		StrafeSpeed = CurrentRotation.UnrotateVector(TryGetPawnOwner()->GetVelocity()).Y;

		if(MovementComponent->GetMaxSpeed() > 0.0f)
		{
			NormalizedForwardSpeed = ForwardSpeed/MaxSpeed;
		}

		if(MovementComponent->GetMaxSpeed() > 0.0f)
		{
			NormalizedStrafeSpeed = StrafeSpeed/MaxSpeed;
		}
		
		bIsInAir = MovementComponent->IsFalling();
		RotationDelta = (CharacterOwner->GetBaseAimRotation() - CharacterOwner->GetActorRotation()).GetNormalized();

		AORotation = FMath::RInterpTo(FRotator(Pitch, Yaw, Roll),RotationDelta, DeltaSeconds, 15.0f);
		Yaw = FMath::ClampAngle(AORotation.Yaw,-90,90);
		Pitch = FMath::ClampAngle(AORotation.Pitch,-90,90);
		Roll = FMath::ClampAngle(AORotation.Roll,-90,90);

		if(Speed == 0.0)
		{
			bTurnLeft = CharacterOwner->bTurnLeft;
			bTurnRight = CharacterOwner->bTurnRight;
		}
		
		if(CharacterOwner->GetAbilitySystemComponent())
		{
			bIsBlocking = CharacterOwner->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("Effect.Block")));	
		}

		if(CharacterOwner->GetAbilitySystemComponent())
		{
			bIsAlive = CharacterOwner->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("State.Dead")));
		}
		if(!bIsInAir)
		{
			FVector RelativeAcceleration = CalculateRelativeAcceleration();
			LeanX = FMath::FInterpTo(LeanX, RelativeAcceleration.X, DeltaSeconds,LeanInterpSpeed)*LeanIntensityScaling;
			LeanY = FMath::FInterpTo(LeanY, RelativeAcceleration.Y, DeltaSeconds, LeanInterpSpeed)*LeanIntensityScaling;
			LastGroundLocation = CharacterOwner->GetActorLocation();

			if(Speed > 0 && Speed < JogThreshold)
			{
				CurrentLocomotionState = ELocomotionState::Walking;
			}
			else if(Speed > 300 && Speed < SprintThreshold)
			{
				CurrentLocomotionState = ELocomotionState::Jogging;
			}
			else if(FMath::IsNearlyZero(Speed) || Speed == 0)
			{
				CurrentLocomotionState = ELocomotionState::Idle;
			}
			else
			{
				CurrentLocomotionState = ELocomotionState::Sprinting;
			}
		}
		else
		{
			//left for implementing time syncing of jump animations later.
		}
		
		if(MovementComponent->GetCurrentAcceleration().Size() > 0.0)
		{
			bIsAccelerating = true;
			Direction = CalculateDirection(Velocity, CharacterOwner->GetActorRotation());
		}
		else
		{
			bIsAccelerating = false;
		}

		if(TempHitReactDirection != EHitReactDirection::None)
		{
			HitReactDirection = TempHitReactDirection;
			TempHitReactDirection = EHitReactDirection::None;
		}
		else
		{
			HitReactDirection = EHitReactDirection::None;
		}
	}
}

FVector UGASAnimInstance::CalculateRelativeAcceleration()
{
	if (FVector::DotProduct(Acceleration,Velocity) > 0.0f)
	{
		const float MaxAcc = MovementComponent->GetMaxAcceleration();
		return CharacterOwner->GetActorRotation().UnrotateVector(
			Acceleration.GetClampedToMaxSize(MaxAcc) / MaxAcc);
	}

	const float MaxBrakingDec = MovementComponent->GetMaxBrakingDeceleration();
	return
		CharacterOwner->GetActorRotation().UnrotateVector(
			Acceleration.GetClampedToMaxSize(MaxBrakingDec) / MaxBrakingDec);
}

void UGASAnimInstance::TurnInPlaceCheck(float DeltaSeconds)
{
	//Left for later implementation.
}

void UGASAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	CharacterOwner = Cast<AGASCharacter>(TryGetPawnOwner());
	if(CharacterOwner)
	{
		MovementComponent = Cast<UCharacterMovementComponent>(CharacterOwner->GetMovementComponent());
		//CharacterOwner->ShowHitReact.AddDynamic(this, &UFantasiaAnimInstance::SetHitReact); Invoke an ensure condition so commented for now.
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Character owner cast failed."))
	}
}

void UGASAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	SetCacheValues(DeltaSeconds);
}

void UGASAnimInstance::UpdateTurnRate(float DeltaSeconds)
{
	FRotator _delta = UKismetMathLibrary::NormalizedDeltaRotator(CurrentRotation, RotationLastTick);
	float _turn = _delta.Yaw;

	TurnRate = FMath::FInterpTo(TurnRate, _turn, DeltaSeconds, TurnInterpSpeed);
	RotationLastTick = CurrentRotation;
}

void UGASAnimInstance::SetWeaponStance(FGameplayTag InStance)
{
	CurrentWeaponStance = InStance;
}

void UGASAnimInstance::SetIsBlocking(bool InBool)
{
	bIsBlocking = InBool;
}

void UGASAnimInstance::SetHitReact(EHitReactDirection InHitReactDirection)
{
	TempHitReactDirection = InHitReactDirection;
}
