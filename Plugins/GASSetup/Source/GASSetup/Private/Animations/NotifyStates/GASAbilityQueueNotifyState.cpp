// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/NotifyStates/GASAbilityQueueNotifyState.h"

#include "Game/Character/GASCharacter.h"
#include "GASSetup/Public/Components/GASAbilityQueueComponent.h"


void UGASAbilityQueueNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueNotifyState:NotifyBegin()"))
	AActor* Owner = MeshComp->GetOwner();
	if (!Owner)
	{
		return;
	}
	if(!Cast<AGASCharacter>(Owner))
	{
		return;
	}
	UGASAbilityQueueComponent* AbilityQueueComponent = Cast<AGASCharacter>(Owner)->AbilityQueueComponent;
	if (!AbilityQueueComponent)
	{
		return;
	}

	if (!AbilityQueueComponent->bAbilityQueueEnabled)
	{
		return;
	}


	UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueNotifyState:NotifyBegin() Open Ability Queue for %d allowed abilities"), AllowedAbilities.Num());
	AbilityQueueComponent->OpenAbilityQueue();
	AbilityQueueComponent->SetAllowAllAbilitiesForAbilityQueue(bAllowAllAbilities);
	AbilityQueueComponent->UpdateAllowedAbilitiesForAbilityQueue(AllowedAbilities);

/*	UGASUWDebugAbilityQueue* DebugWidget = AbilityQueueComponent->GetDebugWidgetFromHUD();
	if (DebugWidget)
	{
		DebugWidget->AddAbilityQueueFromMontageRow(Animation);
	}*/
}

void UGASAbilityQueueNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	UE_LOG(LogTemp, Warning, TEXT("UGASAbilityQueueNotifyState:NotifyEnd()"))

	AActor* Owner = MeshComp->GetOwner();
	if (!Owner)
	{
		return;
	}

	if(!Cast<AGASCharacter>(Owner))
	{
		return;
	}

	UGASAbilityQueueComponent* AbilityQueueComponent = Cast<AGASCharacter>(Owner)->AbilityQueueComponent;
	if (!AbilityQueueComponent)
	{
		return;
	}

	if (!AbilityQueueComponent->bAbilityQueueEnabled)
	{
		return;
	}

	AbilityQueueComponent->CloseAbilityQueue();
}

FString UGASAbilityQueueNotifyState::GetNotifyName_Implementation() const
{
	return "AbilityQueueWindow";
}
