// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerStates/GASPlayerState.h"

#include "Player/PlayerCharacter/GASPlayerCharacter.h"

AGASPlayerState::AGASPlayerState()
{
	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UGASAbilityComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Mixed mode means we only are replicated the GEs to ourself, not the GEs to simulated proxies. If another GDPlayerState (Hero) receives a GE,
	// we won't be told about it by the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	// Create the attribute set, this replicates by default
	// Adding it as a subobject of the owning actor of an AbilitySystemComponent
	// automatically registers the AttributeSet with the AbilitySystemComponent
	AttributeSetBase = CreateDefaultSubobject<UGASAttributeSet>(TEXT("AttributeSetBase"));

	// Set PlayerState's NetUpdateFrequency to the same as the Character.
	// Default is very low for PlayerStates and introduces perceived lag in the ability system.
	// 100 is probably way too high for a shipping game, you can adjust to fit your needs.
	NetUpdateFrequency = 50.0f;

	// Cache tags
	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
	ReviveTag = FGameplayTag::RequestGameplayTag(FName("State.Revive"));
}

UAbilitySystemComponent* AGASPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

UGASAttributeSet* AGASPlayerState::GetAttributeSetBase() const
{
	return AttributeSetBase;
}

bool AGASPlayerState::IsAlive() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetHealth() > 0.0f;
	}
	return false;
}

float AGASPlayerState::GetConstitution() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetConstitution();
	}
	return 0.0f;
}

float AGASPlayerState::GetEndurance() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetEndurance();
	}
	return 0.0f;
}

float AGASPlayerState::GetHealth() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetHealth();
	}
	return 0.0f;
}

float AGASPlayerState::GetMaxHealth() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetMaxHealth();
	}
	return 0.0f;
}

float AGASPlayerState::GetMana() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetMana();
	}
	return 0.0f;
}

float AGASPlayerState::GetMaxMana() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetMaxMana();
	}
	return 0.0f;
}

float AGASPlayerState::GetManaRegenRate() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetManaRegenRate();
	}
	return 0.0f;
}

float AGASPlayerState::GetStamina() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetStamina();
	}
	return 0.0f;	
}

float AGASPlayerState::GetMaxStamina() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetMaxStamina();
	}
	return 0.0f;
}

float AGASPlayerState::GetStaminaRegenRate() const
{
	if(AttributeSetBase)
	{
		return AttributeSetBase->GetStaminaRegenRate();
	}
	return 0.0f;
}

void AGASPlayerState::BeginPlay()
{
	Super::BeginPlay();
	if (AbilitySystemComponent)
	{
		// Attribute change callbacks
		HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).AddUObject(this, &AGASPlayerState::HealthChanged);
		MaxHealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxHealthAttribute()).AddUObject(this, &AGASPlayerState::MaxHealthChanged);
		ManaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaAttribute()).AddUObject(this, &AGASPlayerState::ManaChanged);
		MaxManaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxManaAttribute()).AddUObject(this, &AGASPlayerState::MaxManaChanged);
		ManaRegenRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaRegenRateAttribute()).AddUObject(this, &AGASPlayerState::ManaRegenRateChanged);
		StaminaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaAttribute()).AddUObject(this, &AGASPlayerState::StaminaChanged);
		MaxStaminaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxManaAttribute()).AddUObject(this, &AGASPlayerState::MaxStaminaChanged);
		StaminaRegenRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaRegenRateAttribute()).AddUObject(this, &AGASPlayerState::StaminaRegenRateChanged);

		//Tag change callbacks.
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AGASPlayerState::StunTagChanged);
	}
}

void AGASPlayerState::HealthChanged(const FOnAttributeChangeData& Data)
{
	float Health = Data.NewValue;
	AGASPlayerCharacter* Hero = Cast<AGASPlayerCharacter>(GetPawn());
	
	if(!IsAlive() && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
	{
		if(Hero)
		{
			Hero->Die();
		}	
	}
}

void AGASPlayerState::MaxHealthChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::ManaChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::MaxManaChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::ManaRegenRateChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::StaminaChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::MaxStaminaChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::StaminaRegenRateChanged(const FOnAttributeChangeData& Data)
{
	//Implementation pending for now.
}

void AGASPlayerState::StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	if (NewCount > 0)
	{
		FGameplayTagContainer AbilityTagsToCancel;
		AbilityTagsToCancel.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability")));

		FGameplayTagContainer AbilityTagsToIgnore;
		AbilityTagsToIgnore.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.NotCanceledByStun")));

		AbilitySystemComponent->CancelAbilities(&AbilityTagsToCancel, &AbilityTagsToIgnore);
	}
}
