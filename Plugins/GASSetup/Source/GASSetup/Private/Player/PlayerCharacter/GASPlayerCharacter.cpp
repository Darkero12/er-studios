// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerCharacter/GASPlayerCharacter.h"
#include "AI/AIController/GASAIController.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/CapsuleComponent.h"
#include "Game/GameModes/GASGameMode.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Player/PlayerStates/GASPlayerState.h"

AGASPlayerCharacter::AGASPlayerCharacter(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(FName("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->SetRelativeLocation(FVector(0, 0, 68.492264));

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(FName("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom);
	FollowCamera->FieldOfView = 90.0f;
    
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionProfileName(FName("NoCollision"));

	AIControllerClass = AGASAIController::StaticClass();
    
	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
	ReviveTag = FGameplayTag::RequestGameplayTag(FName("State.Revive"));
	SwapTag = FGameplayTag::RequestGameplayTag(FName("State.Swapping"));
}

void AGASPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGASPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGASPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &AGASPlayerCharacter::LookUp);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGASPlayerCharacter::LookUpRate);
	PlayerInputComponent->BindAxis("Turn", this, &AGASPlayerCharacter::Turn);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGASPlayerCharacter::TurnRate);
}

void AGASPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	if(AbilitySystemComponent)
	{
	    if(!AbilitySystemComponent->HasMatchingGameplayTag(SwapTag) && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
	    {
			InitializeAttributes();
			AddStartupEffects();
			AddCharacterAbilities();

	    	AbilitySystemComponent->SetTagMapCount(DeadTag, 0);
	    	AbilitySystemComponent->SetTagMapCount(ReviveTag, 0);

	    	SetHealth(GetMaxHealth());
	    	SetStamina(GetMaxStamina());
	    	SetMana(GetMaxMana());
		}
		else
		{
			AbilitySystemComponent->RefreshAbilityActorInfo();
			BindAscInput();
		}
	}
	BindAscInput();
}

USpringArmComponent* AGASPlayerCharacter::GetCameraBoom()
{
	return CameraBoom;
}

UCameraComponent* AGASPlayerCharacter::GetFollowCamera()
{
	return FollowCamera;
}

float AGASPlayerCharacter::GetStartingCameraBoomArmLength()
{
	return  StartingCameraBoomArmLength;
}

FVector AGASPlayerCharacter::GetStartingCameraBoomLocation()
{
	return StartingCameraBoomLocation;
}

UGASAbilityComponent* AGASPlayerCharacter::RefreshCharacterRefs(AGASPlayerState* InPS,  UGASAbilityComponent* ASCRef)
{
	CachedASC = AbilitySystemComponent;
	if(InPS)
	{
		AbilitySystemComponent = Cast<UGASAbilityComponent>(InPS->GetAbilitySystemComponent());
		if(IsValid(AbilitySystemComponent))
		{
			AbilitySystemComponent->InitAbilityActorInfo(InPS, this);
			AbilitySystemComponent->RefreshAbilityActorInfo();
		}
		return CachedASC;
	}
	if(ASCRef)
	{
		AbilitySystemComponent = ASCRef;
		if(IsValid(AbilitySystemComponent))
		{
			AbilitySystemComponent->InitAbilityActorInfo(InPS, this);
			AbilitySystemComponent->RefreshAbilityActorInfo();
		}
		return CachedASC;
	}
	return nullptr;
}

void AGASPlayerCharacter::FinishDying()
{
	AGASGameMode* GM = Cast<AGASGameMode>(GetWorld()->GetAuthGameMode());
	UE_LOG(LogTemp, Error, TEXT("GameMode is valid."));
	if(GM )
	{
		GM->DecreaseAliveCount();
	}
	Super::FinishDying();
}

void AGASPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGASPlayerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AGASPlayerCharacter::LookUp(float Value)
{
	 if (IsAlive())
        {
            AddControllerPitchInput(Value);
        }
}

void AGASPlayerCharacter::LookUpRate(float Value)
{
	if (IsAlive())
	{
		AddControllerPitchInput(Value * BaseLookUpRate * GetWorld()->DeltaTimeSeconds);
		
	}
}

void AGASPlayerCharacter::Turn(float Value)
{
	if (IsAlive())
	{
		AddControllerYawInput(Value);
		if(GetVelocity().Size() == 0.0f)
		{
			if(Value > 0.3)
			{
				bTurnRight = true;
				bTurnLeft = false;
			}
			else
			{
				bTurnRight = false;
			}
			if(Value < -0.3)
			{
				bTurnRight = false;
				bTurnLeft = true;
			}
			else
			{
				bTurnLeft = false;
			}
		}
	}
}

void AGASPlayerCharacter::TurnRate(float Value)
{
	if (IsAlive())
	{
		AddControllerYawInput(Value * BaseTurnRate * GetWorld()->DeltaTimeSeconds);
	}
}

void AGASPlayerCharacter::MoveForward(float Value)
{
	AddMovementInput(UKismetMathLibrary::GetForwardVector(FRotator(0, GetControlRotation().Yaw, 0)), Value);
}

void AGASPlayerCharacter::MoveRight(float Value)
{
	AddMovementInput(UKismetMathLibrary::GetRightVector(FRotator(0, GetControlRotation().Yaw, 0)), Value);
}

void AGASPlayerCharacter::InitializeFloatingStatusBar()
{
	//Left to initialize and implement later.	
}

void AGASPlayerCharacter::BindAscInput()
{
	if (IsValid(AbilitySystemComponent) && IsValid(InputComponent))
	{
		AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, FGameplayAbilityInputBinds(FString("ConfirmTarget"),
            FString("CancelTarget"), FString("EAbilityInputID"), static_cast<int32>(EAbilityInputID::Confirm), static_cast<int32>(EAbilityInputID::Cancel)));

		ASCInputBound = true;
	}
}

void AGASPlayerCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	/*Super::NotifyActorBeginOverlap(OtherActor);

	const bool bIsListenServerOrStandalone = GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone;
	if (!HasAuthority() && !bIsListenServerOrStandalone)
	{
		return;
	}
	
	if (OtherActor->IsA(AGASCharacter::StaticClass()))
	{
		if (AGASCharacter* character = Cast<AGASCharacter>(OtherActor))
		{
			OverlappingCharacters.Emplace(character);
			GetWorld()->GetTimerManager().SetTimer(PushTimerHandle, this, &AGASPlayerCharacter::ApplyPersonalSpacePush, ForceApplicationTime, true, 0.f);
		}
	}*/
}

void AGASPlayerCharacter::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	const bool bIsListenServerOrStandalone = GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone;
	if (!HasAuthority() && !bIsListenServerOrStandalone)
	{
		return;
	}
	
	if (OtherActor->IsA(AGASCharacter::StaticClass()))
	{
		if (AGASCharacter* character = Cast<AGASCharacter>(OtherActor))
		{
			if (OverlappingCharacters.Contains(character))
			{
				OverlappingCharacters.RemoveSingle(Cast<AGASCharacter>(OtherActor));
			}

			if (OverlappingCharacters.Num() < 1)
			{
				GetWorld()->GetTimerManager().ClearTimer(PushTimerHandle);
			}
		}
	}
}

void AGASPlayerCharacter::ApplyPersonalSpacePush()
{
	for (AGASCharacter* character : OverlappingCharacters)
	{
		if (character)
		{
			// This is what pushes characters away from each other.
			
			FVector distanceBetweenCharacters = character->GetActorLocation() - this->GetActorLocation();
			//FVector distanceBetweenCharacters = this->GetActorLocation()-character->GetActorLocation();
			const float distance = distanceBetweenCharacters.Length();
			const float capsuleDiameter = character->GetCapsuleComponent()->GetScaledCapsuleRadius() * 2;//GetCapsuleComponent()->GetScaledCapsuleRadius() * 2;//character->GetCapsuleComponent()->GetScaledCapsuleRadius() * 2;
			const float distanceToMultiplyBy = FMath::Max(0.0f, (capsuleDiameter - distance) * DistanceMultiplier);
			 
			FVector direction = distanceBetweenCharacters;
			direction.Normalize();
			character->GetCharacterMovement()->AddForce(direction * PushForce * distanceToMultiplyBy);
			//this->GetCharacterMovement()->AddForce(direction * PushForce * distanceToMultiplyBy);
		}
	}
}