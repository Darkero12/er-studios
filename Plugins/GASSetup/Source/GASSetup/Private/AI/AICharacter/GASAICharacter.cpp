// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/AICharacter/GASAICharacter.h"
#include "Game/AttributeSet/GASAttributeSet.h"
#include "Components/CapsuleComponent.h"

AGASAICharacter::AGASAICharacter(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
}

void AGASAICharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AGASAICharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (IsValid(AbilitySystemComponent))
	{
		AttributeSetBase = AbilitySystemComponent->AttributeSetBase;
			
		InitializeAttributes();
		AddStartupEffects();
		AddCharacterAbilities();

		SetHealth(GetMaxHealth());
		SetStamina(GetMaxStamina());
		SetMana(GetMaxMana());
		
		// Attribute change callbacks
		HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).AddUObject(this, &AGASAICharacter::HealthChanged);

		// Tag change callbacks
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AGASAICharacter::StunTagChanged);
	}
}

void AGASAICharacter::HealthChanged(const FOnAttributeChangeData& Data)
{
	float Health = Data.NewValue;

	OnAIHealthChanged();

	if (!IsAlive() && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
	{
		Die();
	}
}

void AGASAICharacter::StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	if (NewCount > 0)
	{
		FGameplayTagContainer AbilityTagsToCancel;
		AbilityTagsToCancel.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability")));

		FGameplayTagContainer AbilityTagsToIgnore;
		AbilityTagsToIgnore.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.NotCanceledByStun")));

		AbilitySystemComponent->CancelAbilities(&AbilityTagsToCancel, &AbilityTagsToIgnore);
	}
}
