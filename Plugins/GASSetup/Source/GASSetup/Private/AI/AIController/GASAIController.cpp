// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/AIController/GASAIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Navigation/CrowdFollowingComponent.h"

AGASAIController::AGASAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{
	bWantsPlayerState = true;
//	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard"));
//	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree"));
}

void AGASAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

/*
	if (BehaviorTree == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Character %s is not assigned a tree"), *InPawn->GetName());
		return;
	}

	UBlackboardData* const BBData = BehaviorTree->BlackboardAsset;
	
	if (BBData == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Blackboard invalid on character %s"), *InPawn->GetName());
		return;
	}
	
	if (BlackboardComponent->InitializeBlackboard(*(BehaviorTree->BlackboardAsset)))
	{
		Blackboard = BlackboardComponent;
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("Blackboard invalid on character %s"), *InPawn->GetName());
		return;
	}

	keyTargetActor = BlackboardComponent->GetKeyID("TargetActor");
	keyTargetLocation = BlackboardComponent->GetKeyID("TargetLocation");

	BehaviorTreeComponent->StartTree(*BehaviorTree); */
}

/*
AActor* AGASAIController::GetTargetActorBK() const
{
	if (Blackboard)
	{
		AActor* ActorOut = Cast<AActor>(Blackboard->GetValue<UBlackboardKeyType_Object>(keyTargetActor));
		return ActorOut ? ActorOut : nullptr;
	}
	return nullptr;
}

FVector AGASAIController::GetTargetLocationBK() const
{
	if (Blackboard)
	{
		FVector VectorOut = Blackboard->GetValue<UBlackboardKeyType_Vector>(keyTargetLocation);
		return VectorOut;
	}

	return FVector(0.f, 0.f, 0.f);
}

void AGASAIController::SetTargetActorBK(AActor* InActor) const
{
	if (Blackboard)
	{
		Blackboard->SetValue<UBlackboardKeyType_Object>(keyTargetActor, InActor);
	}
}

void AGASAIController::SetTargetLocationBK(FVector InVector) const
{
	if (Blackboard)
	{
		Blackboard->SetValue<UBlackboardKeyType_Vector>(keyTargetLocation, InVector);
	}
} */
