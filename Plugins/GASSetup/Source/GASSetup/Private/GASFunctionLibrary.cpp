// Fill out your copyright notice in the Description page of Project Settings.


#include "GASFunctionLibrary.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "Animations/GASAnimInstance.h"
#include "Game/Character/GASCharacter.h"
#include "Player/PlayerCharacter/GASPlayerCharacter.h"

float UGASFunctionLibrary::FindDegreeFromSourceToTarget(const AActor* SourceActor, const AActor* TargetActor)
{
	if (!IsValid(SourceActor))
	{
		return 0.0f;
	}

	if (!IsValid(TargetActor))
	{
		return 0.0f;
	}

	const FVector FacingVector = (TargetActor->GetActorLocation() - SourceActor->GetActorLocation());
	return FindDegreeToHitNormal(SourceActor->GetActorRotation(), FacingVector);
}

float UGASFunctionLibrary::FindDegreeToHitNormal(const FRotator ActorRotation, const FVector HitNormal)
{
	const FMatrix RotMatrix = FRotationMatrix(ActorRotation);
	const FVector ForwardVec = RotMatrix.GetScaledAxis(EAxis::X);
	const FVector RightVector = RotMatrix.GetScaledAxis(EAxis::Y);
	const FVector ImpactNormal = HitNormal.GetSafeNormal2D(0.1f);

	const float ForwardCosAngle = FVector::DotProduct(ForwardVec, ImpactNormal);
	float ForwardDeltaDegree = FMath::RadiansToDegrees(FMath::Acos(ForwardCosAngle));

	const float RightCosAngle = FVector::DotProduct(RightVector, ImpactNormal);

	// Left
	if (RightCosAngle < 0.0f)
	{
		ForwardDeltaDegree = ForwardDeltaDegree * -1.0f;
	}

	return ForwardDeltaDegree;
}

bool UGASFunctionLibrary::AddLooseGameplayTagsToActor(AActor* Actor, const FGameplayTagContainer GameplayTags)
{
	UAbilitySystemComponent* AbilitySystemComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Actor);

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->AddLooseGameplayTags(GameplayTags);
		return true;
	}

	return false;
}

bool UGASFunctionLibrary::RemoveLooseGameplayTagsFromActor(AActor* Actor, const FGameplayTagContainer GameplayTags)
{
	UAbilitySystemComponent* AbilitySystemComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Actor);

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->RemoveLooseGameplayTags(GameplayTags);
		return true;
	}

	return false;
}

UAbilitySystemComponent* UGASFunctionLibrary::GetAbilitySystemComponentFromCharacter(AActor* Actor)
{
	const AGASCharacter* GASCharacter = Cast<AGASCharacter>(Actor);
	return  GASCharacter->GetAbilitySystemComponent();
}

AGASCharacter* UGASFunctionLibrary::RecastToGASCharacter(AActor* Actor)
{
	AGASCharacter* GASCharacter = Cast<AGASCharacter>(Actor);
	return GASCharacter;
}

AGASPlayerCharacter* UGASFunctionLibrary::RecastToGASPlayerCharacter(AActor* Actor)
{
	AGASPlayerCharacter* GASCharacter = Cast<AGASPlayerCharacter>(Actor);
	return GASCharacter;
}

AGASPlayerState* UGASFunctionLibrary::RecastToGASPlayerState(APlayerState* PlayerState)
{
	return Cast<AGASPlayerState>(PlayerState);
}

bool UGASFunctionLibrary::IsTargetBlocking(AGASCharacter* TargetCharacter)
{
	if(TargetCharacter)
	{
		if(TargetCharacter->GetAbilitySystemComponent())
		{
			if(TargetCharacter->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("State.Block.Shield")))
			{
				return true;
			}
		}
	}
	return false;
}

bool UGASFunctionLibrary::GrantAbilitiesToCharacter(TSubclassOf<UGASAbility> Ability,
	AGASCharacter* InCharacter)
{
	if(IsValid(InCharacter))
	{
		if(IsValid(InCharacter->GetAbilitySystemComponent()))
		{
			UGASAbilityComponent* ASC = Cast<UGASAbilityComponent>(InCharacter->GetAbilitySystemComponent());

			TArray<FGameplayAbilitySpec> GrantedSpecs = ASC->GetActivatableAbilities();
			for (FGameplayAbilitySpec GrantedSpec : GrantedSpecs)
			{
				if(Ability == GrantedSpec.Ability->GetClass())
				{
					return false;
				}
			}
			ASC->GrantAbility(Ability);
			return true;
		}
		return false;
	}
	return false;
}

EHitReactDirection UGASFunctionLibrary::GetHitDirection(const AActor* SourceActor, const AActor* TargetActor)
{
	if (!SourceActor || !TargetActor) {
		return EHitReactDirection::Back;
	}


	FVector RelativeVector = SourceActor->GetActorLocation() - TargetActor->GetActorLocation();

	FVector HitDirection = RelativeVector.GetUnsafeNormal();

	float forwardDot = FVector::DotProduct(TargetActor->GetActorForwardVector(), HitDirection);
	float rightDot = FVector::DotProduct(TargetActor->GetActorRightVector(), HitDirection);
	if (forwardDot >= 0.707f)
		return EHitReactDirection::Front;
	else if (rightDot >= 0.707f)
		return EHitReactDirection::Right;
	else if (rightDot <= -0.707f)
		return EHitReactDirection::Left;
	else
		return EHitReactDirection::Back;
}

bool UGASFunctionLibrary::QueueMontage(UAnimInstance* AnimInstance, UAnimMontage* Montage)
{
	if(!AnimInstance || !Montage)
	{
		return false;
	}
	UGASAnimInstance* GAnimInstance = Cast<UGASAnimInstance>(AnimInstance);
	if(GAnimInstance)
	{
		GAnimInstance->QueuedMontage = Montage;
		return true;	
	}
	return false;
}

bool UGASFunctionLibrary::ShouldHappen(float InPercentage)
{
	return (FMath::RandRange(1, 100) <= InPercentage ? true : false);
}

void UGASFunctionLibrary::DestroyController(AController* InController)
{
	if (InController)
	{
		InController->Destroy();
	}
}

void UGASFunctionLibrary::Visibility_GetRenderedActors(TArray<AActor*>& CurrentlyRenderedActors, float MinRecentTime)
{
	CurrentlyRenderedActors.Empty();

	for (TObjectIterator<AActor> Itr;Itr;++Itr)
	{
		AActor* Actor = *Itr;
		if (Actor->GetLastRenderTime() > MinRecentTime)
		{
			CurrentlyRenderedActors.Add(Actor);
		}
	}
}

