// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/Character/GASCharacter.h"
#include "DebugPlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API ADebugPlayerCharacter : public AGASCharacter
{
	GENERATED_BODY()

	ADebugPlayerCharacter(const FObjectInitializer &ObjectInitializer);
	
public:
	virtual void PossessedBy(AController* NewController) override;
};
