// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/GASAbilityComponent.h"
#include "Game/AttributeSet/GASAttributeSet.h"
#include "GameFramework/Character.h"
#include "DebugCharacter.generated.h"

UCLASS()
class GASSETUP_API ADebugCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADebugCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	UGASAbilityComponent* AbilityComponent;

	UPROPERTY()
	UGASAttributeSet* AttributeSet;

	void SetHeath(float InHealth);
	
	void InitAttributes();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UGameplayEffect> StarterAttributes;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PossessedBy(AController* NewController) override;

};
