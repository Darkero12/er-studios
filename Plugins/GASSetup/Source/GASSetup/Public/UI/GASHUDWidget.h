// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GASHUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API UGASHUDWidget : public UUserWidget
{
	GENERATED_BODY()
	
};
