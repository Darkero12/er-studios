// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

UENUM(BlueprintType)
enum class EHitReactDirection : uint8
{
	// 0
	None			UMETA(DisplayName = "None"),
    // 1
    Left 			UMETA(DisplayName = "Left"),
    // 2
    Front 			UMETA(DisplayName = "Front"),
    // 3
    Right			UMETA(DisplayName = "Right"),
    // 4
    Back			UMETA(DisplayName = "Back")
};

UENUM(BlueprintType)
enum class EAbilityInputID : uint8
{
	// 0 None
	None			UMETA(DisplayName = "None"),
    // 1 Confirm
    Confirm			UMETA(DisplayName = "Confirm"),
    // 2 Cancel
    Cancel			UMETA(DisplayName = "Cancel"),
    // 3 LMB
	LightAttacks	UMETA(DisplayName = "LightAttacks"),

	HeavyAttacks	UMETA(DisplayName = "HeavyAttacks"),

	Block			UMETA(DisplayName = "Block"),

	// Left Ctrl
    Ability1		UMETA(DisplayName = "Ability1"),

	// RMB
    Ability2		UMETA(DisplayName = "Ability2"),

	// Q
    Ability3		UMETA(DisplayName = "Ability3"),

	// E
    Ability4		UMETA(DisplayName = "Ability4"),
    
    Ability5		UMETA(DisplayName = "Ability5"),
    
    Sprint			UMETA(DisplayName = "Sprint"),
    
    Jump			UMETA(DisplayName = "Jump")
};

class FGASSetupModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
