// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GASSetup.h"
#include "Animation/AnimInstance.h"
#include "Game/Character/GASCharacter.h"
#include "GASAnimInstance.generated.h"

UENUM(BlueprintType)
enum class ELocomotionState : uint8
{
	Idle = 0		UMETA(DisplayName = "Idle"),
	Walking			UMETA(DisplayName = "Walking"),
	Jogging			UMETA(DisplayName = "Jogging"),
	Sprinting		UMETA(DisplayName = "Sprinting")
};

/**
 * 
 */
UCLASS()
class GASSETUP_API UGASAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

	protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "GAS|Read Only Values")
	AGASCharacter* CharacterOwner;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "GAS|Read Only Values")
	UCharacterMovementComponent* MovementComponent;

	//Current Locomotion Type
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	ELocomotionState CurrentLocomotionState;

	//Current speed of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float Speed = 0.0f;

	//Current speed of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config")
	float MaxSpeed = 475.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float UpSpeed = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float ForwardSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float NormalizedForwardSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float StrafeSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float NormalizedStrafeSpeed = 0.0f;
	
	//Current direction of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
    float Direction = 0.0f;

	//Current velocity of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FVector Velocity;

	//Current acceleration of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FVector Acceleration;
	
	//Current location of the character.
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
    FVector CurrentLocation;

	//Current rotation of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FRotator CurrentRotation;

	//AO rotation of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FRotator AORotation;

	//Last tick rotation of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FRotator RotationLastTick;

	//Last location of the character on ground.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FVector LastGroundLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "GAS|Read Only Values")
	EHitReactDirection TempHitReactDirection;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "GAS|Read Only Values")
	EHitReactDirection HitReactDirection;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "GAS|Read Only Values")
	bool bTurnRight = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "GAS|Read Only Values")
	bool bTurnLeft = false;
	
	//Aim pitch for AO.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float Pitch  = 0.0f;

	//Aim yaw for AO.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float Yaw  = 0.0f;

	//Aim roll for AO.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float Roll = 0.0f;

	//Aim YawDelta for AO.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float YawDelta = 0.0f;

	//Aim PitchDelta for AO.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float PitchDelta = 0.0f;

	//Aim YawDelta for AO.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float RollDelta = 0.0f;

	//Lean intensity scaling.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values|Leans")
	float LeanIntensityScaling = 1;

	//Lean Interp speed when it goes to 0 from a value while in motion.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values|Leans")
	float LeanInterpSpeed = 4.0;
	
	//Boolean that tells if character is in air.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	bool bIsInAir = false;

	//Rotation Delta
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	FRotator RotationDelta;
	
	//Lean Rotator for spine
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values|Leans")
	float LeanX = 0.0f;

	//Lean Rotator for root
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	float LeanY = 0.0f;

	//Turn Rate for the character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values|Turn")
	float TurnRate = 0.0f;

	//Turn Interp Speed for the character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values|Turn")
	float TurnInterpSpeed = 8.f;

	//Boolean that tells if the character is accelerating.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	bool bIsAccelerating = false;

	//Current weapon stace to switch the upper body pose with.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values|Stances")
	FGameplayTag CurrentWeaponStance;
	
	//Speed below which character state is walking.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config Values")
	float JogThreshold = 0.0f;

	//Speed below which character state is jogging.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config Values")
	float SprintThreshold = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config Values|Snapping")
	float RotationSnapMulti = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config Values|Snapping")
	float MaxSnapMulti = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config Values|Snapping")
	float PreferredDistance = 120.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Config Values|Snapping")
	bool bIsSnapping = false;

	void SetCacheValues(float DeltaSeconds);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	bool bIsBlocking = false;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Cache Values")
	bool bIsAlive = true;
	
	FVector CalculateRelativeAcceleration();

	void TurnInPlaceCheck(float DeltaSeconds);

public:

	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	void UpdateTurnRate(float DeltaSeconds);

	/*float CalculateSnapMultiplier(float DeltaSeconds);
	
	FVector CalculateSnapDirection(float DeltaSeconds);

	void Snap(float DeltaSeconds);*/

	UFUNCTION(BlueprintCallable, Category = "GAS|Animation|Stances")
	void SetWeaponStance(FGameplayTag InStance);

	UFUNCTION(BlueprintCallable, Category = "GAS|Animation|Stances")
	void SetIsBlocking(bool InBool);

	UFUNCTION(BlueprintCallable, Category = "GAS|Animation|HitReact")
	void SetHitReact(EHitReactDirection InHitReactDirection);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Montage Queuing")
	UAnimMontage* QueuedMontage = nullptr;
	
	/*UFUNCTION(BlueprintCallable, Category = "GAS|Animation|Audio")
	void PlaySurfaceSound(int SurfaceIndex);*/
};
