// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "Game/Abilities/GASAbility.h"
#include "Game/AttributeSet/GASAttributeSet.h"

#include "GASAbilityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FReceivedDamageDelegate, class UGASAbilityComponent*, SourceASC, float, UnmitigatedDamage, float, MitigatedDamage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAbilityEndedBP, FGameplayTagContainer, AbilityTags);
                                              
/**
 * 
 */
UCLASS()
class GASSETUP_API UGASAbilityComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	virtual void InitAbilityActorInfo(AActor* InOwnerActor, AActor* InAvatarActor) override;

	void GrantDefaultAttributeSet(AActor* InOwnerActor, AActor* InAvatarActor);

	
	
	bool CharacterAbilitiesGiven = false;
	bool StartupEffectsApplied = false;
	
	UPROPERTY(BlueprintAssignable)
	FAbilityEndedBP OnAbilityEndedBP;
	
	FReceivedDamageDelegate ReceivedDamage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "TOFHA|Attributes")
	TSubclassOf<UGASAttributeSet> AttributeSetClass = UGASAttributeSet::StaticClass();

	UPROPERTY(BlueprintReadOnly)
	class UGASAttributeSet* AttributeSetBase;

	// Called from GDDamageExecCalculation. Broadcasts on ReceivedDamage whenever this ASC receives damage.
	virtual void ReceiveDamage(UGASAbilityComponent* SourceASC, float UnmitigatedDamage, float MitigatedDamage);

	virtual void OnAbilityEndedCallBack(UGameplayAbility* Ability);

	virtual void OnAbilityFailedCallBack(const UGameplayAbility* Ability, const FGameplayTagContainer& Tags);

	/*
	UFUNCTION(BlueprintCallable, Category = "Fantasia|Abilities")
	void TryActivateAbilityByInputID(EGDAbilityInputID InInputID);
	*/

	UFUNCTION(BlueprintCallable, Category = "GAS|Abilities")
    void GrantAbility(TSubclassOf<UGASAbility> Ability);


	UFUNCTION(BlueprintCallable, Category = "GAS|Abilities")
    void RemoveAbility(TSubclassOf<UGASAbility> Ability);
};
