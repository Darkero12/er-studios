// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Game/Abilities/GASAbility.h"
#include "GameplayTagContainer.h"
#include "GASAbilityQueueComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAbilityFailedDelegate, const UGASAbility*, Ability, const FGameplayTagContainer&, ReasonTags);

class UAbilitySystemComponent;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GASSETUP_API UGASAbilityQueueComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGASAbilityQueueComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	UPROPERTY(BlueprintReadOnly, Category = "GAS|Components")
	APawn* OwnerPawn;

	UPROPERTY(BlueprintReadOnly, Category = "GAS|Components")
	UAbilitySystemComponent* OwnerAbilitySystemComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Ability Queue System")
	bool bAbilityQueueEnabled = true;

	/** Setup GetOwner to character and sets references for ability system component and the owner itself. */
	UFUNCTION(BlueprintCallable)
	void SetupOwner();

    /**
     * Set the bAbilityQueue to true and opens the ability queue system for activation
     */
    void OpenAbilityQueue();

    /**
     * Set the bAbilityQueue to false which prevents the ability queue system to activate
     */
    void CloseAbilityQueue();

    /**
     * Updates the Allowed Abilities for the ability queue system
     */
    void UpdateAllowedAbilitiesForAbilityQueue(TArray<TSubclassOf<UGASAbility>> AllowedAbilities);

	/**
	 * Updates the bQueueAllowAllAbilities which prevents the check for queued abilities to be within the QueuedAllowedAbilities array
	 */
	void SetAllowAllAbilitiesForAbilityQueue(bool bAllowAllAbilities);

	bool IsAbilityQueueOpened() const;

	bool IsAllAbilitiesAllowedForAbilityQueue() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
    UGASAbility* GetCurrentQueuedAbility();

    TArray<TSubclassOf<UGASAbility>> GetQueuedAllowedAbilities() const;

	/**
	* Called when an ability is ended for the owner actor.
	*
	* The native implementation handles the ability queuing system, and invoke related BP event.
	*/
	void OnAbilityEnded(const UGASAbility* Ability);

	/**
	* Called when an ability failed to activated for the owner actor, passes along the failed ability
	* and a tag explaining why.
	*
	* The native implementation handles the ability queuing system, and invoke related BP event.
	*/
	void OnAbilityFailed(const UGASAbility* Ability, const FGameplayTagContainer& ReasonTags);

	/**
	 * Returns the Debug Widget from HUD associated with this character (if any)
	 */
	//virtual UGASUWDebugAbilityQueue* GetDebugWidgetFromHUD();

protected:

	/** Ability Queue System */

	bool bAbilityQueueOpened = false;
	bool bAllowAllAbilitiesForAbilityQueue = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Ability|Queue")
	const UGASAbility* QueuedAbility = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAS|Ability|Queue")
	const TSubclassOf<UGASAbility> QueuedAbilityClass = nullptr;

	UPROPERTY(BlueprintAssignable, Category = "GAS|Ability|Queue")
	FOnAbilityFailedDelegate OnAbilityFailedDelegate;
	
	TArray<TSubclassOf<UGASAbility>> QueuedAllowedAbilities;

	/**
	* Reset all variables involved in the Ability Queue System to their original default values.
	*/
	virtual void ResetAbilityQueueState();

	/**
	* Notify Debug Ability Queue Widget by updating its allowed abilities
	*/
	//virtual void UpdateDebugWidgetAllowedAbilities();
		
};
