// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "WeakSpot.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class GASSETUP_API UWeakSpot : public USphereComponent
{
	GENERATED_BODY()
	
};
