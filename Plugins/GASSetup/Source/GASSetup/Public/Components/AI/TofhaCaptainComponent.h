// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Game/Character/GASCharacter.h"
#include "Game/Data/TofhaData.h"
#include "TofhaCaptainComponent.generated.h"

class AGASCharacter;
class UTofhaBehaviorComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCaptainComponentRemoved);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class GASSETUP_API UTofhaCaptainComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTofhaCaptainComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AGASCharacter* GetGASOwner() const {return GASOwner;}

	UFUNCTION()
	void SetPassiveIsReady(){CaptainEventTriggers.EnablePassive();}

	UFUNCTION()
	void SetOnHitIsReady(){CaptainEventTriggers.EnableOnHit();}
	
	UFUNCTION(BlueprintPure, Category="CaptainEvents")
	AGASCharacter* GetCaptainWasHitActor() const {return CaptainHitByActor;}

protected:
	UFUNCTION()
	void CaptainDiedEvent(AGASCharacter* Character);

	UFUNCTION()
	void CaptainOnHitEvent(AGASCharacter* DamageCauser, AGASCharacter* DamageTaker);

private:
	UFUNCTION()
	void KillCaptainComponent();

	/////////////////////////////////
	/////////// VARIABLES ///////////
	/////////////////////////////////
	
public:
	/** The length of time we want to keep the component on the character.
	 * Values of 0 or lower means infinite lifetime.
	 */
	UPROPERTY(EditAnywhere,BlueprintReadOnly,meta=(ExposeOnSpawn))
	float Lifetime = -1.f;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FCaptainComponentRemoved OnRemoveCaptainComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FCaptainEventTriggers CaptainEventTriggers;

private:
	UPROPERTY()
	AGASCharacter* GASOwner;

	UPROPERTY()
	FTimerHandle KillTimer;

	UPROPERTY()
	AGASCharacter* CaptainHitByActor;
	

	UPROPERTY()
	UTofhaBehaviorComponent* BehaviorComponent;
		
};
