// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Game/Character/GASCharacter.h"
#include "Game/Data/TofhaData.h"
#include "TofhaBehaviorComponent.generated.h"

class AGASCharacter;
class UGASAbility;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class GASSETUP_API UTofhaBehaviorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTofhaBehaviorComponent();

	/**
	 * Execute the queue, specifically the FirstAbility. If there is only one ability in the stack,
	 * then we will automatically nullptr the FirstAbility; Otherwise, the SecondAbility becomes
	 * the FirstAbility;
	 * @return Whether we successfully execute the first queued ability
	 * 
	 */
	UFUNCTION(BlueprintCallable)
	bool ExecuteOnAbilityQueue();

	/**
	 * Get the abilities that are currently in the queue.
	 * @param OutFirstAbility The first ability listed in the queue. Returns nullptr if there's no ability there.
	 * @param OutSecondAbility The second ability listed in the queue. Returns nullptr if there's no ability there.
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	void GetAbilitiesInQueue(TSubclassOf<UGASAbility>& OutFirstAbility, TSubclassOf<UGASAbility>& OutSecondAbility)
	{OutFirstAbility = FirstAbility; OutSecondAbility = SecondAbility;}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsBoss() const {return BossSettings.bIsBoss;}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentStage() const {return BossSettings.CurrentStage;}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetNextStage() const {return BossSettings.CurrentStage >= BossSettings.BossStages.Num() ? BossSettings.CurrentStage : BossSettings.CurrentStage + 1;}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetHealthPercentage() {return (GetGASOwner()->GetHealth() / GetGASOwner()->GetMaxHealth()) * 100;}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<TSubclassOf<UGASAbility>> GetUsableAbilities() const {return UsableAbilities;}

	UFUNCTION(BlueprintImplementableEvent, meta =(DisplayName = "On New Stage Check"))
	void OnNewStageCheck(FBossSettings InBossSettings);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetAreAbilitiesContained(TArray<TSubclassOf<UGASAbility>> InAbilities);

	UFUNCTION(BlueprintCallable)
	bool AddAbilityByClass(TSubclassOf<UGASAbility> InAbility, bool bOverrideSecondAbility);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "TOFHA|AI")
	AGASCharacter* GetGASOwner() const;

	UFUNCTION(BlueprintCallable, Category = "TOFHA|AI|BossSettings")
	bool OrganizeBossStages();

	UFUNCTION(Category="TOFHA|AI|BossSettings")
	void CheckForNewStage(AGASCharacter* DamageCauser, AGASCharacter* DamageTaker);

	UFUNCTION(BlueprintCallable)
	void SetNewStage(int InStage);

	/**
	 * Executes the random chance generation for targeting chances.
	 * Returns bool for success or failure of the chance according to targeting settings
	 * Also returns the chance rolled.
	 */
	UFUNCTION(BlueprintPure)
	bool GetTargetChance (float& OutRoll) const;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	///////////////////////////////////////
	////////////// VARIABLES //////////////
	///////////////////////////////////////
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "TOFHA|AI|Behavior")
	float BehaviorCrossoverRange = 500.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TOFHA|AI")
	FBossSettings BossSettings;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TOFHA|AI")
	FTargetingSettings TargetingSettings;

private:
	UPROPERTY()
	AGASCharacter* GASOwner;

	UPROPERTY()
	TArray<TSubclassOf<UGASAbility>> UsableAbilities;

	UPROPERTY()
	TSubclassOf<UGASAbility> FirstAbility;

	UPROPERTY()
	TSubclassOf<UGASAbility> SecondAbility;
		
};
