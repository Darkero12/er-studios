// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GASCharMoveComp.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API UGASCharMoveComp : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	
	virtual float GetMaxSpeed() const override;
};
