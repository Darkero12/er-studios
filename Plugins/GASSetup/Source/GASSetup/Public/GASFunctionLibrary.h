// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/Data/TofhaData.h"
#include "Components/GASAbilityComponent.h"
#include "Game/Character/GASCharacter.h"
#include "Game/GameModes/GASGameMode.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Player/PlayerCharacter/GASPlayerCharacter.h"
#include "GASFunctionLibrary.generated.h"

struct FGameplayTagContainer;
/**
 * 
 */
UCLASS()
class GASSETUP_API UGASFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	public: 
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA | Gameplay")
	static float FindDegreeFromSourceToTarget(const AActor* SourceActor, const AActor* TargetActor);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA | Gameplay")
	static float FindDegreeToHitNormal(const FRotator ActorRotation, const FVector HitNormal);

	UFUNCTION(BlueprintCallable, Category = "GAS|Abilities|GameplayTags")
	static bool AddLooseGameplayTagsToActor(AActor* Actor, const FGameplayTagContainer GameplayTags);

	UFUNCTION(BlueprintCallable, Category = "GAS|Abilities|GameplayTags")
	static bool RemoveLooseGameplayTagsFromActor(AActor* Actor, const FGameplayTagContainer GameplayTags);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS|Abilities|GameplayTags")
	static UAbilitySystemComponent* GetAbilitySystemComponentFromCharacter(AActor* Actor);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS|Abilities|GameplayTags")
	static AGASCharacter* RecastToGASCharacter(AActor* Actor);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS|Abilities|GameplayTags")
	static AGASPlayerCharacter* RecastToGASPlayerCharacter(AActor* Actor);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS|Abilities|GameplayTags")
	static AGASPlayerState* RecastToGASPlayerState(APlayerState* PlayerState);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS|Abilities|GameplayTags")
	static bool IsTargetBlocking(AGASCharacter* TargetCharacter);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS|Abilities|GameplayTags")
    static bool GrantAbilitiesToCharacter(TSubclassOf<UGASAbility> Ability, AGASCharacter* InCharacter);
    
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS | Gameplay")
	static EHitReactDirection GetHitDirection(const AActor* SourceActor, const AActor* TargetActor);

	UFUNCTION(BlueprintCallable, Category = "GAS | Gameplay")
	static bool QueueMontage(UAnimInstance* AnimInstance,UAnimMontage* Montage);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS | Gameplay")
	static bool ShouldHappen(float InPercentage);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GAS | Gameplay")
	static void Visibility_GetRenderedActors(TArray<AActor*>& CurrentlyRenderedActors,float MinRecentTime);
	
	/// JLH ///

	UFUNCTION(BlueprintCallable, Category = "TOFHA | AI | Controller")
	static void DestroyController(AController* InController);
};
