// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/Character/GASCharacter.h"
#include "Player/PlayerStates/GASPlayerState.h"
#include "GASPlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API AGASPlayerCharacter : public AGASCharacter
{
	GENERATED_BODY()

public:
	AGASPlayerCharacter(const class FObjectInitializer& ObjectInitializer);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual void PossessedBy(AController* NewController) override;

	class USpringArmComponent* GetCameraBoom();

	class UCameraComponent* GetFollowCamera();

	UFUNCTION(BlueprintCallable, Category = "GAS|Camera")
    float GetStartingCameraBoomArmLength();

	UFUNCTION(BlueprintCallable, Category = "GAS|Camera")
    FVector GetStartingCameraBoomLocation();

	UFUNCTION(BlueprintCallable, Category = "GAS|CharacterSwitching")
	UGASAbilityComponent* RefreshCharacterRefs(AGASPlayerState* InPS, UGASAbilityComponent* ASCRef);

	virtual void FinishDying() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

	void ApplyPersonalSpacePush();

	FTimerHandle PushTimerHandle;
	
protected:
	
	// The amount of push force on tick to apply when a character collides with this one.
	UPROPERTY(EditDefaultsOnly, Category = "Character|Personal Space")
	float PushForce = 10000.0f;

	// How much more to apply force based on the distance between the characters. Calculation: (capsuleDiameter - distance) * DistanceMultiplier
	UPROPERTY(EditDefaultsOnly, Category = "Character|Personal Space")
	float DistanceMultiplier = 5.0f;

	// How often we should apply the force.
	UPROPERTY(EditDefaultsOnly, Category = "Character|Personal Space")
	float ForceApplicationTime = 0.75f;

	UPROPERTY()
	TArray<AGASCharacter*> OverlappingCharacters;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Camera")
	float BaseTurnRate = 45.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Camera")
	float BaseLookUpRate = 45.0f;

	UPROPERTY(BlueprintReadOnly, Category = "GAS|Camera")
	float StartingCameraBoomArmLength;

	UPROPERTY(BlueprintReadOnly, Category = "GAS|Camera")
	FVector StartingCameraBoomLocation;

	UPROPERTY(BlueprintReadOnly, Category = "GAS|CharacterSwitching")
	UGASAbilityComponent* CachedASC = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "GAS|Camera")
	class USpringArmComponent* CameraBoom;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "GAS|Camera")
	class UCameraComponent* FollowCamera;

	bool ASCInputBound = false;

	FGameplayTag DeadTag;

	FGameplayTag ReviveTag;

	FGameplayTag SwapTag;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void PostInitializeComponents() override;

	// Mouse
	void LookUp(float Value);

	// Gamepad
	void LookUpRate(float Value);

	// Mouse
	void Turn(float Value);

	// Gamepad
	void TurnRate(float Value);

	// Mouse + Gamepad
	void MoveForward(float Value);

	// Mouse + Gamepad
	void MoveRight(float Value);

	// Creates and initializes the floating status bar for heroes.
	// Safe to call many times because it checks to make sure it only executes once.
	UFUNCTION()
    void InitializeFloatingStatusBar();

	// Called from both SetupPlayerInputComponent and OnRep_PlayerState because of a potential race condition where the PlayerController might
	// call ClientRestart which calls SetupPlayerInputComponent before the PlayerState is repped to the client so the PlayerState would be null in SetupPlayerInputComponent.
	// Conversely, the PlayerState might be repped before the PlayerController calls ClientRestart so the Actor's InputComponent would be null in OnRep_PlayerState.
	void BindAscInput();

	
};
