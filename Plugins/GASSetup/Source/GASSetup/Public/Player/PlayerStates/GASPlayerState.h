// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "GameFramework/PlayerState.h"
#include "Game/AttributeSet/GASAttributeSet.h"
#include "GASSetup/Public/Components/GASAbilityComponent.h"
#include "GASPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API AGASPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	
	AGASPlayerState();
    
	// Implement IAbilitySystemInterface
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	class UGASAttributeSet* GetAttributeSetBase() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState")
    bool IsAlive() const;

	/**
	* Getters for attributes from AttributeSetBase. Returns Current Value unless otherwise specified.
	*/

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
	float GetConstitution() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
	float GetEndurance() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetMaxHealth() const;
    
	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetMana() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetMaxMana() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetManaRegenRate() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetStamina() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetMaxStamina() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|PlayerState|Attributes")
    float GetStaminaRegenRate() const;

	protected:
	UPROPERTY()
	class UGASAbilityComponent* AbilitySystemComponent;

	UPROPERTY()
	class UGASAttributeSet* AttributeSetBase;

	FGameplayTag DeadTag;
	FGameplayTag ReviveTag;

	FDelegateHandle HealthChangedDelegateHandle;
	FDelegateHandle MaxHealthChangedDelegateHandle;
	FDelegateHandle ManaChangedDelegateHandle;
	FDelegateHandle MaxManaChangedDelegateHandle;
	FDelegateHandle ManaRegenRateChangedDelegateHandle;
	FDelegateHandle StaminaChangedDelegateHandle;
	FDelegateHandle MaxStaminaChangedDelegateHandle;
	FDelegateHandle StaminaRegenRateChangedDelegateHandle;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Attribute changed callbacks
	virtual void HealthChanged(const FOnAttributeChangeData& Data);
	virtual void MaxHealthChanged(const FOnAttributeChangeData& Data);
	virtual void ManaChanged(const FOnAttributeChangeData& Data);
	virtual void MaxManaChanged(const FOnAttributeChangeData& Data);
	virtual void ManaRegenRateChanged(const FOnAttributeChangeData& Data);
	virtual void StaminaChanged(const FOnAttributeChangeData& Data);
	virtual void MaxStaminaChanged(const FOnAttributeChangeData& Data);
	virtual void StaminaRegenRateChanged(const FOnAttributeChangeData& Data);

	// Tag change callbacks
	virtual void StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount);
};
