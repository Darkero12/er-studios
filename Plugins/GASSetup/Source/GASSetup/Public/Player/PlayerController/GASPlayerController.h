// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GASPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPartyWipe);

/**
 * 
 */
UCLASS()
class GASSETUP_API AGASPlayerController : public APlayerController
{
	GENERATED_BODY()

	virtual void AcknowledgePossession(APawn* P) override;
	
	public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Party|MemeberCount")
	int TotalMembers = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Party|MemeberCount")
	int AliveMembers = 4;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Party|MemeberCount")
	int MembersDown = 0;

	UPROPERTY(BlueprintAssignable, Category = "Party|MemeberCount")
	FOnPartyWipe OnPartyWipe;
};
