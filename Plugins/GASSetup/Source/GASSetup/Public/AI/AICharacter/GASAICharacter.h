// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/Character/GASCharacter.h"
#include "GASSetup/Public/Components/GASAbilityComponent.h"
#include "GASAICharacter.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API AGASAICharacter : public AGASCharacter
{
	GENERATED_BODY()

public:

	AGASAICharacter(const class FObjectInitializer& ObjectInitializer);

protected:
	
	virtual void BeginPlay() override;

	virtual void PossessedBy(AController* NewController) override;
	
	FDelegateHandle HealthChangedDelegateHandle;

	virtual void HealthChanged(const FOnAttributeChangeData& Data);
	// Attribute changed callbacks
	UFUNCTION(BlueprintImplementableEvent, Category = "GAS|AI|HealthChange")
    void OnAIHealthChanged();

	// Tag change callbacks
	virtual void StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount);
	
};
