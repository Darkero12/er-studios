// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AI/AICharacter/GASAICharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "Game/Data/TofhaData.h"

#include "GASAIController.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API AGASAIController : public AAIController
{
	GENERATED_BODY()

public:

	//AGASAIController();
	AGASAIController(const FObjectInitializer& ObjectInitializer);

	virtual void OnPossess(APawn* InPawn) override;

	/*
	UPROPERTY(BlueprintReadOnly)
	UBlackboardComponent* BlackboardComponent;

	UPROPERTY(BlueprintReadOnly)
	UBehaviorTreeComponent* BehaviorTreeComponent;

	UPROPERTY(EditDefaultsOnly)
	UBehaviorTree* BehaviorTree; */
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE FEncounterAlignments GetAlignments() const {return Alignments;}

protected:
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "TOFHA | AI")
	FEncounterAlignments Alignments;

	/*
	uint8 keyTargetActor, keyTargetLocation;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA | AI")
	AActor* GetTargetActorBK() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TOFHA | AI")
	FVector GetTargetLocationBK() const;

	UFUNCTION(BlueprintCallable, Category = "TOFHA | AI")
	void SetTargetActorBK(AActor* InActor) const;

	UFUNCTION(BlueprintCallable, Category = "TOFHA | AI")
	void SetTargetLocationBK(FVector InVector) const;*/
};
