// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectCustomApplicationRequirement.h"
#include "DamageAppRequirement.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API UDamageAppRequirement : public UGameplayEffectCustomApplicationRequirement
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Tags")
	FGameplayTag BlockTag;
	

	virtual bool CanApplyGameplayEffect_Implementation(const UGameplayEffect* GameplayEffect, const FGameplayEffectSpec& Spec, UAbilitySystemComponent* ASC) const override;
};
