// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/EngineSubsystem.h"
#include "AbilitySystemGlobals.h"
#include "GASEngineSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API UGASEngineSubsystem : public UEngineSubsystem
{
	GENERATED_BODY()
	

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

};

inline void UGASEngineSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	UAbilitySystemGlobals::Get().InitGlobalData();
}
