#pragma once

#include "CoreMinimal.h"
#include "GASSetup.h"
#include "TofhaData.generated.h"

class UGASAbility;
class AGASCharacter;
class AGASPlayerCharacter;
class AGASAICharacter;


UENUM(BlueprintType) /// JLH ///
enum class EAlignmentType : uint8
{
	EFriendly = 0		UMETA(DisplayName="Friendly"),
	EHostile			UMETA(DisplayName="Hostile"),
	ENeutralFriendly	UMETA(DisplayName="Neutral-Friendly"),
	ENeutralHostile		UMETA(DisplayName="Neutral-Hostile")
};

UENUM(BlueprintType) /// JLH ///
enum class EBehaviorType : uint8
{
	EIdle=0				UMETA(DisplayName="Idle"),
	EFollow				UMETA(DisplayName="Follow"),
	EFlee				UMETA(DisplayName="Flee"),
	EPatrol				UMETA(DisplayName="Patrol"),
	ESeek				UMETA(DisplayName="Seek"),
	ECombatCooldown		UMETA(DisplayName="CombatCooldown"),
	EAttackMelee		UMETA(DisplayName="AttackMelee"),
	EAttackRanged		UMETA(DisplayName="AttackRanged"),
	EDefend				UMETA(DisplayName="Defend"),
	EHit				UMETA(DisplayName="Hit"),
	EInvestigate		UMETA(DisplayName="Investigate"),
	EParkour			UMETA(DisplayName="Parkour"),
	EStun				UMETA(DisplayName="Stun")
};

UENUM(BlueprintType) /// JLH ///
enum class ECaptainResponseSelection : uint8
{
	EPassive=0		UMETA(DisplayName="Passive Ability"),
	EOnDied			UMETA(DisplayName="When Killed"),
	EOnHit			UMETA(DisplayName="When Hit"),
	EAllyDeath		UMETA(DisplayName="When Ally Dies"),
	EAllyHit		UMETA(DisplayName="When Ally Is Hit"),
	EEnemyDies		UMETA(DisplayName="WhenEnemyDies"),
	EEnemyIsHit		UMETA(DisplayName="WhenEnemyIsHit"),
	EClassTrigger		UMETA(DisplayName="Class-specific Trigger"),
};

UENUM(BlueprintType) /// JLH ///
enum class ETargetSelectionMethod : uint8
{
	ERandom	= 0				UMETA(DisplayName="Random Target"),
	EClosest				UMETA(DisplayName="Closest Target"),
	ESwarm					UMETA(DisplayName="Swarm"),
	EIfPlayerIs				UMETA(DisplayName="Player Is"),
	EIfPlayerIsNot			UMETA(DisplayName="Player Is Not"),
	EPlayerControlled       UMETA(DisplayName="Player Controlled"),
	EPlayerControlledIfNot  UMETA(DisplayName="Player Controlled If Not"),
	ENonPlayerTarget		UMETA(DisplayName="Non-Player Target"),
	ESpecificCompanion		UMETA(DisplayName="Specific Companion"),
	EClassTrigger			UMETA(DisplayName="Class-specific Trigger")
};

USTRUCT(BlueprintType) /// JLH ///
struct FEncounterAlignments
{
	GENERATED_BODY();

	FEncounterAlignments(){};

public:
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TArray<TSubclassOf<AGASCharacter>> Friendly;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TArray<TSubclassOf<AGASCharacter>> Hostile;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TArray<TSubclassOf<AGASCharacter>> NeutralFriendly;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TArray<TSubclassOf<AGASCharacter>> NeutralHostile;
	
};

USTRUCT(BlueprintType) /// JLH ///
struct FBossStage
{
	GENERATED_BODY()

	/**
	 * Automatically generated based on other boss stages in the same array container. -1 if there is a conflict with another stage in the array.
	 */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta=(BlueprintEditCondition="false"))
	int BossStage;

	/*
	 * Health trigger is by percent, not literal.
	 */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (ClampMin = "0.0", ClampMax = "100.0", UIMin = "0.0", UIMax = "100.0"))
	float HealthTrigger;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TArray<TSubclassOf<UGASAbility>> IntroductoryAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TArray<TSubclassOf<UGASAbility>> OffensiveAbilities;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TArray<TSubclassOf<UGASAbility>> DefensiveAbilities;

	bool operator ==(const FBossStage& OtherStage) const
	{
		return HealthTrigger == OtherStage.HealthTrigger;
	}

	/**
	 * Does not look at the validity of classes in the struct, only whether there are any abilities added.
	 */
	bool IsValidWeak() const
	{
		return  IntroductoryAbilities.IsValidIndex(0) ||
				OffensiveAbilities.IsValidIndex(0) ||
				DefensiveAbilities.IsValidIndex(0);
	}

	bool operator !=(const FBossStage& OtherStage) const
	{
		return HealthTrigger != OtherStage.HealthTrigger;
	}

	bool operator <(const FBossStage& OtherStage) const
	{
		return HealthTrigger < OtherStage.HealthTrigger;
	}

	bool operator >(const FBossStage& OtherStage) const
	{
		return HealthTrigger > OtherStage.HealthTrigger;
	}
};

USTRUCT(BlueprintType) /// JLH ///
struct FBossSettings
{
	GENERATED_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bIsBoss;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(EditCondition="bIsBoss"))
	TArray<FBossStage> BossStages;

	UPROPERTY(BlueprintReadOnly)
	int CurrentStage = 0;

public:
	TArray<TSubclassOf<UGASAbility>> GetActiveStageAbilities()
	{
		if (CurrentStage < 0)
		{
			TArray<TSubclassOf<UGASAbility>> Temp;
			return Temp;
		}
		TArray<TSubclassOf<UGASAbility>> Temp;
		Temp.Append(BossStages[CurrentStage - 1].DefensiveAbilities);
		Temp.Append(BossStages[CurrentStage - 1].OffensiveAbilities);
		return Temp;
	}
	
};

USTRUCT(BlueprintType) /// JLH ///
struct FCaptainEventTriggers
{

	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	ECaptainResponseSelection CaptainResponse = ECaptainResponseSelection::EPassive;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta=(EditCondition="CaptainResponse == ECaptainResponseSelection::EPassive", EditConditionHides))
	float PassiveEventTimer = 0.f;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta=(EditCondition="CaptainResponse == ECaptainResponseSelection::EPassive", EditConditionHides))
	TSubclassOf<UGASAbility> PassiveEvent;

	// Cooldown for assessing the OnHitEvent.
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta=(EditCondition="CaptainResponse == ECaptainResponseSelection::EOnHit", EditConditionHides))
	float OnHitCooldownTimer = 5.f;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta=(EditCondition="CaptainResponse == ECaptainResponseSelection::EOnHit", EditConditionHides))
	TSubclassOf<UGASAbility> OnHitEvent;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta=(EditCondition="CaptainResponse == ECaptainResponseSelection::EOnDied", EditConditionHides))
	TSubclassOf<UGASAbility> OnDiedEvent;

	UPROPERTY(BlueprintReadWrite)
	FTimerHandle OnHitCooldownHandle;

	UPROPERTY(BlueprintReadWrite)
	FTimerHandle OnPassiveTimerHandle;
	
	
	UPROPERTY(BlueprintReadOnly)
	bool bIsOnHitReady;

	UPROPERTY(BlueprintReadOnly)
	bool bIsPassiveReady;

	void EnablePassive()
	{
		if (!bIsPassiveReady) bIsPassiveReady = true;
	}

	void EnableOnHit()
	{
		if (!bIsOnHitReady) bIsOnHitReady = true;
	}

	
};

USTRUCT(BlueprintType) /// JLH ///
struct FTargetingSettings
{
	GENERATED_BODY();
	
	/**
	 *	"Random" will look for a random target, of any distance. Use bool to distribute.
	 *	"Closest" will look for the closest target. Use bool to distribute.
	 *	"Swarm" will target the target of the selected class, if that class exists in the group. 
	 *	"Player Is" will target a particular class if it's player-controlled, with percentage chance.
	 *	"Player Is Not" will target a particular class if it is NOT player-controlled, with percentage chance.
	 *	"Player Controlled" will target the player's character with a percentage chance.
	 *	"Player Controlled If Not" will target the player's character with a percentage chance as long as they are NOT of TargetClass
	 *	"Non Player Target" always selects a target that is not currently player controlled, but otherwise operates as "random". 
	 *	"Specific Companion" always targets a particular companion, if they're still alive.
	 *	"Class Trigger" is not implemented.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ETargetSelectionMethod TargetingType;

	/**
	 *	Whether to find targets that aren't being targeted by the current group.
	 *	Only valid for Random and Closest targeting methods.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta=(EditCondition="TargetingType == ETargetSelectionMethod::ERandom || TargetingType == ETargetSelectionMethod::EClosest || TargetingType == ETargetSelectionMethod::ENonPlayerTarget",
			EditConditionHides))
	bool bDistributeTargets;

	/**
	 *	Indicates the class relevant to the targeting method. 
	 *	Use this to select the companion / PlayerCharacter class relevant to the method.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta=(EditCondition="TargetingType == ETargetSelectionMethod::EIfPlayerIs || TargetingType == ETargetSelectionMethod::EIfPlayerIsNot || TargetingType == ETargetSelectionMethod::ESpecificCompanion || TargetingType == ETargetSelectionMethod::EPlayerControlledIfNot",
			EditConditionHides))
	TSubclassOf<AGASPlayerCharacter> TargetClass;

	/*
	 * Select the class to swarm with. Does not have to be the same class, but works better if the selected class is also set to swarm.
	 * If you want this character to swarm with a particular actor, use the SwarmWithActor variable, only available in the instance.
	 * It overrides the class assignment here.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta=(EditCondition="TargetingType == ETargetSelectionMethod::ESwarm",
			EditConditionHides))
	TSubclassOf<AGASAICharacter> SwarmWithClass;

	/*
	 * Actor in the world to swarm with, as to be more specific and allow pet-like behavior.
	 */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly,
		meta=(EditCondition="TargetingType == ETargetSelectionMethod::ESwarm",
			EditConditionHides))
	AGASAICharacter* SwarmWithActor;	
	
	/**
	 *	Percentage chance that the selected class will be attacked by the enemy.
	 *	Only relevant to IfPlayerIs or IfPlayerIsNot.
	 */ 
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta=(EditCondition="TargetingType == ETargetSelectionMethod::EIfPlayerIs || TargetingType == ETargetSelectionMethod::EIfPlayerIsNot || TargetingType == ETargetSelectionMethod::EPlayerControlledIfNot || TargetingType == ETargetSelectionMethod::EPlayerControlled",
			EditConditionHides, ClampMin = "0.0", ClampMax = "100.0", UIMin = "0.0", UIMax = "100.0"))
	float PercentageChanceOfAttack;
	
};

USTRUCT(BlueprintType)
struct FLastHitInfo
{
	GENERATED_BODY();

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector LastHitLocation = FVector(0,0,0);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	AActor* HittingActor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float HitAngle = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EHitReactDirection HitReactDirection;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FHitResult HitResult;
};

USTRUCT(BlueprintType) /// JLH ///
struct FActorTriggerBehavior
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EAlignmentType AlignmentType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Range;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EBehaviorType BehaviorUnderRange;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EBehaviorType BehaviorOverRange;
};

USTRUCT(BlueprintType) /// JLH ///
struct FBasicBehavior
{
	GENERATED_BODY()

public:

	// CONSTRUCTORS
	FBasicBehavior(){};
	FBasicBehavior(EBehaviorType DefaultBehavior)
	{
		BehaviorType = DefaultBehavior;
	};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EBehaviorType BehaviorType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MovementSpeed = 600.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FActorTriggerBehavior> OnSightTriggers;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FActorTriggerBehavior> LoseSightTriggers;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<TSubclassOf<UGASAbility>> GameplayAbilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<EAlignmentType> TargetAlignments;


};

USTRUCT(BlueprintType) /// JLH ///
struct FBehaviorConfiguration
{

	GENERATED_BODY();
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="IdleBehavior")
	FBasicBehavior IdleBehavior = FBasicBehavior(EBehaviorType::EIdle);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="FollowBehavior")
	FBasicBehavior FollowBehavior = FBasicBehavior(EBehaviorType::EFollow);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="FleeBehavior")
	FBasicBehavior FleeBehavior = FBasicBehavior(EBehaviorType::EFlee);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PatrolBehavior")
	FBasicBehavior PatrolBehavior = FBasicBehavior(EBehaviorType::EPatrol);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="SeekBehavior")
	FBasicBehavior SeekBehavior = FBasicBehavior(EBehaviorType::ESeek);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="CombatCooldownBehavior")
	FBasicBehavior CombatCooldownBehavior = FBasicBehavior(EBehaviorType::ECombatCooldown);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="AttackMeleeBehavior")
	FBasicBehavior AttackMeleeBehavior = FBasicBehavior(EBehaviorType::EAttackMelee);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="AttackRangedBehavior")
	FBasicBehavior AttackRangedBehavior = FBasicBehavior(EBehaviorType::EAttackRanged);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DefendBehavior")
	FBasicBehavior DefendBehavior = FBasicBehavior(EBehaviorType::EDefend);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="HitBehavior")
	FBasicBehavior HitBehavior = FBasicBehavior(EBehaviorType::EHit);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="InvestigateBehavior")
	FBasicBehavior InvestigateBehavior = FBasicBehavior(EBehaviorType::EInvestigate);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="ParkourBehavior")
	FBasicBehavior ParkourBehavior = FBasicBehavior(EBehaviorType::EParkour);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="StunBehavior")
	FBasicBehavior StunBehavior = FBasicBehavior(EBehaviorType::EStun);

};