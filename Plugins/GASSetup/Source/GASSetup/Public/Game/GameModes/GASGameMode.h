// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GASGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API AGASGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	AGASGameMode();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Game")
	TArray<APlayerController*> ConnectedControllers;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Game")
	int AliveMembersCount = 4;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Game")
	int TotalMembersCount = 4;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Game")
	int MembersDown = 0;

	UFUNCTION(BlueprintCallable, Category = "GAS|Game")
	void DecreaseAliveCount();

	UFUNCTION(BlueprintCallable, Category = "GAS|Game")
	void IncreaseAliveCount();	

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "GAS|Game")
	void PartyWipe();
	
	virtual void BeginPlay() override;
	
};
