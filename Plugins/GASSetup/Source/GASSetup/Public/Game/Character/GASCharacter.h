// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <stdbool.h>

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GASSetup.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "CollisionHandler/CCCollisionHandlerComponent.h"
#include "Components/AbilityComboManager.h"
#include "Game/Abilities/GASAbility.h"
#include "Game/Data/TofhaData.h"
#include "GASCharacter.generated.h"

class UGASAbilityQueueComponent;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterBaseHitReactDelegate, EHitReactDirection, Direction);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterDiedDelegate, class AGASCharacter*, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCharacterTookDamage, class AGASCharacter*, DamageCauser, class AGASCharacter*, DamageTaker);

UCLASS()
class GASSETUP_API AGASCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGASCharacter(const class FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Ability Queue Component */
	UPROPERTY(Category = "GAS|Components", BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UGASAbilityQueueComponent* AbilityQueueComponent;

	/** Ability Combo Component */
	UPROPERTY(Category = "GAS|Components", VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UAbilityComboManager* AbilityComboComponent;

	/** CCollision Handler Component */
	UPROPERTY(Category = "GAS|Components", VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UCCCollisionHandlerComponent* CollisionHandlerComponent;

	// Set the Hit React direction in the Animation Blueprint
	UPROPERTY(BlueprintAssignable, Category = "GAS|Character")
	FCharacterBaseHitReactDelegate ShowHitReact;

	// Set the Hit React direction in the Animation Blueprint
	UPROPERTY(BlueprintReadWrite, Category = "GAS|Character|Abilities")
	UGASAbility* CurrentActiveAbility = nullptr;

	UPROPERTY(BlueprintAssignable, Category = "GAS|Character")
	FCharacterDiedDelegate OnCharacterDied;

	UPROPERTY(BlueprintAssignable, Category = "GAS|Character")
	FCharacterTookDamage OnCharacterTakeDamage;

	// Implement IAbilitySystemInterface
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character")
    virtual bool IsAlive() const;

	// Switch on AbilityID to return individual ability levels. Hardcoded to 1 for every ability in this project.
	UFUNCTION(BlueprintCallable, Category = "GAS|Character")
    virtual int32 GetAbilityLevel(EAbilityInputID AbilityID) const;

	// Removes all CharacterAbilities. Can only be called by the Server. Removing on the Server will remove from Client too.
	virtual void RemoveCharacterAbilities();

	UFUNCTION(BlueprintCallable)
    EHitReactDirection GetHitReactDirection(const FVector& ImpactPoint);

    virtual void PlayHitReact(FGameplayTag HitDirection, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    int32 GetCharacterLevel() const;

    UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetMana() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetMaxMana() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetStamina() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetMaxStamina() const;

	// Gets the Current value of MoveSpeed
	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetMoveSpeed() const;

	// Gets the Base value of MoveSpeed
	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
    float GetMoveSpeedBaseValue() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	void Revive();

	virtual void Die();

	UFUNCTION(BlueprintCallable, Category = "GAS|Character")
    virtual void FinishDying();

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "GAS|Character")
	FName GetInputDirection();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS|Character")
	FText CharacterName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Combo")
	int CurrentComboCount = 1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Combo")
	int MaxComboCount = 4;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Combo")
	float ComboWindow = 3.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Combo")
	FGameplayTag CurrentWeaponStance;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|HitInfo")
	FLastHitInfo LastHitInfo;

	UPROPERTY()
	FTimerHandle ComboTimerHandle;

	//This is the function used to start and increase the count of the combo.
	UFUNCTION(BlueprintCallable, Category = "GAS|Combo")
	void IncreaseComboCount();

	//This is the function used to reset and end the combo.
	UFUNCTION(BlueprintCallable, Category = "GAS|Combo")
	void ResetCombo();

	UFUNCTION(BlueprintCallable, Category = "GAS")
	void RemoveCoolDownWithAbility(TSubclassOf<UGameplayAbility> InAbilityClass);

	UFUNCTION(BlueprintCallable, Category = "GAS")
	float GetActiveEffectsTimeRemaining(FGameplayTagContainer Tag) const;

	//This is the function used to call an event in the blueprint.
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "GAS|Death")
	void CharacterDiedEvent();

	//This is the function used to call an event in the blueprint.
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "GAS|Death")
	void CharacterReviveEvent();

	UFUNCTION(BlueprintImplementableEvent)
	void ComboCountChange(int InComboCount);

	UFUNCTION(BlueprintImplementableEvent)
	void MaxComboReached();
	
	UFUNCTION(BlueprintImplementableEvent)
	void ComboReset();
	
protected:
	
	//virtual void Destroyed() override;

	// Default attributes for a character for initializing on spawn/respawn.
	// This is an instant GE that overrides the values for attributes that get reset on spawn/respawn.
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Abilities")
	TSubclassOf<class UGameplayEffect> DefaultAttributes;

	// These effects are only applied one time on startup
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Abilities")
	TArray<TSubclassOf<class UGameplayEffect>> StartupEffects;

	// Default abilities for this Character. These will be removed on Character death and regiven if Character respawns.
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Abilities")
	TArray<TSubclassOf<class UGASAbility>> CharacterAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Abilities")
	TArray<UAnimMontage*> DeathMontages;
	
	// Grant abilities on the Server. The Ability Specs will be replicated to the owning client.
	virtual void AddCharacterAbilities();
	
	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	virtual void InitializeAttributes();

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	void ResetCharacterAttributes();

	virtual void AddStartupEffects();
	
	FTimerHandle DeathTimerHandle;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "TOFHA|AbilitySystem")
	class UGASAbilityComponent* AbilitySystemComponent;

	UPROPERTY(BlueprintReadOnly)
	class UGASAttributeSet* AttributeSetBase;

	FGameplayTag HitDirectionFrontTag;
	FGameplayTag HitDirectionBackTag;
	FGameplayTag HitDirectionRightTag;
	FGameplayTag HitDirectionLeftTag;
	FGameplayTag DeadTag;
	FGameplayTag ReviveTag;
	FGameplayTag EffectRemoveOnDeathTag;

	FDelegateHandle HealthChangedDelegateHandle;
	FDelegateHandle MaxHealthChangedDelegateHandle;
	FDelegateHandle ManaChangedDelegateHandle;
	FDelegateHandle MaxManaChangedDelegateHandle;
	FDelegateHandle ManaRegenRateChangedDelegateHandle;
	FDelegateHandle StaminaChangedDelegateHandle;
	FDelegateHandle MaxStaminaChangedDelegateHandle;
	FDelegateHandle StaminaRegenRateChangedDelegateHandle;

	virtual void HealthChanged(const FOnAttributeChangeData& Data);
	/*virtual void MaxHealthChanged(const FOnAttributeChangeData& Data);
	virtual void ManaChanged(const FOnAttributeChangeData& Data);
	virtual void MaxManaChanged(const FOnAttributeChangeData& Data);
	virtual void ManaRegenRateChanged(const FOnAttributeChangeData& Data);
	virtual void StaminaChanged(const FOnAttributeChangeData& Data);
	virtual void MaxStaminaChanged(const FOnAttributeChangeData& Data);
	virtual void StaminaRegenRateChanged(const FOnAttributeChangeData& Data);*/

	public:

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Animation")
	bool bTurnLeft = false;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Animation")
	bool bTurnRight = false;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Abilities")
	bool bAutoRelease = false;
	
	/**
	* Setters for Attributes. Only use these in special cases like Respawning, otherwise use a GE to change Attributes.
	* These change the Attribute's Base Value.
	*/
    virtual void SetConstitution(float Constitution);
	virtual void SetEndurance(float Endurance);
	virtual void SetHealth(float Health);
	virtual void SetMaxHealth(float MaxHealth);
	virtual void SetMana(float Mana);
	virtual void SetMaxMana(float MaxMana);
	virtual void SetStamina(float Stamina);
	virtual void SetMaxStamina(float Stamina);


	UFUNCTION()
	void DeathDelay_Elapsed();

	bool bKnockUp = false;
};
