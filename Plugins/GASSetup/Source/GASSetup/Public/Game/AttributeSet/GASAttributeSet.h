// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemComponent.h"
#include "GASAttributeSet.generated.h"

//Macros for getters and setters.
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)
/**
 * 
 */
UCLASS()
class GASSETUP_API UGASAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	UGASAttributeSet();

	//AttributeSet Overrides

	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Scaling")
	float StrengthScaling = 10.0f;
	
	//Strength Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Strength")
	FGameplayAttributeData Strength;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Strength)

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Scaling")
	float EnduranceScaling = 6.25f;
	
	//Endurance Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Endurance")
	FGameplayAttributeData Endurance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Endurance)
	
	//Agility Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Agility")
	FGameplayAttributeData Agility;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Agility)

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Scaling")
	float DexterityScaling = 0.1f;
	
	//Dexterity Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Dexterity")
	FGameplayAttributeData Dexterity;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Dexterity)

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Scaling")
	float ConstitutionScaling = 20.0f;
	
	//Constitution Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Constitution")
	FGameplayAttributeData Constitution;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Constitution)

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Scaling")
	float IntelligenceScaling = 0.0f;
	
	//Intelligence Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Intelligence")
	FGameplayAttributeData Intelligence;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Intelligence)

	//Tenacity Attribute
	UPROPERTY(BlueprintReadOnly, Category = "Tenacity")
	FGameplayAttributeData Tenacity = 0.0f;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Tenacity)
	
	// Current Health, when 0 we expect owner to die unless prevented by an ability. Capped by MaxHealth.
	// Positive changes can directly use this.
	// Negative changes to Health should go through Damage meta attribute.
	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Health)

	// MaxHealth is its own attribute since GameplayEffects may modify it
    UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, MaxHealth)

	// Current Mana, used to execute special abilities. Capped by MaxMana.
    UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Mana)

    // MaxMana is its own attribute since GameplayEffects may modify it
    UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData MaxMana;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, MaxMana)

	// Mana regen rate will passively increase Mana every second
    UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData ManaRegenRate;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, ManaRegenRate)

	// Current Stamina, used to execute movement abilities. Capped by MaxMana.
    UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData Stamina;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Stamina)

    // MaxStamina is its own attribute since GameplayEffects may modify it
    UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData MaxStamina;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, MaxStamina)

    // Stamina regen rate will passively increase Stamina every second
    UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData StaminaRegenRate;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, StaminaRegenRate)

	// Damage is a meta attribute used by the DamageExecution to calculate final damage, which then turns into -Health
    // Temporary value that only exists on the Server. Not replicated.
    UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Damage)
	
	UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData WeakSpotMultiplier = 1.0f;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, WeakSpotMultiplier)
	
	UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData Shield = 1.0f;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Shield)
	
	// MoveSpeed affects how fast characters can move.
    UPROPERTY(BlueprintReadOnly, Category = "MoveSpeed")
	FGameplayAttributeData MoveSpeed;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, MoveSpeed)

	// Expereince points that a player character has.
	UPROPERTY(BlueprintReadOnly, Category = "EXP")
	FGameplayAttributeData Expereince;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Expereince)

	// Current Weight, when 0 we expect owner to die unless prevented by an ability. Capped by MaxWeight.
	// Positive changes can directly use this.
	UPROPERTY(BlueprintReadOnly, Category = "Weight")
	FGameplayAttributeData Weight;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, Weight)

	// MaxHealth is its own attribute since GameplayEffects may modify it
	UPROPERTY(BlueprintReadOnly, Category = "Weight")
	FGameplayAttributeData MaxWeight;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, MaxWeight)

	//Cut ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData CutResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, CutResistance)

	//Pierce ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData PierceResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, PierceResistance)

	//Impact ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData ImpactResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, ImpactResistance)

	//Heat ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData HeatResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, HeatResistance)

	//Cold ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData ColdResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, ColdResistance)

	//Void ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData VoidResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, VoidResistance)

	//Poison ResistanceType Attribute
	UPROPERTY(BlueprintReadOnly, Category = "ResistanceType")
	FGameplayAttributeData PosionResistance;
	ATTRIBUTE_ACCESSORS(UGASAttributeSet, PosionResistance)

protected:
	// Helper function to proportionally adjust the value of an attribute when it's associated max attribute changes.
	// (i.e. When MaxHealth increases, Health increases by an amount that maintains the same percentage as before)
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);
	
	
private:
	FGameplayTag HitDirectionFrontTag;
	FGameplayTag HitDirectionBackTag;
	FGameplayTag HitDirectionRightTag;
	FGameplayTag HitDirectionLeftTag;
	
};
