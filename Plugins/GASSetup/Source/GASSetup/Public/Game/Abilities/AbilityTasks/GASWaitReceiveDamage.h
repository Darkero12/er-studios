// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "GASWaitReceiveDamage.generated.h"

/**
 * 
 */
UCLASS()
class GASSETUP_API UGASWaitReceiveDamage : public UAbilityTask
{
	GENERATED_BODY()
	
};
