// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GASSetup.h"
#include "Abilities/GameplayAbility.h"
#include "GASAbility.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAbilityEnded);


UCLASS()
class GASSETUP_API UGASAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UGASAbility();

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
    const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
    const FGameplayEventData* TriggerEventData) override;

	void OnCancelTagEventCallback(const FGameplayTag CallbackTag, int32 NewCount);
    
    // Abilities with this set will automatically activate when the input is pressed
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Ability")
    EAbilityInputID AbilityInputID = EAbilityInputID::None;

    // Value to associate an ability with an slot without tying it to an automatically activated input.
    // Passive abilities won't be tied to an input so we need a way to generically associate abilities with slots.
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Ability")
    EAbilityInputID AbilityID = EAbilityInputID::None;

    // Tells an ability to activate immediately when its granted. Used for passive abilities and abilities forced on others.
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Ability")
    bool ActivateAbilityOnGranted = false;

	//Tells the Ability queue component whether to queue the ability or not.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Ability")
	bool bEnableAbilityQueue;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Ability")
	float AbilityRange = 0.0f;

	UFUNCTION(BlueprintCallable, Category = "GAS|Abilities")
	FORCEINLINE	EAbilityInputID GetAbilityInputID() const {return AbilityInputID;}

	UFUNCTION(BlueprintCallable, Category = "GAS|Abilities")
	void RemoveAbilityCoolDown();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Ability|Tags")
	FGameplayTagContainer AbilityCancelTag;

    // If an ability is marked as 'ActivateAbilityOnGranted', activate them immediately when given here
    // Epic's comment: Projects may want to initiate passives or do other "BeginPlay" type of logic here.
    virtual void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	/** Called on ability end */
	void AbilityEnded(UGameplayAbility* Ability);
	
	/**
	* Called when the ability ends.
	*/
	UPROPERTY(BlueprintAssignable, Category = "GAS|Ability")
	FOnAbilityEnded OnAbilityEnded;

	virtual void PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate, const FGameplayEventData* TriggerEventData) override;
};
