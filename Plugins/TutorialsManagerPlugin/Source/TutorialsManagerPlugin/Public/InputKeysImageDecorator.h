// Copyright 2022 Koorosh Torabi <thekooroshtorabi@gmail.com> - All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Object.h"
#include "Fonts/SlateFontInfo.h"
#include "Styling/SlateTypes.h"
#include "Framework/Text/TextLayout.h"
#include "Framework/Text/ISlateRun.h"
#include "Framework/Text/ITextDecorator.h"
#include "Components/RichTextBlockDecorator.h"
#include "Engine/DataTable.h"
#include "Components/RichTextBlockImageDecorator.h"
#include "InputKeysImageDecorator.generated.h"

class ISlateStyle;

/**
 * This is pretty much the default RichTextBlockImageDecorator, It only uses the correct height for the image!
 * The image won't necessarily be Square shape.
 * also aligns the image vertically to the center of line
 *
 * Allows you to setup an image decorator that can be configured
 * to map certain keys to certain images.  We recommend you subclass this
 * as a blueprint to configure the instance.
 *
 * Understands the format <img id="NameOfBrushInTable"></>
 */
UCLASS(Abstract, Blueprintable)
class TUTORIALSMANAGERPLUGIN_API UInputKeysImageDecorator : public URichTextBlockDecorator
{
	GENERATED_BODY()

public:
	UInputKeysImageDecorator(const FObjectInitializer& ObjectInitializer);

	virtual TSharedPtr<ITextDecorator> CreateDecorator(URichTextBlock* InOwner) override;

	virtual const FSlateBrush* FindImageBrush(FName TagOrId, bool bWarnIfMissing);

	float GetRelativeImageScale() { return RelativeImageScale; }

	void SetDataTable(class UDataTable* InDataTable) { ImageSet = InDataTable; }

	class UDataTable* GetDataTable() { return ImageSet; }

protected:

	FRichImageRow* FindImageRow(FName TagOrId, bool bWarnIfMissing);

	UPROPERTY(EditAnywhere, Category = Appearance, meta = (RowType = "RichImageRow"))
		class UDataTable* ImageSet;

	// Image size calculates depend on font size of rich text
	UPROPERTY(EditAnywhere, Category = Appearance, meta = (RowType = "RichImageRow"))
		float RelativeImageScale = 1.5f;
};

