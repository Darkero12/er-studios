// Copyright 2022 Koorosh Torabi <thekooroshtorabi@gmail.com> - All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TutorialWidget.generated.h"

class UTextBlock;
class UImage;
class UProgressBar;
class UOverlay;
class URichTextBlock;
class UButton;

enum class ETutorialType : uint8;

/**
 * Tutorial Widget with 2 types of tutorial support
 * 1. Message style tutorial (like GTA)
 * 2. Modal windows tutorial (Most of the game)
 */
UCLASS(Abstract)
class TUTORIALSMANAGERPLUGIN_API UTutorialWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void NativeOnFocusChanging(const FWeakWidgetPath& PreviousFocusPath, const FWidgetPath& NewWidgetPath, const FFocusEvent& InFocusEvent) override;

	/*virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

	virtual FReply NativeOnKeyUp(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;*/

	// Message Tutorial	
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		URichTextBlock* MessageTutorialText;
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		UOverlay* TutorialBoxOverlay;

	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		class UCanvasPanel* TutorialModalCanvas;	

	// Modal Tutorial	
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		URichTextBlock* ModalTutorialText;
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		UTextBlock* ModalTitleText;
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		UImage* ModalTutorialMedia;
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		UProgressBar* ConfirmationProgressBar;
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (BindWidget))
		UButton* ConfirmButton;

	UPROPERTY(BlueprintReadWrite, Transient, Category = "Animations", meta = (BindWidgetAnim))
		UWidgetAnimation* ShowModalAnim;
	UPROPERTY(BlueprintReadWrite, Transient, Category = "Animations", meta = (BindWidgetAnim))
		UWidgetAnimation* HideModalAnim;
	UPROPERTY(BlueprintReadWrite, Transient, Category = "Animations", meta = (BindWidgetAnim))
		UWidgetAnimation* ShowMessageAnim;
	UPROPERTY(BlueprintReadWrite, Transient, Category = "Animations", meta = (BindWidgetAnim))
		UWidgetAnimation* HideMessageAnim;

	UPROPERTY(BlueprintReadWrite, Category = "Tutorial")
		ETutorialType CurrentTutorialType;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		class UMaterialInterface* TutorialMediaMaterial;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		class UMediaPlayer* TutorialMediaPlayer;

public:

	// It's better to not call these manually
	// All of these functions are under use by the tutorials manager

	// Shows a message tutorial with custom text
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void ShowMessageTutorial(FText InContent);

	// Shows a custom modal tutorial
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void ShowModalTutorial(FText InTitle, FText InContent, UObject* InMedia);

	// Hides the current displaying tutorial (message/modal)
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void HideTutorial();

	// You can setup the key hold down time for the modal approval with this function
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void SetupModal(float InHoldTime) { ModalApprovalHoldTime = InHoldTime; }

	// Sets the tutorials manager reference
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void SetTutorialsManager(class ATutorialsManager* InManager) { TutorialsManager = InManager; }

	// Show/Hide will no longer work
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void Lock() { bIsLocked = true; }

	// Show/Hide will start working again
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void Unlock() { bIsLocked = false; }

	// This function invokes when you change the platform at runtime
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void UpdateMessageTutorialText(FText NewContent);

	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void UpdateModalTutorialText(FText NewContent);

	UFUNCTION(BlueprintCallable, Category = "Platform")
		void SetDecorator(class UDataTable* InPlatformDataTable);

	UFUNCTION(BlueprintPure, Category = "Data")
		bool IsMessageTutorialVisible() { return bIsTutorialMessageVisible; }

protected:

	UPROPERTY(BlueprintReadOnly, Category = "Variables")
		class ATutorialsManager* TutorialsManager;

private:

	bool bIsLocked = false;

	UFUNCTION(Category = "Holding")
		void HoldingStarted();
	UFUNCTION(Category = "Holding")
		void HoldingCanceled();

	float ModalApprovalHoldTime = 2;

	float HoldingProgressBarChangeRate;
	float HoldingLerpAlpha = 0;

	bool bIsTutorialMessageVisible = false;
	bool bIsModalOpen = false;

	FTimerHandle CollapseTimer;

	bool bTick = false;

};
