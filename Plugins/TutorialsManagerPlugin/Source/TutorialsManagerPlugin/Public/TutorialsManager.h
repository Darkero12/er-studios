// Copyright 2022 Koorosh Torabi <thekooroshtorabi@gmail.com> - All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InputComponent.h"
#include "Engine/DataTable.h"
#include "GameFramework/PlayerInput.h" // :)
#include "TutorialsManager.generated.h"

class UTutorialWidget;
class UWidgetComponent;

// Enums and Structs

UENUM(BlueprintType)
enum class ETutorialType :uint8 {
	Message UMETA(ToolTip = "The tutorial will show up as message at the top left corner of the screen (on default). You can change the widget later on"),
	Modal UMETA(ToolTip = "The tutorial will show up as a windows and will pause the game untill the player holds down a specefic key")
};


UENUM(BlueprintType)
enum class ETutorialUIMode :uint8 {
	TwoDimensional UMETA(DisplayName = "2D", ToolTip = "Default UI Mode"),
	ThreeDimensional UMETA(DisplayName = "3D", ToolTip = "This mode needs some extra steps, you have to expose the widget component to TutorialsManager using the Set3DWidgetComponent function. (It's mostly used for VR))")
};


UENUM(BlueprintType)
enum class ETutorialPlatform : uint8 {
	None,
	PC,
	GreenConsole,
	BlueConsole,
	OcsQuest,
	VIdx,
	HVive,
	CustomPlatform1,
	CustomPlatform2,
	CustomPlatform3,
	CustomPlatform4,
	CustomPlatform5,
	CustomPlatform6,
	CustomPlatform7,
	CustomPlatform8,
	CustomPlatform9
};

UENUM(BlueprintType)
enum class ETutorialTheme : uint8 {
	Light,
	Dark
};

UENUM(BlueprintType)
enum class ETutorialTriggerType : uint8 {
	InRange UMETA(ToolTip = "The tutorial will show up if the player be within the range of set trigger location"),
	OnCrossed UMETA(ToolTip = "This type works based on the dot product and the location will not matter. Keep in mind that the target point's forward vector is so important."),
	OnPrevTutorialEnd UMETA(ToolTip = "When the previous tutorial ended this tutorial will show up automatically"),
	Manual UMETA(ToolTip = "You have to call \"PopTutorial\" function yourself. Get the TutorialsManager's reference and call the function. You can store the reference in GameMode or GameInstance or even in the player character's blueprint!")
};

UENUM(BlueprintType)
enum class ETutorialEndType : uint8 {
	AutoExpire UMETA(ToolTip = "The tutorial will be visible till its ExpireTime elapses"),
	OnAnyButtonPressed UMETA(ToolTip = "The tutorial will hide if the player presses any keys in the tutorial's context"),
	OnAllButtonsPressed UMETA(ToolTip = "The tutorial will hide if the player presses all of the keys in the tutorial's context"),
	Manual UMETA(ToolTip = "You have to call \"LearnedTutorial\" function yourself. Get the TutorialsManager's reference and call the function. You can store the reference in GameMode or GameInstance or even in the player character's blueprint!")
};

UENUM(BlueprintType)
enum class ETutorialTick : uint8 {
	MovementSpeedBased UMETA(ToolTip = "The algorithm will estimate the arrival time of the player based on movement speed and tick based on that. If the player can use vehicles then this might not be the correct choice for you. (Most Optimal/Cheapest)"),
	FixedTimeInterval UMETA(ToolTip = "It will check the player position in a fixed time range (Cheap)"),
	EveryFrame UMETA(ToolTip = "If the player is super fast and can have crazy speed then you should use it (Most Reliable/Most Expensive)")
};

UENUM(BlueprintType)
enum class ETutorialAbandonAction : uint8 {
	Show UMETA(ToolTip = "The tutorial will show up"),
	Forget UMETA(ToolTip = "The tutorial will be forgotten")
};

USTRUCT(BlueprintType)
struct FTutorialsManagerPlatfromData
{
	GENERATED_BODY()

public:

	FTutorialsManagerPlatfromData() { LightDecoratorDataTable = DarkDecoratorDataTable = nullptr, SearchExpression = TEXT(""), bIsGamepad = false; }
	FTutorialsManagerPlatfromData(FString InExp, bool IsGamepad) { LightDecoratorDataTable = DarkDecoratorDataTable = nullptr, SearchExpression = InExp, bIsGamepad = IsGamepad; }

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Icons")
		class UDataTable* LightDecoratorDataTable;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Icons")
		class UDataTable* DarkDecoratorDataTable;

	// Search Expressions only support "|" as or .e.g. "Gamepad|PS4" means if the key name contain "Gamepad" or "PS4" it will be considered as the platform's key
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SearchQuery")
		FString SearchExpression;

	// This flag filters out most of keys and improves the searching time
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SearchQuery")
		bool bIsGamepad;
};

USTRUCT(BlueprintType)
struct FTutorialRow : public FTableRowBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default")
		ETutorialType Type = ETutorialType::Message;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default")
		FText Title;

	// You can choose pretty much anything, Videos (File Media Source), Images, Materials and etc...
	//
	// * Anything you're choosing should have aspect ratio of 16:9 for best result othervise you're going to have to change the main widget
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "Type == ETutorialType::Modal", DisplayThumbnail = "true", AllowedClasses = "Texture,MaterialInterface,SlateTextureAtlasInterface,FileMediaSource", DisallowedClasses = "MediaTexture"))
		UObject* Media;

	// * If you want to place a key in text type it's like this => " {Action/Axis Name} "	
	// * Combanational Keys: Use + to add them. e.g. {ActionName1+ActionName2+ActionName3+...}
	// * You can also enter key name instead of action name but that would be static! e.g. {Enter}, {Espace}, {Gamepad_FaceButton_Bottom} and etc. Check out the key names list by clicking on 'Print All the Keys' button in the details panel (You should select the world actor in the scene first)
	// * Use "Shift + Enter" for line breaks
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (MultiLine = "true"))
		FText Content;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default")
		ETutorialTriggerType TriggerType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "TriggerType == ETutorialTriggerType::InRange"))
		bool bUseManualVector;

	// Location that triggers the tutorial	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "!bUseManualVector && TriggerType == ETutorialTriggerType::InRange || !bUseManualVector && TriggerType == ETutorialTriggerType::OnCrossed"))
		TSoftObjectPtr<class ATargetPoint> TargetPoint;

	// You can set this if you don't want to use TargetPoints		
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "bUseManualVector && TriggerType == ETutorialTriggerType::InRange"))
		FVector CustomLocation;

	//The radius around the location that triggers the tutorial
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "TriggerType == ETutorialTriggerType::InRange"))
		float Radius;

	// Tutorial's lifetime. Values <= 0 means it will never expire (immortal tutorial)
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "Type != ETutorialType::Modal", ClampMin = "-1"))
		float ExpireTime;

	// Read each item's tooltip for more information	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "Type != ETutorialType::Modal"))
		ETutorialEndType EndType;

	// If the player pawn's range exceeds this the AbandonAcion will happen
	// 
	// * Negative value means no abandonment
	// 
	// * The AbandonRadius will be calculated differently based on the tutorials trigger type
	// 1. On Crossed/In Range: The radius will be calculated from the tutorial's trigger location
	// 2. Manual: The radius will be calculated based on the player's location when this tutorial popped
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (ClampMin = "-1", EditConditionHides, EditCondition = "TriggerType != ETutorialTriggerType::OnPrevTutorialEnd"))
		float AbandonRadius;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default", meta = (EditConditionHides, EditCondition = "TriggerType != ETutorialTriggerType::OnPrevTutorialEnd && AbandonRadius > 0"))
		ETutorialAbandonAction AbandonAction;

	FTutorialRow() { Title = FText::FromString(TEXT("Title")), Content = FText(), TriggerType = ETutorialTriggerType::InRange, TargetPoint = nullptr, bUseManualVector = false, Radius = 200, ExpireTime = 5, EndType = ETutorialEndType::AutoExpire, CustomLocation = FVector::ZeroVector, AbandonRadius = -1, AbandonAction = ETutorialAbandonAction::Show, Media = nullptr; }
};

template <class T>
class ULinkedList
{
public:

	struct ULinkedListNode
	{
		ULinkedListNode(T InValue) {
			Value = InValue;
			Next = nullptr;
		}

		T Value;
		ULinkedListNode* Next;
	};

private:

	ULinkedListNode* Head;

public:

	ULinkedList() { Head = nullptr; }

	ULinkedListNode* Add(T InElement)
	{
		if (Head)
		{
			auto NewNode = new ULinkedListNode(InElement);
			NewNode->Next = Head;
			Head = NewNode;
		}
		else
			Head = new ULinkedListNode(InElement);

		return Head;
	}

	bool Remove(T InElement)
	{
		ULinkedListNode* Prev = nullptr;
		ULinkedListNode* Temp = Head;

		while (Temp)
		{
			if (Temp->Value == InElement)
			{
				if (Prev)
				{
					Prev->Next = Temp->Next;
				}
				else
					Head = Temp->Next;

				return true;
			}

			Prev = Temp;
			Temp = Temp->Next;
		}

		return false;
	}

	void Clear() { Head = nullptr; }

	ULinkedListNode* GetHead() { return Head; }
	void SetHead(ULinkedListNode* NewHead) { Head = NewHead; }
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTutorialsManagerDelegate);

UCLASS(hideCategories = (Input, Rendering, Actor, LOD, "Variable", "Sockets", "Component Replication", "Activation", "Cooking", "Events", "Asset User Data", "Collision"))
class TUTORIALSMANAGERPLUGIN_API ATutorialsManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATutorialsManager();

	// Events	

	UPROPERTY(BlueprintAssignable, Category = "Tutorials Events")
		FOnTutorialsManagerDelegate OnTutorialLearned;

	UPROPERTY(BlueprintAssignable, Category = "Tutorials Events")
		FOnTutorialsManagerDelegate OnInitialized;

	UPROPERTY(BlueprintAssignable, Category = "Tutorials Events")
		FOnTutorialsManagerDelegate OnAllTutorialsLearned;

	UPROPERTY(BlueprintAssignable, Category = "Tutorials Events")
		FOnTutorialsManagerDelegate OnTutorialSetup;

	UPROPERTY(BlueprintAssignable, Category = "Tutorials Events")
		FOnTutorialsManagerDelegate OnAbandoned;

	UPROPERTY(BlueprintAssignable, Category = "UI Events")
		FOnTutorialsManagerDelegate OnModalShow;

	UPROPERTY(BlueprintAssignable, Category = "UI Events")
		FOnTutorialsManagerDelegate OnModalHide;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
		class UBillboardComponent* Billboard;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Setup")
		APawn* PlayerPawn;

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetPlayerPawn(APawn* InPawn);

	UFUNCTION(BlueprintPure, Category = "Settings")
		APawn* GetPlayerPawn() { return PlayerPawn; }

protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings")
		ETutorialTick TickType = ETutorialTick::MovementSpeedBased;

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetTickType(ETutorialTick InType) { TickType = InType; }

	UFUNCTION(BlueprintPure, Category = "Settings")
		ETutorialTick GetTickType() { return TickType; }

protected:

	// Set it a bit higher than the player's max movement speed just for assurance
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (ClampMin = "0", EditConditionHides, EditCondition = "TickType == ETutorialTick::MovementSpeedBased"))
		float MaxMovementSpeed = 1000;

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetMaxMovementSpeed(float InSpeed) { MaxMovementSpeed = InSpeed; }

	UFUNCTION(BlueprintPure, Category = "Settings")
		float GetMaxMovementSpeed() { return MaxMovementSpeed; }

protected:

	// In Seconds
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (ClampMin = "0", EditConditionHides, EditCondition = "TickType == ETutorialTick::FixedTimeInterval"))
		float TickTimeInterval = 1;

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetTickTimeInterval(float Interval) { TickTimeInterval = Interval; }

	UFUNCTION(BlueprintPure, Category = "Settings")
		float GetTickTimeInterval() { return TickTimeInterval; }

protected:

	// The icons will be different based on the selected platform
	// Use SetPlatform if you like to change it at runtime
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings|Platform")
		ETutorialPlatform TargetPlatform = ETutorialPlatform::PC;

public:
	UFUNCTION(BlueprintCallable, Category = "InPlatform")
		void SetPlatform(ETutorialPlatform InPlatform);

protected:

	// Please set the decorator table for each platform
	// 
	// We distinguish platform keys with a quick search in the key's name	
	// You can see all the key names by pressing the "PrintAllTheKeys" button
	// If you're using custom platforms and you wish to use action mapping based key detection then you have to set this variable up with proper information
	// 
	// Check out the documentation for more information	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Platform")
		TMap<ETutorialPlatform, FTutorialsManagerPlatfromData> PlatformsData;

	// Check out the output log
	UFUNCTION(CallInEditor, Category = "Assistance")
		void PrintAllTheKeys();

	UFUNCTION(CallInEditor, Category = "Assistance")
		void CheckDataTable();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings", meta = (RowType = "FTutorialRow"))
		UDataTable* TutorialsDataTable;

	// Whatever UI widget you're using it MUST be derrived from TutorialWidget class.
	UPROPERTY(EditAnywhere, DisplayName = "UI Mode", Category = "Settings|UI")
		ETutorialUIMode UIMode;

	// Key icons theme can be changed to dark or light based on your personal taste.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, DisplayName = "Icons Theme", Category = "Settings|UI")
		ETutorialTheme UIIconsTheme = ETutorialTheme::Light;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings|UI", meta = (EditConditionHides, EditCondition = "UIMode == ETutorialUIMode::TwoDimensional"))
		TSubclassOf<UTutorialWidget> TutorialMasterWidgetBlueprint;

	// If you're losing focus on clicking the modal tutorial you should definitely increase this value.
	UPROPERTY(EditDefaultsOnly, DisplayName = "UI ZOrder", BlueprintReadWrite, Category = "Settings|UI", meta = (EditConditionHides, EditCondition = "UIMode == ETutorialUIMode::TwoDimensional"))
		int UIZOrder = 9999;

	// If you're not planning to use the manager after all of the tutorials were learned
	// It's better to just destroy it to save some memory!
	UPROPERTY(EditAnywhere, Category = "Settings|Optimization")
		bool bDestroyOnAllTutorialsLearned = true;

	// Unbinds everything from the event after it fired
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Settings")
		bool bClearEventsOnCall = false;	

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetClearEventsOnCall(bool bValue) { bClearEventsOnCall = bValue; }

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetDestroyOnAllTutorialsLearned(bool bValue) { bDestroyOnAllTutorialsLearned = bValue; }

	UFUNCTION(BlueprintCallable, Category = "Settings|UI")
		void SetIconsTheme(ETutorialTheme NewTheme);

public:
	/* -- Utility Functions for Getting InKey Textures */

	// It will go through the decorator table and find the key texture
	// 
	// * Use GetActivePlatformKeyMappingTexture() if you wish to get the texture for the selected platform.				
	UFUNCTION(BlueprintCallable, Category = "Icons")
		bool GetKeyTextureOfPlatform(UTexture2D*& OutTexture, FKey InKey, ETutorialPlatform InPlatform = ETutorialPlatform::PC);

	// It will go through the decorator table and find the key texture
	UFUNCTION(BlueprintCallable, Category = "Icons")
		bool GetActivePlatformKeyTexture(UTexture2D*& OutTexture, FKey InKey);

	// It will go through the decorator tables one by one and find the key texture so it's a bit expensive in terms of performance
	UFUNCTION(BlueprintCallable, Category = "Icons")
		bool GetKeyTexture(UTexture2D*& OutTexture, FKey InKey);

	// This is better in terms of optimizations
	// 
	// * Use GetActivePlatformKeyMappingTexture() if you wish to get the texture for the selected platform.
	// 
	// @param KeyMappingName This can be both Action or Axis mapping.
	// @param KeyMappingIndex If you have multiple keys added to a key mapping this will come handy, especially for the axis mappings. If you had binded two keys then, Index 0 would the first one and Index 1 would be second one!
	// @return false if couldn't find the key mapping, true otherwise.
	UFUNCTION(BlueprintCallable, Category = "Icons")
		bool GetKeyMappingTextureOfPlatform(UTexture2D*& OutTexture, FName InKeyMappingName, int KeyMappingIndex = 0, ETutorialPlatform InPlatform = ETutorialPlatform::PC);

	// This is better in terms of optimizations
	// @param KeyMappingName This can be both Action or Axis mapping.
	// @param KeyMappingIndex If you have multiple keys added to a key mapping this will come handy, especially for the axis mappings. If you had binded two keys then, Index 0 would the first one and Index 1 would be second one!
	// @return false if couldn't find the key mapping, true otherwise.
	UFUNCTION(BlueprintCallable, Category = "Icons")
		bool GetActivePlatformKeyMappingTexture(UTexture2D*& OutTexture, FName InKeyMappingName, int KeyMappingIndex = 0);

private:

	UTutorialWidget* TutorialWidget;
	UWidgetComponent* TutorialWidgetComponent;

protected:

	// When a tutorial finishes this delay will prevent the next one to pop immediately
	UPROPERTY(EditAnywhere, Category = "Settings|UI", meta = (ClampMin = "0"))
		float NextTutorialPopingDelay = 1.5;

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetNextTutorialPopingDelay(float Delay) { NextTutorialPopingDelay = Delay; }

	UFUNCTION(BlueprintPure, Category = "Settings")
		float GetNextTutorialPopingDelay() { return NextTutorialPopingDelay; }

protected:

	// When modal showed up player needs to hold down the approval key for this amount of time
	UPROPERTY(EditAnywhere, Category = "Settings|UI", meta = (ClampMin = "0"))
		float ModalApprovalHoldTime = 1;

public:

	UFUNCTION(BlueprintCallable, Category = "Settings")
		void SetModalApprovalHoldTime(float HoldTime) { ModalApprovalHoldTime = HoldTime; }

	UFUNCTION(BlueprintPure, Category = "Settings")
		float GetModalApprovalHoldTime() { return ModalApprovalHoldTime; }

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Sounds")
		class USoundBase* MessageTutorialShowSound;
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Sounds")
		class USoundBase* MessageTutorialHideSound;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Sounds")
		class USoundBase* TutorialModalShowSound;
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Sounds")
		class USoundBase* TutorialModalHideSound;

	// Enter the row's number of the tutorial from the DataTable to start from that
	UPROPERTY(EditAnywhere, Category = "Debugging")
		int StartRowNumber = 0;

	// If the start up logs are annoying you can turn them off for good! No logs will be displayed on BeginPlay.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debugging")
		bool bMuteInitLogs = false;

public:

	UPROPERTY(EditInstanceOnly, Category = "Preview", meta = (ClampMin = "1"))
		int PreviewRowNumber = 1;

	UFUNCTION(CallInEditor, Category = "Preview")
		void PreviewTutorialTriggerArea();

private:

	TArray<FTutorialRow*> Tutorials;

public:

	UFUNCTION(BlueprintPure, Category = "Information")
		FTutorialRow GetTutorial(int RowNumber = 1);

	UFUNCTION(BlueprintPure, Category = "Information")
		FTutorialRow GetActiveTutorial(int& Idx);

	UFUNCTION(BlueprintPure, Category = "Information")
		int GetTutorialsNum() { return Tutorials.Num(); }

	UFUNCTION(BlueprintPure, Category = "Information")
		TArray<FTutorialRow> GetAllOfTutorials();

	// Call it to prepare the actor for destruction, it clears all of its timers and etc...
	UFUNCTION(BlueprintCallable, Category = "Destruction")
		void Reset() override;

	// This will only change the visibility! The functionality won't change
	// The tutorial manager will work in the background anyway
	UFUNCTION(BlueprintCallable, Category = "Widget")
		void SetVisibility(bool bVisible);

	UFUNCTION(BlueprintPure, Category = "Widget")
		UTutorialWidget* GetTutorialsWidget() { return TutorialWidget; }

	// If you're using 3D UI mode you, this function will return the widget component
	UFUNCTION(BlueprintPure, Category = "Widget")
		UWidgetComponent* GetTutorialsWidgetComponent() { return TutorialWidgetComponent; }

	UFUNCTION(BlueprintPure, Category = "Widget")
		ETutorialUIMode GetUIMode() { return UIMode; }

	UFUNCTION(BlueprintCallable, Category = "Widget")
		void SetUIMode(ETutorialUIMode Mode);

	UFUNCTION(BlueprintCallable, DisplayName = "Set 3D Widget Component", Category = "Widget")
		void Set3DWidgetComponent(UWidgetComponent* InWidgetComponent);

	// If the tutorial trigger type is set to manual, you should trigger it by this function
	UFUNCTION(BlueprintCallable, Category = "Manual Trigger")
		void PopTutorial();

	// The current awaiting tutorial will get shown
	// And the next tutorial on the list will start preparing for getting triggered	
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void Next(bool bPlaySounds = false);

	// It will prepare the previous tutorial on the list for getting triggered
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		bool Previous(bool bPlaySounds = false);

	/**
	* Jumps to another tutorial
	*
	* @param RowNumber The DataTable's row number should be passed
	* @return false if RowNumber was invalid, true otherwise
	*/
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		bool JumpTo(int RowNumber, bool bPlaySounds = false);

	// The current tutorial will be marked as learned and the next tutorial will pop
	UFUNCTION(BlueprintCallable, Category = "Manual Control")
		void LearnedTutorial();

	UFUNCTION(BlueprintPure, Category = "Save & Load")
		int GetActiveTutorialRowNumber() { return CurrentIndex + 1; }

	/**
	* Loads a specefic tutorial based on it's row number
	* You should probably call it on level begin play when you've loaded your game
	*
	* @param RowNumber The DataTable's row number should be passed
	* @param bHideAllTutorials Hides the current visible tutorials
	* @return false if RowNumber was invalid, true otherwise
	*/
	UFUNCTION(BlueprintCallable, Category = "Save & Load")
		bool LoadTutorial(int RowNumber = 1, bool bHideAllTutorials = true);

	/**
	* Do not call it manually unless you recieved initializing errors.
	* If the plugin failed to initialize you can mostly fix the problems through the details panel in the editor
	* But if you decided to initialize the plugin some time later than than the game began then you should call this function.
	*/
	UFUNCTION(BlueprintCallable, Category = "Advanced")
		void Initialize();

private:

	// Manages ticking of the actor
	void SetupTicking();

	// Fires if all of the tutorials were learned
	void AllTutorialsLearned();

	UWorld* World;

	FTimerHandle Timer;
	FTimerHandle LearnedTimer;

	FTimerHandle DelayTimer;

	void InRangeTick();

	// InRange Tick Main Execution Function
	// @return true if popped tutorial
	bool InRangeTickExec();

	void OnCrossedTick();

	// OnCrossed Tick Main Execution Function
	// @return true if popped tutorial
	bool OnCrossedTickExec();

	float OnCrossedDotProduct;

	FTimerHandle AbandonTimer;
	void StartAbandonTick();
	void AbandonTick();
	const float AbandonTickInterval = 1;
	FVector AbandonOrigin;

	// The tutorial which is currently waiting to pop in the right time
	int CurrentIndex = 0;

	// Finds action name and replaces it with decorator format
	// And also binds all of the inputs
	FText PlaceDecorators(FText Text);

	// Inputs Handling	
	const FName TutorialActionName = TEXT("TutorialManagerActions");
	void ClearActionBindings();

	DECLARE_DELEGATE_TwoParams(FActionKeyDelegate, int, bool);

	void OnActionKeyDown(int Idx = 0, bool bIsCombanational = false);
	void OnActionKeyUp(int Idx = 0, bool bIsCombanational = true);

	FString GenerateDefaultCombinationKeys(FInputActionKeyMapping ActionMapping);

	// Generates a new action binding and sets up key press and key release
	void GenerateKeyBinding(int& BindingCounter, FInputActionKeyMapping KeyMapping, bool bIsCombational = false, ULinkedList<int>& CombinationalKeys = *(new ULinkedList<int>()));

	class UPlayerInput* PlayerInput;
	TArray<bool> PressedActionKeys;

	ULinkedList<ULinkedList<int>> CombinationalKeysLists;

	// Estimates arrival time to pending tutorial
	float EstimateArrivalTime();
	// Estimates cross time to pending tutorial
	float EstimateCrossTime();
	// Dot product of player and tutorial's location
	float InRangeDotProduct;

	// Reset() will turn it true!
	bool bIsPendingReload = false;

	bool bIsInitialized = false;

	bool bMuteSounds = false;
	FTimerHandle MuteTimer;
public:

	// Mute for duration
	// @param Duration Negative values means forever!
	UFUNCTION(BlueprintCallable, Category = "Sounds")
		void Mute(float Duration = 0.5f);

	UFUNCTION(BlueprintCallable, Category = "Sounds")
		void Unmute();
};
