// Copyright 2022 Koorosh Torabi <thekooroshtorabi@gmail.com> - All Rights Reserved.

#include "../Public/TutorialsManager.h"
#include "../Public/TutorialWidget.h"
#include "TutorialsManager.h"
#include "Sound/SoundWave.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/InputSettings.h"
#include "Components/BillboardComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "Engine/Engine.h"
#include "Components/RichTextBlockImageDecorator.h"
#include "Components/WidgetComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
ATutorialsManager::ATutorialsManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Billboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	SetRootComponent(Billboard);

	DelayTimer = LearnedTimer = Timer = FTimerHandle();

	PlatformsData.Add(ETutorialPlatform::PC, FTutorialsManagerPlatfromData(TEXT(""), false));

	// Console
	PlatformsData.Add(ETutorialPlatform::BlueConsole, FTutorialsManagerPlatfromData(TEXT("Gamepad|PS4"), true));
	PlatformsData.Add(ETutorialPlatform::GreenConsole, FTutorialsManagerPlatfromData(TEXT("Gamepad"), true));

	// VR
	PlatformsData.Add(ETutorialPlatform::OcsQuest, FTutorialsManagerPlatfromData(TEXT("usTouch"), true));
	PlatformsData.Add(ETutorialPlatform::VIdx, FTutorialsManagerPlatfromData(TEXT("Index"), true));
	PlatformsData.Add(ETutorialPlatform::HVive, FTutorialsManagerPlatfromData(TEXT("Vive"), true));

	UIMode = ETutorialUIMode::TwoDimensional;
}

// Called when the game starts or when spawned
void ATutorialsManager::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();

	Initialize();
}

void ATutorialsManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	switch (Tutorials[CurrentIndex]->TriggerType)
	{
	case ETutorialTriggerType::InRange:
		InRangeTickExec();
		break;

	case ETutorialTriggerType::OnCrossed:
		OnCrossedTickExec();
		break;
	}
}

void ATutorialsManager::SetPlatform(ETutorialPlatform InPlatform)
{
	UDataTable* NewDecorator = nullptr;
	if (UIIconsTheme == ETutorialTheme::Light)
	{
		NewDecorator = PlatformsData.FindRef(InPlatform).LightDecoratorDataTable;
	}
	else
	{
		NewDecorator = PlatformsData.FindRef(InPlatform).DarkDecoratorDataTable;
	}

	if (!NewDecorator)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Setting Platform Failed."));
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: %s platform's decorator's data table hasn't been set! Please set it through the \"BP_TutorialsManager\" blueprint."), *StaticEnum<ETutorialPlatform>()->GetNameStringByValue((int64)InPlatform));
		return;
	}

	TargetPlatform = InPlatform;
	TutorialWidget->SetDecorator(NewDecorator);

	if (TutorialWidget->IsVisible())
	{
		// Because the CurrentIndex increaments on tutorial pop!
		CurrentIndex -= 1;

		if (CurrentIndex < 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Tutorials Manager: You are calling SetPlatform way too early!"));
			return;
		}

		// Message Box Tutorial
		if (Tutorials[CurrentIndex]->Type == ETutorialType::Message)
		{
			TutorialWidget->UpdateMessageTutorialText(PlaceDecorators(Tutorials[CurrentIndex]->Content));
		}
		// Modal Tutorial
		else
		{
			TutorialWidget->UpdateModalTutorialText(PlaceDecorators(Tutorials[CurrentIndex]->Content));
		}

		// Setting index back to what it was
		CurrentIndex += 1;
	}
}

void ATutorialsManager::PrintAllTheKeys()
{
	TArray<FKey> Keys;
	EKeys::GetAllKeys(Keys);

	UE_LOG(LogTemp, Warning, TEXT("Display Name  |  Key's Name (The one that search through)"));
	for (auto Key : Keys)
	{
		UE_LOG(LogTemp, Log, TEXT("%s : %s"), *Key.GetDisplayName().ToString(), *Key.GetFName().ToString());
	}
}

void ATutorialsManager::CheckDataTable()
{
	if (!TutorialsDataTable)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: You forgot to set the data table! Please set it through the details panel."));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("----------------------- Tutorials Manager Data Table Check Logs -----------------------"));

	TArray<FTutorialRow*> Rows;
	TutorialsDataTable->GetAllRows<FTutorialRow>(*(new FString()), Rows);
	int ErrorsCounter = 0;

	for (int r = 0; r < Rows.Num(); ++r)
	{
		auto Tutorial = Rows[r];

		if ((Tutorial->TriggerType == ETutorialTriggerType::InRange && !Tutorial->bUseManualVector && !Tutorial->TargetPoint.IsValid()) // In Range
			|| (!Tutorial->TargetPoint.IsValid() && Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)) // Crossed
		{
			UE_LOG(LogTemp, Error, TEXT("Row %d Target Point: Please set a valid target point in the DataTable."), r);
			++ErrorsCounter;
		}

		if (Tutorial->Type == ETutorialType::Modal && !Tutorial->Media)
		{
			UE_LOG(LogTemp, Error, TEXT("Row %d Media: You forgot to set the tutorial's media of \"%s\" in the tutorials data table."), r, *Tutorial->Title.ToString());
			++ErrorsCounter;
		}

		if (Tutorial->Type == ETutorialType::Message && Tutorial->EndType == ETutorialEndType::AutoExpire && Tutorial->ExpireTime <= 0)
		{
			UE_LOG(LogTemp, Error, TEXT("Row %d Immortal Tutorial: This tutorial will neven hide! Please set an auto expire time for it unless you're going to call LearnedTutorial yourself."), r);
			++ErrorsCounter;
		}

		// Key Mapping Errors
		TArray<FString> Words;
		Tutorial->Content.ToString().ParseIntoArray(Words, TEXT(" "));

		int ValidKeys = 0;
		for (int i = 0; i < Words.Num(); ++i)
		{
			if (Words[i][0] == '{' && Words[i][Words[i].Len() - 1] != '}' || Words[i][0] != '{' && Words[i][Words[i].Len() - 1] == '}')
			{
				UE_LOG(LogTemp, Error, TEXT("Row %d Key Mapping: You had probably forgot to add space before or after the '{ | }' in your tutorial's text! Check out the %s data table."), r + 1, *TutorialsDataTable->GetName());
				++ErrorsCounter;
			}
			else if (Words[i][0] == '{' && Words[i][Words[i].Len() - 1] == '}')
			{
				++ValidKeys;
			}
		}

		if ((Tutorial->EndType == ETutorialEndType::OnAllButtonsPressed || Tutorial->EndType == ETutorialEndType::OnAnyButtonPressed) && ValidKeys <= 0)
		{
			UE_LOG(LogTemp, Error, TEXT("Row %d No Keys: The tutorial's end type is key based and no valid keys were found in the tutorial's content. Please try adding actions or direct keys to the content using {ActionName/Key Name}"), r + 1, *TutorialsDataTable->GetName());
			++ErrorsCounter;
		}
	}

	if (ErrorsCounter <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("All Good! No Errors Found"));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%d %s"), ErrorsCounter, ErrorsCounter > 1 ? TEXT("Errors Found.") : TEXT("Error Found"));
	}

	UE_LOG(LogTemp, Warning, TEXT("---------------------------------------------------------------------------------------"));

}

void ATutorialsManager::SetIconsTheme(ETutorialTheme NewTheme)
{
	UIIconsTheme = NewTheme;

	// Updates the Theme
	SetPlatform(TargetPlatform);
}

bool ATutorialsManager::GetKeyTextureOfPlatform(UTexture2D*& OutTexture, FKey InKey, ETutorialPlatform InPlatform)
{
	UDataTable* PlatformDecoratorTable = nullptr;
	if (UIIconsTheme == ETutorialTheme::Light)
	{
		PlatformDecoratorTable = PlatformsData.FindRef(InPlatform).LightDecoratorDataTable;
	}
	else
	{
		PlatformDecoratorTable = PlatformsData.FindRef(InPlatform).DarkDecoratorDataTable;
	}

	if (!PlatformDecoratorTable)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: GetKeyTexture Failed. There is no decorator table for %s platform."), *StaticEnum<ETutorialPlatform>()->GetNameStringByValue((int64)InPlatform));
		OutTexture = nullptr;
		return false;
	}

	auto FoundRow = PlatformDecoratorTable->FindRow<FRichImageRow>(InKey.GetFName(), *(new FString()));

	if (FoundRow)
	{
		if (FoundRow->Brush.GetResourceObject()->GetClass()->IsChildOf<UTexture2D>())
		{
			OutTexture = Cast<UTexture2D>(FoundRow->Brush.GetResourceObject());
			return true;
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("The key (%s) texture that is set in the data table is not a texture therefore this function cannot work for that!"), *InKey.GetDisplayName().ToString());
		}
	}

	OutTexture = nullptr;
	return false;
}

bool ATutorialsManager::GetActivePlatformKeyTexture(UTexture2D*& OutTexture, FKey InKey)
{
	return GetKeyTextureOfPlatform(OutTexture, InKey, TargetPlatform);
}

bool ATutorialsManager::GetKeyTexture(UTexture2D*& OutTexture, FKey InKey)
{
	// Target Platform has higher priority	
	if (GetKeyTextureOfPlatform(OutTexture, InKey, TargetPlatform))
		return true;

	for (auto PlatformData : PlatformsData)
	{
		// Skip the TargetPlatform
		if (PlatformData.Key == TargetPlatform)
			continue;

		if (GetKeyTextureOfPlatform(OutTexture, InKey, PlatformData.Key))
			return true;
	}

	return false;
}

bool ATutorialsManager::GetKeyMappingTextureOfPlatform(UTexture2D*& OutTexture, FName InKeyMappingName, int KeyMappingIndex, ETutorialPlatform InPlatform)
{
	TArray<FKey> FoundKeys;

	// Finding Action
	TArray<FInputActionKeyMapping> KeyMappings;
	UInputSettings::GetInputSettings()->GetActionMappingByName(InKeyMappingName, KeyMappings);

	FTutorialsManagerPlatfromData* PlatformQuery;
	if (PlatformsData.Contains(InPlatform))
	{
		PlatformQuery = PlatformsData.Find(InPlatform);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Get Key Texure Failed. There is no decorator data table for %s platform!"), *StaticEnum<ETutorialPlatform>()->GetNameStringByValue((int64)InPlatform));
		return false;
	}

	if (KeyMappings.Num() > 0)
	{
		// If we have more than one key mapping we're gonna use PlatformsData				
		if (KeyMappings.Num() > 1)
		{
			for (int k = 0; k < KeyMappings.Num(); ++k)
			{
				// We won't return "None" keys
				if (!KeyMappings[k].Key.IsValid())
					continue;

				if (PlatformQuery->bIsGamepad == KeyMappings[k].Key.IsGamepadKey())
				{
					if (PlatformQuery->SearchExpression.IsEmpty())
					{
						FoundKeys.Push(KeyMappings[k].Key);
					}
					else
					{
						TArray<FString> SearchKeys;
						PlatformQuery->SearchExpression.ParseIntoArray(SearchKeys, TEXT("|"));

						for (int wi = SearchKeys.Num() - 1; wi >= 0; --wi)
							if (KeyMappings[k].Key.GetFName().ToString().Contains(SearchKeys[wi]))
							{
								FoundKeys.Push(KeyMappings[k].Key);
								break;
							}
					}
				}
			}
		}
		// For the purpose of optimization of single key mapping inputs
		else if(KeyMappings[0].Key.IsValid())
		{
			FoundKeys.Push(KeyMappings[0].Key);
		}
	}
	// Finding Axis		
	else
	{
		TArray<FInputAxisKeyMapping> AxisMappings;
		UInputSettings::GetInputSettings()->GetAxisMappingByName(InKeyMappingName, AxisMappings);

		if (AxisMappings.Num() > 0)
		{
			// If we have more than one key mapping we're gonna use PlatformsData
			if (AxisMappings.Num() > 1)
			{
				for (int j = AxisMappings.Num() - 1; j >= 0; --j)
				{
					// We won't return "None" keys
					if (!AxisMappings[j].Key.IsValid())
						continue;

					if (PlatformQuery->bIsGamepad == AxisMappings[j].Key.IsGamepadKey())
					{
						if (PlatformQuery->SearchExpression.IsEmpty())
						{
							FoundKeys.Push(AxisMappings[j].Key);
						}
						else
						{
							TArray<FString> SearchKeys;
							PlatformQuery->SearchExpression.ParseIntoArray(SearchKeys, TEXT("|"));

							for (int wi = SearchKeys.Num() - 1; wi >= 0; --wi)
								if (AxisMappings[j].Key.GetFName().ToString().Contains(SearchKeys[wi]))
								{
									FoundKeys.Push(AxisMappings[j].Key);
									break;
								}
						}
					}
				}
			}
			// For the purpose of optimization of single key mapping inputs
			else if (AxisMappings[0].Key.IsValid())
			{
				FoundKeys.Push(AxisMappings[0].Key);
			}
		}
		// No Axis Mapping and No Action Mapping Found with This Key Mapping Name.
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Get Key Texture Failed. Key Mapping Name (%s) Not Found."), *InKeyMappingName.ToString());
			return false;
		}
	}

	if (KeyMappingIndex >= FoundKeys.Num())
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Get Key Texture Failed. Index out of range. You're trying to get index %d of size %d for %s key mapping!"), KeyMappingIndex, FoundKeys.Num(), *InKeyMappingName.ToString());
		return false;
	}

	return GetKeyTextureOfPlatform(OutTexture, FoundKeys[KeyMappingIndex], InPlatform);
}

bool ATutorialsManager::GetActivePlatformKeyMappingTexture(UTexture2D*& OutTexture, FName InKeyMappingName, int KeyMappingIndex)
{
	return GetKeyMappingTextureOfPlatform(OutTexture, InKeyMappingName, KeyMappingIndex, TargetPlatform);
}

void ATutorialsManager::PreviewTutorialTriggerArea()
{
	Tutorials.Empty();

	if (!TutorialsDataTable)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Please set the Tutorials Data Table of %s actor through the details panel."), *GetName());
		return;
	}

	TutorialsDataTable->GetAllRows<FTutorialRow>(*(new FString()), Tutorials);

	if (Tutorials.Num() <= PreviewRowNumber - 1)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Previewing Tutorial Failed. Index (%d) out of range, this data table only has %d entries."), CurrentIndex, Tutorials.Num());

		if (FApp::GetBuildConfiguration() != EBuildConfiguration::Shipping && GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "Index out of range!", true, FVector2D(1));

		return;
	}

	auto Tutorial = Tutorials[PreviewRowNumber - 1];

	if (Tutorial->TriggerType == ETutorialTriggerType::InRange)
	{
		if (!Tutorial->bUseManualVector && !Tutorial->TargetPoint.IsValid())
		{
			UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Previewing Tutorial Failed. In Range tutorials must have a valid Target Point!"));
			return;
		}

		DrawDebugSphere(GetWorld(), Tutorial->bUseManualVector ? Tutorial->CustomLocation : Tutorial->TargetPoint->GetActorLocation(), Tutorial->Radius, 16, FColor::Red, false, 5, 0, 2);

		if (Tutorial->AbandonRadius > 0)
			DrawDebugSphere(GetWorld(), Tutorial->bUseManualVector ? Tutorial->CustomLocation : Tutorial->TargetPoint->GetActorLocation(), Tutorial->AbandonRadius, 32, FColor(0, 100, 250), false, 5, 0, 5);
	}
	else if (Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)
	{
		if (!Tutorial->TargetPoint.IsValid())
		{
			UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Previewing Tutorial Failed. OnCrossed tutorials must have a valid Target Point!"));
			return;
		}

		DrawDebugSolidPlane(GetWorld(), FPlane(Tutorial->TargetPoint->GetTargetLocation(), Tutorial->TargetPoint->GetActorForwardVector()),
			Tutorial->TargetPoint->GetTargetLocation(), FVector2D(9999, 2000), FColor(255, 0, 0, 100), false, 5, 0);

		if (Tutorial->AbandonRadius > 0)
			DrawDebugSphere(GetWorld(), Tutorial->TargetPoint->GetActorLocation(), Tutorial->AbandonRadius, 32, FColor(0, 100, 250), false, 5, 0, 5);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Previewing Tutorial Failed. Because the tutorial trigger type is not location based"));

		if (FApp::GetBuildConfiguration() != EBuildConfiguration::Shipping && GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "The selected preview tutorial's trigger type is not location based", true, FVector2D(1));
	}
}

void ATutorialsManager::Reset()
{
	Super::Reset();

	World->GetTimerManager().ClearAllTimersForObject(this);

	World->GetTimerManager().ClearTimer(LearnedTimer);
	World->GetTimerManager().ClearTimer(Timer);
	World->GetTimerManager().ClearTimer(DelayTimer);

	bIsPendingReload = true;
}

void ATutorialsManager::SetVisibility(bool bVisible)
{
	if (bVisible)
	{
		// Message Box Tutorial
		if (Tutorials[CurrentIndex]->Type == ETutorialType::Message)
		{
			TutorialWidget->ShowMessageTutorial(PlaceDecorators(Tutorials[CurrentIndex]->Content));
		}
		// Modal Tutorial
		else
		{
			// There will be no holding if the widget is 3D and in world space
			float ModalHoldTime =
				(UIMode == ETutorialUIMode::ThreeDimensional && TutorialWidgetComponent && TutorialWidgetComponent->GetWidgetSpace() == EWidgetSpace::World) ?
				0 : ModalApprovalHoldTime;

			TutorialWidget->SetupModal(ModalHoldTime);
			TutorialWidget->ShowModalTutorial(Tutorials[CurrentIndex]->Title, PlaceDecorators(Tutorials[CurrentIndex]->Content), Tutorials[CurrentIndex]->Media);
		}

		TutorialWidget->Unlock();
	}
	else
	{
		TutorialWidget->HideTutorial();

		TutorialWidget->Lock();
	}
}

void ATutorialsManager::Initialize()
{
	if (!PlayerPawn)
	{
		if (!bMuteInitLogs)
		{
			World->GetTimerManager().SetTimer(*(new FTimerHandle()), [this]()
				{
					if (!IsValidLowLevel()) return;

					if (!PlayerPawn)
						UE_LOG(LogTemp, Error, TEXT("TutorialsManager: Please set the player pawn of %s actor through the details panel. \nOr you can set it at runtime via calling SetPlayerPawn() function, to do this you just need a reference of TutorialsManager. You can expose it to GameMode/GameInstance to be able to access it all the time and everywhere."), *GetName());
				}
			, 1, false);
		}
		return;
	}

	if (!TutorialsDataTable)
	{
		if (!bMuteInitLogs)
		{
			UE_LOG(LogTemp, Error, TEXT("TutorialsManager: Please set the Tutorials Data Table of %s actor through the details panel."), *GetName());
		}
		return;
	}

	auto PlayerController = PlayerPawn->GetController<APlayerController>();

	if (!PlayerController)
	{
		if (!bMuteInitLogs)
		{
			UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Unexpected Error. \nThere are 2 possible reasons: \n1. The player pawn is not possessed by any player controller. \n2. Your player pawn's controller is not inheriting from APlayerController class."));
			UE_LOG(LogTemp, Warning, TEXT(" --- SOLUTIONS --- \nSolution #1: Select the pawn and go to the details panel, search \"Possess\" then click on the \"Auto Possess Player\" drop down and select \"Player0\". \nSolution #2: Select PlayerController class as the parent class of your current controller. \n* Different Solution: If you don't want to auto possess the pawn, then you can just call \"Initialize\" function of the Tutorials Manager actor after you possessed the selected player pawn."));
		}
		return;
	}

	if (UIMode == ETutorialUIMode::TwoDimensional)
	{
		if (!TutorialMasterWidgetBlueprint)
		{
			if (!bMuteInitLogs)
			{
				UE_LOG(LogTemp, Error, TEXT("TutorialsManager: Please set the Tutorial Master Widget Blueprint of %s actor through the details panel."), *GetName());
			}
			return;
		}

		if (!TutorialWidget)
		{
			TutorialWidget = CreateWidget<UTutorialWidget>(PlayerController, TutorialMasterWidgetBlueprint);

			if (!TutorialWidget)
			{
				if (!bMuteInitLogs)
				{
					UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Unexpected Error. Tutorial Master Widget is glitched out, please clear and re-select the widget blueprint in the details panel of %s."), *GetName());
				}
				return;
			}

			TutorialWidget->AddToViewport(UIZOrder);
			TutorialWidget->SetTutorialsManager(this);
		}
	}
	else if (UIMode == ETutorialUIMode::ThreeDimensional && !TutorialWidget)
	{
		if (!bMuteInitLogs)
		{
			World->GetTimerManager().SetTimer(*(new FTimerHandle()), [this]()
				{
					if (!IsValidLowLevel()) return;

					if (!TutorialWidget)
						UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: You need to expose your 3D widget component to TutorialsManager since you're using 3D UI Mode! Use \"Set 3D Widget Component\" function to set the widget component."));
				}
			, 1, false);
		}

		return;
	}

	SetPlatform(TargetPlatform);

	// We use the above code for other re-initiazing UI as well
	if (bIsInitialized)
		return;

	// Making the action binding to use it later	
	PlayerInput = PlayerController->PlayerInput;

	// Loading Data from Table	
	TutorialsDataTable->GetAllRows<FTutorialRow>(*(new FString()), Tutorials);

	if (Tutorials.Num() > 0)
	{
		if (FApp::GetBuildConfiguration() != EBuildConfiguration::Shipping && StartRowNumber >= 1 && StartRowNumber <= Tutorials.Num())
		{
			CurrentIndex = StartRowNumber - 1;

			if (Tutorials[CurrentIndex]->TriggerType == ETutorialTriggerType::OnPrevTutorialEnd)
			{
				Mute();
				/* Because of the 3D widgets we postpone it to next tick! */
				World->GetTimerManager().SetTimerForNextTick(this, &ATutorialsManager::PopTutorial);
			}
		}
		else if (Tutorials.Num() > 0 && Tutorials[0]->TriggerType == ETutorialTriggerType::OnPrevTutorialEnd)
			World->GetTimerManager().SetTimerForNextTick(this, &ATutorialsManager::PopTutorial); /* Because of the 3D widgets we postpone it to next tick! */

		SetupTicking();
	}
	// Just for Protection
	else
	{
		FTutorialRow* EmptyRow = new FTutorialRow();
		EmptyRow->TriggerType = ETutorialTriggerType::Manual;
		Tutorials.Add(EmptyRow);

		if (!bMuteInitLogs)
		{
			UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: The Tutorials Data Table is empty! This might cause some issues."));
		}
	}

	OnInitialized.Broadcast();

	if (bClearEventsOnCall)
		OnInitialized.RemoveAll(this);

	bIsInitialized = true;
}

void ATutorialsManager::SetPlayerPawn(APawn* InPawn)
{
	if (!InPawn)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Set Player Pawn Failed. Null object feeded into this function!"));
		return;
	}

	auto PlayerController = InPawn->GetController<APlayerController>();

	if (!PlayerController)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Set Player Pawn Failed due to Controller Issues. \nThere are 2 possible reasons: \n1. The new player pawn is not possessed by any player controller. \n2. Your new player pawn's controller is not inheriting from APlayerController class."));
		return;
	}

	PlayerPawn = InPawn;
	PlayerInput = PlayerController->PlayerInput;

	if (!bIsInitialized)
		Initialize();
}

void ATutorialsManager::SetUIMode(ETutorialUIMode Mode)
{
	// 2D
	if (Mode == ETutorialUIMode::TwoDimensional)
	{
		if (TutorialWidgetComponent && TutorialWidget)
			TutorialWidget->HideTutorial();
	}
	// 3D
	else
	{
		if (!TutorialWidget) return;

		TutorialWidget->RemoveFromParent();
	}

	TutorialWidget = nullptr;

	UIMode = Mode;

	Initialize();
}

void ATutorialsManager::Set3DWidgetComponent(UWidgetComponent* InWidgetComponent)
{
	if (!InWidgetComponent) return;

	// Removing the 2D Widget
	if (TutorialWidget && UIMode == ETutorialUIMode::TwoDimensional)
		TutorialWidget->RemoveFromParent();

	UIMode = ETutorialUIMode::ThreeDimensional;

	if (!InWidgetComponent->GetUserWidgetObject()->GetClass()->IsChildOf<UTutorialWidget>())
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Set3DWidgetComponent failed because the selected component's user widget is not derrived from UTutorialWidget!"));
		return;
	}

	TutorialWidgetComponent = InWidgetComponent;
	InWidgetComponent->SetTickWhenOffscreen(true);
	InWidgetComponent->SetTickableWhenPaused(true);

	TutorialWidget = Cast<UTutorialWidget>(InWidgetComponent->GetUserWidgetObject());
	TutorialWidget->SetTutorialsManager(this);

	if (!bIsInitialized)
	{
		// 3D Widget Takes some time to construct!
		World->GetTimerManager().SetTimerForNextTick(this, &ATutorialsManager::Initialize);
	}
	else
	{
		if (CurrentIndex < 1 || !World->GetTimerManager().IsTimerActive(LearnedTimer)) return;

		// Message Box Tutorial
		if (Tutorials[CurrentIndex - 1]->Type == ETutorialType::Message)
		{
			TutorialWidget->ShowMessageTutorial(PlaceDecorators(Tutorials[CurrentIndex - 1]->Content));
		}
	}
}

void ATutorialsManager::PopTutorial()
{
	if (Tutorials.Num() <= CurrentIndex) return;

	World->GetTimerManager().ClearTimer(LearnedTimer);
	World->GetTimerManager().ClearTimer(Timer);
	World->GetTimerManager().ClearTimer(AbandonTimer);

	// Message Box Tutorial
	if (Tutorials[CurrentIndex]->Type == ETutorialType::Message)
	{
		TutorialWidget->ShowMessageTutorial(PlaceDecorators(Tutorials[CurrentIndex]->Content));

		// Sound
		if (!bMuteSounds)
			UGameplayStatics::PlaySound2D(GetWorld(), MessageTutorialShowSound);
	}
	// Modal Tutorial
	else
	{
		// There will be no holding if the widget is 3D and in world space
		float ModalHoldTime =
			(UIMode == ETutorialUIMode::ThreeDimensional && TutorialWidgetComponent && TutorialWidgetComponent->GetWidgetSpace() == EWidgetSpace::World) ?
			0 : ModalApprovalHoldTime;

		TutorialWidget->SetupModal(ModalHoldTime);
		TutorialWidget->ShowModalTutorial(Tutorials[CurrentIndex]->Title, PlaceDecorators(Tutorials[CurrentIndex]->Content), Tutorials[CurrentIndex]->Media);

		// Sound
		if (!bMuteSounds)
			UGameplayStatics::PlaySound2D(GetWorld(), TutorialModalShowSound);
	}

	if (Tutorials[CurrentIndex]->Type != ETutorialType::Modal && Tutorials[CurrentIndex]->ExpireTime > 0)
		World->GetTimerManager().SetTimer(LearnedTimer, this, &ATutorialsManager::LearnedTutorial, Tutorials[CurrentIndex]->ExpireTime);

	// Preparing The PopTutorial Tutorial
	++CurrentIndex;

	if (Tutorials.Num() <= CurrentIndex)
	{
		SetActorTickEnabled(false);
		return;
	}

	SetupTicking();
}

void ATutorialsManager::Next(bool bPlaySounds)
{
	if (!bPlaySounds)
		Mute();

	if ((CurrentIndex - 1) > 0 && CurrentIndex < Tutorials.Num() && Tutorials[CurrentIndex]->Type != Tutorials[CurrentIndex - 1]->Type)
		TutorialWidget->HideTutorial();

	PopTutorial();
}

bool ATutorialsManager::Previous(bool bPlaySounds)
{
	if (CurrentIndex - 2 < 0)
	{
		CurrentIndex = 0;
		TutorialWidget->HideTutorial();

		return false;
	}

	if (!bPlaySounds)
		Mute();

	if (Tutorials[CurrentIndex - 2]->Type != Tutorials[CurrentIndex - 1]->Type)
		TutorialWidget->HideTutorial();

	CurrentIndex -= 2;

	PopTutorial();

	return true;
}

void ATutorialsManager::SetupTicking()
{
	if (Tutorials.Num() <= CurrentIndex)
		return;

	auto Tutorial = Tutorials[CurrentIndex];

	if ((!Tutorial->TargetPoint.IsValid() && Tutorial->TriggerType == ETutorialTriggerType::InRange && !Tutorial->bUseManualVector) // In Range
		|| (!Tutorial->TargetPoint.IsValid() && Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)) // Crossed
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: '%s' Tutorial Invalid Target Point! \n\r\t Please set a valid target point in the DataTable."), *Tutorials[CurrentIndex]->Title.ToString());
		return;
	}

	if (Tutorial->TriggerType == ETutorialTriggerType::InRange)
	{
		auto TargetLocation = Tutorial->bUseManualVector ? Tutorial->CustomLocation : Tutorial->TargetPoint->GetActorLocation();
		InRangeDotProduct = PlayerPawn->GetActorForwardVector().GetSafeNormal() | (TargetLocation - PlayerPawn->GetActorLocation()).GetSafeNormal();
	}
	else if (Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)
	{
		OnCrossedDotProduct = 0;
	}

	if (TickType != ETutorialTick::EveryFrame)
	{
		if (Tutorial->TriggerType == ETutorialTriggerType::InRange)
		{
			InRangeTick();
		}
		else if (Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)
		{
			OnCrossedTick();
		}
		else
		{
			World->GetTimerManager().ClearTimer(Timer);
		}
	}
	else
	{
		// Start/Stop Ticking
		if (Tutorial->TriggerType == ETutorialTriggerType::InRange || Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)
		{
			SetActorTickEnabled(true);
		}
	}

	if (Tutorial->AbandonRadius > 0 && Tutorial->TriggerType != ETutorialTriggerType::OnPrevTutorialEnd)
	{
		StartAbandonTick();
	}
	else
	{
		World->GetTimerManager().ClearTimer(AbandonTimer);
	}

	OnTutorialSetup.Broadcast();

	if (bClearEventsOnCall)
		OnTutorialSetup.RemoveAll(this);
}

bool ATutorialsManager::JumpTo(int RowNumber, bool bPlaySounds)
{
	if (!bPlaySounds)
		Mute(NextTutorialPopingDelay);

	return LoadTutorial(RowNumber);
}

void ATutorialsManager::LearnedTutorial()
{
	World->GetTimerManager().ClearTimer(LearnedTimer);

	TutorialWidget->HideTutorial();

	// Sound
	if (!bMuteSounds && CurrentIndex - 1 >= 0)
	{
		UGameplayStatics::PlaySound2D(
			World,
			Tutorials[CurrentIndex - 1]->Type == ETutorialType::Message
			? TutorialModalHideSound : MessageTutorialHideSound
		);
	}

	// Blueprint Event
	OnTutorialLearned.Broadcast();

	if (bClearEventsOnCall)
		OnTutorialLearned.RemoveAll(this);

	// All Tutorials Learned
	if (Tutorials.Num() <= CurrentIndex)
	{
		AllTutorialsLearned();
		return;
	}

	if (Tutorials[CurrentIndex]->TriggerType == ETutorialTriggerType::OnPrevTutorialEnd)
	{
		World->GetTimerManager().SetTimer(DelayTimer, this, &ATutorialsManager::PopTutorial, NextTutorialPopingDelay, false);
	}
}

void ATutorialsManager::AllTutorialsLearned()
{
	// Blueprint Event
	OnAllTutorialsLearned.Broadcast();

	if (bClearEventsOnCall)
		OnAllTutorialsLearned.RemoveAll(this);

	if (bDestroyOnAllTutorialsLearned)
		Destroy();
}

void ATutorialsManager::InRangeTick()
{
	if (!InRangeTickExec())
		World->GetTimerManager().SetTimer(
			Timer,
			this,
			&ATutorialsManager::InRangeTick,
			TickType == ETutorialTick::FixedTimeInterval ? TickTimeInterval : EstimateArrivalTime() * 0.9f
		);
}

bool ATutorialsManager::InRangeTickExec()
{
	if (bIsPendingReload) return false;

	auto Tutorial = Tutorials[CurrentIndex];
	auto TargetLocation = Tutorial->bUseManualVector ? Tutorial->CustomLocation : Tutorial->TargetPoint->GetActorLocation();

	// Player's distance from tutorial
	auto DistanceFromTutorial = (PlayerPawn->GetActorLocation() - TargetLocation).Size();

	if (DistanceFromTutorial < Tutorials[CurrentIndex]->Radius)
	{
		PopTutorial();

		return true;
	}
	else
	{
		float temp = PlayerPawn->GetActorForwardVector().GetSafeNormal() | (TargetLocation - PlayerPawn->GetActorLocation()).GetSafeNormal();

		// in case that player passed the tutorial point and we didn't noticed!		
		if (FMath::Abs(InRangeDotProduct - temp) > 0.75f && DistanceFromTutorial < FMath::Max<float>(Tutorials[CurrentIndex]->Radius * 1.5, 250))
		{
			PopTutorial();

			return true;
		}

		InRangeDotProduct = temp;
	}

	return false;
}

void ATutorialsManager::OnCrossedTick()
{
	if (!OnCrossedTickExec())
		World->GetTimerManager().SetTimer(
			Timer,
			this,
			&ATutorialsManager::OnCrossedTick,
			TickType == ETutorialTick::FixedTimeInterval ? TickTimeInterval : EstimateCrossTime() * 0.9f
		);
}

bool ATutorialsManager::OnCrossedTickExec()
{
	if (bIsPendingReload) return false;

	auto Tutorial = Tutorials[CurrentIndex];
	auto DotProduct = (PlayerPawn->GetActorLocation() - Tutorial->TargetPoint->GetActorLocation()).GetSafeNormal2D() | Tutorial->TargetPoint->GetActorForwardVector().GetSafeNormal2D();

	if (OnCrossedDotProduct * DotProduct < 0)
	{
		PopTutorial();
		return true;
	}
	else
	{
		OnCrossedDotProduct = DotProduct;
	}

	return false;
}

FText ATutorialsManager::PlaceDecorators(FText Text)
{
	ClearActionBindings();

	TArray<FString> Words;
	Text.ToString().ParseIntoArray(Words, TEXT(" "));
	FString FinalResult;
	int ActionBindingCounter = 0;

	FTutorialsManagerPlatfromData* PlatformQuery;
	if (PlatformsData.Contains(TargetPlatform))
	{
		PlatformQuery = PlatformsData.Find(TargetPlatform);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Placing Decorators Failed."));
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: %s platform's decorator's data table hasn't been set! Please set it through the \"BP_TutorialsManager\" blueprint."), *StaticEnum<ETutorialPlatform>()->GetNameStringByValue((int64)TargetPlatform));
		return Text;
	}

	for (int i = 0; i < Words.Num(); ++i)
	{
		bool bInsertSpace = true;
		if (Words[i][0] == '{' && Words[i][Words[i].Len() - 1] == '}')
		{
			// Removing Braces
			Words[i].RemoveAt(Words[i].Len() - 1);
			Words[i].RemoveAt(0);

			TArray<FString> Actions;
			Words[i].ParseIntoArray(Actions, TEXT("+"));
			bool bIsCombational = Actions.Num() > 1;

			// Estimating the number of characters required to show the keys
			// int KeyCount = KeyMappings.Num();
			Words[i].Empty(20 * Actions.Num());

			ULinkedList<int> CombinationalKeys;

			// Iterating Through Different Actions (if the format be combational)
			for (int a = 0; a < Actions.Num(); ++a)
			{
				// Finding Action
				TArray<FInputActionKeyMapping> KeyMappings;
				UInputSettings::GetInputSettings()->GetActionMappingByName((FName)Actions[a], KeyMappings);

				int PlatformKeyMappingIdx = 0;
				// If we have more than one key mapping we're gonna use PlatformsData				
				if (KeyMappings.Num() > 1)
				{
					for (int k = KeyMappings.Num() - 1; k >= 0; --k)
					{
						bool bFoundKey = false;

						if (PlatformQuery->bIsGamepad == KeyMappings[k].Key.IsGamepadKey())
						{
							// For the queries that doesn't have an expression
							if (PlatformQuery->SearchExpression.IsEmpty())
							{
								PlatformKeyMappingIdx = k;
								bFoundKey = true;
								break;
							}

							TArray<FString> SearchKeys;
							PlatformQuery->SearchExpression.ParseIntoArray(SearchKeys, TEXT("|"));

							for (int wi = 0; wi < SearchKeys.Num(); ++wi)
								if (KeyMappings[k].Key.GetFName().ToString().Contains(SearchKeys[wi]))
								{
									PlatformKeyMappingIdx = k;
									bFoundKey = true;
									break;
								}
						}

						if (bFoundKey)
							break;
					}
				}				

				if (KeyMappings.Num() > 0)
				{
					// We won't display "None" keys
					if (!KeyMappings[PlatformKeyMappingIdx].Key.IsValid())
						break;

					if (Tutorials[CurrentIndex]->EndType == ETutorialEndType::OnAllButtonsPressed || Tutorials[CurrentIndex]->EndType == ETutorialEndType::OnAnyButtonPressed)
					{
						// Binding to Action Key Down (If wasn't modal)
						if (Tutorials[CurrentIndex]->Type != ETutorialType::Modal)
						{
							// Generate a new action name for each button
							GenerateKeyBinding(ActionBindingCounter, FInputActionKeyMapping(NAME_None, KeyMappings[PlatformKeyMappingIdx].Key, KeyMappings[PlatformKeyMappingIdx].bShift, KeyMappings[PlatformKeyMappingIdx].bCtrl, KeyMappings[PlatformKeyMappingIdx].bAlt, KeyMappings[PlatformKeyMappingIdx].bCmd), bIsCombational, CombinationalKeys);
						}
					}

					Words[i].Append(GenerateDefaultCombinationKeys(KeyMappings[PlatformKeyMappingIdx]));
					Words[i].Append(FText::Format(NSLOCTEXT("UI", "Tutorial", "<img id=\"{0}\"/>"), FText::FromName(KeyMappings[PlatformKeyMappingIdx].Key.GetFName())).ToString());
				}
				// Finding Axis
				else
				{
					TArray<FInputAxisKeyMapping> AxisMappings;
					UInputSettings::GetInputSettings()->GetAxisMappingByName((FName)Actions[a], AxisMappings);

					if (AxisMappings.Num() > 0)
					{
						TArray<float> KnownScales;
						int KeyMapsNumber = AxisMappings.Num();
						Words[i].Empty(20 * KeyMapsNumber);

						for (int j = KeyMapsNumber - 1; j >= 0; --j)
						{						
							// We won't display "None" keys
							if (!AxisMappings[j].Key.IsValid())
								continue;

							// If we have more than one key mapping we're gonna use PlatformsData
							if (AxisMappings.Num() > 1)
							{
								bool bFoundKey = false;
								
								if (PlatformQuery->bIsGamepad == AxisMappings[j].Key.IsGamepadKey())
								{
									// For the queries that doesn't have an expression
									if (PlatformQuery->SearchExpression.IsEmpty())
									{
										bFoundKey = true;
									}
									else
									{
										TArray<FString> SearchKeys;
										PlatformQuery->SearchExpression.ParseIntoArray(SearchKeys, TEXT("|"));

										for (int wi = 0; wi < SearchKeys.Num(); ++wi)
											if (AxisMappings[j].Key.GetFName().ToString().Contains(SearchKeys[wi]))
											{																								
												bFoundKey = true;
												break;
											}
									}
								}
								
								if (!bFoundKey)
									continue;
							}

							// We only show the primary keys
							if (!KnownScales.Contains(AxisMappings[j].Scale))
							{
								KnownScales.Add(AxisMappings[j].Scale);								
							}
							else
							{
								continue;
							}

							if (Tutorials[CurrentIndex]->EndType == ETutorialEndType::OnAllButtonsPressed || Tutorials[CurrentIndex]->EndType == ETutorialEndType::OnAnyButtonPressed)
							{
								// Binding to Action Key Down (If wasn't modal)
								if (Tutorials[CurrentIndex]->Type != ETutorialType::Modal)
								{
									// Generate a new action name for each button
									GenerateKeyBinding(ActionBindingCounter, FInputActionKeyMapping(NAME_None, AxisMappings[j].Key));
								}
							}

							// Setting up the Key Icon
							Words[i].Append(FText::Format(NSLOCTEXT("UI", "Tutorial", "<img id=\"{0}\"/>"), FText::FromName(AxisMappings[j].Key.GetFName())).ToString());
						}
					}
					// It's a custom key!
					else
					{
						auto KeyName = *Actions[a];

						if (Tutorials[CurrentIndex]->EndType == ETutorialEndType::OnAllButtonsPressed || Tutorials[CurrentIndex]->EndType == ETutorialEndType::OnAnyButtonPressed)
						{
							// Binding to Action Key Down (If wasn't modal)
							if (Tutorials[CurrentIndex]->Type != ETutorialType::Modal)
							{
								// Generate a new action name for each button
								GenerateKeyBinding(ActionBindingCounter, FInputActionKeyMapping(NAME_None, FKey(KeyName)), bIsCombational, CombinationalKeys);
							}
						}

						// Setting up the Key Icon
						Words[i].Append(FText::Format(NSLOCTEXT("UI", "Tutorial", "<img id=\"{0}\"/>"), FText::FromString(KeyName)).ToString());
					}
				}

				if (a < Actions.Num() - 1)
					Words[i].Append(TEXT(" + "));
			}

			CombinationalKeysLists.Add(CombinationalKeys);

			// Don't Insert Space if the next word was a key as well
			if (i + 1 < Words.Num() && Words[i + 1][0] == '{')
				bInsertSpace = false;
		}
		else if (Words[i][0] == '{' && Words[i][Words[i].Len() - 1] != '}' || Words[i][0] != '{' && Words[i][Words[i].Len() - 1] == '}')
		{
			UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: You had probably forgotten to add space before or after the '{ | }' in your tutorial's text! \nCheck out row number %d in the %s data table."), CurrentIndex + 1, *TutorialsDataTable->GetName());
		}

		FinalResult.Append(Words[i] + (bInsertSpace ? TEXT(" ") : TEXT("")));
	}

	PressedActionKeys.SetNumZeroed(ActionBindingCounter);

	return FText::FromString(FinalResult);
}

void ATutorialsManager::GenerateKeyBinding(int& BindingCounter, FInputActionKeyMapping KeyMapping, bool bIsCombational, ULinkedList<int>& CombinationalKeys)
{
	KeyMapping.ActionName = *(TutorialActionName.ToString() + FString::FromInt(BindingCounter));
	// Adding the action mapping
	PlayerInput->AddActionMapping(KeyMapping);
	// Key Press	
	PlayerPawn->InputComponent->BindAction<FActionKeyDelegate>(KeyMapping.ActionName, EInputEvent::IE_Pressed, this, &ATutorialsManager::OnActionKeyDown, BindingCounter, bIsCombational);

	if (bIsCombational)
	{
		// Key Release
		PlayerPawn->InputComponent->BindAction<FActionKeyDelegate>(KeyMapping.ActionName, EInputEvent::IE_Released, this, &ATutorialsManager::OnActionKeyUp, BindingCounter, bIsCombational);
		CombinationalKeys.Add(BindingCounter);
	}

	++BindingCounter;
}

void ATutorialsManager::ClearActionBindings()
{
	// a very rare case that happens when the pressed key of tutorial will change the pawn!
	if (!IsValid(PlayerPawn->InputComponent))
		return;	

	int Counter = 0;
	for (int j = PlayerPawn->InputComponent->GetNumActionBindings() - 1; j >= 0; --j)
		if (PlayerPawn->InputComponent->GetActionBinding(j).GetActionName().ToString().Contains(TutorialActionName.ToString()))
		{
			PlayerPawn->InputComponent->RemoveActionBinding(j);
			++Counter;

			// Optimizing
			if (Counter > PressedActionKeys.Num() * 2)
				break;
		}

	for (int i = 0; i < PressedActionKeys.Num(); ++i)
	{
		auto KeyMappings = PlayerInput->GetKeysForAction((FName)(TutorialActionName.ToString() + FString::FromInt(i)));

		if (KeyMappings.Num())
		{
			PlayerInput->RemoveActionMapping(KeyMappings[0]);
		}

		PressedActionKeys[i] = false;
	}

	// Clearing Combanational Keys List
	CombinationalKeysLists.Clear();
}

void ATutorialsManager::StartAbandonTick()
{
	if (CurrentIndex >= Tutorials.Num())
		return;

	auto Tutorial = Tutorials[CurrentIndex];

	if (Tutorial->TriggerType == ETutorialTriggerType::InRange || Tutorial->TriggerType == ETutorialTriggerType::OnCrossed)
	{
		AbandonOrigin = Tutorial->bUseManualVector ? Tutorial->CustomLocation : Tutorial->TargetPoint->GetActorLocation();
	}
	else if (Tutorial->TriggerType == ETutorialTriggerType::Manual)
	{
		AbandonOrigin = PlayerPawn->GetActorLocation();
	}
	else
	{
		return;
	}

	World->GetTimerManager().SetTimer(AbandonTimer, this
		, &ATutorialsManager::AbandonTick, AbandonTickInterval, true
	);
}

void ATutorialsManager::AbandonTick()
{
	auto Tutorial = Tutorials[CurrentIndex];

	if ((AbandonOrigin - PlayerPawn->GetActorLocation()).Size() > Tutorial->AbandonRadius)
	{
		// Show
		if (Tutorial->AbandonAction == ETutorialAbandonAction::Show)
		{
			PopTutorial();
		}
		// Forget
		else
		{
			LoadTutorial(CurrentIndex + 2, false);
		}

		OnAbandoned.Broadcast();

		if (bClearEventsOnCall)
			OnAbandoned.RemoveAll(this);
	}
}

float ATutorialsManager::EstimateArrivalTime()
{
	auto TargetLocation = Tutorials[CurrentIndex]->bUseManualVector ? Tutorials[CurrentIndex]->CustomLocation : Tutorials[CurrentIndex]->TargetPoint->GetActorLocation();
	return (TargetLocation - PlayerPawn->GetActorLocation()).Size() / MaxMovementSpeed;
}

float ATutorialsManager::EstimateCrossTime()
{
	auto TargetLocation = FVector::PointPlaneProject(PlayerPawn->GetActorLocation(), FPlane(Tutorials[CurrentIndex]->TargetPoint->GetTargetLocation(), Tutorials[CurrentIndex]->TargetPoint->GetActorForwardVector()));
	return (TargetLocation - PlayerPawn->GetActorLocation()).Size() / MaxMovementSpeed;
}

void ATutorialsManager::Mute(float Duration)
{
	bMuteSounds = true;

	if (Duration > 0)
		World->GetTimerManager().SetTimer(MuteTimer, [this]()
			{
				if (!IsValidLowLevel()) return;

				bMuteSounds = false;
			}
	, Duration, false);
}

void ATutorialsManager::Unmute()
{
	World->GetTimerManager().ClearTimer(MuteTimer);
	bMuteSounds = false;
}

void ATutorialsManager::OnActionKeyDown(int Idx, bool bIsCombanational)
{
	bool bAllKeyPressed = true;
	FName ActionMappingName;

	// If ALL of the combanational keys be down this will be true
	bool bCombanationKeyDownDetected = true;

	// Combanational Keys Checking
	if (bIsCombanational)
	{
		ULinkedList<ULinkedList<int>>::ULinkedListNode* prev = nullptr;
		auto temp = CombinationalKeysLists.GetHead();

		while (temp)
		{
			bool bKeyFound = false;
			auto subtemp = temp->Value.GetHead();

			bCombanationKeyDownDetected = true;

			while (subtemp)
			{
				if (subtemp->Value == Idx)
				{
					PressedActionKeys[Idx] = true;
					bKeyFound = true;
				}
				else if (!PressedActionKeys[subtemp->Value])
				{
					bCombanationKeyDownDetected = false;
				}

				subtemp = subtemp->Next;
			}

			// A Combinational Key Press Found
			if (bCombanationKeyDownDetected && bKeyFound)
			{
				// Removing All of Keys
				subtemp = temp->Value.GetHead();
				while (subtemp)
				{
					// We let this one to take care of the events!
					if (subtemp->Value != Idx)
					{
						// Removing the other Action Mappings
						auto Index = subtemp->Value;
						ActionMappingName = (FName)(TutorialActionName.ToString() + FString::FromInt(Index));
						auto KeyMappings = PlayerInput->GetKeysForAction(ActionMappingName);
						if (KeyMappings.Num())
						{
							for (int j = PlayerPawn->InputComponent->GetNumActionBindings() - 1; j >= 0; --j)
								if (PlayerPawn->InputComponent->GetActionBinding(j).GetActionName().IsEqual(ActionMappingName))
								{
									PlayerPawn->InputComponent->RemoveActionBinding(j);

									// If it be combanational then we have to remove the action twice! because release is also binded!
									if (!bIsCombanational)
									{
										break;
									}
									else
									{
										bIsCombanational = false;
									}
								}

							PlayerInput->RemoveActionMapping(KeyMappings[0]);
						}
					}

					subtemp = subtemp->Next;
				}

				// Removing the entire Combanational Sub-list
				if (prev)
				{
					prev->Next = temp->Next;
				}
				else
				{
					CombinationalKeysLists.SetHead(temp->Next);
				}

				break;
			}

			prev = temp;
			temp = temp->Next;
		}
	}

	ActionMappingName = (FName)(TutorialActionName.ToString() + FString::FromInt(Idx));

	// All Buttons Should Get Pressed
	if (Tutorials[CurrentIndex - 1]->EndType == ETutorialEndType::OnAllButtonsPressed)
	{
		if (Idx < PressedActionKeys.Num())
			PressedActionKeys[Idx] = true;
		// ActionMappingName = *(TutorialActionName.ToString() + FString::FromInt(Idx));

		// Checking if any keys waiting to be pressed
		for (int i = 0; i < PressedActionKeys.Num(); ++i)
			if (!PressedActionKeys[i])
			{
				bAllKeyPressed = false;
				break;
			}
	}

	// Removing the action mapping since we don't need it anymore!
	if (!bIsCombanational || (bIsCombanational && bCombanationKeyDownDetected))
	{
		if (bAllKeyPressed)
		{
			ClearActionBindings();
			LearnedTutorial();
		}
		else
		{
			auto KeyMappings = PlayerInput->GetKeysForAction(ActionMappingName);
			if (KeyMappings.Num())
			{
				for (int j = PlayerPawn->InputComponent->GetNumActionBindings() - 1; j >= 0; --j)
					if (PlayerPawn->InputComponent->GetActionBinding(j).GetActionName().IsEqual(ActionMappingName))
					{
						PlayerPawn->InputComponent->RemoveActionBinding(j);

						// If it be combanational then we have to remove the action twice! because release is also binded!
						if (!bIsCombanational)
						{
							break;
						}
						else
						{
							bIsCombanational = false;
						}
					}

				PlayerInput->RemoveActionMapping(KeyMappings[0]);
			}
		}
	}
}

void ATutorialsManager::OnActionKeyUp(int Idx, bool bIsCombanational)
{
	PressedActionKeys[Idx] = false;
}

FString ATutorialsManager::GenerateDefaultCombinationKeys(FInputActionKeyMapping ActionMapping)
{
	FString Result = TEXT("");
	if (ActionMapping.bAlt)
	{
		Result.Append(FText::Format(NSLOCTEXT("UI", "Tutorial", "<img id=\"{0}\"/>"), FText::FromName(EKeys::LeftAlt.GetFName())).ToString());
		Result.Append(TEXT("+"));
	}
	if (ActionMapping.bShift)
	{
		Result.Append(FText::Format(NSLOCTEXT("UI", "Tutorial", "<img id=\"{0}\"/>"), FText::FromName(EKeys::LeftShift.GetFName())).ToString());
		Result.Append(TEXT("+"));
	}
	if (ActionMapping.bCtrl || ActionMapping.bCmd)
	{
		Result.Append(FText::Format(NSLOCTEXT("UI", "Tutorial", "<img id=\"{0}\"/>"), FText::FromName(EKeys::LeftControl.GetFName())).ToString());
		Result.Append(TEXT("+"));
	}

	return Result;
}

bool ATutorialsManager::LoadTutorial(int RowNumber, bool bHideAllTutorials)
{
	ClearActionBindings();

	World->GetTimerManager().ClearTimer(Timer);
	World->GetTimerManager().ClearTimer(DelayTimer);
	World->GetTimerManager().ClearTimer(AbandonTimer);

	if (bHideAllTutorials)
	{
		World->GetTimerManager().ClearTimer(LearnedTimer);
		TutorialWidget->HideTutorial();
	}

	CurrentIndex = RowNumber - 1;

	if (Tutorials.Num() <= CurrentIndex || CurrentIndex < 0)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Loading Tutorial Failed! Index (%d) out of range, this data table only has %d entries."), CurrentIndex, Tutorials.Num());
		return false;
	}

	SetupTicking();

	// Player left off in the middle of tutorial
	if (Tutorials[CurrentIndex]->TriggerType == ETutorialTriggerType::OnPrevTutorialEnd)
		World->GetTimerManager().SetTimer(DelayTimer, this, &ATutorialsManager::PopTutorial, NextTutorialPopingDelay / 2, false);

	return true;
}

FTutorialRow ATutorialsManager::GetTutorial(int RowNumber)
{
	if ((RowNumber - 1) < 0 || RowNumber - 1 >= Tutorials.Num())
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Get Tutorial Failed! Index (%d) out of range, this data table only has %d entries."), RowNumber - 1, Tutorials.Num());
		return FTutorialRow();
	}

	return *Tutorials[RowNumber - 1];
}

FTutorialRow ATutorialsManager::GetActiveTutorial(int& Idx)
{
	if (CurrentIndex >= Tutorials.Num())
	{
		Idx = Tutorials.Num() - 1;
		return *Tutorials[Idx];
	}

	Idx = CurrentIndex;
	return *Tutorials[CurrentIndex];
}

TArray<FTutorialRow> ATutorialsManager::GetAllOfTutorials()
{
	auto Output = TArray<FTutorialRow>();

	for (int i = 0; i < Tutorials.Num(); ++i)
	{
		Output.Add(*Tutorials[i]);
	}

	return Output;
}
