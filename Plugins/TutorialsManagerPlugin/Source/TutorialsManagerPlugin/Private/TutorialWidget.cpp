// Copyright 2022 Koorosh Torabi <thekooroshtorabi@gmail.com> - All Rights Reserved.

#include "../Public/TutorialWidget.h"
#include "../Public/TutorialsManager.h"
#include "../Public/InputKeysImageDecorator.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Components/Overlay.h"
#include "Components/Image.h"
#include "Components/RichTextBlock.h"
#include "Components/CanvasPanel.h"
#include "Components/Button.h"
#include "Materials/MaterialInterface.h"
#include "FileMediaSource.h"
#include "MediaPlayer.h"
#include "TimerManager.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/WidgetComponent.h"
#include "Animation/WidgetAnimation.h"
#include "Kismet/GameplayStatics.h"

void UTutorialWidget::NativeConstruct()
{
	Super::NativeConstruct();

	SetVisibility(ESlateVisibility::Collapsed);

	TutorialBoxOverlay->SetVisibility(ESlateVisibility::Collapsed);	
	TutorialModalCanvas->SetVisibility(ESlateVisibility::Collapsed);

	ConfirmButton->OnPressed.AddDynamic(this, &UTutorialWidget::HoldingStarted);
	ConfirmButton->OnReleased.AddDynamic(this, &UTutorialWidget::HoldingCanceled);

	if (!ModalTutorialMedia || !TutorialMediaPlayer)
	{
		UE_LOG(LogTemp, Error, TEXT("TutorialsManager: Please set the TutorialMediaPlayer and ModalTutorialMedia of %s widget blueprint, otherwise the modal tutorials won't work."), *GetName());
		return;
	}
}

void UTutorialWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (!bTick) return;

	HoldingLerpAlpha += (HoldingLerpAlpha > 0 || HoldingProgressBarChangeRate > 0) * HoldingProgressBarChangeRate * InDeltaTime;
	ConfirmationProgressBar->SetPercent(HoldingLerpAlpha);

	if (HoldingLerpAlpha >= 1)
	{
		TutorialsManager->LearnedTutorial();
		bTick = false;
	}
}

void UTutorialWidget::NativeOnFocusChanging(const FWeakWidgetPath& PreviousFocusPath, const FWidgetPath& NewWidgetPath, const FFocusEvent& InFocusEvent)
{
	if (bIsModalOpen && InFocusEvent.GetCause() == EFocusCause::Mouse)
	{
		ConfirmButton->SetKeyboardFocus();
		return;
	}

	Super::NativeOnFocusChanging(PreviousFocusPath, NewWidgetPath, InFocusEvent);
}

void UTutorialWidget::ShowMessageTutorial(FText InContent)
{
	// We collapse it after hiding tutorials to save performance
	SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	// This Prevents the text block to take weird sizes and 
	// it will maximize the size of the textblock based on the context
	MessageTutorialText->SetRenderOpacity(0);
	MessageTutorialText->SetAutoWrapText(false);

	MessageTutorialText->SetText(InContent);

	// Continue of above code (Maximizing Size)	
	GetWorld()->GetTimerManager().SetTimer(*(new FTimerHandle()), [this]()
		{
			if (!IsValidLowLevel()) return;

			MessageTutorialText->SetRenderOpacity(1);
			MessageTutorialText->SetAutoWrapText(true);
		}
	, 0.1, false);

	CurrentTutorialType = ETutorialType::Message;

	// Animation
	if (!bIsTutorialMessageVisible)
		PlayAnimation(ShowMessageAnim);

	bIsTutorialMessageVisible = true;

	GetWorld()->GetTimerManager().ClearTimer(CollapseTimer);
}

void UTutorialWidget::ShowModalTutorial(FText InTitle, FText InContent, UObject* InMedia)
{
	if (!ModalTutorialMedia || !TutorialMediaPlayer)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: Please set the TutorialMediaPlayer and ModalTutorialMedia of %s widget blueprint, otherwise the modal tutorials won't work."), *GetName());
		return;
	}

	// We collapse it after hiding tutorials to save performance
	SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	auto Controller = GetOwningPlayer();

	if (TutorialsManager->GetUIMode() != ETutorialUIMode::ThreeDimensional
		|| !TutorialsManager->GetTutorialsWidgetComponent() || TutorialsManager->GetTutorialsWidgetComponent()->GetWidgetSpace() != EWidgetSpace::World)
	{
		Controller->SetPause(true);
		Controller->SetShowMouseCursor(true);
		UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(Controller);
	}
	else if(bIsTutorialMessageVisible)
	{
		HideTutorial();
	}

	ModalTutorialText->SetText(InContent);
	ModalTitleText->SetText(InTitle);

	if (InMedia)
	{
		if (InMedia->GetClass()->IsChildOf<UFileMediaSource>())
		{
			TutorialMediaPlayer->OpenSource(Cast<UFileMediaSource>(InMedia));
			ModalTutorialMedia->SetBrushFromMaterial(TutorialMediaMaterial);
		}
		else
			ModalTutorialMedia->SetBrushResourceObject(InMedia);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: You forgot to set the tutorial's media of \"%s\" in the tutorials data table."), *InTitle.ToString());
	}

	CurrentTutorialType = ETutorialType::Modal;

	// Animation
	if (!bIsModalOpen)
		PlayAnimation(ShowModalAnim);

	HoldingLerpAlpha = 0;
	ConfirmationProgressBar->SetPercent(0);

	bIsModalOpen = true;

	TutorialsManager->OnModalShow.Broadcast();

	GetWorld()->GetTimerManager().ClearTimer(CollapseTimer);
}

void UTutorialWidget::HideTutorial()
{
	if (CurrentTutorialType == ETutorialType::Modal && !bIsModalOpen || CurrentTutorialType == ETutorialType::Message && !bIsTutorialMessageVisible)
	{
		return;
	}

	auto AnimToPlay =
		CurrentTutorialType == ETutorialType::Message ? HideMessageAnim : HideModalAnim;

	if (IsAnimationPlaying(AnimToPlay))
	{
		return;
	}

	PlayAnimation(AnimToPlay);

	auto AnimLength = AnimToPlay->GetEndTime();

	// If both of tutorials were visible
	if (bIsModalOpen + bIsTutorialMessageVisible == 2)
	{
		auto OtherAnimToPlay =
			CurrentTutorialType == ETutorialType::Message ? HideModalAnim : HideMessageAnim;

		// Hiding the other one as well
		PlayAnimation(OtherAnimToPlay);

		if (OtherAnimToPlay->GetEndTime() > AnimLength)
			AnimLength = OtherAnimToPlay->GetEndTime();
	}

	GetWorld()->GetTimerManager().SetTimer(CollapseTimer, [this]()
		{
			// If the last thing we did was hiding tutorial, we'll collapse it			
			SetVisibility(ESlateVisibility::Collapsed);
		}
	, AnimLength + 0.5, false);

	if (bIsModalOpen)
	{
		auto Controller = GetOwningPlayer();
		Controller->SetPause(false);
		Controller->SetShowMouseCursor(false);
		UWidgetBlueprintLibrary::SetInputMode_GameOnly(Controller);
		TutorialsManager->OnModalHide.Broadcast();
	}

	bIsModalOpen = false;
	bIsTutorialMessageVisible = false;
}

void UTutorialWidget::UpdateMessageTutorialText(FText NewContent)
{
	if (MessageTutorialText->GetText().ToString().Len() == NewContent.ToString().Len())
	{
		NewContent = FText::FromString(NewContent.ToString() + TEXT(" "));
	}

	MessageTutorialText->SetText(NewContent);
}

void UTutorialWidget::UpdateModalTutorialText(FText NewContent)
{
	if (ModalTutorialText->GetText().ToString().Len() == NewContent.ToString().Len())
	{
		NewContent = FText::FromString(NewContent.ToString() + TEXT(" "));
	}

	ModalTutorialText->SetText(NewContent);
}

void UTutorialWidget::SetDecorator(UDataTable* InPlatformDataTable)
{	
	auto MessageDecorator = Cast<UInputKeysImageDecorator>(MessageTutorialText->GetDecoratorByClass(UInputKeysImageDecorator::StaticClass()));
	auto ModalDecorator = Cast<UInputKeysImageDecorator>(ModalTutorialText->GetDecoratorByClass(UInputKeysImageDecorator::StaticClass()));

	if (!MessageDecorator || !ModalDecorator)
	{
		UE_LOG(LogTemp, Error, TEXT("Tutorials Manager: You had changed the decorator class of the tutorials rich text (ModalTutorialText or MessageTutorialText)! Please use the \"BP_TutorialsManager_MasterDecorator\" if you want the cross-platform feature to work."));
		return;
	}

	MessageDecorator->SetDataTable(InPlatformDataTable);
	ModalDecorator->SetDataTable(InPlatformDataTable);
}

void UTutorialWidget::HoldingStarted()
{
	if (!bIsModalOpen) return;

	if (ModalApprovalHoldTime <= 0)
	{
		TutorialsManager->LearnedTutorial();
		return;
	}

	HoldingProgressBarChangeRate = 1 / ModalApprovalHoldTime;

	bTick = true;
}

void UTutorialWidget::HoldingCanceled()
{
	HoldingProgressBarChangeRate = -2;
}
