// Copyright 2022 Koorosh Torabi <thekooroshtorabi@gmail.com> - All Rights Reserved.

#include "TutorialsManagerPlugin.h"

#define LOCTEXT_NAMESPACE "FTutorialsManagerPluginModule"

void FTutorialsManagerPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FTutorialsManagerPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FTutorialsManagerPluginModule, TutorialsManagerPlugin)